(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cmdliner

open Legacy_monad_globals
let ( >>= ) = Lwt.bind


let index
    ~force_from
    ?use_disk_cache
    ~first_alpha_level
    ~tezos_url
    ~tezos_client_dir
    ~db
    ~no_snapshot_blocks
    ~snapshot_blocks_only
    ~verbose
    ~debug
    watch_hook
    ~mempool_indexing
    ~verbosity
    ~balance_updates_only
    ~no_balance_updates
    ~ignore_db_version
    ~tokens
    ~do_not_get_missing_scripts
    up_to
    ~no_watch
    ~timeout
    ~binary
    ~no_big_maps_cmd
  =
  Verbose.vlevel := verbosity;
  Verbose.verbose := verbose;
  Verbose.debug := debug;
  Tezos_indexer_lib.Db_base.connect db >>= fun conn ->
  Tezos_indexer_lib.Db_base.connect db >>= fun conn2 ->
  (* conn2 is mandatory because of local address caching and reorgs *)
  let set_timeout timeout =
    let open Tezos_indexer_lib.Db_base in
    let open Caqti_type.Std in
    let s = "set statement_timeout = '" ^ timeout ^ "'" in
    query_zero unit unit s
  in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let module Conn2 = (val conn2 : Caqti_lwt.CONNECTION) in
  Conn.exec (set_timeout timeout) () >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
  Conn2.exec (set_timeout "5s") () >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
  Caqti_lwt.connect_pool db |> Db_base.caqti_or_fail ~__LOC__ >>= fun pool ->
  Tezos_indexer_lib.Config.no_big_maps := no_big_maps_cmd;
  begin
    (* if no_watch then *)
      Caqti_lwt.Pool.drain pool
    (* else
     *   Lwt.return_unit *)
  end >>= fun () ->
  begin if ignore_db_version then
      Lwt.return_unit
    else
      Indexer_helpers.get_version conn >>= Db_base.caqti_or_fail ~__LOC__ >>= function
      | None ->
        (Verbose.error "The DB schema is absent or outdated." ; exit 1)
      | Some (v, dev, multicore) ->
        Tezos_indexer_lib.Config.multicore_mode := multicore;
        let db_version =
          match String.split '.' v with
          | [ major ; minor ; rev ] ->
            (major, minor), rev
          | _ ->
            (Verbose.error "DB version (%s) is unreadable." v; exit 1)
        in
        let exe_version =
          match String.split '.' Tezos_indexer_lib.Version.sql with
          | [ major ; minor ; rev ] ->
            (major, minor), rev
          | _ ->
            (Verbose.error "Executable version (%s) is unreadable." Tezos_indexer_lib.Version.sql; exit 1)
        in
        if fst db_version < fst exe_version then
          (Verbose.error "The DB schema (%s) is outdated. Required: %s." v Tezos_indexer_lib.Version.sql; exit 1);
        if fst db_version > fst exe_version then
          (Verbose.error "The DB schema (%s) is more advanced than this executable. Required: %s." v Tezos_indexer_lib.Version.sql; exit 1);
        if multicore && not no_watch then
          (Verbose.error "The DB schema is in multicore mode but you did not use --no-watch. Therefore I'll exit now."; exit 1);

        if db_version <> exe_version then
          Verbose.warn "Warning: The DB schema (%s) does not match this executable's (%s)." v Tezos_indexer_lib.Version.sql;
        if Tezos_indexer_lib.Version.sql_dev then
          (
            Tezos_indexer_lib.Config.dev_mode := true;
            Verbose.warn "Warning: You're using a development version of the database schema. Any change can happen without prior notice.";
          );
        if dev <> Tezos_indexer_lib.Version.sql_dev then
          Verbose.warn "Warning: The development status of the database schema and of the indexer differ! Use at your own risk.";
        Lwt.return_unit
  end >>= fun () ->
  Indexer_helpers.record_launch conn2 >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
  Tezos_cfg.mk_rpc_cfg ~binary
    tezos_client_dir tezos_url >>= function
  | Error err ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "%a@." pp_print_trace err;
    exit Verbose.ExitCodes.tezos_client_config
  | Ok (rpc_config, confirmations, host, port, tls) ->
  Verbose.CLog.printf ~force:true "# host=%S ; port=%d ; tls=%b" host port tls;
  let cctxt =
    new Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full
      (Tezos_cfg.tezos_indexer_full ~rpc_config ?confirmations ()) in
  Verbose.Debug.printf ~vl:0 "# Context created";
  let start_watch, push_start_watch = Lwt.wait () in
  let start_snapshots, push_start_snapshots = Lwt.wait () in
  if mempool_indexing then
    Indexer_helpers.do_mempool_indexing pool cctxt
  else if snapshot_blocks_only then
    Indexer_helpers.snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt
  else if balance_updates_only then
    Indexer_helpers.update_balances no_balance_updates pool cctxt
  else
    begin
      if force_from <> 0l then
        Lwt.return_unit
      else
        begin (* store initial blocks (0 and 1) *)
          Conn.find Db_base.Block_table.booted ()
          >>= Db_base.caqti_or_fail ~__LOC__ >>= begin function
            | true ->
              Lwt.return_unit
            | false ->
              Indexer_helpers.store_block_header conn cctxt 0l >>= fun () ->
              Indexer_helpers.store_block_header conn cctxt 1l
          end >>= fun () ->
          if up_to <= 2l then
            Stdlib.exit 0
          else
            Lwt.return_unit
        end
    end >>= fun () ->
    let a =
      Lwt.catch
        (fun () -> Indexer_helpers.snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt)
        (fun e ->
           Verbose.Debug.eprintf ~vl:0 "Getting snapshot blocks ended with %s" (Printexc.to_string e);
           return_unit)
    and b =
      Indexer_helpers.watch
        ~no_watch
        ~watch_hook
        cctxt
        conn conn2 pool
        use_disk_cache
        first_alpha_level
        start_watch
        tokens
        ~do_not_get_missing_scripts
        ~up_to
    and c =
      Indexer_helpers.bootstrap
        (* first bootstrap *)
        ~exit_when_done:no_watch
        ~force_from
        ?use_disk_cache
        ~conn
        ~conn2
        ~cctxt
        ~first_alpha_level
        ~push_start_snapshots
        ~push_start_watch
        ~tokens
        ~up_to ()
    and d = Indexer_helpers.update_balances no_balance_updates pool cctxt
    in
    a >>=? fun _ -> b >>=? fun _ -> c >>=? fun _ -> d


let print_then_index
    _heads_only_deprecated_flag
    force_from
    use_disk_cache
    first_alpha_level
    tezos_url
    tezos_client_dir
    db
    no_snapshot_blocks
    snapshot_blocks_only
    verbose
    debug
    print_db_schema
    print_db_schema_multicore
    watch_hook
    version
    mempool_indexing
    verbosity
    balance_updates_only
    no_balance_updates
    no_smart_contract_extraction
    cache_blocks_dir
    ignore_db_version
    tokens
    skip_babylon_contracts
    do_not_get_missing_scripts
    up_to
    no_watch
    _auto_recovery_credit (* ignored *)
    print_opspeed
    extracted_address_length_limit
    timeout
    node_timeout
    _make_conn2 (* ignored *)
    notify
    _binary
    textual
    no_big_maps_cmd
    alert
    () =
  if print_db_schema then begin
    Db_schema.print ();
    exit 0;
  end;
  if print_db_schema_multicore then begin
    Db_schema.print_multicore ();
    exit 0;
  end;
  if version then
    Version.version ();

  let do_not_get_missing_scripts = skip_babylon_contracts || do_not_get_missing_scripts in

  Tezos_indexer_lib.Config.extracted_address_length_limit :=
    if no_smart_contract_extraction then 0 else extracted_address_length_limit;
  Tezos_indexer_lib.Config.no_smart_contract_extraction :=
    no_smart_contract_extraction;
  Tezos_indexer_lib.Config.print_opspeed :=
    print_opspeed;
  Tezos_indexer_lib.Config.node_timeout :=
    node_timeout;
  Tezos_indexer_lib.Config.notify :=
    notify;
  if alert <> "" then
    Tezos_indexer_lib.Config.alert_cmd := Some alert;

  index
    ~force_from
    ?use_disk_cache:(if use_disk_cache then Some cache_blocks_dir else None)
    ~first_alpha_level
    ~tezos_url
    ~tezos_client_dir
    ~db
    ~no_snapshot_blocks
    ~snapshot_blocks_only
    ~verbose
    ~debug
    (if watch_hook = "" then (fun () -> Lwt.return_none) else (fun () -> Lwt.return_some (Sys.command watch_hook)))
    ~mempool_indexing
    ~verbosity
    ~balance_updates_only
    ~no_balance_updates
    ~ignore_db_version
    ~tokens
    ~do_not_get_missing_scripts
    (if up_to < 0l then Int32.max_int else up_to)
    ~no_watch
    ~timeout
    ~binary:(not textual)
    ~no_big_maps_cmd

let index_cmd_lwt : _ Lwt.t Term.t =
  let open Cmdliner_helpers in
  Term.(const print_then_index $
       Options_cmd.heads_only_cmd $
       Options_cmd.force_from_cmd $
       Options_cmd.use_disk_cache_cmd $
       Options_cmd.first_block_level_cmd $
       Options_cmd.uri_option_cmd $
       Options_cmd.tezos_client_dir_cmd $
       Options_cmd.db_cmd $
       Options_cmd.no_snapshot_blocks_cmd $
       Options_cmd.snapshot_blocks_only_cmd $
       Options_cmd.verbose_mode_cmd $
       Options_cmd.debug_mode_cmd $
       Options_cmd.print_db_schema_cmd $
       Options_cmd.print_db_schema_multicore_cmd $
       Options_cmd.watch_hook_cmd $
       Options_cmd.version_cmd $
       Options_cmd.mempool_indexing_cmd $
       Options_cmd.verbosity_cmd $
       Options_cmd.balance_updates_only_cmd $
       Options_cmd.no_balance_updates_cmd $
       Options_cmd.no_smart_contract_extraction $
       Options_cmd.cache_blocks_dir_cmd $
       Options_cmd.ignore_db_version_cmd $
       Options_cmd.tokens_support_cmd $
       Options_cmd.skip_babylon_contracts_cmd $
       Options_cmd.do_not_get_missing_scripts_cmd $
       Options_cmd.up_to_cmd $
       Options_cmd.no_watch_cmd $
       Options_cmd.auto_recovery_credit_cmd $
       Options_cmd.print_opspeed_cmd $
       Options_cmd.extracted_address_length_limit $
       Options_cmd.timeout_cmd $
       Options_cmd.node_timeout_cmd $
       Options_cmd.make_conn2_cmd $
       Options_cmd.notify_cmd $
       Options_cmd.binary_cmd $
       Options_cmd.textual_cmd $
       Options_cmd.no_big_maps_cmd $
       Options_cmd.alert_cmd $
       const ())

let info =
  let doc = "Store and index a Tezos blockchain into a Postgres database." in
  Cmd.info ~doc ~sdocs:"" "tezos-indexer"


let lwt_run v =
  Lwt.async_exception_hook := begin fun exn ->
    let backtrace = Printexc.get_backtrace () in
    Verbose.error "[Uncaught Error] %s." (Printexc.to_string exn);
    Verbose.error "Please file a bug report at https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/new";
    Verbose.error "with the following data:";
    Verbose.error "Version: %s" Tezos_indexer_lib.Version.t;
    Verbose.error "Error: uncaught exception: %s" (Printexc.to_string exn);
    Verbose.error "Backtrace: %s" backtrace;
  end ;
  match Lwt_main.run v with
  | Error err ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "%a@." pp_print_trace err;
    exit Verbose.ExitCodes.other
  | Ok () ->
    ()


let () =
  let e : _ Lwt.t Cmd.t = Cmd.v info index_cmd_lwt in
  match Cmd.eval_value e with
  | Ok (`Ok x) -> lwt_run x ; exit 0
  | Ok _ -> exit 0
  | _ -> exit Verbose.ExitCodes.other

  (* Cmd.eval (Term.(const lwt_run $ index_cmd_lwt)) *)
