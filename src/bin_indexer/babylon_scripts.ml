(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2021 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Legacy_monad_globals
let ( >>= ) = Lwt.bind

let get_script cctxt k =
    Tezos_protocol_010_PtGRANAD.Protocol.Alpha_services.Contract.info
      cctxt ((`Main, `Head 0):Block_services.chain * Block_services.block) k >>=? fun
      {
        balance = _ ; delegate = _ ; counter = _ ; script ;
      } ->
    return script


(** The functions using the value below are not reentrant. If you need
   concurrency support, don't forget to adapt the code that uses this. *)
let latest_seen = ref 0L


let rec update_scripts cctxt pool =
  let cpt = ref 0 in
  Caqti_lwt.Pool.use (fun conn ->
      Verbose.printf ~vl:1 "# Update scripts for pre-Babylon scriptless contracts";
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      let declare_script_not_found kid =
        Conn.exec Db_base.(
            query_zero kid Caqti_type.Std.unit
              "update c.contract_script set missing_script = 1 where address_id = $1")
          kid
      in
      let invalid_contract_address kid =
        Conn.exec Db_base.(
            query_zero kid Caqti_type.Std.unit
              "delete from c.contract_script where address_id = $1")
          kid
      in
      Verbose.printf ~vl:1 "# Trying to get scripts for scriptless contracts";
      Conn.collect_list
        Tezos_indexer_010_PtGRANAD.Db_alpha.Contract_table.get_scriptless_contracts
        !latest_seen
      >>= Db_base.caqti_or_fail ~__LOC__ >>= function
      | [] ->
        Verbose.printf ~force:true ~vl:1 "# No script to fetch at this time";
        Lwt.return (Ok ())
      | scriptless_contracts ->
        Conn.start () >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt_list.iter_s
          (function
            | (None, kid) ->
              invalid_contract_address kid >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
            | (Some k, kid) ->
              latest_seen := kid;
              incr cpt;
              Verbose.printf ~vl:1 "# Getting script for contract %a" Tezos_raw_protocol_010_PtGRANAD.Alpha_context.Contract.pp k;
              get_script cctxt k >>= function
              | Ok (Some s) ->
                Verbose.printf ~vl:1 "# Updating script for contract %a" Tezos_raw_protocol_010_PtGRANAD.Alpha_context.Contract.pp k;
                Conn.find_opt Tezos_indexer_010_PtGRANAD.Db_alpha.Contract_table.update_script
                  (Db_tups.mtup5 kid s 655360l (Tezos_indexer_010_PtGRANAD.Db_alpha.Utils.extract_strings_and_bytes s) Tezos_indexer_lib.Config.extracted_address_length_limit.contents)
                >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
              | Ok None ->
                Verbose.Debug.printf ~vl:1 "Getting script for %a failed" Tezos_raw_protocol_010_PtGRANAD.Alpha_context.Contract.pp k;
                declare_script_not_found kid >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
              | Error e ->
                Verbose.Debug.printf ~vl:1 "Getting script for %a failed with error %a" Tezos_raw_protocol_010_PtGRANAD.Alpha_context.Contract.pp k pp_print_trace e;
                declare_script_not_found kid >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
          )
          scriptless_contracts
        >>= fun () ->
        Conn.commit () >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt.return (Ok ())
    ) pool
  >>= fun _ ->
  begin if !cpt = 0 then
      Lwt_unix.sleep 60.
    else
      Lwt.return_unit
  end >>= fun () ->
  cpt := 0;
  update_scripts cctxt pool
