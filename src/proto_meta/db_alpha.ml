(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


(* open Legacy_monad_globals *)
let ( >>= ) = Lwt.bind

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X ifdef __META3__ open Protocol X#**)
(**#X ifdef __META6__ open Protocol X#**)

open Alpha_context

module Utils = struct

  let extract_strings_and_bytes_from_expr expr =
    Verbose.Script.extract_strings_and_bytes (Tezos_micheline.Micheline.root expr)

  let extract_strings_and_bytes script =
    match script with
    | Script.{ code ; storage } ->
      match Data_encoding.force_decode code, Data_encoding.force_decode storage with
      | None, None -> None
      | Some x, None
      | None, Some x ->
        extract_strings_and_bytes_from_expr x
      | Some code, Some storage ->
        match
          extract_strings_and_bytes_from_expr storage,
          extract_strings_and_bytes_from_expr code
        with
        | None, None -> None
        | Some l, Some l2 -> let module S = Set.Make(String) in
          Some(
            List.fold_left (fun r e -> S.add e r)
              (List.fold_left (fun r e -> S.add e r) S.empty l)
              l2
            |> S.elements
          )
        | Some l, None
        | None, Some l -> Some l

  let extract_strings_and_bytes_from_lazy_expr le =
    match Data_encoding.force_decode le with
    | None -> None
    | Some e -> extract_strings_and_bytes_from_expr e

end

(**# ifdef __META12__
let implicit_contract pkh = Alpha_context.Contract.Implicit pkh
#**)(**#X else
let implicit_contract =
  Alpha_context.Contract.implicit_contract
X#**)

open Caqti_type.Std
open Db_tups
open Db_base

(**# ifndef __META14__ OPEN_COMMENTS #**)
module Public_key_hash = Tezos_crypto.Signature.V1.Public_key_hash
module Public_key = Tezos_crypto.Signature.V1.Public_key
module Protocol_hash = Protocol_hash
let pkh =
  custom
    ~encode:(fun a -> Ok (Public_key_hash.to_b58check a))
    ~decode:(fun a -> Db_base.to_caqti_error (Public_key_hash.of_b58check a))
    string
let pk =
  custom
    ~encode:(fun a -> Ok (Public_key.to_b58check a))
    ~decode:(fun a -> Db_base.to_caqti_error (Public_key.of_b58check a))
    string
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
module Public_key_hash = Tezos_crypto.Signature.V0.Public_key_hash
module Public_key = Tezos_crypto.Signature.V0.Public_key
module Protocol_hash = Protocol_hash
(**# ifdef __META14__ CLOSE_COMMENTS #**)


let int16 = Caqti_type.int16

let cycle =
  let open Cycle in
  custom
    ~encode:(fun a -> Ok (to_int32 a))
    ~decode:(fun a -> Ok (add root (Int32.to_int a)))
    int32

let voting_period =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct Voting_period.encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct Voting_period.encoding a))
    json

let voting_period_kind =
  let open Voting_period in
  custom
    ~encode:(function
        | Proposal       -> Ok 0
(**# ifndef __META7__
        | Testing_vote   -> Ok 1
        | Testing        -> Ok 2
        | Promotion_vote -> Ok 3
#**)
(**# ifdef __META6__
        | Adoption       -> Ok 4
#**)
(**# ifdef __META7__
      | Exploration    -> Ok 5
      | Cooldown       -> Ok 6
      | Promotion      -> Ok 7
   #**)
      )
    ~decode:(function
        | 0 -> Ok Proposal
(**# ifndef __META7__
        | 1 -> Ok Testing_vote
        | 2 -> Ok Testing
        | 3 -> Ok Promotion_vote
 #**)
(**# ifdef __META6__
        | 4 -> Ok Adoption
   #**)
(**# ifdef __META7__
      | 5 -> Ok Exploration
      | 6 -> Ok Cooldown
      | 7 -> Ok Promotion
   #**)

        | _ -> assert false
      )
    int16

let to_caqti_error = function
  | Ok a -> Ok a
  | Error _e ->
    (* Format.eprintf "to_caqti_error failed %a\n%!" pp_print_trace (Obj.magic _e); *)
    Format.eprintf "to_caqti_error failed\n%!";
    Stdlib.exit 1

let k =
  custom
    ~encode:(fun a -> Ok (Contract.to_b58check a))
    ~decode:(fun a ->
        match Contract.of_b58check a with
        | Ok _ as ok -> ok
        | Error _ as e ->
          Format.eprintf "Error: couldn't decode contract %S using b58 (**# get PROTO#**)\n%!" a;
          to_caqti_error e
      )
    string


let option_k =
  custom
    ~encode:(fun _ -> assert false)
    ~decode:(fun a ->
        match Contract.of_b58check a with
        | Ok k -> Ok (Some k)
        | Error _ -> Ok (None)
      )
    string


let tez =
  custom
    ~encode:(fun a -> Ok (Tez.to_mutez a))
    ~decode:begin fun a ->
      match Tez.of_mutez a with
      | None -> Error "tez"
      | Some t -> Ok t
    end
    int64

let opaid = int64
type opaid = int64

let pg_array_of_string_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%S," e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%S" e
  in
  fun l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b


let pg_array_of_int_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%d," e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%d" e
  in
  function
  | [] -> "{}"
  | l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b

let int_list_from_string s =
  String.sub s 1 (String.length s - 2)
  |> String.split_on_char ','
  |> List.map (fun e -> String.trim e |> int_of_string)

(* let _ = int_list_from_string "{1,2,3 , 4 }" *)

let slots =
  custom
    ~encode:(fun a -> Ok (pg_array_of_int_list a))
    ~decode:(fun a -> Ok (int_list_from_string a))
    string


let pg_array_of_int64_list =
  let rec loop b = function
    | [] -> ()
    | e::(_::_ as tl) ->
      Printf.bprintf b "%Ld," e;
      loop b tl
    | [e] ->
      Printf.bprintf b "%Ld" e
  in
  function
  | [] -> "{}"
  | l ->
    let b = Buffer.create 64 in
    Printf.bprintf b "{";
    loop b l;
    Printf.bprintf b "}";
    Buffer.contents b

let int64_array =
  custom
    ~encode:(fun a -> Ok (pg_array_of_int64_list a))
    ~decode:(fun _ -> assert false)
    string


let text_array =
  custom
    ~encode:(fun a -> Ok (pg_array_of_string_list a))
    ~decode:(fun _ -> assert false)
    string


let custom_of_encoding encoding =
  custom
    ~encode:(fun a -> Ok (Data_encoding.Json.construct encoding a))
    ~decode:(fun a -> Ok (Data_encoding.Json.destruct encoding a))
    json

(*# ifdef __META13__ Beware: this is not dead code, it's just some comments. (Or else, compilation would fail.)
let custom_to_string_of_encoding encoding = (* this is tricky: it adds double-quotes around the value *)
  custom
    ~encode:(fun a -> Ok (Format.asprintf "%a" Data_encoding.Json.pp (Data_encoding.Json.construct encoding a)))
    ~decode:(fun _ -> assert false)
    string
#*)

let custom_to_string_of_pp pp =
  custom
    ~encode:(fun a -> Ok (Format.asprintf "%a" pp a))
    ~decode:(fun _ -> assert false)
    string

(**# ifdef __META10__
let bpkh =
  custom_to_string_of_pp Blinded_public_key_hash.pp
#**)(**# else
let bpkh = custom ~encode:(fun _ -> assert false) ~decode:(fun _ -> assert false) string
 #**)



let nonce =
  custom_of_encoding Nonce.encoding
(* let nonce encoding =
 *   custom
 *     ~encode:(fun a -> Ok (Data_encoding.Json.construct Nonce.encoding a))
 *     ~decode:(fun a -> Ok (Data_encoding.Json.destruct Nonce.encoding a))
 *     json *)

let lazy_expr =
  custom
    ~encode:(fun a ->
        match Script_repr.force_decode a with
        | Ok (v (**# ifndef __META8__ , _gas_cost #**)) ->
          Ok (Data_encoding.Json.construct Script_repr.expr_encoding v)
        | _ -> Data_encoding.Json.from_string "{\"error\":\"invalid_data\"}"
        | exception _ -> Data_encoding.Json.from_string "{\"error\":\"invalid_data\"}")
    ~decode:(fun _ -> assert false)
    json

(**# ifdef __PROTO1__
(* let big_map_diff =
 *   custom
 *     ~encode:(function [] -> Ok `Null | _ -> assert false)
 *     ~decode:(function `Null -> Ok [] | _ -> assert false)
 *     json *)
(* let lazy_storage_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
#**)(**# elseifdef __PROTO8__
(* let big_map_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
(* let lazy_storage_diff =
 *   custom_of_encoding Lazy_storage.encoding *)
#**)(**# else
(* let big_map_diff =
 *   custom_of_encoding Contract.big_map_diff_encoding *)
(* let lazy_storage_diff =
 *   custom
 *     ~encode:(function () -> Ok `Null)
 *     ~decode:(function `Null -> Ok () | _ -> assert false)
 *     json *)
#**)

let script =
  custom_of_encoding Script.encoding
(* let script =
 *   let encoding =
 *     let open Data_encoding in
 *     def "scripted.contracts_decoded"
 *     @@ conv
 *       (fun Script.{code; storage} ->
 *          match Script_repr.force_decode code, Script_repr.force_decode storage with
 *          | Error _, _  -> assert false
 *          | _, Error _  -> assert false
 *          | Ok (code, _), Ok (storage, _) ->
 *            Format.printf ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> %a %a\n%!"
 *              Data_encoding.Json.pp (Data_encoding.Json.construct Script_repr.expr_encoding code)
 *              Data_encoding.Json.pp (Data_encoding.Json.construct Script_repr.expr_encoding storage)
 *            ;
 *            code, storage
 *       )
 *       (fun _ -> assert false)
 *       (obj2 (req "code" Script_repr.expr_encoding) (req "storage" Script_repr.expr_encoding))
 *   in
 *   custom_of_encoding encoding *)


(* let script_expr_hash =
 *   custom_to_string_of_encoding (Tezos_raw_protocol_(\**# get PROTO#**\).Script_expr_hash.encoding) *)

let script_expr_hash =
  custom_to_string_of_pp (Tezos_raw_protocol_(**# get PROTO#**).Script_expr_hash.pp)

let script_expr =
  custom_of_encoding Script.expr_encoding


(**# ifndef __META11__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
let message = custom_of_encoding Tx_rollup_message.encoding
let message_position = integer
let message_path = custom_of_encoding Tx_rollup_inbox.Merkle.path_encoding
let message_result_hash = custom_of_encoding Tx_rollup_message_result_hash.encoding
let message_result_path = custom_of_encoding Tx_rollup_commitment.Merkle.path_encoding
let previous_message_result = custom_of_encoding Tx_rollup_message_result.encoding
let previous_message_result_path = custom_of_encoding Tx_rollup_commitment.Merkle.path_encoding
let proof = custom_of_encoding Tx_rollup_l2_proof.(**# ifdef __META13__ serialized_#**)encoding

let tickets_info = custom_of_encoding (Data_encoding.list Tx_rollup_reveal.encoding)
let context_hash = custom_of_encoding Context_hash.encoding

let tx_rollup = kid

let tx_rollup_level =
  custom_of_encoding Tx_rollup_level.encoding

let tx_rollup_commitment =
  custom_of_encoding Tx_rollup_commitment.Full.encoding
(**# ifdef __META16__ CLOSE_COMMENTS #**)

let ticket_hash =
  custom_of_encoding Ticket_hash.encoding

let entrypoint =
  custom_of_encoding Entrypoint_repr.smart_encoding

let sc_rollup_inbox = custom_of_encoding Sc_rollup.Inbox.encoding
(**# ifndef __META11__ CLOSE_COMMENTS #**)

(**# ifndef __META13__ OPEN_COMMENTS #**)
let public_parameters = custom_of_encoding Tezos_protocol_environment_(**# get PROTO #**).Plonk.public_parameters_encoding
let zk_state = custom_of_encoding Zk_rollup.State.encoding
let ticket_receipt =
  custom_of_encoding Ticket_receipt.encoding
let sc_rollup_commitment = custom_of_encoding Sc_rollup.Commitment.encoding
let sc_rollup_commitment_hash =
  custom
    ~encode:(fun a -> Ok (Sc_rollup.Commitment.Hash.to_b58check a))
    ~decode:(fun _ -> assert false)
    string

let parameters_ty = lazy_expr
let origination_proof = string
let genesis_commitment_hash = sc_rollup_commitment_hash
let sc_rollup_kind = custom_of_encoding Sc_rollup.Kind.encoding
let boot_sector_bin = octets
let boot_sector_txt = string

(**# ifndef __META13__ CLOSE_COMMENTS #**)

(**# ifndef __META12__ OPEN_COMMENTS #**)
let sc_rollup_address = kid
let vdf_solution = custom_of_encoding Seed.vdf_solution_encoding
let event_ty = script_expr
let event_tag = entrypoint
let event_payload = script_expr
(**# ifndef __META12__ CLOSE_COMMENTS #**)


(**# ifdef __META7__ module Delegate = Receipt #**)

let balance =
  custom
    ~encode:begin function
      | Delegate.Contract _ -> Ok 0
(**# ifdef __META10__ OPEN_COMMENTS #**)
      | Rewards _ -> Ok 1
      | Fees _ -> Ok 2
      | Deposits _ -> Ok 3
(**# ifdef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
      | Block_fees -> Ok 4
      | Nonce_revelation_rewards -> Ok 5
(**# ifdef __META16__ OPEN_COMMENTS #**)
      | Double_signing_evidence_rewards -> Ok 6
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      | Endorsing_rewards -> Ok 7
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
      | Attesting_rewards -> Ok 7
(**# ifndef __META16__ CLOSE_COMMENTS #**)
      | Baking_rewards -> Ok 8
      | Baking_bonuses -> Ok 9
      | Storage_fees -> Ok 10
      | Double_signing_punishments -> Ok 11
(**# ifdef __META16__ OPEN_COMMENTS #**)
      | Lost_endorsing_rewards _ -> Ok 12
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
      | Lost_attesting_rewards _ -> Ok 12
(**# ifndef __META16__ CLOSE_COMMENTS #**)
      | Liquidity_baking_subsidies -> Ok 13
      | Burned -> Ok 14
      | Commitments _ -> Ok 15
      | Bootstrap -> Ok 16
      | Invoice -> Ok 17
      | Initial_commitments -> Ok 18
      | Minted -> Ok 19
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
      | Deposits _ -> Ok 3
      | Frozen_bonds _ -> Ok 20
(**# ifdef __META16__ OPEN_COMMENTS #**)
      | Tx_rollup_rejection_punishments -> Ok 21
      | Tx_rollup_rejection_rewards -> Ok 22
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
(**# ifdef __PROTO12__
      | Legacy_rewards _ -> Ok 1
      | Legacy_fees _ -> Ok 2
      | Deposits _
      | Legacy_deposits _ -> Ok 3
#**)
(**# ifdef __META12__
      | Sc_rollup_refutation_punishments -> Ok 23
#**)
(**# ifdef __META13__
      | Sc_rollup_refutation_rewards -> Ok 24
#**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
      | Unstaked_deposits (_, _) -> Ok 25
      | Staking_delegator_numerator _ -> Ok 26
      | Staking_delegate_denominator _ -> Ok 27
(**# ifndef __META16__ CLOSE_COMMENTS #**)
    end
    ~decode:(fun _ -> assert false)
    int16

(**# ifdef __META16__ OPEN_COMMENTS #**)
let balance_update = custom
    ~encode:begin function
      | Delegate.Credited t -> Ok (Tez.to_mutez t)
      | Debited t -> Ok (Int64.neg (Tez.to_mutez t))
    end
    ~decode:begin fun _ -> assert false
    end
    int64
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
(* let tez_or_pseudotoken_to_tez : _ -> Tez.t = Obj.magic *)
let balance_update = custom
    ~encode:begin fun balance_update_item ->
    (* let (_balance_kind, (t : Tez.t Receipt.balance_update), _update_origin) = *)
    (*   Obj.magic (balance_update_item : _ * _ Receipt.balance_update * _) *)
    (* in *)
    let (t : Tez.t Receipt.balance_update) =
      Obj.magic (balance_update_item : _ Receipt.balance_update)
    in
    match t with
    | Delegate.Credited (t) ->
       Ok (Tez.to_mutez (t))
    | Delegate.Debited (t) ->
       Ok (Int64.neg (Tez.to_mutez t))
    end
    ~decode:begin fun _ -> assert false
    end
    int64
(**# ifndef __META16__ CLOSE_COMMENTS #**)

module Block_alpha_table = struct
  let insert =
    query_zero_or_one
      ((**# ifdef __META13__ tup10 #**)(**# else tup9 #**)
         block_level kid int32 cycle
         int32 voting_period int32 voting_period_kind
         milligas
         (**# ifdef __META13__ kid #**)
       )
      int32
      (**# ifdef __META13__ "SELECT I.block_alpha(?,?,?,?,?,?,?,?,?,?)" #**)
      (**# else "SELECT I.block_alpha(?,?,?,?,?,?,?,?,?,null)" #**)
end

module Operation_alpha_table = struct
  let insert =
    query_zero_or_one
      (tup6 ophid int16 int16 block_level int16 opaid)
      unit
      "select I.opalpha($1,$2,$3,$4,$5,$6)"
end


module Manager_numbers = struct
  let counter =
    (**# ifdef __META14__ custom ~encode:(fun a -> Ok (Format.asprintf "%a" Manager_counter.pp a))
       ~decode:(fun _ -> assert false)
       string
       #**)(**# else z #**)
  let insert =
    query_zero_or_one
      (tup4 opaid counter z z)
      unit
      "select I.manager_numbers(?,?,?,?)"
      (* "select I.manager_numbers(?,?::numeric,?::numeric,?::numeric)" *)
end

module Contract_table = struct
  let get_scriptless_contracts =
    query_zero_or_more
      kid
      (tup2 option_k kid)
      "select * from G.scriptless_contracts($1)"

  let update_script =
    query_zero_or_one
      (tup5 kid script block_level (option text_array) int16)
      unit
      "select U.script($1,$2,$3,$4,$5)"
end

module Contract_balance_table = struct

  let update =
    query_zero_or_one
      (tup4 kid block_level tez (option script))
      unit
      "select U.c_bs($1,$2,$3,$4,null,null)"

  let update_via_bh =
    query_zero_or_one
      (tup4 kid bh tez (option script))
      unit
      "select U.c_bs2($1,$2,$3,$4,null,null)"

  let insert_balance_full =
    query_zero_or_one
      (tup6 kid tez block_level (option script) (option text_array) int16)
      unit
      "select I.c_bs($1,$2,$3,$4,$5,$6)"

  let pre_insert_balance =
    query_zero_or_one
      (tup2 kid block_level)
      unit
      "select H.c_bs($1,$2)"

  let get_balanceless_contracts =
    query_zero_or_more
      integer
      (tup4 option_k kid bh block_level)
          "select address,address_id,block_hash,block_level \
           from G.balanceless_contracts($1)"
end


module Balance_table = struct
(**# ifndef __META7__ OPEN_COMMENTS #**)
  let int_of_update_origin = function
    | Receipt.Block_application -> 0
    | Protocol_migration -> 1
(**# ifdef __META8__ | Subsidy -> 2 #**)
(**# ifdef __META10__ | Simulation -> 3 #**)
(**# ifdef __META16__ | Delayed_operation _ -> 4 #**)
(**# ifndef __META7__ CLOSE_COMMENTS #**)

(**# ifndef __META7__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
  type t =
    (Delegate.balance * Delegate.balance_update * Receipt.update_origin) list
(**# ifdef __META16__ CLOSE_COMMENTS #**)

(**# ifndef __META16__ OPEN_COMMENTS #**)
  type t = Delegate.balance_update_item list
(**# ifndef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META7__ CLOSE_COMMENTS #**)
(**# ifdef __META7__ OPEN_COMMENTS #**)
  type t = (Delegate.balance * Delegate.balance_update) list
(**# ifdef __META7__ CLOSE_COMMENTS #**)

  type (**# ifdef __META16__ 'token #**) table = {
    block_level: block_level ;
    opaid: opaid option ;
    iorid : int option;
    balance: (**# ifdef __META16__ 'token #**) Delegate.balance ;
    k: kid option ;
    cycle: Cycle.t option ;
    diff: (**# ifdef __META16__ 'token #**) Delegate.balance_update ;
    id : int; (* balance update "index" (or "counter") within the current list of balance updates -
                 this is to make sure we allow legitimate duplicates
                 and reject illegitimate duplicates *)
(**# ifndef __META7__ OPEN_COMMENTS #**)
    update_origin : int;
(**# ifndef __META7__ CLOSE_COMMENTS #**)
  }

(**# ifdef __META10__
   let extract_bpkh = function
   | Delegate.Commitments bpkh -> Some bpkh
   | _ -> None
#**)(**# else let extract_bpkh _ = None #**)

  let table =
    custom
      ~encode:begin fun { block_level ; opaid ; iorid ; id ;
                          balance ; k ; cycle ; diff (**# ifdef __META7__ ; update_origin #**) } ->
        Ok ((block_level, opaid, iorid, balance)
           , (k, cycle, diff, id)
           , extract_bpkh balance (**# ifdef __META7__ , update_origin #**))
      end
      ~decode:begin fun _ -> assert false end
      ((**# ifdef __META7__ tup10 #**)(**# else tup9 #**)
         block_level
         (option opaid)
         (option integer)
         balance
         (option kid) (option cycle) balance_update integer
         (option bpkh)
         (**# ifdef __META7__ int16 #**)
      )

  let insert =
    query_zero_or_one
      table
      unit
  (**# ifdef __META7__ "select I.balance(?,?,?,?,?,?,?,?,?,?)" #**)
  (**# else "select I.balance(?,?,?,?,?,?,?,?,?,null)" #**)

  let current_block_level = ref 0l (* FIXME: maybe don't use a global variable *)
  let counter = ref 0 (* FIXME: maybe don't use a global variable *)

  let update ~get_kid ?opaid ?iorid conn ~block_level (t: t) =
    if (current_block_level.contents : int32) <> (block_level : int32) then
      (counter := -1;
       current_block_level := block_level);
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    (**# ifndef __META16__ OPEN_COMMENTS #**)
    let t : (Tez.t Receipt.balance * Tez.t Receipt.balance_update * Receipt.update_origin) list =
      Obj.magic (t : Delegate.balance_update_item list) (* FIXME <-------------------- *) in
    (**# ifndef __META16__ CLOSE_COMMENTS #**)
    Lwt_list.map_s (fun (balance, diff (**# ifdef __META7__ , update_origin #**)) ->
        (**# ifdef __META7__ let update_origin = int_of_update_origin update_origin in #**)
        incr counter;
        match balance with
        | Delegate.Contract k ->
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; block_level ; opaid ; iorid ;
              balance ; cycle = None ; diff ; id = !counter
              (**# ifdef __META7__ ; update_origin #**) }
(**# ifdef __META16__
        | Unstaked_deposits ((Single (_ (* FIXME *), pkh) | Shared pkh), cycle) ->
          let k = implicit_contract pkh in
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; block_level ; opaid ; iorid ;
              balance ; cycle = Some cycle; diff ; id = !counter
              ; update_origin }
        | Staking_delegator_numerator { delegator (* : Contract.t *) } ->
          get_kid delegator >>= fun delegator_id ->
          Lwt.return {
            k = Some delegator_id;
            block_level;
            opaid;
            iorid;
            balance;
            cycle = None;
            diff;
            id = !counter;
            update_origin;
          }
        | Staking_delegate_denominator { delegate = pkh } ->
          let k = implicit_contract pkh in
          get_kid k >>= fun k ->
          Lwt.return {
            k = Some k;
            block_level;
            opaid;
            iorid;
            balance;
            cycle = None;
            diff;
            id = !counter;
            update_origin;
          }
#**)
(**#X ifndef __META11__
(**# ifndef __META10__
        | Rewards (pkh, cycle)
        | Fees (pkh, cycle)
        | Deposits (pkh, cycle) ->
#**)(**# else
        | Legacy_rewards (pkh, cycle)
        | Legacy_fees (pkh, cycle)
        | Legacy_deposits (pkh, cycle) ->
#**)
          let k = implicit_contract pkh in
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; block_level ; opaid ; iorid ;
              balance ; cycle = Some cycle; diff ; id = !counter
              (**# ifdef __META7__ ; update_origin #**) }
X#**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
(**# ifdef __META16__
   | Deposits (Single_staker {staker = _ (* FIXME *); delegate = pkh}
             | Shared_between_stakers { delegate = pkh }
             | Baker pkh)
   | Lost_attesting_rewards (pkh, _, _)  #**)
(**# else
   | Deposits pkh
   | Lost_endorsing_rewards (pkh, _, _) #**)
          ->
          let k = implicit_contract pkh in
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; block_level ; opaid ; iorid ;
              balance ; cycle = None; diff ; id = !counter
            ; update_origin }
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
        | Frozen_bonds (k, _ (* bond *)) ->
          get_kid k >>= fun k ->
          Lwt.return
            { k = Some k ; block_level ; opaid ; iorid ;
              balance ; cycle = None; diff ; id = !counter
            ; update_origin }
(**# ifdef __META16__ OPEN_COMMENTS #**)
        | Tx_rollup_rejection_punishments
        | Tx_rollup_rejection_rewards
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
(**# ifdef __META12__
        | Sc_rollup_refutation_punishments #**)
(**# ifdef __META13__
        | Sc_rollup_refutation_rewards #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
        | Block_fees
        | Nonce_revelation_rewards
(**# ifdef __META16__ OPEN_COMMENTS #**)
        | Double_signing_evidence_rewards
        | Endorsing_rewards
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
        | Attesting_rewards
(**# ifndef __META16__ CLOSE_COMMENTS #**)
        | Baking_rewards
        | Baking_bonuses
        | Storage_fees
        | Double_signing_punishments
        | Liquidity_baking_subsidies
        | Burned
        | Bootstrap
        | Invoice
        | Initial_commitments
        | Minted
        | Commitments _ (* bpkh *) ->
          Lwt.return
            { k = None ; block_level ; opaid ; iorid ;
              balance ; cycle = None; diff ; id = !counter
            ; update_origin }
(**# ifndef __META10__ CLOSE_COMMENTS #**)
      ) t
    >>= fun l ->
    without_transaction_ignore conn insert l


end

type operation_status = int
let opstatus = int16

type error_list = string
let error_list = string


module Origination_table = struct
  type t = {
    opaid : opaid;
    src: kid ;
    k: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    paid_storage_size_diff : Z.t option;
    fee : Tez.t option;
    nonce : int option;
    preorigination_id : kid option;
    script : Script.t option;
    delegate_id : kid option;
    credit : Tez.t;
    manager_id : kid option;
    block_level : block_level;
    status : operation_status;
    error_list : string option;
    strings : string list option;
  }

  let encoding =
    custom
      ~encode:(fun { opaid ; src ; k ; consumed_gas ; storage_size ;
                     paid_storage_size_diff ; fee ; nonce
                   ; preorigination_id ; script ; delegate_id ; credit
                   ; manager_id
                   ; block_level
                   ; status
                   ; error_list
                   ; strings
                   } ->
                Ok (( (opaid, src, k, consumed_gas)
                    , (storage_size, paid_storage_size_diff, fee, nonce)
                    , (preorigination_id, script, delegate_id, credit)
                    , (manager_id, block_level, status, error_list))
                   , strings
                   , Tezos_indexer_lib.Config.extracted_address_length_limit.contents))
      ~decode:(fun _ -> assert false)
      (tup18
         opaid kid (option kid) (option milligas)
         (option z) (option z) (option tez) (option integer)
         (option kid) (option script) (option kid) tez
         (option kid) block_level opstatus (option error_list)
         (option text_array)
         int16
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.origination($1,$2,$3,$4,\
       $5,\
       $6,\
       $7,\
       $8,\
       $9,$10,$11,$12,$13,$14,$15,$16,$17,$18)"
      (* "select I.origination($1,$2,$3,$4::numeric,\
       *  $5::numeric,\
       *  $6::numeric,\
       *  $7,\
       *  $8,\
       *  $9,$10,$11,$12,$13,$14,$15,$16,$17,$18)" *)


end

module Tx_table = struct
  type t = {
    opaid : opaid;
    source : kid ;
    destination : kid ;
    fee : Tez.t option ;
    amount : Tez.t ;
    originated_contracts : kid list;
    parameters : Script.lazy_expr option ;
    storage : Script.lazy_expr option ;
    consumed_gas : Fpgas.t option ;
    storage_size : Z.t option ;
    paid_storage_size_diff : Z.t option ;
    entrypoint : string option ;
    nonce : int option ;
    status : operation_status ;
    error_list : error_list option;
    strings : string list option;
    (**# ifdef __META11__ ticket_hash : Ticket_hash.t option; #**)
    (**# ifdef __META13__ ticket_receipt : Ticket_receipt.t option; #**)
  }

  let encoding =
    custom
      ~encode:begin fun { opaid ; source ; destination ; fee
                        ; amount ; parameters ; storage ; consumed_gas
                        ; storage_size ; paid_storage_size_diff ; entrypoint ; nonce
                        ; status ; error_list ; strings ; originated_contracts
                          (**# ifdef __META11__ ; ticket_hash #**)
                          (**# ifdef __META13__ ; ticket_receipt #**)
                        } ->
        Ok (
              (**# ifdef __META13__ mtup19 #**)(**# elseifdef __META11__ mtup18 #**)(**# else mtup17 #**)
            opaid source destination fee
            amount (if originated_contracts = [] then None else Some originated_contracts) parameters storage
            consumed_gas storage_size paid_storage_size_diff entrypoint
            nonce status error_list strings Tezos_indexer_lib.Config.extracted_address_length_limit.contents
            (**# ifdef __META11__ ticket_hash #**)
            (**# ifdef __META13__ ticket_receipt #**)
        )
      end
      ~decode:begin fun _ -> assert false end
      ((**# ifdef __META13__ tup19 #**)(**# elseifdef __META11__ tup18 #**)(**# else tup17 #**)
         opaid kid kid (option tez)
         tez (option int64_array) (option lazy_expr) (option lazy_expr)
         (option milligas) (option z) (option z) (option string)
         (option int16) opstatus (option error_list) (option text_array)
         int16
         (**# ifdef __META11__ (option ticket_hash) #**)
         (**# ifdef __META13__ (option ticket_receipt) #**)
       )

  let insert =
    query_zero_or_one
      encoding
      unit
      (**# ifdef __META13__ "select I.tx($1,$2,$3,$4,$5,$6,$7,$8,\
           $9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)" #**)
      (**# elseifdef __META11__ "select I.tx($1,$2,$3,$4,$5,$6,$7,$8,\
           $9,$10,$11,$12,$13,$14,$15,$16,$17,$18,null)" #**)
      (**# else "select I.tx($1,$2,$3,$4,$5,$6,$7,$8,\
           $9,$10,$11,$12,$13,$14,$15,$16,$17,null,null)" #**)

end

module Delegation_table = struct
  let insert =
    query_zero_or_one
      (tup8
         opaid kid (option kid) (option milligas)
         (option tez) (option integer) opstatus (option error_list))
      unit
      "select I.delegation($1,$2,$3,$4,$5,$6,$7,$8)"
end


module Reveal_table = struct
  let insert =
    query_zero_or_one
      (tup8
         opaid kid pk (option milligas)
         (option tez) (option integer) opstatus (option error_list))
      unit
      "select I.reveal($1,$2,$3,$4,$5,$6,$7,$8)"
end


let eoperation = custom_of_encoding Alpha_context.Operation.encoding

module Endorsement_or_preendorsement_tables = struct
  type t = {
    opaid : opaid ;
    level : block_level ;
    delegate_id : kid ;
    slots : int list ;
    endorsement_or_preendorsement_power : int option ;
    round : int32 option ;
    block_payload_hash : string option ;
  }

  let encoding =
    custom
      ~encode:begin fun { opaid ; level ; delegate_id ; slots
                        ; endorsement_or_preendorsement_power ; round ; block_payload_hash } ->
        Ok (mtup7 opaid level delegate_id slots endorsement_or_preendorsement_power round block_payload_hash)
      end
      ~decode:begin fun _ -> assert false
      end
      (tup7
         opaid
         block_level
         kid
         slots
         (option integer)
         (option int32)
         (option string)
      )

  let insert =
    let a =
      query_zero_or_one
        encoding
        unit
        "select I.preendorsement($1,$2,$3,$4,$5,$6,$7)"
    and b =
      query_zero_or_one
        encoding
        unit
        "select I.endorsement($1,$2,$3,$4,$5,$6,$7)"
    in
    fun ~pre ->
      if pre then
        a
      else
        b

end

let proto_hash = custom_to_string_of_pp Protocol_hash.pp

module Proposals_table = struct
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; i; source; period; proposal; } ->
        Ok (mtup5 opaid i source period proposal)
      end
      ~decode:begin fun _ -> assert false end
      (tup5 opaid int64 kid int32 proto_hash)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.proposal($1,$2,$3,$4,$5)"
  let insert2 =
    query_zero_or_one
      encoding
      unit
      "select I.proposal2($1,$2,$3,$4,$5)"
end

module Ballot_table = struct
  type t = {
    opaid    : opaid;
    i        : int64;
    source   : kid;
    period   : int32;
    proposal : Protocol_hash.t;
    ballot   : Alpha_context.Vote.ballot;
  }

  let ballot =
    custom
      ~encode:(fun a -> Ok Vote.(match a with Yay -> "yay" | Nay -> "nay" | Pass -> "pass"))
      ~decode:(fun a -> Ok Vote.(match a with "yay" -> Yay | "nay" -> Nay | "pass" -> Pass
                                            | _ -> assert false))
      string
  let encoding =
    custom
      ~encode:(fun { opaid; i; source; period; proposal; ballot; } ->
        Ok (mtup6 opaid i source period proposal ballot)
        )
      ~decode:(fun _ -> assert false)
      (tup6 opaid int64 kid int32 proto_hash ballot)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.ballot($1,$2,$3,$4,$5,$6)"
end

module Double_endorsement_evidence_table = struct
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid option;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; baker_id; offender_id; op1; op2; } ->
        Ok (mtup5 opaid baker_id offender_id op1 op2)
      end
      ~decode:begin fun _ -> assert false end
      (tup5 opaid kid (option kid) eoperation eoperation)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.double_endorsement($1,$2,$3,$4,$5)"
end

module Double_preendorsement_evidence_table = struct
  type t = {
    opaid : opaid;
    baker_id : kid;
    offender_id : kid option;
    op1 : Alpha_context.Operation.packed;
    op2 : Alpha_context.Operation.packed;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; baker_id; offender_id; op1; op2; } ->
        Ok (mtup5 opaid baker_id offender_id op1 op2)
      end
      ~decode:begin fun _ -> assert false end
      (tup5 opaid kid (option kid) eoperation eoperation)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.double_preendorsement($1,$2,$3,$4,$5)"
end

module Double_baking_evidence_table = struct
  let bheader = custom_of_encoding Block_header.encoding

  type t = {
    opaid : opaid;
    bh1 : Block_header.t;
    bh2 : Block_header.t;
    baker_id : kid;
    offender_id : kid option;
  }
  let encoding =
    custom
      ~encode:begin fun { opaid; bh1; bh2; baker_id; offender_id; } ->
        Ok (mtup5 opaid bh1 bh2 baker_id offender_id)
      end
      ~decode:begin fun _ -> assert false end
      (tup5 opaid bheader bheader kid (option kid))
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.double_baking($1,$2,$3,$4,$5)"
end


module Mempool_operations = struct
  type status =
    | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

  type t = {
    branch: Block_hash.t;
    op_hash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Public_key_hash.t option;
    destination: Addresses.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level : int32;
  }

  let string_of_status = function
    | Applied -> "applied"
    | Refused -> "refused"
    | Branch_refused -> "branch_refused"
    | Unprocessed -> "unprocessed"
    | Branch_delayed -> "branch_delayed"
  let status_of_string = function
    | "applied" -> Some Applied
    | "refused" -> Some Refused
    | "branch_refused" -> Some Branch_refused
    | "unprocessed" -> Some Unprocessed
    | "branch_delayed" -> Some Branch_delayed
    | _ -> None


  let sql_encoding =
    custom
      ~encode:(fun {
          branch;
          op_hash;
          status;
          id;
          operation_kind;
          source;
          destination;
          seen;
          json_op;
          context_block_level;
        } -> Ok (mtup10
                   branch
                   context_block_level
                   op_hash
                   (string_of_status status)
                   id
                   operation_kind
                   source
                   destination
                   seen
                   (Lazy.force json_op)))
      ~decode:(function
          | (branch,
             context_block_level,
             op_hash,
             ("applied"|"refused"|"branch_refused"|"branch_delayed"|"unprocessed"
              as status)),
            (id,
             operation_kind,
             source,
             destination),
            seen,
            json_op ->
            Ok {
              branch;
              context_block_level;
              op_hash;
              status =
                (match status_of_string status with
                 | Some s -> s
                 | _ -> assert false);
              id;
              operation_kind;
              source;
              destination;
              seen;
              json_op = lazy json_op;
            }
          | _ -> Error ("status_of_string"))
      (tup10
         bh
         int32
         oph string int16 int16
         (option pkh) (option Addresses.address) float string)

  let insert =
    query_zero_or_one
      sql_encoding
      unit
      "select M.I_operation_alpha(?,?,?,?,?,?,?,?,?,?)"

end

module Seed_nonce_revelation_table = struct

  type t = {
    opaid : opaid;
    sender_id : kid;
    baker_id : kid;
    level : block_level;
    nonce : Nonce.t
  }

  let encoding =
    custom
      ~encode:(fun { opaid; sender_id; baker_id; level; nonce } ->
          Ok (mtup5 opaid sender_id baker_id level nonce))
      ~decode:(fun _ -> assert false)
      (tup5 opaid kid kid block_level nonce)
  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.seed_nonce($1,$2,$3,$4,$5)"

end


module Bigmap = struct
  let old_insert =
    query_zero_or_one
      (tup12
         script_expr
         script_expr_hash
         (option script_expr)
         block_level
         (option kid)
         (option kid)
         int64
         (option opaid)
         (option integer) (* iorid *)
         (option text_array)
         int16
         (option int64) (* ohid *)
      )
      unit
      "select B.update(null::bigint,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)"

  let update =
    query_zero_or_one
      (tup13
         z
         script_expr
         script_expr_hash
         (option script_expr)
         block_level
         (option kid)
         (option kid)
         int64
         (option opaid)
         (option integer) (* iorid *)
         (option text_array)
         int16
         (option int64) (* ohid *)
      )
      unit
      "select B.update($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)"

  let clear =
    query_zero_or_one
      (tup8
         z
         block_level
         (option kid)
         (option kid)
         int64
         (option opaid)
         (option integer) (* iorid *)
         (option int64) (* ohid *)
      )
      unit
      "select B.clear($1,$2,$3,$4,$5,$6,$7,$8)"

  let alloc =
    query_zero_or_one
      (tup11
         z
         script_expr
         script_expr
         block_level
         (option kid)
         (option kid)
         int64
         (option opaid)
         (option integer) (* iorid *)
         (option string)
         (option int64) (* ohid *)
      )
      unit
      "select B.alloc($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)"

  let copy =
    query_zero_or_one
      (tup9
         z z block_level (option kid)
         (option kid) int64 (option opaid) (option integer) (* iorid *)
         (option int64) (* ohid *)
      )
      unit
      "select B.copy($1,$2,$3,$4,$5,$6,$7,$8,$9)"

end

module Tokens = struct

  module Contract_table = struct

    type kind = Contract_utils.Tokens.kind
    type t = {
      address : kid ;
      block_level : block_level;
      kind : kind;
    }

    let string_of_kind = function
      | Contract_utils.Tokens.FA12 -> Ok "fa1-2"
      | FA2 -> Ok "fa2"
    let kind_of_string = function
      | "fa1-2" -> Ok Contract_utils.Tokens.FA12
      | "fa2" -> Ok FA2
      | k -> Error ("Unknown kind of token: " ^ k)

    let kind_encoding =
      custom
        ~encode:(string_of_kind)
        ~decode:(kind_of_string)
        string

    let encoding =
      custom
        ~encode:(fun { address ; block_level ; kind } -> Ok (address, block_level, kind))
        ~decode:(fun (address, block_level, kind)-> Ok { address ; block_level ; kind })
        (tup3 kid block_level kind_encoding)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_contract($1,$2,$3)"

    let is_token =
      query_zero_or_one
        kid
        kind_encoding
        "select T.is_token($1)"
  end

  module Balance_table = struct
    type t = {
      token_address : kid ;
      address : kid ;
      amount : int ;
    }

    let encoding =
      custom
        ~encode:(fun { token_address ; address ; amount } ->
            Ok (token_address, address, amount))
        ~decode:(fun (token_address, address, amount)->
            Ok { token_address; address ; amount })
        (tup3 kid kid int16)

    let upsert =
      query_zero_or_one
        encoding
        unit
        "select T.I_balance($1,$2,$3)"
  end

  module Accounts_registry_table = struct
    type t = {
      token_address : kid ;
      account : kid ;
      block_level : int32 ;
    }

    let encoding =
      custom
        ~encode:(fun { token_address ; account ; block_level } ->
            Ok (token_address, account, block_level))
        ~decode:(fun (token_address, account, block_level)->
            Ok { token_address; account; block_level })
        (tup3 kid kid block_level)

    let insert =
      query_zero_or_one
        encoding
        unit
        "select T.I_accounts_registry($1,$2,$3)"
  end

  module FA12 = struct

    module Operation_table = struct

      type kind = Transfer | Approve | GetBalance | GetAllowance | GetTotalSupply

      type t = {
        opaid : opaid ;
        token_address : kid ;
        caller : kid ;
        kind : kind ;
      }

      let kind_encoding =
        custom
          ~encode:(function
                Transfer -> Ok "transfer"
              | Approve -> Ok "approve"
              | GetBalance -> Ok "getBalance"
              | GetAllowance -> Ok "getAllowance"
              | GetTotalSupply -> Ok "getTotalSupply")
          ~decode:(function
                "transfer" -> Ok Transfer
              | "approve" -> Ok Approve
              | "getBalance" -> Ok GetBalance
              | "getAllowance" -> Ok GetAllowance
              | "getTotalSupply" -> Ok GetTotalSupply
              | s -> Error ("unknown kind of token operation: " ^ s))
          string

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         token_address ; caller ; kind } ->
                    Ok (opaid, token_address, caller, kind))
          ~decode:(fun _ -> assert false)
          (tup4 opaid kid kid kind_encoding)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_operation($1,$2,$3,$4)"
    end

    module Transfer_table = struct

      type t = {
        opaid : opaid ;
        source : kid ;
        destination : kid ;
        amount : Z.t ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         source ; destination ; amount } ->
                    Ok ((opaid, source, destination, amount)))
          ~decode:(fun _ -> assert false)
          (tup4 opaid kid kid z)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_transfer($1,$2,$3,$4)"

    end

    module Approve_table = struct

      type t = {
        opaid : opaid ;
        address : kid ;
        amount : Z.t ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         address ; amount } ->
                    Ok (opaid, address, amount))
          ~decode:(fun _ -> assert false)
          (tup3 opaid kid z)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_approve($1,$2,$3)"

    end

    module Get_balance_table = struct

      type t = {
        opaid : opaid ;
        address : kid ;
        callback : kid ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         address ; callback } ->
                    Ok (opaid, address, callback))
          ~decode:(fun _ -> assert false)
          (tup3 opaid kid kid)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_get_balance($1,$2,$3)"

    end

    module Get_allowance_table = struct

      type t = {
        opaid : opaid ;
        source : kid ;
        destination : kid ;
        callback : kid ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         source ; destination ; callback } ->
                    Ok (opaid, source, destination, callback))
          ~decode:(fun _ -> assert false)
          (tup4 opaid kid kid kid)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_get_allowance($1,$2,$3,$4)"

    end

    module Get_total_supply_table = struct

      type t = {
        opaid : opaid ;
        callback : kid ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ; callback } ->
              Ok (opaid, callback))
          ~decode:(fun _ -> assert false)
          (tup2 opaid kid)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa12_get_total_supply($1,$2)"

    end

  end

  module FA2 = struct

    module Operation_table = struct

      type kind = Transfer | Update_operators | Balance_of

      type t = {
        opaid : opaid ;
        token_address : kid ;
        caller : kid ;
        kind : kind ;
      }

      let kind_encoding =
        custom
          ~encode:(function
                Transfer -> Ok "transfer"
              | Update_operators -> Ok "update_operators"
              | Balance_of -> Ok "balance_of")
          ~decode:(function
                "transfer" -> Ok Transfer
              | "update_operators" -> Ok Update_operators
              | "balance_of" -> Ok Balance_of
              | s -> Error ("unknown kind of fa2 operation: " ^ s))
          string

      let encoding =
        custom
          ~encode:(fun { opaid ;
                         token_address ; caller ; kind } ->
                    Ok (opaid, token_address, caller, kind))
          ~decode:(fun _ -> assert false)
          (tup4 opaid kid kid kind_encoding)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa2_operation($1,$2,$3,$4)"
    end

    module Transfer_table = struct

      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        source : kid ;
        destination : kid ;
        amount : Z.t ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ; internal_op_id ; token_id ;
                         source ; destination ; amount } ->
                    Ok (mtup6 opaid internal_op_id token_id
                          source destination amount))
          ~decode:(fun _ -> assert false)
          (tup6 opaid int z kid kid z)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa2_transfer($1,$2,$3,$4,$5,$6)"

    end

    module Update_operators_table = struct

      type kind = Contract_utils.FA2.operator_update_action

      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        kind : kind ;
        owner : kid ;
        operator : kid ;
      }

      (* Assumes the encoding for bool takes only one bit of storage *)
      let kind_encoding =
        custom
          ~encode:(function
                Contract_utils.FA2.Add_operator -> Ok true
              | Contract_utils.FA2.Remove_operator -> Ok false)
          ~decode:(function
                true -> Ok Contract_utils.FA2.Add_operator
              | false -> Ok Contract_utils.FA2.Remove_operator)
          bool

      let encoding =
        custom
          ~encode:(fun { opaid ; internal_op_id ; token_id ;
                         kind ; owner ; operator } ->
                    Ok (mtup6 opaid internal_op_id token_id
                          kind owner operator))
          ~decode:(fun _ -> assert false)
          (tup6 opaid int z kind_encoding kid kid)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa2_update_operators($1,$2,$3,$4,$5,$6)"

    end

    module Balance_of_table = struct

      type t = {
        opaid : opaid ;
        internal_op_id: int ;
        token_id : Z.t ;
        owner : kid ;
        callback : kid ;
      }

      let encoding =
        custom
          ~encode:(fun { opaid ; internal_op_id ; token_id ;
                         owner ; callback } ->
                    Ok (mtup5 opaid internal_op_id token_id
                          owner callback))
          ~decode:(fun _ -> assert false)
          (tup5 opaid int z kid kid)

      let insert =
        query_zero_or_one
          encoding
          unit
          "select T.I_fa2_balance_of($1,$2,$3,$4,$5)"

    end

  end

end

module Activation_table = struct
  let insert =
    query_zero_or_one
      (tup3 opaid kid string)
      unit
      "select I.activate(?,?,?)"
end


(**# ifndef __META8__ OPEN_COMMENTS #**)
module Implicit_operations_results_table = struct
  type t =
    { block_level : int32
    ; operation_kind : int
    ; originated_contracts : kid list option
    ; strings : string list option
    ; storage : Script.expr option
    ; consumed_gas : Fpgas.t
    ; storage_size : Z.t option
    ; paid_storage_size_diff : Z.t option
    ; allocated_destination_contract : bool option
    ; iorid : int
    (**# ifdef __META11__
       ; originated_tx_rollup : kid option
       ; ticket_hash : Ticket_hash.t option
       ; tx_rollup_level : int32 option
       ; sc_address : kid option
       ; sc_size : Z.t option
       ; sc_inbox_after : Sc_rollup.Inbox.t option
       #**)
    (**# ifdef __META13__ ; ticket_receipt : Ticket_receipt.t option
       ; genesis_commitment_hash : Sc_rollup.Commitment.Hash.t option #**)
    }

  let encoding = custom
      ~encode:(fun { operation_kind
                   ; originated_contracts
                   ; strings
                   ; storage
                   ; consumed_gas
                   ; storage_size
                   ; paid_storage_size_diff
                   ; allocated_destination_contract
                   ; iorid
                   ; block_level
                     (**# ifdef __META11__ ; originated_tx_rollup ; sc_address ; sc_size ; sc_inbox_after
                        ; tx_rollup_level ; ticket_hash #**)
                     (**# ifdef __META13__ ; ticket_receipt
                        ; genesis_commitment_hash #**)
                   } ->
                Ok (
                  (**# ifdef __META13__ mtup18 #**)(**# elseifdef __META11__ mtup16 #**)(**# else mtup10 #**)
                      operation_kind
                      originated_contracts
                      strings
                      storage
                      consumed_gas
                      storage_size
                      paid_storage_size_diff
                      allocated_destination_contract
                      iorid
                      block_level
                   (**# ifdef __META11__ originated_tx_rollup sc_address sc_size sc_inbox_after
                      tx_rollup_level ticket_hash #**)
                     (**# ifdef __META13__ ticket_receipt genesis_commitment_hash #**)
                  )
              )
      ~decode:(fun _ -> assert false)
      ((**# ifdef __META13__ tup18 #**)(**# elseifdef __META11__ tup16 #**)(**# else tup10 #**)
         int16 (option int64_array) (option text_array) (option script_expr)
         milligas (option z) (option z) (option bool)
         integer int32
         (**# ifdef __META11__ (option kid) (option kid) (option z) (option sc_rollup_inbox) (option int32) (option ticket_hash) #**)
         (**# ifdef __META13__ (option ticket_receipt) (option genesis_commitment_hash) #**)
      )

  let insert = query_zero_or_one
      encoding
      unit
      (**# ifdef __META13__    "select I.ior($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)" #**)
      (**# elseifdef __META11__ "select I.ior($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,null,null)" #**)
      (**# else                "select I.ior($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,null,null,null,null,null,null,null,null)" #**)

end
(**# ifndef __META8__ CLOSE_COMMENTS #**)


(**# ifndef __META9__ OPEN_COMMENTS #**)
module Register_global_constant_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; value : Script_repr.lazy_expr
  ; size_of_constant : Z.t option
  ; global_address : Script_expr_hash.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; value
      ; size_of_constant
      ; global_address
      ; error_list
      } ->
        Ok (
          mtup10
            opaid source_id nonce fee
            status value consumed_gas size_of_constant
            (Option.map (fun e -> Format.asprintf "%a" Script_expr_hash.pp e) global_address) error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup10
         opaid kid (option integer) (option tez)
         opstatus lazy_expr (option milligas) (option z)
         (option string) (option error_list))

  let insert =
    query_zero_or_one
      encoding
      unit
          (* "select I.rgc($1,$2,$3,$4::bigint,$5::smallint,$6::jsonb,$7::numeric,$8::integer,$9,$10::jsonb)" *)
          "select I.rgc($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"

end
(**# ifndef __META9__ CLOSE_COMMENTS #**)


(**# ifndef __META10__ OPEN_COMMENTS #**)
module Set_deposits_limit_table= struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; value : Tez.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; value
      ; error_list
      } ->
        Ok (
          mtup8 opaid source_id nonce fee
            status value consumed_gas error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus (option tez) (option milligas) (option error_list))

  let insert =
    query_zero_or_one
      encoding
      unit
          (* "select I.set_deposits_limit($1,$2,$3,$4::bigint,$5::smallint,$6::numeric,$7::numeric,$8::jsonb)" *)
          "select I.set_deposits_limit($1,$2,$3,$4,$5,$6,$7,$8)"
end
(**# ifndef __META10__ CLOSE_COMMENTS #**)


(**# ifndef __META11__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
module Tx_rollup_origination_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; originated_tx_rollup : kid option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; originated_tx_rollup
      } ->
        Ok (
          mtup8 opaid source_id nonce fee
            status originated_tx_rollup consumed_gas error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus (option tx_rollup) (option milligas) (option error_list)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_origination($1,$2,$3,$4,$5,$6,$7,$8)"
      (* "select I.tx_rollup_origination($1,$2,$3,$4::bigint,$5::smallint,$6,$7::numeric,$8::jsonb)" *)
end

module Tx_rollup_submit_batch_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; content : string
  ; burn_limit : Tez.t option
  ; paid_storage_size_diff : Z.t option
  }

  let encoding =
    custom
      ~encode:begin fun ({
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; content
      ; burn_limit
      ; paid_storage_size_diff
      } : t) ->
        Ok (
          mtup11 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
            content burn_limit
            paid_storage_size_diff
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup11
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
         string (option tez)
         (option z)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_submit_batch($1,$2,$3,$4,$5,$6,$7,$8,cast($9 as bytea),$10,$11)"
end

module Tx_rollup_commit_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; commitment : Tx_rollup_commitment.Full.t
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; commitment
      } ->
        Ok (
          mtup9 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
            commitment
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
         tx_rollup_commitment
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_commit($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end


module Tx_rollup_finalize_commitment_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; level
      } ->
        Ok (
          mtup9 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
            level
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
         (option tx_rollup_level)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_finalize_commitment($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end

module Tx_rollup_rejection_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t
  ; message : Tx_rollup_message.t
  ; message_position : int
  ; message_path : Tx_rollup_inbox.Merkle.path
  ; message_result_hash : Tx_rollup_message_result_hash.t
  ; message_result_path : Tx_rollup_commitment.Merkle.path
  ; previous_message_result : Tx_rollup_message_result.t
  ; previous_message_result_path : Tx_rollup_commitment.Merkle.path
  ; proof : Tx_rollup_l2_proof.(**# ifdef __META13__ serialized #**)(**# else t #**)
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; level
      ; message
      ; message_position
      ; message_path
      ; message_result_hash
      ; message_result_path
      ; previous_message_result
      ; previous_message_result_path
      ; proof
      } ->
        Ok (
          mtup17
            opaid
            source_id
            nonce
            fee
            status
            tx_rollup
            consumed_gas
            error_list
            level
            message
            message_position
            message_path
            message_result_hash
            message_result_path
            previous_message_result
            previous_message_result_path
            proof)
      end
      ~decode:begin fun _ -> assert false end
      (tup17
         opaid
         kid
         (option integer)
         (option tez)
         opstatus
         tx_rollup
         (option milligas)
         (option error_list)
         tx_rollup_level
         message
         message_position
         message_path
         message_result_hash
         message_result_path
         previous_message_result
         previous_message_result_path
         proof
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_rejection($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17)"
end

module Tx_rollup_dispatch_tickets_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t
  ; context_hash : Context_hash.t
  ; message_index : int
  ; message_result_path : Tx_rollup_commitment.Merkle.path
  ; tickets_info : Tx_rollup_reveal.t list
  ; paid_storage_size_diff : Z.t option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; level
      ; context_hash
      ; message_index
      ; message_result_path
      ; tickets_info
      ; paid_storage_size_diff
      } ->
        Ok (
          mtup14 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
            level context_hash message_index message_result_path
            tickets_info paid_storage_size_diff
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup14
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
         tx_rollup_level context_hash integer message_result_path
         tickets_info (option z)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_dispatch_tickets($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)"
end


module Tx_rollup_return_bond_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      } ->
        Ok (
          mtup8 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_return_bond($1,$2,$3,$4,$5,$6,$7,$8)"
end

module Tx_rollup_remove_commitment_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; tx_rollup : kid
  ; level : Tx_rollup_level.t option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; tx_rollup
      ; level
      } ->
        Ok (
          mtup9 opaid source_id nonce fee
            status tx_rollup consumed_gas error_list
            level
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option integer) (option tez)
         opstatus tx_rollup (option milligas) (option error_list)
         (option tx_rollup_level)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.tx_rollup_remove_commitment($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end
(**# ifdef __META16__ CLOSE_COMMENTS #**)
module Transfer_ticket_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; contents : Script_repr.lazy_expr
  ; ty : Script_repr.lazy_expr
  ; ticketer : kid
  ; amount : Z.t
  ; destination : kid
  ; entrypoint : Entrypoint_repr.t
  ; paid_storage_size_diff : Z.t option
  (**# ifdef __META14__ ; ticket_receipt : Ticket_receipt.t option #**)
  }
  (**# ifdef __META14__ let ticket_receipt = custom_of_encoding Ticket_receipt.encoding #**)

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; contents
      ; ty
      ; ticketer
      ; amount
      ; destination
      ; entrypoint
      ; paid_storage_size_diff
      (**# ifdef __META14__ ; ticket_receipt #**)
      } ->
        Ok (
          (**# ifdef __META14__ mtup14 #**)(**# else mtup13 #**)
            opaid
            source_id
            fee
            status
            consumed_gas
            error_list
            contents
            ty
            ticketer
            amount
            destination
            entrypoint
            paid_storage_size_diff
            (**# ifdef __META14__ ticket_receipt #**)
        )
      end
      ~decode:begin fun _ -> assert false end
      ((**# ifdef __META14__ tup14 #**)(**# else tup13 #**)
         opaid
         kid
         (option tez)
         opstatus
         (option milligas)
         (option error_list)
         lazy_expr
         lazy_expr
         kid
         z
         kid
         entrypoint
         (option z)
         (**# ifdef __META14__ (option ticket_receipt) #**)
       )

  let insert =
    query_zero_or_one
      encoding
      unit
      (**# ifdef __META14__
         "select I.transfer_ticket($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)" #**)
      (**# else
         "select I.transfer_ticket($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,null)" #**)

end
(**# ifndef __META11__ CLOSE_COMMENTS #**)


(**# ifndef __META14__ OPEN_COMMENTS #**)
module Sc_rollup_add_messages_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; messages : string list
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; messages
      } ->
        Ok (
          mtup8 opaid source_id nonce fee
            status consumed_gas error_list
            messages
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list)
         text_array)

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_add_messages($1,$2,$3,$4,$5,$6,$7,$8)"
end

module Sc_rollup_cement_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; inbox_level : int32 option
  ; rollup : kid
  ; commitment : Sc_rollup.Commitment.Hash.t option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; inbox_level
      ; rollup
      ; commitment
      } ->
        Ok (
          mtup9 opaid source_id fee consumed_gas
            status error_list inbox_level rollup
            commitment
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option tez) (option milligas)
         opstatus (option error_list) (option int32) sc_rollup_address
         (option sc_rollup_commitment_hash))

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_cement($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end
(**# ifndef __META14__ CLOSE_COMMENTS #**)

(**# ifndef __META12__ OPEN_COMMENTS #**)
module Vdf_revelation_table = struct
  let insert = query_zero_or_one
    (tup2 opaid vdf_solution)
    unit
    "select I.vdf_revelation($1, $2)"
end
module Event_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , ty
        , tag
        , payload)
      , consumed_gas
      , error_list
      ) ->
        Ok (
          mtup10 opaid source_id nonce fee
            status ty tag payload consumed_gas error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup10
         opaid kid integer (option tez)
         opstatus event_ty event_tag event_payload (option milligas) (option error_list)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.event($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"
end


module Increase_paid_storage_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , amount_in_bytes
        , destination
        , consumed_gas)
      , error_list
      ) ->
        Ok (
          mtup9 opaid source_id nonce fee
            status amount_in_bytes destination consumed_gas
            error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option integer) (option tez)
         opstatus z kid (option milligas)
         (option error_list)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.increase_paid_storage($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end

module Sc_rollup_recover_bond_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , consumed_gas
        , error_list
        , rollup)) ->
        Ok (
          mtup8 opaid source_id nonce fee
            status consumed_gas error_list rollup
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list) sc_rollup_address
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_recover_bond($1,$2,$3,$4,$5,$6,$7,$8)"
end

module Sc_rollup_dal_slot_subscribe_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , consumed_gas
        , error_list
        , rollup)
      , level
      , slot_index) ->
        Ok (
          mtup10 opaid source_id nonce fee
            status consumed_gas error_list rollup
            (Option.map Raw_level.to_int32 level) (int_of_string @@ Format.asprintf "%a" Dal.Slot_index.pp slot_index)
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup10
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list) sc_rollup_address
         (option int32) int16
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_dal_slot_subscribe($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"
end

(**# ifndef __META12__ CLOSE_COMMENTS #**)

(**# ifndef __META13__ OPEN_COMMENTS #**)
module Update_consensus_key_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , consumed_gas
        , error_list
        , pk)) ->
        Ok (
          mtup8 opaid source_id nonce fee
            status consumed_gas error_list pk
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup8
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list) pk
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.update_consensus_key($1,$2,$3,$4,$5,$6,$7,$8)"
end

module Sc_rollup_execute_outbox_message_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , nonce
        , fee)
      , (status
        , consumed_gas
        , error_list
        , paid_storage_size_diff)
      , (rollup
        , cemented_commitment
        , output_proof
        , ticket_receipt)) ->
        Ok (
          mtup12 opaid source_id nonce fee
            status consumed_gas error_list paid_storage_size_diff
            rollup cemented_commitment output_proof ticket_receipt
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup12
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list) (option z)
         sc_rollup_address sc_rollup_commitment_hash string (option ticket_receipt)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_execute_outbox_message($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)"
end

module Sc_rollup_originate_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; kind : Sc_rollup.Kind.t
  ; boot_sector_hash : string
  ; address : kid option
  ; size : Z.t option
  ; parameters_ty : Script.lazy_expr
  ; origination_proof : string option
  ; genesis_commitment_hash : Sc_rollup.Commitment.Hash.t option
  }

  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; kind
      ; boot_sector_hash
      ; address
      ; size
      ; parameters_ty
      ; origination_proof
      ; genesis_commitment_hash
      } ->
        Ok (
          mtup14
            opaid source_id nonce fee
            status consumed_gas error_list kind
            boot_sector_hash address size parameters_ty
            origination_proof genesis_commitment_hash

        )
      end
      ~decode:begin fun _ -> assert false end
      (tup14
         opaid kid (option integer) (option tez)
         opstatus (option milligas) (option error_list) sc_rollup_kind
         string (option sc_rollup_address) (option z)
         parameters_ty (option origination_proof) (option genesis_commitment_hash)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_originate($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)"
end

module Sc_rollup_boot_sector_table = struct
  type t = {
    boot_sector_hash : string
  ; boot_sector : string
  }
  let encoding boot_sector_encoding =
    custom
      ~encode:begin fun {
         boot_sector_hash
       ; boot_sector
      } ->
        Ok (
          mtup2
            boot_sector_hash boot_sector
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup2
         string boot_sector_encoding
      )

  let insert_txt =
    query_zero_or_one
      (encoding boot_sector_txt)
      string
      "select I.sc_rollup_boot_sector_txt($1,$2)"

  let insert_bin =
    query_zero_or_one
      (encoding boot_sector_bin)
      string
      "select I.sc_rollup_boot_sector_bin($1,$2)"
end

module Sc_rollup_publish_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; nonce : int option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; commitment : Sc_rollup.Commitment.t
  ; staked_hash : Sc_rollup.Commitment.Hash.t option
  ; published_at_level : Raw_level.t option
  }
  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; nonce
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; rollup
      ; commitment
      ; staked_hash
      ; published_at_level
      } ->
        Ok (
          mtup11 opaid source_id nonce fee
            status rollup consumed_gas error_list
            commitment staked_hash (Option.map Raw_level.to_int32 published_at_level)
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup11
         opaid kid (option integer) (option tez)
         opstatus sc_rollup_address (option milligas) (option error_list)
         sc_rollup_commitment (option sc_rollup_commitment_hash) (option int32)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_publish($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)"
end

module Zk_rollup_origination_table = struct
  type 'circuits_info t = {
    opaid : opaid;
    source_id: kid ;
    originated_zk_rollup: kid option;
    consumed_gas : Fpgas.t option;
    storage_size : Z.t option;
    fee : Tez.t option;
    public_parameters : Tezos_protocol_environment_(**# get PROTO #**).Plonk.public_parameters;
    circuits_info : 'circuits_info;
    init_state : Zk_rollup.State.t;
    nb_ops : int;
    status : operation_status;
    error_list : error_list option;
  }


  let encoding circuits_info_encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; originated_zk_rollup
      ; fee
      ; status
      ; consumed_gas
      ; public_parameters
      ; circuits_info
      ; init_state
      ; nb_ops
      ; storage_size
      ; error_list
      } ->
        Ok (
          mtup12
            opaid source_id originated_zk_rollup fee
            status consumed_gas public_parameters circuits_info
            init_state nb_ops storage_size error_list
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup12
         opaid kid (option kid) (option tez)
         opstatus (option milligas) public_parameters (custom_of_encoding circuits_info_encoding)
         zk_state integer (option z) (option error_list)
      )

  let insert circuits_info_encoding =
    query_zero_or_one
      (encoding circuits_info_encoding)
      unit
      "select I.Zk_rollup_origination($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)" (* TODO/FIXME: ZK *)


end

module Zk_rollup_publish_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; zk_rollup : kid
  ; ops : (Zk_rollup.Operation.t * Zk_rollup.Ticket.t option) list
  ; paid_storage_size_diff : Z.t option
  }
  let zk_ops =
    custom_of_encoding
      Data_encoding.(
        list
          (tup2
             (Obj.magic Zk_rollup_operation_repr.encoding)
             (option (Obj.magic Zk_rollup_ticket_repr.encoding)))
      )
  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; zk_rollup
      ; ops
      ; paid_storage_size_diff
      } ->
        Ok (
          mtup9
            opaid source_id fee status
            zk_rollup consumed_gas error_list ops
            paid_storage_size_diff
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option tez) opstatus
         kid (option milligas) (option error_list) zk_ops
         (option z)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.zk_rollup_publish($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end

module Drain_delegate_table = struct
  type t = {
    opaid: opaid;
    consensus_key : kid;
    delegate : kid;
    destination : kid;
    allocated_destination_contract: bool;
  }
let encoding =
  custom
    ~encode:begin fun {
      opaid
    ; consensus_key
    ; delegate
    ; destination
    ; allocated_destination_contract
    } ->
      Ok (
        mtup5
          opaid consensus_key delegate destination
          allocated_destination_contract
      )
    end
    ~decode:begin fun _ -> assert false end
    (tup5
       opaid kid kid kid
       bool
    )
let insert =
  query_zero_or_one
    encoding
    unit
    "select I.drain_delegate($1,$2,$3,$4,$5)"
end

(**# ifndef __META13__ CLOSE_COMMENTS #**)


(**# ifndef __META14__ OPEN_COMMENTS #**)
let dal_slot_index = custom_of_encoding Dal.Slot_index.encoding
let dal_slot_commitment = custom_of_encoding Dal.Slot.Commitment.encoding
let dal_slot_commitment_proof = custom_of_encoding Dal.Slot.Commitment_proof.encoding
module Dal_publish_slot_header_table = struct
  let encoding =
    custom
      ~encode:begin fun (
        (opaid
        , source_id
        , fee
        , status)
      , (consumed_gas
        , published_level
        , error_list
        , index
        )
      , commitment
      , proof) ->
        Ok (
          mtup10 opaid source_id fee status
            consumed_gas published_level error_list index
            commitment proof
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup10
         opaid kid (option tez) opstatus
         (option milligas) (option int32) (option error_list) dal_slot_index
         dal_slot_commitment dal_slot_commitment_proof
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.dal_publish_slot_header($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"
end

module Dal_attestation_table = struct
  let dal_attestation = custom_of_encoding Dal.Attestation.encoding

  let insert :
    ((opaid(**# ifndef __META16__ , kid #**), Dal.Attestation.t, int32, kid) (**# ifdef __META16__ tup4 #**)(**# else tup5 #**)
    , unit, Caqti_mult.zero_or_one) Caqti_request.t
    =
    query_zero_or_one
      ((**# ifdef __META16__ tup4 #**)(**# else tup5 #**)
         opaid (**# ifndef __META16__ kid #**) dal_attestation int32
         kid
      )
      unit
      (**# ifdef __META16__ "select I.dal_attestation($1,null,$2,$3,$4)" #**)
      (**# else "select I.dal_attestation($1,null,$3,$4,$5)" #**)

end
(* module Dal_slot_availability_table = struct
 *   let dal_slot_availability =
 *     custom_of_encoding Dal.Endorsement.encoding
 *   let encoding =
 *     custom
 *       ~encode:begin fun (
 *         (opaid
 *         , source_id
 *         , nonce
 *         , fee)
 *       , (status
 *         , consumed_gas
 *         , slot
 *         , error_list
 *         )
 *       , (endorser
 *         , slot_availability
 *         , level
 *         )) ->
 *         Ok (
 *           mtup11 opaid source_id nonce fee
 *             status consumed_gas slot error_list
 *             endorser slot_availability level
 *         )
 *       end
 *       ~decode:begin fun _ -> assert false end
 *       (tup11
 *          opaid kid (option integer) tez
 *          opstatus (option milligas) dal_slot (option error_list)
 *          pkh dal_slot_availability int32
 *       )
 *
 *   let insert =
 *     query_zero_or_one
 *       encoding
 *       unit
 *       "select I.dal_slot_availability($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) -- WIP"
 * end *)

let sc_refutation = custom_of_encoding Sc_rollup.Game.refutation_encoding
let sc_game_status = custom_of_encoding Sc_rollup.Game.status_encoding

module Sc_rollup_refute_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; opponent : kid
  ; refutation : Sc_rollup.Game.refutation
  ; game_status : Sc_rollup.Game.status option
  }
  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; rollup
      ; opponent
      ; refutation
      ; game_status
      } ->
        Ok (
          mtup10 opaid source_id fee
            status rollup consumed_gas error_list
            opponent refutation game_status
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup10
         opaid kid (option tez) opstatus
         sc_rollup_address (option milligas) (option error_list) kid
         sc_refutation (option sc_game_status)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_refute($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)"

end
let sc_stakers = custom_of_encoding Sc_rollup.Game.Index.encoding
module Sc_rollup_timeout_table = struct
  type t = {
    opaid : opaid
  ; source_id : kid
  ; fee : Tez.t option
  ; consumed_gas : Fpgas.t option
  ; status : operation_status
  ; error_list : error_list option
  ; rollup : kid
  ; stakers : Sc_rollup.Game.Index.t
  ; game_status : Sc_rollup.Game.status option
  }
  let encoding =
    custom
      ~encode:begin fun {
        opaid
      ; source_id
      ; fee
      ; status
      ; consumed_gas
      ; error_list
      ; rollup
      ; stakers
      ; game_status
      } ->
        Ok (
          mtup9
            opaid source_id fee status
            rollup consumed_gas error_list stakers
            game_status
        )
      end
      ~decode:begin fun _ -> assert false end
      (tup9
         opaid kid (option tez) opstatus
         sc_rollup_address (option milligas) (option error_list) sc_stakers
         (option sc_game_status)
      )

  let insert =
    query_zero_or_one
      encoding
      unit
      "select I.sc_rollup_timeout($1,$2,$3,$4,$5,$6,$7,$8,$9)"
end

(**# ifndef __META14__ CLOSE_COMMENTS #**)
