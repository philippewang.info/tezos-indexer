(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


open Legacy_monad_globals
let ( >>= ) = Lwt.bind

let warn_if_error ~__LOC__ : ('a, 'b) result -> ('a, 'b) result Lwt.t = function
  | Error _e as r ->
    Verbose.Debug.eprintf ~vl:1 "Warning: Error at %s: %a" __LOC__ pp_print_trace _e;
    Lwt.return r
  | Ok _ as r -> Lwt.return r

let _ = warn_if_error

let proto = "(**# get PROTO #**)"

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X else open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
module Environment = Tezos_protocol_environment_(**# get PROTO #**)

(**# ifdef __META12__
let implicit_contract pkh = Alpha_context.Contract.Implicit pkh
#**)(**#X else
let implicit_contract =
  Alpha_context.Contract.implicit_contract
X#**)

(* Notes: protocol transition files, including `chain_db.ml`, are generated and
   placed in the `bin_indexer` folder, where they use `bin_indexer/dune`,
   while the other `chain_db.ml` files are placed in protocol-specific
   folders, each having its own `dune` file.
   That's the first reason why transition files require different `open`s.

   Then, with protocol 5, the architecture of the modules changed. That's the
   second reason why `open`s are different for the two categories of protocols
   (before 5, and from 5).
*)

open Alpha_context

open Db_tups
open Db_base
open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Db_alpha
open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Db_alpha.Utils


(**# ifndef __META14__ OPEN_COMMENTS #**)
module Public_key = Tezos_crypto.Signature.V1.Public_key
module Public_key_hash = Tezos_crypto.Signature.V1.Public_key_hash
let _f (x : Alpha_context.public_key_hash) (y : Tezos_crypto.Signature.V1.Public_key_hash.t) = x = y
let _g (x : Public_key_hash.t) (y : Tezos_crypto.Signature.V1.Public_key_hash.t) = x = y
let _h (x : Alpha_context.public_key_hash) (y : Public_key_hash.t) = x = y
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
module Public_key = Tezos_crypto.Signature.V0.Public_key
module Public_key_hash = Tezos_crypto.Signature.V0.Public_key_hash
let _f (x : Alpha_context.public_key_hash) (y : Tezos_crypto.Signature.V0.Public_key_hash.t) = x = y
let _g (x : Public_key_hash.t) (y : Tezos_crypto.Signature.V0.Public_key_hash.t) = x = y
let _h (x : Alpha_context.public_key_hash) (y : Public_key_hash.t) = x = y
(**# ifdef __META14__ CLOSE_COMMENTS #**)



module BS = Tezos_indexer_lib.BS.(**# get BS #**)
module Misc = Tezos_indexer_lib.Misc
let ( !- ) msg = Misc.lwt_limited_run ~msg

module Client = Tezos_client_(**# get PROTO #**)

class type rpc_context = (**#X ifdef __META1__ Client.Alpha_client_context.full
     X#**)(**#X else Client.Protocol_client_context.full X#**)

(**# ifdef __META7__ module Delegate = Receipt #**)

module Script_utils = struct

  let bump_pairs =
    let rec bump_pairs =
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      function
      | ( String _
        | Bytes _
        | Int _) as x -> x
      | Seq (loc, l) ->
        Seq (loc, List.map bump_pairs l)
      | Prim (loc, T_pair, l, annots) ->
        Prim (loc, T_pair, List.map bump_pairs (bump l), annots)
      | Prim (loc, i, l, annots) ->
        Prim (loc, i, List.map bump_pairs l, annots)
    and bump l =
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      List.map
        (function
          | ( String _
            | Bytes _
            | Int _) as x -> [x]
          | Seq (loc, l) ->
            [Seq (loc, List.map bump_pairs l)]
          | Prim (_loc, T_pair, l, _annots) ->
            l
          | Prim _ as e ->
            [e]
        )
        l
      |> List.flatten
    and f r = (* run until fix point *)
      let x = bump_pairs r in
      if r = x then
        r
      else
        f x
    in
    f

  let _ = bump_pairs

  let rec compare_script x y =
    let open Tezos_micheline.Micheline in
    (* let open Michelson_v1_primitives in *)
    match x, y with
    | Int (_, a), Int (_, b) ->
      compare a b
    | String (_, a), String (_, b) ->
      compare a b
    | Bytes (_, a), Bytes (_, b) ->
      compare a b
    | Prim (loc, a, b, c), Prim (_, d, e, f) ->
      begin match compare (a, c) (d, f) with
      | 0 -> compare_script (Seq (loc, b)) (Seq (loc, e))
      | x -> x
      end
    | Seq (_, []), Seq (_, []) -> 0
    | Seq (loc1, a::l1), Seq (loc2, b::l2) ->
      begin match compare_script a b with
      | 0 -> compare_script (Seq (loc1, l1)) (Seq (loc2, l2))
      | x -> x
      end
    | _ -> compare x y


  let compare_script x y =
    if compare_script x y = 0 then
      0
    else
      compare_script (bump_pairs x) (bump_pairs y)

  let _ = compare_script

end
open Script_utils


let bigmap_debug = 10

module Verbose = struct
  include Tezos_indexer_lib.Verbose
  (**# ifndef __META16__ open Utils #**)

  module PP = struct
    type contract = Contract.t
    let contract = Contract.pp
    (**# ifdef __META11__
    type destination = Destination.t
    let destination = Destination.pp
 #**)
        (**# else
    type destination = Contract.t
    let destination = Contract.pp
#**)
    type tez = Tez.t
    let tez = Tez.pp
    type nonce = Nonce.t
    let nonce out n =
      Format.fprintf out "%a"
        Data_encoding.Json.pp
        (Data_encoding.Json.construct Nonce.encoding n)
    type public_key_hash = Public_key_hash.t
    let public_key_hash = Public_key_hash.pp
    type public_key = Public_key.t
    let public_key = Public_key.pp
  end

  module Debug = struct
    include DebugF(PP)

    module Script = struct
    (**# ifdef __META1__ OPEN_COMMENTS #**)
      let rec pp_canonical out node =
        Format.fprintf out "%a" pp (Tezos_micheline.Micheline.root node)
      and pp out node =
        match node with
        | Tezos_micheline.Micheline.Int (_loc, z) -> Format.fprintf out "%s" (Z.to_string z)
        | String (_loc, string) -> Format.fprintf out "%S" string
        | Bytes (_loc, bytes) -> Format.fprintf out "%S" (Bytes.to_string bytes)
        | Prim (_loc, prim, node_list, (annot:string list)) ->
          let p =
            (**#X ifdef __META3__
               (**# ifdef __TRANSITION__ let open Client in #**)
               (Michelson_v1_printer.ocaml_constructor_of_prim prim)
               X#**)
            (**# else
               (Michelson_v1_primitives.string_of_prim prim)
               #**)
          in
          if node_list = [] then
            Format.fprintf out "%s" p
          else
            begin
              Format.fprintf out "%s(" p;
              print_node_list out node_list;
              Format.fprintf out ")"
            end;
          if annot <> [] then Format.fprintf out "[annots:";
          List.iter (fun e -> Format.fprintf out "%S" e) annot;
          if annot <> [] then Format.fprintf out "]";
        | Seq (_loc, node_list) ->
          if node_list = [] then
            Format.fprintf out "{empty_seq}"
          else
            begin
              Format.fprintf out "{";
              print_node_list out node_list;
              Format.fprintf out "}"
            end
      and print_node_list out node_list =
        let print_sep : unit -> unit = match node_list with
          | _::_::_ -> fun () -> Format.fprintf out ";"
          | _ -> ignore
        in
        List.iter (fun e ->
            Format.fprintf out "%a" pp e;
            print_sep ()
          ) node_list
    (**# ifdef __META1__ CLOSE_COMMENTS #**)


      let rec pp_strings_and_bytes out = function
        | Tezos_micheline.Micheline.Int (_, _) -> ()
        | String (_, string) -> Format.fprintf out "$S=%S" string
        | Bytes (_, bytes) ->
          begin
            let ascii = Hex.show (`Hex (Bytes.to_string bytes)) in
            Format.fprintf out "$H=%S" ascii;
            match Contract.of_b58check (Bytes.to_string bytes) with
            | Ok _ ->
              Format.fprintf out "$C=%S" ascii
            | Error _e ->
              Format.fprintf out "$B=%S(error)" (Bytes.to_string bytes)
          end
        | Seq (_, node_list)
        | Prim (_, _, node_list, _) ->
          List.iter (pp_strings_and_bytes out) node_list
      let pp_strings_and_bytes_canonical out node = pp_strings_and_bytes out (Tezos_micheline.Micheline.root node)
      let pp_strings_and_bytes_script out script =
        match (script : Alpha_context.Script.t option) with
        | None -> Format.fprintf out "None"
        | Some { code ; storage } ->
          match Data_encoding.force_decode code, Data_encoding.force_decode storage with
          | None, None -> ()
          | Some code, None ->
            Format.fprintf out "code=%a storage=[bogus data]"
              pp_strings_and_bytes_canonical code
          | None, Some storage ->
            Format.fprintf out "code=[bogus data] storage=%a"
              pp_strings_and_bytes_canonical storage
          | Some code, Some storage ->
            Format.fprintf out "code=%a storage=%a"
              pp_strings_and_bytes_canonical code
              pp_strings_and_bytes_canonical storage

    end (* of module Script *)

  end (* of module Debug *)

  module Log = struct
    module Log = Tezos_indexer_lib.Verbose.Log(PP)
    include Log

    let string_of_voting_period_kind = function
      | Voting_period.Proposal -> "proposal"
      (**# ifndef __META7__
      | Testing_vote           -> "testing_vote"
      | Testing                -> "testing"
      | Promotion_vote         -> "promotion_vote" #**)
(**# ifdef __META6__
      | Adoption       -> "adoption"
   #**)
(**# ifdef __META7__
      | Exploration    -> "exploration"
      | Cooldown       -> "cooldown"
      | Promotion      -> "promotion"
   #**)


    let _ = string_of_voting_period_kind

(**# ifdef __META16__ OPEN_COMMENTS #**)
    let balance_update ?force ?op level bh balance_updates =
      List.iter
        (fun (balance, diff (**# ifdef __META7__ , _origin #**)) ->
           let bt, k, cycle =
             match balance with
             | Delegate.Contract k -> "delegate", Some k, None
(**# ifdef __META10__ OPEN_COMMENTS #**)
             | Rewards (pkh, cycle) -> "rewards", Some (implicit_contract pkh), Some cycle
             | Fees (pkh, cycle) -> "fees", Some (implicit_contract pkh), Some cycle
             | Deposits (pkh, cycle) -> "deposits", Some (implicit_contract pkh), Some cycle
(**# ifdef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
             | Block_fees -> "Block_fees", None, None
             | Nonce_revelation_rewards -> "Nonce_revelation_rewards", None, None
(**# ifdef __META16__ OPEN_COMMENTS #**)
             | Double_signing_evidence_rewards -> "Double_signing_evidence_rewards", None, None
             | Endorsing_rewards -> "Endorsing_rewards", None, None
             | Lost_endorsing_rewards (pkh, _, _) -> "Lost_endorsing_rewards", Some (implicit_contract pkh), None
             | Deposits pkh -> "Deposits", Some (implicit_contract pkh), None
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
             | Attesting_rewards -> "Attesting_rewards", None, None
             | Lost_attesting_rewards (pkh, _, _) -> "Lost_attesting_rewards", Some (implicit_contract pkh), None
             | Deposits (Single_staker { staker = _ (* FIXME *); delegate = pkh }
                       | Shared_between_stakers { delegate = pkh }
                       | Baker pkh) -> "Deposits", Some (implicit_contract pkh), None
             | Unstaked_deposits ((Single (_ (* FIXME *), pkh) | Shared pkh), cycle) -> "Unstaked_deposits", Some (implicit_contract pkh), Some cycle
             | Staking_delegator_numerator { delegator = k } -> "Staking_delegator_numerator", Some k, None
             | Staking_delegate_denominator { delegate = pkh } -> "Staking_delegate_denominator", Some (implicit_contract pkh), None
(**# ifndef __META16__ CLOSE_COMMENTS #**)
             | Baking_rewards -> "Baking_rewards", None, None
             | Baking_bonuses -> "Baking_bonuses", None, None
             | Storage_fees -> "Storage_fees", None, None
             | Double_signing_punishments -> "Double_signing_punishments", None, None
             | Liquidity_baking_subsidies -> "Liquidity_baking_subsidies", None, None
             | Burned -> "Burned", None, None
             | Bootstrap -> "Bootstrap", None, None
             | Invoice -> "Invoice", None, None
             | Initial_commitments -> "Initial_commitments", None, None
             | Minted -> "Minted", None, None
             | Commitments _ -> "Commitments", None, None
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifdef __PROTO12__
             | Legacy_rewards (pkh, cycle) -> "rewards", Some (implicit_contract pkh), Some cycle
             | Legacy_deposits (pkh, cycle) -> "deposits", Some (implicit_contract pkh), Some cycle
             | Legacy_fees (pkh, cycle) -> "fees", Some (implicit_contract pkh), Some cycle
   #**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
             | Frozen_bonds (k, _bond) -> "frozen_bonds", Some k, None
(**# ifdef __META16__ OPEN_COMMENTS #**)
             | Tx_rollup_rejection_punishments -> "Tx_rollup_rejection_punishments", None, None
             | Tx_rollup_rejection_rewards -> "Tx_rollup_rejection_rewards", None, None
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
(**# ifdef __META12__
             | Sc_rollup_refutation_punishments -> "Sc_rollup_refutation_punishments", None, None
#**)
(**# ifdef __META13__
             | Sc_rollup_refutation_rewards -> "Sc_rollup_refutation_rewards", None, None
#**)
           in
             printf ?force ~vl:4
               "event=update block_level=%ld block=%a type=balance\
                %a%a balance_kind=%s%a diff=%Ld"
               level
               Block_hash.pp_short bh
               (fun out -> function
                  | None -> ()
                  | Some k -> Format.fprintf out " contract=%a" Contract.pp k)
               k
               (fun out -> function
                  | None -> ()
                  | Some (op_hash, id, 0) ->
                    Format.fprintf out " op_hash=%a id=%d" Operation_hash.pp op_hash id
                  | Some (op_hash, id, internal) ->
                    Format.fprintf out " op_hash=%a id=%d internal=%d" Operation_hash.pp op_hash id internal)
               op
               bt
               (pp_option ~name:"cycle" ~pp:Cycle.pp) cycle
               (match diff with
                | Delegate.Debited v -> Int64.neg (Tez.to_mutez v)
                | Credited v -> Tez.to_mutez v
               )
        )
        balance_updates
(**# ifdef __META16__ CLOSE_COMMENTS #**)

  end

end (* of module Verbose *)

module LatestBlockHashes = struct
  let size = 8
  let data = Array.make size (-1l, None)
  let pointer = ref 0
  let block_seen_recently l h =
    let exception Found in
    try for i = 0 to size - 1 do
        match data.(i) with
        | bl, Some b when bl = l && b = h -> raise Found
        | _ -> ()
      done;
      false
    with Found -> true
  let record_hash l h =
    pointer := (!pointer + 1) mod size;
    data.(!pointer) <- (l, Some h)
end


(* SQL queries have a huge cost compared to keeping things inside the process,
   therefore it should help to have quite a large maximum size for the cache.
   Emptying the cache when it's full is not a great solution, but it's simple.
   Otherwise we'd have to keep track of the recently looked-up addresses
   (not the "recently recorded" ones).
*)
let cache_size = 10_000l (* this is mainly a number of blocks *)

module Cache_cache (S: sig type t val size : int end) = struct
  let _ = assert (S.size > 0)
  let cache = Array.make S.size ((Obj.magic 0) : S.t) (* FIXME: maybe use a safe dummy value instead? *)
  let i = ref (-1)
  let add e =
    i := (succ !i) mod S.size; Array.set cache !i e
  let get_elements () = cache
end

module type Address = sig type t val compare : t -> t -> int (* val to_string : t -> string *) end

module type Address_cache = sig
  type t
  val empty : unit -> unit
  val max_level : int32 ref
  val add : t -> int64 -> unit
  module C : sig val get_elements : unit -> (t * int64) array end
end

module Make_address_cache (Address : Address) = struct
  type t = Address.t
  module T = struct type t = Address.t * int64 let size = Int32.to_int cache_size end
  module C = Cache_cache (T)
  let max_level = ref 0l
  module M = Stdlib.Map.Make (Address)
  let map = ref M.empty
  let add e id = map := M.add e id !map ; C.add (e, id)
  let get e = M.find_opt e !map
  let empty () = map := M.empty
end

let _f (x : Alpha_context.public_key_hash) (y : Public_key_hash.t) = x = y

let record_pkh, record_k (**#X ifdef __META11__ , record_destination, record_sc_rollup(**# ifndef __META15__ , record_tx_rollup #**) X#**) =
  (* Simple cache-based optimisation to avoid putting the same address over and
     over in the address table. The size of this cache is limited by the size of
     [cache_size] blocks: the cache is reset at each [cache_size] blocks. *)
  let module CC = Make_address_cache (Contract_hash) in
  let module PC = Make_address_cache (Public_key_hash) in

  (**# ifndef __META11__ OPEN_COMMENTS #**)
  (**# ifdef __META16__ OPEN_COMMENTS #**)
  let module RC = Make_address_cache (Tx_rollup) in
  (**# ifdef __META16__ CLOSE_COMMENTS #**)
  let module SC = Make_address_cache (struct include Sc_rollup.Address let compare = compare end) in
  (**# ifndef __META11__ CLOSE_COMMENTS #**)
  (**# ifdef __META13__
  let module ZK = Make_address_cache (struct include Zk_rollup let compare = compare end) in
     #**)
  let check_overflow level (module A : Address_cache) =
    if level > !A.max_level then begin
      if !A.max_level = 0l then
        A.empty ()
      else
        Array.iter (fun (e, id) -> A.add e id) (A.C.get_elements());
      A.max_level := Int32.add cache_size level;
    end
  in
  let record_in_db conn conn2 k addr_counter =
    let module Conn = (val conn2 : Caqti_lwt.CONNECTION) in
    Conn.find_opt Addresses.insert (k, addr_counter ())
    >>= function
    | Ok (Some id) ->
      Lwt.return (id:int64)
    | Ok None ->
      Verbose.error
        "Failed to record address %s for unforseen reason. I'll exit now." k;
      exit 1
    | Error _ as e ->
      if conn == conn2 then
        (* no auto-recovery for when addresses fail to be recorded *)
        caqti_or_fail ~__LOC__ e
      else
        let module Conn = (val conn : Caqti_lwt.CONNECTION) in
        Conn.find_opt Addresses.insert (k, addr_counter ())
        >>= function
        | Ok (Some id) ->
          Lwt.return (id:int64)
        | Ok None ->
          Verbose.error
            "Failed to record address %s for unforseen reason. I'll exit now." k;
          exit 1
        | Error _ as e ->
          caqti_or_fail ~__LOC__ e
  in
  let contract_repr_of_contract : Contract.t -> Contract_repr.t = Obj.magic in
  let record_pkh ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~pkh =
    (* let pkh : Public_key_hash.t = Obj.magic (pkh : public_key_hash) in *)
    Verbose.Debug.eprintf ~vl:5 "Recording implicit contract address %a" Public_key_hash.pp pkh;
    begin match PC.get pkh with
      | Some id ->
        Verbose.Debug.eprintf ~vl:5 "Found pre-recorded implicit contract address %a with id=%Ld" Public_key_hash.pp pkh id;
        PC.C.add (pkh, id);
        Lwt.return id
      | None ->
        check_overflow level (module PC);
        record_in_db conn conn2 (Public_key_hash.to_b58check pkh) addr_counter >>= fun id ->
        Verbose.Debug.eprintf ~vl:5 "Recorded implicit contract address %a with id=%Ld" Public_key_hash.pp pkh id;
        PC.add pkh id;
        Lwt.return id
    end
  in
  let record_k ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~k =
    match contract_repr_of_contract k with
    | (Contract_repr.Implicit pkh) ->
      record_pkh ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~pkh
    | (Contract_repr.Originated o as k) ->
      Verbose.Debug.eprintf ~vl:5 "Recording originated contract address %a" Contract_repr.pp k;
      begin match CC.get o with
        | Some id ->
          Verbose.Debug.eprintf ~vl:5 "Found pre-recorded originated contract address %a with id=%Ld" Contract_repr.pp k id;
          CC.C.add (o, id);
          Lwt.return id
        | None ->
          check_overflow level (module CC);
          record_in_db conn conn2 (Contract_hash.to_b58check o) addr_counter >>= fun id ->
          Verbose.Debug.eprintf ~vl:5 "Recorded originated contract address %a with id=%Ld" Contract_repr.pp k id;
          CC.add o id;
          Lwt.return id
      end
  in
(**# ifndef __META11__ OPEN_COMMENTS #**)
  let record_sc_rollup ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~sc_rollup:scr =
    Verbose.Debug.eprintf ~vl:5 "Recording SC rollup address %a" Sc_rollup.Address.pp scr;
    begin match SC.get scr with
      | Some id ->
        Verbose.Debug.eprintf ~vl:5 "Found pre-recorded SC rollup address %a with id=%Ld" Sc_rollup.Address.pp scr id;
        SC.C.add (scr, id);
        Lwt.return id
      | None ->
        check_overflow level (module SC);
        record_in_db conn conn2 (Sc_rollup.Address.to_b58check scr) addr_counter >>= fun id ->
        Verbose.Debug.eprintf ~vl:5 "Recorded pre-recorded SC rollup address %a with id=%Ld" Sc_rollup.Address.pp scr id;
        SC.add scr id;
        Lwt.return id
    end
  in
(**# ifdef __META16__ OPEN_COMMENTS #**)
  let record_tx_rollup ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~tx_rollup:r =
    Verbose.Debug.eprintf ~vl:5 "Recording TX rollup address %a" Tx_rollup.pp r;
    begin match RC.get r with
      | Some id ->
        Verbose.Debug.eprintf ~vl:5 "Found pre-recorded TX rollup address %a with id=%Ld" Tx_rollup.pp r id;
        RC.C.add (r, id);
        Lwt.return id
      | None ->
        check_overflow level (module RC);
        record_in_db conn conn2 (Tx_rollup.to_b58check r) addr_counter >>= fun id ->
        Verbose.Debug.eprintf ~vl:5 "Recorded pre-recorded TX rollup address %a with id=%Ld" Tx_rollup.pp r id;
        RC.add r id;
        Lwt.return id
    end
  in
(**# ifdef __META16__ CLOSE_COMMENTS #**)
  let record_destination ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~destination =
    match destination with
    | Destination.Contract k ->
      record_k ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~k
(**# ifdef __META16__ OPEN_COMMENTS #**)
    | Destination.Tx_rollup r ->
      record_tx_rollup ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~tx_rollup:r
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META12__ OPEN_COMMENTS #**)
    | Destination.Sc_rollup scr ->
      record_sc_rollup ~__LINE__ ~addr_counter ~block_level:level conn conn2 ~sc_rollup:scr
    (* | Some (Event _contract_event) <--- for meta10? *)
(**# ifndef __META12__ CLOSE_COMMENTS #**)
(**# ifndef __META13__ OPEN_COMMENTS #**)
    | Destination.Zk_rollup zkr as d ->
      Verbose.Debug.eprintf ~vl:5 "Recording ZK rollup address %a" Destination.pp d;
      begin match ZK.get zkr with
        | Some id ->
          Verbose.Debug.eprintf ~vl:5 "Found pre-recorded ZK rollup address %a with id=%Ld" Destination.pp d id;
          ZK.C.add (zkr, id);
          Lwt.return id
        | None ->
          check_overflow level (module ZK);
          record_in_db conn conn2 (Destination.to_b58check d) addr_counter >>= fun id ->
          Verbose.Debug.eprintf ~vl:5 "Recorded pre-recorded ZK rollup address %a with id=%Ld" Destination.pp d id;
          ZK.add zkr id;
          Lwt.return id
      end
(**# ifndef __META13__ CLOSE_COMMENTS #**)
  in
(**# ifndef __META11__ CLOSE_COMMENTS #**)
  record_pkh, record_k(**#X ifdef __META11__ , record_destination, record_sc_rollup(**# ifndef __META15__ , record_tx_rollup #**) X#**)

(**# ifndef __FA2__ OPEN_COMMENTS #**)
module Tokens = struct

  module Contract_utils = (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Contract_utils
  module FA2 = Contract_utils.FA2
  module FA12 = Contract_utils.FA12

  module Token_account_cache = struct
    module S = Stdlib.Set.Make (struct
        type t = int64 * int64
        let compare = compare
      end)
    let s = ref S.empty
    let level = ref 0l
    let is_present block_level token account =
      if !level <> block_level then
        (level := block_level;
         s := S.empty);
      let old_s = !s in
      let new_s = S.add (token, account) old_s in
      s := new_s;
      new_s == old_s (* physical equality: no addition=>true, otherwise=>false *)
  end


  let record_token_account ~__LINE__ ~addr_counter ~block_level ~k contract contract_id conn conn2 () =
    (* since v10: block_level is added for ensuring that things are deleted in case of reorg-induced deletion *)
    (* In multicore mode, using the same connection can lead to a
       deadlock because of concurrent address insertions from
       different blocks. Also, there is no foreign key for
       block_level.

       In default mode, block_level is bound to C.block.level, which
       forces the insertion to use the same connection as the
       connection for recording the whole block.
    *)
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let module Conn2 = (val conn2 : Caqti_lwt.CONNECTION) in
    Verbose.Log.token_accounts_registry block_level contract k;
    record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k >>= fun account_id ->
    if Token_account_cache.is_present block_level contract_id account_id then
      Lwt.return account_id
    else
      Tokens.Accounts_registry_table.(
        (if !Tezos_indexer_lib.Config.multicore_mode then Conn2.find_opt else Conn.find_opt)
          insert { token_address = contract_id; account = account_id ; block_level })
      >>= caqti_or_fail ~__LOC__ >>= function
      | Some () | None -> Lwt.return account_id

  let store_fa12_action ~opaid ~addr_counter ~block_level ~contract_id ~caller_id contract caller caller_pp op_hash op_id action conn conn2 =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let op kind =
      Tokens.FA12.Operation_table.{
        opaid;
        token_address = contract_id;
        caller = caller_id;
        kind;
      }
    in
    let record_token_account k () =
      record_token_account
        ~__LINE__ ~block_level ~addr_counter contract contract_id conn conn2 ~k ()
    in
    match action with
    | FA12.Transfer (source, destination, amount) ->
      Verbose.Log.fa12_transfer
        block_level contract op_hash op_id caller caller_pp source destination amount;
      Tokens.FA12.(Conn.find_opt Operation_table.insert (op Operation_table.Transfer))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_token_account source () >>= fun source_id ->
      record_token_account destination () >>= fun destination_id ->
      Tokens.FA12.Transfer_table.(
        Conn.find_opt insert
          { opaid;
            source = source_id;
            destination = destination_id;
            amount;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Approve (address, amount) ->
      Verbose.Log.fa12_approve
        block_level contract op_hash op_id caller caller_pp address amount;
      Tokens.FA12.(Conn.find_opt Operation_table.insert (op Operation_table.Approve))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_token_account address () >>= fun address_id ->
      Tokens.FA12.Approve_table.(
        Conn.find_opt insert
          { opaid;
            address = address_id;
            amount;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_balance (address, (callback, _)) ->
      Verbose.Log.fa12_get_balance
        block_level contract op_hash op_id caller caller_pp address callback;
      Tokens.FA12.(Conn.find_opt Operation_table.insert (op Operation_table.GetBalance))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_token_account address () >>= fun address_id ->
      record_token_account callback () >>= fun callback_id ->
      Tokens.FA12.Get_balance_table.(
        Conn.find_opt insert
          { opaid;
            address = address_id;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_allowance (source, destination, (callback, _)) ->
      Verbose.Log.fa12_get_allowance
        block_level contract op_hash op_id caller caller_pp source destination callback;
      Tokens.FA12.(Conn.find_opt Operation_table.insert (op Operation_table.GetAllowance))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_token_account source () >>= fun source_id ->
      record_token_account destination () >>= fun destination_id ->
      record_token_account callback () >>= fun callback_id ->
      Tokens.FA12.Get_allowance_table.(
        Conn.find_opt insert
          { opaid;
            source = source_id;
            destination = destination_id;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | Get_total_supply (callback, _) ->
      Verbose.Log.fa12_get_total_supply
        block_level contract op_hash op_id caller caller_pp callback;
      Tokens.FA12.(Conn.find_opt Operation_table.insert (op Operation_table.GetTotalSupply))
      >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      record_token_account callback () >>= fun callback_id ->
      Tokens.FA12.Get_total_supply_table.(
        Conn.find_opt insert
          { opaid;
            callback = callback_id;
          })
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit

  let store_fa2_action ~opaid ~addr_counter ~block_level ~contract_id ~caller_id contract caller caller_pp op_hash op_id action conn conn2 =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let op kind =
      Tokens.FA2.Operation_table.{
        opaid;
        token_address = contract_id;
        caller = caller_id;
        kind;
      }
    in
    let record_token_account k =
      record_token_account
        ~__LINE__ ~block_level ~addr_counter contract contract_id conn conn2 ~k
    in
    match action with
    | FA2.Transfer transfers ->
      Contract_utils.FA2.iteri_es_transfers transfers
        (fun internal_op_id source destination amount token_id ->
           Verbose.Log.fa2_transfer
             block_level contract op_hash op_id caller caller_pp internal_op_id token_id
             source destination amount;
           Tokens.FA2.(Conn.find_opt Operation_table.insert (op Operation_table.Transfer))
           >>= caqti_or_fail ~__LOC__ >>= fun _ ->
           record_token_account source ()
           >>= fun source_id ->
           record_token_account destination ()
           >>= fun destination_id ->
           Tokens.FA2.Transfer_table.
             (Conn.find_opt insert
                { opaid;
                  internal_op_id;
                  token_id;
                  source = source_id;
                  destination = destination_id;
                  amount;
                })
           >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit)
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | FA2.Update_operators updates ->
      Contract_utils.FA2.iteri_es_update_operators updates
        (fun internal_op_id kind owner operator token_id ->
           Verbose.Log.fa2_update_operators
             block_level contract op_hash op_id caller caller_pp internal_op_id token_id
             (FA2.operator_update_action_to_string kind) owner operator;
           Tokens.FA2.(
             Conn.find_opt
               Operation_table.insert (op Operation_table.Update_operators))
           >>= caqti_or_fail ~__LOC__ >>= fun _ ->
           record_token_account owner ()
           >>= fun owner_id ->
           record_token_account operator ()
           >>= fun operator_id ->
           Tokens.FA2.Update_operators_table.
             (Conn.find_opt insert
                { opaid;
                  internal_op_id;
                  token_id;
                  owner = owner_id;
                  operator = operator_id;
                  kind;
                })
           >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit)
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    | FA2.Balance_of (requests, (callback, _)) ->
      Contract_utils.FA2.iteri_es_balance_of requests callback
        (fun internal_op_id owner token_id callback ->
           Verbose.Log.fa2_balance_of
             block_level contract op_hash op_id caller caller_pp internal_op_id token_id
             owner callback;
           Tokens.FA2.(
             Conn.find_opt
               Operation_table.insert (op Operation_table.Balance_of))
           >>= caqti_or_fail ~__LOC__ >>= fun _ ->
           record_token_account owner ()
           >>= fun owner_id ->
           record_token_account callback ()
           >>= fun callback_id ->
           Tokens.FA2.Balance_of_table.
             (Conn.find_opt insert
                { opaid;
                  internal_op_id;
                  token_id;
                  owner = owner_id;
                  callback = callback_id;
                })
           >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit)
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit

  let store_token_contract ~addr_counter ~chain ~block_level bh cctxt contract conn conn2 =
(**# ifndef __META12__ OPEN_COMMENTS #**)
    match contract with
    | Contract.Implicit _ -> return_none
    | Contract.Originated contract_hash ->
(**# ifndef __META12__ CLOSE_COMMENTS #**)
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    !- "Contract_utils.Tokens.check_contract" @@
    Contract_utils.Tokens.check_contract
      cctxt ~chain ~block:(`Hash (bh, 0)) ~contract(**# ifdef __META12__ :contract_hash #**) ()
    >>= function
    | Ok kind ->
      Verbose.Log.token_contract
        contract bh block_level (Contract_utils.Tokens.(string_of_kind kind));
      record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:contract >>= fun contract_id ->
      Tokens.Contract_table.(Conn.find_opt insert {address = contract_id; block_level; kind})
      >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_some kind
    | Error _ -> return_none


  let check_or_store_token_contract
      ~addr_counter ~chain ~block_level ~contract_id ~contract bh cctxt conn conn2 =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    Conn.find_opt Tokens.Contract_table.is_token contract_id
    >>= caqti_or_fail ~__LOC__
    >>= function
    | None ->
      store_token_contract
        ~addr_counter ~chain ~block_level bh cctxt contract conn conn2
    | Some k -> return_some k

  let store_token_op ~opaid ~addr_counter
      ~block_level ~source_id ~contract_id ~cctxt ~bh
      contract source source_pp op_hash op_id entrypoint data conn conn2 =
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let scan_for_address data =
      Contract_utils.Tokens.scan_for_address data (fun address ->
          record_token_account
            ~__LINE__ ~block_level ~addr_counter
            contract contract_id conn conn2 ~k:address ()
          >>= fun _ -> return ())
    in
    check_or_store_token_contract
      ~addr_counter ~chain:cctxt#chain ~block_level ~contract_id ~contract bh cctxt conn conn2
    (* >>= warn_if_error ~__LOC__ *) >>=? function
    | Some FA12 ->
      begin
        match FA12.action_of_data entrypoint data with
        | Error _ -> scan_for_address data
        | Ok action ->
          store_fa12_action ~opaid ~addr_counter ~caller_id:source_id ~contract_id
            ~block_level contract source source_pp op_hash op_id action conn conn2
      end
    | Some FA2 ->
      begin
        match FA2.action_of_data entrypoint data with
        | Error _ -> scan_for_address data
        | Ok action ->
          store_fa2_action ~opaid ~addr_counter ~caller_id:source_id ~contract_id
            ~block_level contract source source_pp op_hash op_id action conn conn2
      end
    | None -> return ()

end
(**# ifndef __FA2__ CLOSE_COMMENTS #**)

(**# ifdef __TRANSITION__ OPEN_COMMENTS #**)
let get_balance_from_cctxt cctxt bh k =
  !- "Alpha_services.Contract.balance" @@
  Alpha_services.Contract.balance (* CCTX *)
    cctxt ((`Main, `Hash ((bh:Block_hash.t), 0)):Block_services.chain * Block_services.block) k

let record_contract_balance conn cctxt ~block_level:level ~bh ~k ~kid ~db_max_level =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  get_balance_from_cctxt cctxt bh k >>= function
  | Ok (balance) ->
    Verbose.Log.contract_update_balance ~bh ~level ~k ~balance ();
    (if Int32.add level 10l < db_max_level then
       Conn.find_opt Contract_balance_table.update (kid, level, balance, None (* placeholder for script *))
     else
       Conn.find_opt Contract_balance_table.update_via_bh (kid, bh, balance, None (* placeholder for script *))
    )
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Connection_failed _; meth=_})::_) as e) ->
    Verbose.Debug.eprintf ~vl:5 "Error at %s" __LOC__;
    Verbose.error "Connection to node failed, so I'll exit. Details: %a"
      pp_print_trace e;
    Misc.unless_auto_recovery Stdlib.exit 1
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Unexpected_content _; meth=_})::_) as em) as e ->
    Verbose.Debug.eprintf ~vl:4
      "Warning: Couldn't get balance of %a (contract_id=%Ld) from context of block=%a, I might be using the wrong protocol, I'll try another one."
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:5 "Warning: Error @@ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:5 "Warning: Error was: %a" pp_print_trace em;
    Lwt.return e
  | Error ([Tezos_rpc.Context.Not_found _] as em) ->
    Verbose.Debug.eprintf ~vl:4
      "Warning: Couldn't get balance of %a (contract_id=%Ld) from context of block=%a, contract probably not actually originated. Will assign balance of 0."
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:5 "Warning: Error @@ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:5 "Warning: Error was: %a" pp_print_trace em;
    let balance : Tez.t = Alpha_context.Tez.zero in
    Verbose.Log.contract_update_balance ~bh ~level ~k ~balance ();
    (if Int32.add level 10l < db_max_level then
       Conn.find_opt Contract_balance_table.update (kid, level, balance, None (* placeholder for script *))
     else
       Conn.find_opt Contract_balance_table.update_via_bh (kid, bh, balance, None (* placeholder for script *))
    )
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | Error em as e ->
    Verbose.Debug.eprintf ~vl:5 "Warning: Error @@ %s" __LOC__;
    Verbose.Debug.eprintf ~vl:4
      "Warning: Couldn't get balance of %a (contract_id=%Ld) from context of block=%a"
      Contract.pp k kid Block_hash.pp bh;
    Verbose.Debug.eprintf ~vl:5 "Warning: Error was: %a" pp_print_trace em;
    Lwt.return e
(**# ifdef __TRANSITION__ CLOSE_COMMENTS #**)



let resume_from_block_hash = ref None
let second_chances = ref 256

(**# ifdef __META8__
let process_implicit_operations_results =
ref (fun ~conn:_
         ~conn2:_
         ~bh:_
         ~level:_
         ~addr_counter:_
         ~bigmap_counter:_
         _ -> assert false)
#**)


let normalize_gas x = (* __NEW_PROTO__ *)
  (**# ifndef __MILLIGAS__ Fpgas.t_of_z #**)
  (**# ifdef __PROTO8__ Fpgas.p8_to_p7 #**)
  (**# ifdef __PROTO9__ Fpgas.p9_to_p7 #**)
  (**# ifdef __PROTO10__ Fpgas.p10_to_p7 #**)
  (**# ifdef __PROTO11__ Fpgas.p11_to_p7 #**)
  (**# ifdef __PROTO12__ Fpgas.p12_to_p7 #**)
  (**# ifdef __PROTO13__ Fpgas.p13_to_p7 #**)
  (**# ifdef __PROTO14__ Fpgas.p14_to_p7 #**)
  (**# ifdef __PROTO15__ Fpgas.p15_to_p7 #**)
  (**# ifdef __PROTO16__ Fpgas.p16_to_p7 #**)
  (**# ifdef __PROTO17__ Fpgas.p17_to_p7 #**)
  (**# ifdef __PROTO18__ Fpgas.p18_to_p7 #**)
  (**# ifdef __PROTOA__ Fpgas.alpha_to_p7 #**)
  x

let store_block =
  let open Apply_results in
  fun
    conn conn2 bh
    ({ shell ; protocol_data = _ } : BS.raw_block_header)
    ({ protocol_data =
         { baker (**# ifdef __META13__ = { delegate ; consensus_pkh = baker } #**);
           (**# ifdef __META16__ adaptive_issuance_vote_ema = _ ; adaptive_issuance_launch_cycle = _ ; #**)
           (**# ifdef __META8__
              implicit_operations_results ;
              level_info = {
                level ;
                level_position ;
                cycle ;
                cycle_position ;
                expected_commitment = _ ; (* TODO/FIXME *)
              };
              #**)
           (**# ifdef __META11__ liquidity_baking_toggle_ema = _ ; (* TODO/FIXME *) #**)
           (**# elseifdef __META8__
              liquidity_baking_escape_ema = _ ; (* TODO/FIXME *)
              #**)
           (**# elseifdef __META6__ level_info = _ ; (* TODO/FIXME *) #**)
           (**#X ifndef __META8__
             level = ({ level_position;
                      cycle; cycle_position;
                      voting_period (**# ifdef __META6__ = _ #**);
                      voting_period_position;
                      level;
                      expected_commitment = _ ; (* TODO/FIXME *)
                    } (* : Alpha_context.Level.t *));
           voting_period_kind;
             X#**)
           (**# ifdef __META6__ voting_period_info; #**)
           consumed_gas ;
           deactivated ;
           balance_updates ;
           nonce_hash = _ ; (* TODO/FIXME *)
           (**# ifdef __META10__ proposer = _ ; (* TODO/FIXME *) #**)
           (**#Y ifdef __META12__ (**#X ifndef __META14__ dal_slot_availability = _ ; (* TODO/FIXME *) X#**) Y#**)
           (**# ifdef __META14__ dal_attestation = _ (* TODO/FIXME *) #**)
};
       test_chain_status = _; (* TODO/FIXME *)
       max_operations_ttl = _; (* TODO/FIXME *)
       max_operation_data_length = _; (* TODO/FIXME *)
       max_block_header_length = _; (* TODO/FIXME *)
       operation_list_quota = _; (* TODO/FIXME *)
     } : BS.block_metadata)
    (**# ifdef __META8__ ~bigmap_counter #**)
    ->
    (**# ifdef __META8__
       let voting_period_position = voting_period_info.position in
       let voting_period_kind = voting_period_info.Voting_period.voting_period.kind in
       #**)
    let consumed_gas = normalize_gas consumed_gas in
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    let module Conn2 = (val conn : Caqti_lwt.CONNECTION) in
    let rare_addr_counter = fun () -> Int64.add Misc.min_int53 (Int64.of_int32 shell.level) in
    record_pkh ~__LINE__
      (* this address will be recorded ****here**** only when indexing on multicore mode (by segments) and
         the baker's address hasn't been recorded yet. It can actually happen. *)
      ~addr_counter:rare_addr_counter
      ~block_level:shell.level conn conn2 ~pkh:baker >>= fun baker_id ->
(**# ifndef __META13__ OPEN_COMMENTS #**)
    record_pkh ~__LINE__
      ~addr_counter:rare_addr_counter
      ~block_level:shell.level conn conn2 ~pkh:delegate >>= fun delegate_id ->
(**# ifndef __META13__ CLOSE_COMMENTS #**)
    (* Store shell header *)
    begin (* logged *)
      Verbose.Log.shell bh shell;
      Conn.find Block_table.insert (bh, shell)
    end >>= begin function
      | Ok (Some block_level) when block_level < 0l ->
        resume_from_block_hash := None;
        Verbose.warn ~force:true "Reorg happened (block_level=%ld, level=%ld)." block_level shell.level;
        Lwt.fail (Misc.Reorg shell.level)
      | Ok block_level ->
        resume_from_block_hash := None;
        Lwt.return block_level
      | Error (`Request_failed _ as ce) as e ->
        begin match !resume_from_block_hash with
          | Some p when p <> shell.predecessor -> (* This test makes sense only when the query fails because of the possiblity to reindex an already indexed block. *)
            (* FIXME: Instead of this, we need to pass the information to the caller in order to change the call parameters. *)
            (* resume_from_block_hash := None; *)
            Verbose.warn
              ~force:true
              "💥 Failed to insert a block because I cannot find its predecessor in the database!";
            Verbose.Debug.warn "Error is: %a" Caqti_error.pp ce;
            Verbose.warn
              ~force:true
              "You might be resuming indexing after being offline. I'll delete the highest previously indexed block and try again.";
            Lwt.fail (Misc.Reorg shell.level)
          | Some _ ->
            if LatestBlockHashes.block_seen_recently (Int32.pred shell.level) shell.predecessor then begin
              Verbose.warn
                ~force:true
                "💥 Failed to insert a block because I cannot find its predecessor in the database! However it was indexed recently!";
              Verbose.warn
                ~force:true
                "I'll delete the highest previously indexed block and try again.";
              Lwt.fail (Misc.Reorg shell.level)
            end else
              caqti_or_fail ~__LOC__ e
          | None ->
            if not (LatestBlockHashes.block_seen_recently (Int32.pred shell.level) shell.predecessor) && !second_chances <= 0 then
              caqti_or_fail ~__LOC__ e
            else
              begin
                if LatestBlockHashes.block_seen_recently (Int32.pred shell.level) shell.predecessor then decr second_chances;
                Verbose.warn
                  ~force:true
                  "💥 Failed to insert a block because I cannot find its predecessor in the database!";
                if LatestBlockHashes.block_seen_recently (Int32.pred shell.level) shell.predecessor then
                  Verbose.warn
                    ~force:true
                    "However it was seen recently, so I'll try again, hoping I won't get stuck in an infinite loop."
                else
                  Verbose.warn
                    ~force:true
                    "In case it's because you are resuming indexing after being offline, I'll delete the highest previously indexed block \
                     and try again, but I won't try indefinitely. Attempts credit left: %d. \
                     If credit goes to 0, and you think you need more, just restart me. Otherwise, fix the mentionned issue with the database."
                    !second_chances;
                Lwt.fail (Misc.Reorg shell.level)
              end
        end
      | Error _ as e ->
        caqti_or_fail ~__LOC__ e
    end >>= function
    | None ->
      Verbose.error "Insertion of block %a %ld failed!"
        Block_hash.pp bh
        (Raw_level.to_int32 level);
      Misc.unless_auto_recovery Stdlib.exit 1
    | Some (block_level:int32) ->
      LatestBlockHashes.record_hash block_level bh;
      let addr_counter =
        let addr_counter_prefix =
          (* this limits the number of operations per block at about 99M *)
          (* this also makes removing *all* reorg-involved blocks mandatory *)
          Int64.mul (Int64.of_int32 block_level) (10_000_000L) in
        Misc.make_counter64 ~start:addr_counter_prefix ()
      in
      (* Discover deactivated pkhs and fill up deactivated table *)
      begin (* self logged / logged *)
        Lwt_list.iter_s (fun pkh ->
            record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh >>= fun kid ->
            Verbose.Log.deactivated_delegate_table ~block_level:shell.level bh pkh;
            Conn.find_opt Deactivated_delegate_table.insert (kid, block_level) >>= caqti_or_fail ~__LOC__ >>= fun _ ->
            Lwt.return_unit
          ) deactivated

      end >>= fun () ->
      (* Store alpha header *)
      begin (* logged *)
        Verbose.Log.store_alpha_header bh shell.level baker level_position
          (Cycle.to_int32 cycle) cycle_position
          (**# ifdef __META6__
             (fun out vp -> Format.fprintf out "{%s}" @@ Format.asprintf "%a" Voting_period.pp vp)
             voting_period_info.voting_period #**)(**# else Voting_period.pp voting_period #**)
          voting_period_position
          (Verbose.Log.string_of_voting_period_kind voting_period_kind)
          consumed_gas; (* <--- FIXME *)
        (* Conn2.exec Db_base.Indexer_log.record_action ("about to store block alpha... " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
        Verbose.Log.printf ~vl:1 "store block alpha";
        Conn.find_opt Block_alpha_table.insert @@
        (**# ifdef __META13__ mtup10 #**)(**# else mtup9 #**)
          block_level
          baker_id
          level_position
          cycle
          cycle_position
          (**# ifdef __META6__ voting_period_info.#**)voting_period
          voting_period_position
          voting_period_kind
          consumed_gas
          (**# ifdef __META13__ delegate_id #**)
      end >>= caqti_or_fail ~__LOC__ >>=
      function
      | None ->
        Verbose.error "Error: DB failure when recording block %ld %a" shell.level Block_hash.pp bh;
        Stdlib.exit 1
      | Some l ->
        assert (l=block_level);
(**# ifndef __META8__ OPEN_COMMENTS #**)
        Verbose.Log.printf "process_implicit_operations_results";
        !process_implicit_operations_results ~conn ~conn2 ~bh ~level:shell.level ~addr_counter ~bigmap_counter implicit_operations_results >>= fun () ->
(**# ifndef __META8__ CLOSE_COMMENTS #**)
        (* Update balances tables *)
        begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update shell.level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
          Balance_table.update ~get_kid conn ~block_level balance_updates
        end >>= fun () ->
        return (block_level, addr_counter, baker_id)

open (**#X ifdef __TRANSITION__ Tezos_indexer_(**# get PROTO#**). X#**)Proto_misc

(* let list_of_option_list =
 *   List.fold_left (fun r -> function
 *       | None -> r
 *       | Some e -> e :: r)
 *     [] *)

let rec list_iter_es f = function
  | [] -> return_unit
  | e :: tl ->
    f e >>= function
    | Error e -> Lwt.return (Error e)
    | Ok () -> list_iter_es f tl

module type Manager_operation =
sig
  val process :
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    opaid:int64 ->
    (**# ifndef __PROTO1__ bigmap_counter:(int32 -> int64) -> #**)
    addr_counter:(unit -> int64) ->
    (**# ifdef __FA2__ tokens:bool -> cctxt:#rpc_context -> #**)
    block_level:block_level ->
    ?nonce:int ->
    internal:int ->
    source_id:kid ->
    (**# ifndef __PROTO1__ ophid:int64 -> #**)
    Block_hash.t ->
    Operation_hash.t ->
    int ->
    (**# ifdef __META14__ Destination.t -> #**)(**# else Contract.t -> #**)
    Tez.tez option ->
    'a manager_operation ->
    'b Apply_results.manager_operation_result ->
    unit tzresult Lwt.t

(**# ifndef __META12__ OPEN_COMMENTS #**)
  val process_internal :
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    opaid:int64 ->
    bigmap_counter:(int32 -> int64) ->
    addr_counter:(unit -> int64) ->
    tokens:bool ->
    cctxt:#rpc_context ->
    block_level:block_level ->
    nonce:int ->
    internal:int ->
    source_id:kid ->
    ophid:int64 ->
    Block_hash.t ->
    Operation_hash.t ->
    int ->
    (**# ifdef __META14__ Destination.t #**)(**# else Contract.t #**) ->
    'a internal_operation_contents ->
    'b internal_operation_result ->
    unit tzresult Lwt.t
(**# ifndef __META12__ CLOSE_COMMENTS #**)

(**# ifndef __META8__ OPEN_COMMENTS #**)
  (* This is exported outside of the module only for ≥ __META8__.
     Before, it's used inside the module only. *)
  val process_bigmap_diffs :
    ?script:Script.t ->
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    bigmap_counter:(int32 -> int64) ->
    block_level:block_level ->
    bh:Block_hash.t ->
    opaid:opaid option ->
    iorid:int option ->
    sender:'a option ->
    sender_pp:(Format.formatter -> 'a -> unit) ->
    contract:Contract.t option ->
    senderid:kid option ->
    contract_id:kid option ->
    diffs:Lazy_storage.diffs option ->
    ophid:int64 option ->
    unit Lwt.t
(**# ifndef __META8__ CLOSE_COMMENTS #**)

end

let error_list_to_json (l: _ option) =
  let open Tezos_protocol_environment_(**# get PROTO #**) in
  let enc =
    Data_encoding.list
    (**# ifdef __META7__ error_encoding #**)
    (**# else Environment.Error_monad.error_encoding #**) in
  Std.Option.map
    (fun e ->
       Data_encoding.Json.(
         Format.asprintf "%a" pp
           (construct enc
              ((**#X ifdef __META7__ Environment.wrap_tztrace X#**) e))
       )
    )
    l



module Manager_operation : Manager_operation = struct

  let process_tx
      conn conn2
      ~originated_contracts
      ~opaid
      ~addr_counter
      ~block_level ~source_id ~destination_id
      ~bh ~source ~op_hash ~op_id ~fee
      ~source_pp
      ~internal(**# ifdef __META16__ :_ #**) ~nonce
      (**# ifdef __META3__ ~entrypoint #**)
      (**# elseifdef __META6__ ~entrypoint #**)
      ~consumed_gas ~storage_size ~paid_storage_size_diff
      ~amount ~destination ~balance_updates
      ~parameters
      ~storage
      ~status
      (**# ifdef __META11__ ~ticket_hash #**)
      (**# ifdef __META13__ ~ticket_receipt #**)
      ~error_list
    =
    let _ : Tez.t option = fee in
    (* begin
     *   Option.iter (
     *     Option.iter (fun parameters ->
     *         Verbose.Debug.printf ~vl:bigmap_debug "TX parameters:%a"
     *           Verbose.Debug.Script.pp_strings_and_bytes_canonical parameters
     *       ))
     *     (Std.Option.map Data_encoding.force_decode parameters)
     * end;
     * begin
     *   Option.iter (
     *     Option.iter (fun storage ->
     *         Verbose.Debug.printf ~vl:bigmap_debug "TX storage:%a"
     *           Verbose.Debug.Script.pp_strings_and_bytes_canonical storage
     *       ))
     *     (Std.Option.map Data_encoding.force_decode storage)
     * end; *)

    let strings =
      if !Tezos_indexer_lib.Config.no_smart_contract_extraction then
        None
      else
        match Misc.flatten_option @@ Std.Option.map extract_strings_and_bytes_from_lazy_expr parameters
            , Misc.flatten_option @@ Std.Option.map extract_strings_and_bytes_from_lazy_expr storage
        with
        | None, None -> None
        | Some l, Some l2 ->
          let module S = Set.Make(String) in
          let s =
            List.fold_left (fun r e -> S.add e r)
              (List.fold_left (fun r e -> S.add e r) S.empty l)
              l2
          in
          Some (S.elements s)
        | Some l, None
        | None, Some l -> Some l
    in

    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    begin (* logged *)
      (**# ifdef __META1__ let entrypoint = None in #**)(**# else let entrypoint = Some entrypoint in #**)
      Verbose.Log.store_tx block_level bh op_hash op_id
        ~source ~source_pp ~destination fee amount parameters
        storage consumed_gas storage_size paid_storage_size_diff entrypoint;
      Conn.find_opt Tx_table.insert
        Tx_table.{ opaid ; source = source_id ; destination = destination_id
                 ; fee ; amount ; parameters ; storage
                 ; consumed_gas ; storage_size ; paid_storage_size_diff
                 ; entrypoint
                 ; nonce
                 ; status
                 ; error_list
                 ; strings
                 ; originated_contracts
                   (**# ifdef __META11__ ; ticket_hash #**)
                   (**# ifdef __META13__ ; ticket_receipt #**)
                 }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* Update balances *)
    begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
      let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
      Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
    end >>= fun () ->
    return_unit

  let string_of_string_list l = List.fold_left (fun r e -> r ^ "; " ^ e) "" l

  let bigmap_annots ~big_map ~script ~value_type ~i : string list option =
    match script with
    | None -> None
    | Some script ->
      let open Tezos_micheline.Micheline in
      let open Michelson_v1_primitives in
      let code = Std.Option.map root @@ Data_encoding.force_decode script.Script.code in
      match code with
      | None ->
        None
      | Some code ->
        if false then Verbose.Debug.printf ~vl:bigmap_debug "bigmap=%a" Z.pp_print big_map (* Data_encoding.Json.pp (Data_encoding.Json.construct Script.lazy_expr_encoding script.Script.code) *);
        let rec get_storage_type = function
          | Prim (_loc', K_storage, storage_type, _annot) ->
            if false then Verbose.Debug.printf ~vl:bigmap_debug "get_storage_type: %S" (string_of_string_list _annot);
            Some storage_type
          | Seq (_loc, (Prim (_loc', K_storage, storage_type, _annot)) :: _tail) ->
            assert (get_storage_type (Seq (_loc, _tail)) = None);
            if false then Verbose.Debug.printf ~vl:bigmap_debug "get_storage_type: %S" (string_of_string_list _annot);
            Some storage_type
          | Seq (loc, _::e) -> get_storage_type (Seq (loc, e))
          | Seq (_, [])
          | String _
          | Bytes _
          | Prim _
          | Int _ -> None
        in
        (* * [i] is meant to match [counter]
           * for some reason, we need to count in the more difficult direction
        *)
        let rec get_annots ~rvt st counter =
          (**# ifdef __META7__
                  if false then Verbose.Debug.printf ~vl:bigmap_debug "bigmap %a get_annots st=%a"
                    Z.pp_print big_map
                    Verbose.Debug.Script.print_node_list st;
                     #**)
          match st with
          | [] -> None, counter
          | Prim (_, T_big_map, bm_type, bm_annots) :: e ->
            begin
              let _initial_counter = counter in
              match get_annots ~rvt e counter with
              | (Some _ as r), counter ->
                r, counter
              | None, counter ->
                match
                  List.find_opt
                    (fun b -> 0 = compare_script b rvt)
                    (match bm_type with _::tl -> tl | _ -> [])
                with
                | Some _some_node ->
                  (**# ifdef __META7__
                       if false then Verbose.Debug.printf ~vl:bigmap_debug "FOUND CANDIDATE bigmap %a alloc index %d; big map type index counter %d; FOUND %s -- %a initial_counter=%d" Z.pp_print big_map i counter (string_of_string_list bm_annots)
                       Verbose.Debug.Script.print_node_list [_some_node] _initial_counter;
                       #**)
                  if counter = i then
                    Some bm_annots, counter
                  else
                    None, counter + 1
                | None ->
                  (**# ifdef __META7__
                       if false then Verbose.Debug.printf ~vl:bigmap_debug "FAILED TO FIND bigmap %a alloc index %d; big map type index counter %d; NOT FOUND IN %s" Z.pp_print big_map i counter (string_of_string_list bm_annots);
                       #**)
                  None, counter + 1
            end
          | Int _ :: e
          | String _ :: e
          | Bytes _ :: e -> get_annots ~rvt e (counter)
          | Prim (_, _, node_list, _) :: e
          | Seq (_, node_list) :: e ->
            match get_annots ~rvt e (counter) with
            | (Some _r as r), counter ->
              (**# ifdef __META7__
                   if false then Verbose.Debug.printf ~vl:bigmap_debug "FOUND get_annots: _r=%s Counter=%d"
                   (string_of_string_list _r) counter
                   ;
                     #**)
              r, counter
            | None, counter ->
              match get_annots ~rvt node_list (counter) with
              | (Some _r' as r'), counter ->
                (**# ifdef __META7__
                   if false then Verbose.Debug.printf ~vl:bigmap_debug "get_annots: _r'=%s Counter=%d"
                   (string_of_string_list _r')
                   counter
                   ;
                     #**)
                r', (counter)
              | None, counter -> None, counter
        in
        match get_storage_type code with
        | None -> None
        | Some storage_type ->
          (**# ifdef __META7__
             if false then Verbose.Debug.printf ~vl:bigmap_debug "get_storage_type from code=%a -> %a" Verbose.Debug.Script.print_node_list [code] Verbose.Debug.Script.print_node_list storage_type;
             #**)
          fst @@ get_annots ~rvt:(root value_type) storage_type 0


  let _ = bigmap_annots

(**# ifdef __PROTO1__ OPEN_COMMENTS #**)
  let process_bigmap_diffs :
    (**# ifndef __META1__ ?script:_ -> #**)
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    bigmap_counter:_ ->
    block_level:block_level ->
    bh:Block_hash.t ->
    opaid:opaid option ->
    iorid:int option ->
    sender:_ ->
    sender_pp:_ ->
    contract:Contract.t option ->
    senderid:kid option ->
    contract_id:kid option ->
    diffs:(**# ifdef __BIG_MAP_DIFF__ (Contract.big_map_diff option) #**) (**# elseifdef __STORAGE_DIFF__ (Lazy_storage.diffs option) #**)(**#X else unit X#**) ->
    ophid:int64 option ->
    unit Lwt.t =
    fun (**# ifndef __META1__ ?script #**) conn _conn2 ~bigmap_counter ~block_level ~bh ~opaid ~iorid ~sender ~sender_pp ~contract ~senderid ~contract_id ~diffs ~ophid ->
    if !Tezos_indexer_lib.Config.no_big_maps then
      Lwt.return_unit
    else
        let () =
          if Verbose.debug.contents then
            Option.iter (Verbose.Debug.printf ~vl:bigmap_debug "big map contract: %a" Contract.pp) contract
        in
        let module Conn = (val conn : Caqti_lwt.CONNECTION) in
        let _ = bh, sender, sender_pp, contract in (* FIXME: log me! *)
        (**# ifndef __META2__ OPEN_COMMENTS #**)
        begin match diffs with
          | None -> Lwt.return_unit
          | Some l ->
            Lwt_list.iter_s (fun Contract.{ diff_key; diff_key_hash; diff_value } ->
                Conn.find_opt
                  Bigmap.old_insert
                  (mtup12 diff_key diff_key_hash diff_value block_level
                     senderid contract_id (bigmap_counter block_level) opaid
                     iorid
                     (if !Tezos_indexer_lib.Config.no_smart_contract_extraction then None
                      else Misc.flatten_option (Std.Option.map extract_strings_and_bytes_from_expr diff_value))
                     Tezos_indexer_lib.Config.extracted_address_length_limit.contents
                     ophid)
                >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
                Lwt.return_unit
              ) l
        end
        (**# ifndef __META2__ CLOSE_COMMENTS #**)
        (**# ifdef __META1__ OPEN_COMMENTS #**)
        begin match diffs with
          | None -> Lwt.return_unit
          | Some l ->
            (**# ifdef __META6__ let module Contract_diff = Contract.Legacy_big_map_diff in
               let l = (Contract_diff.of_lazy_storage_diff l :> Contract_diff.item list) in #**)
            let i = ref (-1) in
            Lwt_list.iter_s (fun e ->
                match e with
                | Contract(**# ifdef __META6__ .Legacy_big_map_diff #**).Update { big_map ; diff_key; diff_key_hash; diff_value } ->
                  Conn.find_opt
                    Bigmap.update
                    (mtup13 big_map diff_key diff_key_hash diff_value
                       block_level senderid contract_id (bigmap_counter block_level)
                       opaid
                       iorid
                       (if !Tezos_indexer_lib.Config.no_smart_contract_extraction then None
                        else Misc.flatten_option (Std.Option.map extract_strings_and_bytes_from_expr diff_value))
                       Tezos_indexer_lib.Config.extracted_address_length_limit.contents
                       ophid)
                  >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
                  Lwt.return_unit
                | Clear big_map ->
                  Conn.find_opt
                    Bigmap.clear
                    (mtup8 big_map block_level senderid contract_id (bigmap_counter block_level) opaid iorid ophid)
                  >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
                  Lwt.return_unit
                | Copy (**# ifdef __PROTO7__ ({ src = b1 ; dst = b2 ; }) #**)(**# elseifdef __META6__ ({ src = b1 ; dst = b2 ; }) #**)(**# else (b1, b2) #**)->
                  let counter = bigmap_counter block_level in
                  Verbose.Debug.printf ~vl:bigmap_debug "Bigmap copy from %s to %s - ophid=%Ld opaid=%Ld counter=%Ld"
                    (Z.to_string b1) (Z.to_string b2) (match ophid with None -> 0L | Some o -> o) (match opaid with None -> 0L | Some o -> o) counter;
                  Conn.find_opt
                    Bigmap.copy
                    (mtup9 b1 b2 block_level senderid contract_id counter opaid iorid ophid)
                  >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
                  Lwt.return_unit
                | Alloc { big_map ; key_type ; value_type } ->
                  incr i;
                  let annots =
                    begin match bigmap_annots ~big_map ~i:i.contents ~script ~value_type with
                      | None | Some [] ->
                        Verbose.Debug.printf ~vl:bigmap_debug "big map annot not found for %a"
                          Verbose.Debug.Script.pp_canonical value_type;
                        None
                      | Some (e::[]) ->
                        Verbose.Debug.printf ~vl:bigmap_debug "bigmap_annot: %s\n%!" e;
                        Some e
                      | Some (_::_) ->
                        Verbose.error "Error: multiple big map annots found. Please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
                        Stdlib.exit 1
                    end
                  in
                  Conn.find_opt
                    Bigmap.alloc
                    (mtup11 big_map key_type value_type block_level
                       senderid contract_id (bigmap_counter block_level) opaid
                       iorid annots ophid)
                  >>= Db_base.caqti_or_fail ~__LOC__ >>= fun _ ->
                  Lwt.return_unit
              ) l
        end
(**# ifdef __META1__ CLOSE_COMMENTS #**)
(**# ifdef __PROTO1__ CLOSE_COMMENTS #**)

  let status_applied = 0

  let int_of_status : _ (**# ifdef __META12__ Apply_operation_result.operation_result #**)(**# else Apply_results.manager_operation_result #**) -> int = function
    | Applied _ -> status_applied
    | Backtracked _ -> 1
    | Failed _ -> 2
    | Skipped _ -> 3

  let process_origination
      conn conn2
      ~opaid
      (**# ifndef __PROTO1__ ~bigmap_counter #**)
      ~addr_counter
      ~block_level ~preorigination_id ~source_id
      ~bh ~source ~op_hash ~op_id ~fee
      (**# ifdef __FA2__ ~tokens ~cctxt #**)
      ~source_pp
      ~internal(**# ifdef __META16__ :_ #**) ~nonce
      ~delegate:_ ~delegate_id (**# ifdef __META1__ ~manager ~manager_id ~spendable ~delegatable #**)
      (**# ifndef __PROTO1__ ~ophid #**)
      credit preorigination script balance_updates originated_contracts
      (**# ifdef __META3__ ~big_map_diff #**)
      (**# elseifdef __META6__ ~lazy_storage_diff #**)
      ~consumed_gas ~storage_size ~paid_storage_size_diff
      ~status
      ~error_list
    =
    begin
      Verbose.Debug.printf ~vl:bigmap_debug "Origination script:%a"
        Verbose.Debug.Script.pp_strings_and_bytes_script script
    end;
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    (**# ifdef __META1__ let manager_id = Some manager_id in  #**)
    (**# ifdef __META3__ let spendable = None and delegatable = None and manager = None and manager_id = None in  #**)
    (**# elseifdef __META6__ let spendable = None and delegatable = None and manager = None and manager_id = None in  #**)
    (**# else let spendable = Some spendable and delegatable = Some delegatable in  #**)
    let originated_contract = match originated_contracts with
      | [hd] -> Some hd
      | [] -> None
      | _ ->
        Verbose.error "Error: multiple originated contracts at once. Please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
        Stdlib.exit 1
    in
    (* Insert full contract row *)
    begin match originated_contract with
      | None ->
        Lwt.return_none
      | Some originated_contract ->
        record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:originated_contract
        >>= fun okid ->
        Lwt.return_some okid
    end >>= fun okid ->
    (* Insert in origination table *)
    ignore (delegatable, spendable, manager, preorigination); (* FIXME *)
    begin (* logged *)
      Verbose.Log.insert_origination block_level bh op_hash op_id source source_pp originated_contract fee;
      Conn.find_opt Origination_table.insert {
        opaid ;
        src = source_id ;
        k = okid ;
        fee ;
        consumed_gas ; storage_size ; paid_storage_size_diff ;
        nonce ;
        preorigination_id ;
        script ;
        delegate_id;
        credit ;
        manager_id ;
        block_level ;
        status ;
        error_list ;
        strings =
          if !Tezos_indexer_lib.Config.no_smart_contract_extraction then None
          else Misc.flatten_option (Std.Option.map extract_strings_and_bytes script) ;
      }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* Update balances *)
    begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
      let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
      Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
    end >>= fun () ->
    Verbose.Log.store_origination block_level bh source source_pp;
    (**# ifndef __FA2__ OPEN_COMMENTS #**)
    begin (* self logged *)
      match tokens && status = status_applied, originated_contract, okid with
      | (false, _, _) | (_, None, _) | (_, _, None) ->
        return ()
      | true, Some originated_contract, Some contract_id ->
        Tokens.check_or_store_token_contract
          ~addr_counter
          ~chain:cctxt#chain
          ~block_level ~contract_id ~contract:originated_contract bh cctxt conn conn2
        >>= fun _ -> return_unit
    end >>= caqti_or_fail ~__LOC__ >>= fun () ->
    (**# ifndef __FA2__ CLOSE_COMMENTS #**)
    (**# ifdef __PROTO1__ OPEN_COMMENTS #**)
    begin
      let diffs =
        (**# ifdef __META3__ big_map_diff #**)(**# elseifdef __META6__ lazy_storage_diff #**)(**# elseifdef __BIG_MAP_DIFF__ None #**)(**# elseifdef __STORAGE_DIFF__ None #**)(**#X else () X#**)
      in
      match originated_contract, okid with
      | _, None | None, _ -> Lwt.return_unit
      | Some originated_contract, Some okid  ->
        if status = status_applied then
          process_bigmap_diffs
            (**# ifndef __META1__ ?script #**)
            conn conn2
            (**# ifndef __PROTO1__ ~bigmap_counter #**)
            ~block_level
            ~bh
            ~opaid:(Some opaid)
            ~iorid:None
            ~sender:(Some source)
            ~sender_pp:source_pp
            ~contract:(Some originated_contract)
            ~senderid:(Some source_id)
            ~contract_id:(Some okid)
            ~diffs
            ~ophid:(Some ophid)
        else
          Lwt.return_unit
    end >>= fun () ->
    (**# ifdef __PROTO1__ CLOSE_COMMENTS #**)
    return_unit

  let process :
    type a b.
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    opaid:_ ->
    (**# ifndef __PROTO1__ bigmap_counter:_ -> #**)
    addr_counter:_ ->
    (**# ifdef __FA2__ tokens:bool -> cctxt:#rpc_context -> #**)
    block_level:block_level ->
    ?nonce:_ -> (* for internal operations *)
    internal:int ->
    source_id:kid ->
    (**# ifndef __PROTO1__ ophid:int64 -> #**)
    Block_hash.t -> Operation_hash.t -> int ->
    (**# ifdef __META14__ Destination.t -> #**)(**# else Contract.t -> #**)
    Tez.t option -> (* [fee] is optional because internal operations do not have fees and this function is also for non-internal operations as well as for internal operations (although for more recent protocols, internal operations are processed by [process_internal]) *)
    a manager_operation ->
    b Apply_results.manager_operation_result -> unit tzresult Lwt.t =
    fun conn conn2
      ~opaid
      (**# ifndef __PROTO1__ ~bigmap_counter #**)
      ~addr_counter
      (**# ifdef __FA2__ ~tokens ~cctxt #**)
      ~block_level ?nonce ~internal ~source_id
      (**# ifndef __PROTO1__ ~ophid #**)
      bh op_hash op_id source fee operation operation_result ->
      (**# ifdef __META14__ let source_pp = Destination.pp in #**)
      (**# else
         let source_pp = Contract.pp in
         Verbose.Log.process_mgr_operation block_level bh op_hash op_id source fee; #**)
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      match operation with
(**# ifndef __PROTO14__ OPEN_COMMENTS #**)(* This is for proto 14 ONLY *)
      | Sc_rollup_execute_outbox_message { rollup = _ (* : Sc_rollup.t *); cemented_commitment = _ (* : Sc_rollup.Commitment.Hash.t *);
                                           outbox_level = _ (* : Raw_level.t*); message_index = _ (* : int*); inclusion_proof = _ (* : string*); message = _ (* : string*);
                                         } (* NOT activated in Proto 14 *) -> assert false
(**# ifndef __PROTO14__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
      | Zk_rollup_update {
          zk_rollup = _ (* : Zk_rollup_repr.t *);
          update = _ (* : Zk_rollup_update_repr.t; *) } -> assert false (* TODO/FIXME ZK *)
      | Dal_publish_slot_header {
          (**# ifdef __META15__ OPEN_COMMENTS #**)
          header (* Dal.Slot.Header.t *) =
            { id = { published_level (* : Raw_level.t *)
                   ; index (* : Dal.Slot_index.t *) }
            ; commitment (* Dal.Slot.Commitment.t *) }
        ; proof (* Dal.Slot.Commitment_proof.t *)
          (**# ifdef __META15__ CLOSE_COMMENTS #**)
          (**# ifndef __META15__ OPEN_COMMENTS #**)
          (**# ifdef __META16__ OPEN_COMMENTS #**)
          published_level ;
          (**# ifdef __META16__ CLOSE_COMMENTS #**)
        slot_index = index
        ; commitment
        ; commitment_proof = proof
          (**# ifndef __META15__ CLOSE_COMMENTS #**)
        } ->
         (**# ifdef __META16__ OPEN_COMMENTS #**)
         let published_level = Some (Raw_level.to_int32 published_level) in
         (**# ifdef __META16__ CLOSE_COMMENTS #**)
           (**# ifndef __META16__ OPEN_COMMENTS #**)
         let published_level = None in
         (**# ifndef __META16__ CLOSE_COMMENTS #**)
        let status, error_list, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Dal_publish_slot_header_result { consumed_gas (**# ifdef __META15__ ; slot_header = _FIXME_TODO_FEATURE_FLAG #**) }) ->
            int_of_status operation_result, None, Some consumed_gas
          | Backtracked (Apply_results.Dal_publish_slot_header_result { consumed_gas (**# ifdef __META15__ ; slot_header = _FIXME_TODO_FEATURE_FLAG #**) }, error_list_option) ->
            int_of_status operation_result, error_list_option, Some consumed_gas
          | Failed (Dal_publish_slot_header_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, None
          | Skipped Dal_publish_slot_header_manager_kind ->
            int_of_status operation_result, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        let consumed_gas = Option.map normalize_gas consumed_gas in
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Dal_publish_slot_header_table.insert
            (mtup10 opaid source_id fee
               status consumed_gas published_level (error_list_to_json error_list)
               index commitment proof
            )
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META13__ OPEN_COMMENTS #**)
      | Update_consensus_key pk ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (Apply_results.Update_consensus_key_result { consumed_gas }) ->
            int_of_status operation_result, Some consumed_gas, None
          | Backtracked (Apply_results.Update_consensus_key_result { consumed_gas }, error_list_option) ->
            int_of_status operation_result, Some consumed_gas, error_list_option
          | Failed (Update_consensus_key_manager_kind, error_list) ->
            int_of_status operation_result, None, Some error_list
          | Skipped Update_consensus_key_manager_kind ->
            int_of_status operation_result, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        let consumed_gas = Option.map normalize_gas consumed_gas in
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Update_consensus_key_table.insert
            (mtup8 opaid source_id nonce fee
               status consumed_gas (error_list_to_json error_list) pk)
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
      | Sc_rollup_execute_outbox_message { rollup (* : Sc_rollup.t *); cemented_commitment (* : Sc_rollup.Commitment.Hash.t *);
                                           output_proof (* : string*);
                                         } ->
        let status, error_list, consumed_gas, balance_updates, paid_storage_size_diff(**# ifdef __META14__ , ticket_receipt #**) =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_execute_outbox_message_result
                       { balance_updates ; consumed_gas ; paid_storage_size_diff (**# ifdef __META14__ ; ticket_receipt #**)(**# ifdef __META16__ ; whitelist_update = _#**) }) ->
            int_of_status operation_result, None, Some consumed_gas, balance_updates
            , Some paid_storage_size_diff(**# ifdef __META14__ , Some ticket_receipt #**)
          | Backtracked (Apply_results.Sc_rollup_execute_outbox_message_result
                           { balance_updates ; consumed_gas ; paid_storage_size_diff (**# ifdef __META14__ ; ticket_receipt #**)(**# ifdef __META16__ ; whitelist_update = _#**)}
                        , error_list_option) ->
            int_of_status operation_result, error_list_option, Some consumed_gas, balance_updates
            , Some paid_storage_size_diff(**# ifdef __META14__ , Some ticket_receipt #**)
          | Failed (Sc_rollup_execute_outbox_message_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, None, []
            , None(**# ifdef __META14__ , None #**)
          | Skipped Sc_rollup_execute_outbox_message_manager_kind ->
            int_of_status operation_result, None, None, []
            , None(**# ifdef __META14__ , None #**)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup ->
        begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
          Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
        end >>= fun () ->
        let consumed_gas = Option.map normalize_gas consumed_gas in
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Sc_rollup_execute_outbox_message_table.insert
            (mtup12 opaid source_id nonce fee
               status consumed_gas (error_list_to_json error_list)
               paid_storage_size_diff rollup cemented_commitment output_proof
               (**# ifdef __META14__ ticket_receipt #**)(**# else None #**)
            )
        end >>= caqti_BUT_NO_FAIL ~__LOC__ ~cmd:!Tezos_indexer_lib.Config.alert_cmd ~msg:"A sc_rollup_execute_outbox_message operation failed to be recorded in the DB" >>= fun _ ->
        return_unit
(**# ifndef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META12__ OPEN_COMMENTS #**)
      | Sc_rollup_recover_bond { sc_rollup = rollup (**# ifdef __META14__ ; staker = _FIXME_TODO #**) } -> (* FIXME/TODO *)
        let status, error_list, consumed_gas, balance_updates =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_recover_bond_result
                       { balance_updates ; consumed_gas }) ->
            int_of_status operation_result, None, Some consumed_gas, balance_updates
          | Backtracked (Apply_results.Sc_rollup_recover_bond_result
                           { balance_updates ; consumed_gas }
                        , error_list_option) ->
            int_of_status operation_result, error_list_option, Some consumed_gas, balance_updates
          | Failed (Sc_rollup_recover_bond_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, None, []
          | Skipped Sc_rollup_recover_bond_manager_kind ->
            int_of_status operation_result, None, None, []
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup ->
        begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
          Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
        end >>= fun () ->
        let consumed_gas = Option.map normalize_gas consumed_gas in
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Sc_rollup_recover_bond_table.insert
            (mtup8 opaid source_id nonce fee
               status consumed_gas (error_list_to_json error_list)
               rollup)
        end >>= caqti_BUT_NO_FAIL ~__LOC__ ~cmd:!Tezos_indexer_lib.Config.alert_cmd ~msg:"A sc_rollup_recover_bond operation failed to be recorded in the DB" >>= fun _ ->
        return_unit
(**# ifdef __META14__ OPEN_COMMENTS #**)
      | Dal_publish_slot_header _ -> assert false (* not used until at least __META14__ *)
      | Sc_rollup_dal_slot_subscribe { rollup = _ ; slot_index = _ } ->
        assert false (* this operation was never used and was then deleted *)
(**# ifdef __META14__ CLOSE_COMMENTS #**)
      | Increase_paid_storage { amount_in_bytes (* : Z.t *); destination (* : Contract_hash.t *); } ->
        record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:(Contract.Originated destination)
        >>= fun destination_id ->
        let status, error_list, consumed_gas, balance_updates =
          match operation_result with
          | Applied (Apply_results.Increase_paid_storage_result { balance_updates; consumed_gas }) ->
            let consumed_gas = normalize_gas consumed_gas in
            int_of_status operation_result, None, Some consumed_gas, balance_updates
          | Backtracked (Apply_results.Increase_paid_storage_result { balance_updates; consumed_gas }, error_list_option) ->
            let consumed_gas = normalize_gas consumed_gas in
            int_of_status operation_result, error_list_option, Some consumed_gas, balance_updates
          | Failed (Increase_paid_storage_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, None, []
          | Skipped Increase_paid_storage_manager_kind ->
            int_of_status operation_result, None, None, []
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, internal) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
          Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
        end >>= fun () ->
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Increase_paid_storage_table.insert
            (mtup9 opaid source_id nonce fee
               status amount_in_bytes destination_id consumed_gas
               (error_list_to_json error_list))
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
(**# ifndef __META12__ CLOSE_COMMENTS #**)
      | Transaction { amount ; destination ; parameters ;
                      (**# ifdef __META3__ entrypoint #**)
                      (**# ifdef __META6__ entrypoint #**) } ->
        let status, error_list, balance_updates, originated_contracts, storage
            , (consumed_gas : _ option), storage_size, paid_storage_size_diff
            (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**)
            (**# ifdef __META11__ , ticket_hash #**)
            (**# ifdef __META13__ , ticket_receipt #**)
          =
          match operation_result with
          | Applied (Apply_results.Transaction_result
                       ((**#X ifdef __META11__ Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_contract_result X#**)

                         { balance_updates ; originated_contracts ; storage ;
                           (**# ifdef __BIG_MAP_DIFF__ big_map_diff ; (* 1 *) #**)
                           (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                           consumed_gas ; storage_size ; paid_storage_size_diff ;
                           (**# ifdef __PROTO1__ #**)
                           (**# elseifdef __PROTO2__ #**)
                           (**# else allocated_destination_contract = (true|false) ; #**)
                           (**# ifdef __META13__ ticket_receipt ; #**)
                         }
                       )
                    )
            ->
            (int_of_status operation_result, None, balance_updates, originated_contracts, storage
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
            (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**)
            (**# ifdef __META11__ , None #**)
            (**# ifdef __META13__ , Some ticket_receipt #**))
          | Backtracked (Apply_results.Transaction_result
                           (((**#X ifdef __META11__ Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_contract_result X#**)

                             { balance_updates ; originated_contracts ; storage ;
                               (**# ifdef __BIG_MAP_DIFF__ big_map_diff ; (* 2 *) #**)
                               (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                               consumed_gas ; storage_size ; paid_storage_size_diff ;
                               (**# ifdef __PROTO1__ #**)
                               (**# elseifdef __PROTO2__ #**)
                               (**# else allocated_destination_contract = (true|false) ; #**)
                               (**# ifdef __META13__ ticket_receipt #**)
                             })), error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, originated_contracts
            , storage, Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
              (**# ifdef __BIG_MAP_DIFF__ , big_map_diff #**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**)
              (**# ifdef __META11__ , None #**)
              (**# ifdef __META13__ , Some ticket_receipt #**))
          | Failed (Transaction_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, []
            , [], None, None, None, None
              (**# ifdef __BIG_MAP_DIFF__ , None #**)
              (**# ifdef __STORAGE_DIFF__ , None #**)
              (**# ifdef __META11__ , None #**)
              (**# ifdef __META13__ , None #**))
          | Skipped Transaction_manager_kind ->
            (int_of_status operation_result, None, []
            , [], None, None, None, None
              (**# ifdef __BIG_MAP_DIFF__ , None #**)
              (**# ifdef __STORAGE_DIFF__ , None #**)
              (**# ifdef __META11__ , None #**)
              (**# ifdef __META13__ , None #**))
(**# ifndef __META11__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          | Applied (Apply_results.Transaction_result (Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_tx_rollup_result
                                                         { balance_updates ;
                                                           consumed_gas ; paid_storage_size_diff ;
                                                           ticket_hash
                                                         })) ->
            (int_of_status operation_result, None, balance_updates, [], None
            , Some (normalize_gas consumed_gas), None, Some paid_storage_size_diff
            , None
            , Some ticket_hash
            (**# ifdef __META13__ , None #**))
          | Backtracked ((Apply_results.Transaction_result (Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_tx_rollup_result
                                                              { balance_updates ;
                                                                consumed_gas ; paid_storage_size_diff ;
                                                                ticket_hash
                                                              }))
                        , error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, []
            , None, Some (normalize_gas consumed_gas), None, Some paid_storage_size_diff
            , None
            , Some ticket_hash
            (**# ifdef __META13__ , None #**))
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        (**# ifdef __META12__ let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in #**)
        Lwt_list.map_s (fun originated_contract ->
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:originated_contract
          )
          originated_contracts
        >>= fun originated_contracts ->
        (**# ifdef __PROTO13__ record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination #**)
        (**# else record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:destination #**)
        >>= fun destination_id ->
        (**# ifdef __PROTO1__ OPEN_COMMENTS #**)
        begin
          if status = status_applied then
            begin
              (**# ifdef __META11__ let contract = None in #**)
              (**# else let contract = Some destination in #**)
              process_bigmap_diffs
                conn conn2
                (**# ifndef __PROTO1__ ~bigmap_counter #**)
                ~block_level
                ~bh
                ~opaid:(Some opaid)
                ~iorid:None
                ~sender:(Some source)
                ~sender_pp:source_pp
                ~contract
                ~senderid:(Some source_id)
                ~contract_id:(Some destination_id)
                ~diffs:(**# ifdef __BIG_MAP_DIFF__ big_map_diff #**) (**# elseifdef __STORAGE_DIFF__ lazy_storage_diff #**)(**#X else () X#**)
                ~ophid:(Some ophid)
            end
            (**# ifndef __FA2__ OPEN_COMMENTS #**)
            >>= fun () ->
            begin (* self logged *)
              match tokens, destination with
              | true, (**# ifdef __PROTO13__ Destination.Contract destination #**)(**# else _ #**) ->
                let entrypoint =
                  (**# ifdef __META1__ None #**)(**# else Some entrypoint #**)
                in
                (**# ifdef __META11__ let entrypoint = Std.Option.map Entrypoint.to_string entrypoint in #**)
                Tokens.store_token_op ~cctxt ~opaid ~addr_counter ~block_level ~bh ~source_id ~contract_id:destination_id
                  destination source source_pp op_hash op_id entrypoint parameters conn conn2
              | _ -> return_unit
            end >>= caqti_or_fail ~__LOC__
            (**# ifndef __FA2__ CLOSE_COMMENTS #**)
          else
            Lwt.return_unit
        end >>= fun () ->
        (**# ifdef __PROTO1__ CLOSE_COMMENTS #**)
        (**# ifdef __META11__ let entrypoint = Entrypoint.to_string entrypoint in #**)
        (**# ifndef __META1__ let parameters = Some parameters in #**)
        let storage =
          match storage with
          | Some storage -> Some (Script.lazy_expr storage)
          | None -> None in
        begin (* self logged *)
          (**# ifdef __META12__ let destination = Destination.Contract destination in #**)
          process_tx
            conn conn2
            ~originated_contracts
            ~opaid
            ~addr_counter
            ~destination_id
            ~nonce ~internal
            ~block_level ~bh ~source ~source_id ~op_hash ~op_id ~fee
            ~source_pp
            ~consumed_gas:(consumed_gas : _ option)
            ~storage_size ~paid_storage_size_diff
            ~amount ~destination (*  *)
            (**# ifdef __META3__ ~entrypoint #**)(**# ifdef __META6__ ~entrypoint #**)
            ~balance_updates ~parameters
            ~storage
            ~status
            (**# ifdef __META11__ ~ticket_hash #**)
            (**# ifdef __META13__ ~ticket_receipt #**)
            ~error_list:(error_list_to_json error_list)
        end
      | Origination { (**# ifdef __META1__ manager; spendable; delegatable; #**)
          delegate; credit; (**# ifndef __META11__ preorigination ; #**) script } ->
        (**# ifdef __META11__ let preorigination = None in #**)
        let status, error_list, balance_updates, originated_contracts
            , consumed_gas, storage_size, paid_storage_size_diff
            (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 3 *) #**) X#**)
            (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**) =
          match operation_result with
          | Applied (Apply_results.Origination_result
                       { balance_updates ; originated_contracts ;
                         (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ big_map_diff ; (* 3 *) #**) X#**)
                         (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                         consumed_gas ; storage_size ; paid_storage_size_diff ;
                       }) ->
            (int_of_status operation_result, None, balance_updates, originated_contracts
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 3 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Backtracked (Apply_results.Origination_result
                           { balance_updates ; originated_contracts ;
                             (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ big_map_diff ; (* 4 *) #**) X#**)
                             (**# ifdef __STORAGE_DIFF__ lazy_storage_diff ; #**)
                             consumed_gas ; storage_size ; paid_storage_size_diff ;
                           }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, originated_contracts
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , big_map_diff (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , lazy_storage_diff #**))
          | Failed (Origination_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, []
            , [], None, None, None
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , None (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | Skipped Origination_manager_kind ->
            (int_of_status operation_result, None, []
            , [], None, None, None
              (**#X ifdef __BIG_MAP_DIFF__ (**# ifndef __META1__ , None (* 4 *) #**) X#**)
              (**# ifdef __STORAGE_DIFF__ , None #**))
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin match delegate with
          | None -> Lwt.return_none
          | Some d ->
            let k = implicit_contract d in
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k
            >>= fun kid -> Lwt.return_some kid
        end >>= fun delegate_id ->
        begin match preorigination with
          | None -> Lwt.return_none
          | Some k ->
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k
            >>= fun kid -> Lwt.return_some kid
        end >>= fun preorigination_id ->
        (**# ifdef __META1__
        record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:manager
        >>= fun manager_id ->
           #**)
        (**# ifdef __META3__ let script = Some script in #**)
        (**# ifdef __META6__ let script = Some script in #**)
        begin (* self logged *)
          (**# ifdef __META12__ let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in #**)
          process_origination
            conn conn2
            ~opaid
            ~addr_counter
            (**# ifndef __PROTO1__ ~bigmap_counter #**)
            ~block_level ~delegate_id ~preorigination_id
            ~internal ~nonce
            ~bh ~source ~source_id ~op_hash ~op_id ~fee
            (**# ifdef __FA2__ ~tokens ~cctxt #**)
            ~source_pp
            ~delegate (**# ifdef __META1__ ~manager ~manager_id ~spendable ~delegatable #**)
            (**# ifndef __PROTO1__ ~ophid #**)
            credit preorigination script balance_updates originated_contracts
            (**# ifdef __META3__ ~big_map_diff #**)
            (**# ifdef __META6__ ~lazy_storage_diff #**)
            ~consumed_gas
            ~storage_size ~paid_storage_size_diff
            ~status
            ~error_list:(error_list_to_json error_list)
        end
      | Delegation maybe_pkh ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (
              Apply_results.Delegation_result
              (**# ifdef __PROTO1__ #**)
              (**# elseifdef __PROTO2__ #**)
              (**# elseifdef __META16__ { consumed_gas ; balance_updates = _ (* FIXME *) } #**)
              (**# else { consumed_gas } #**)
            ) ->
            (int_of_status operation_result
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# else Some (normalize_gas consumed_gas) #**)
            )
            , None)
          | Backtracked (
              Apply_results.Delegation_result
            (**# ifdef __PROTO1__ #**)
            (**# elseifdef __PROTO2__ #**)
            (**# elseifdef __META16__ { consumed_gas ; balance_updates = _ (* FIXME *) } #**)
            (**# else { consumed_gas } #**)
            , error_list_option) ->
            (int_of_status operation_result
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# else Some (normalize_gas consumed_gas) #**)
            )
            , error_list_option)
          | Failed (Delegation_manager_kind, error_list) ->
            int_of_status operation_result, None, Some error_list
          | Skipped Delegation_manager_kind ->
            int_of_status operation_result, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin
          match maybe_pkh with
          | None -> Lwt.return_none
          | Some pkh ->
            let receiver = implicit_contract pkh in
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:receiver
            >>= fun addr -> Lwt.return_some addr
        end >>= fun maybe_pkhid ->
        begin (* logged *)
          Verbose.Log.add_delegation block_level bh source source_pp maybe_pkh (consumed_gas: Fpgas.t option) fee;
          Conn.find_opt Delegation_table.insert
            (mtup8 opaid source_id maybe_pkhid consumed_gas
               fee nonce status (error_list_to_json error_list))
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
      | Reveal pk
        ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (
              Apply_results.Reveal_result
              (**# ifdef __PROTO1__ #**)
              (**# elseifdef __PROTO2__ #**)
              (**# else { consumed_gas } #**)
            ) ->
            (int_of_status operation_result
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# else Some (normalize_gas consumed_gas) #**)
            )
            , None)
          | Backtracked (
              Apply_results.Reveal_result
            (**# ifdef __PROTO1__ #**)
            (**# elseifdef __PROTO2__ #**)
            (**# else { consumed_gas } #**)
            , error_list_option) ->
            (int_of_status operation_result
            , (
              (**# ifdef __PROTO1__ None #**)
              (**# elseifdef __PROTO2__ None #**)
              (**# else Some (normalize_gas consumed_gas) #**)
            )
            , error_list_option)
          | Failed (Reveal_manager_kind, error_list) ->
            int_of_status operation_result, None, Some error_list
          | Skipped Reveal_manager_kind ->
            int_of_status operation_result, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          Verbose.Log.store_revelation block_level bh pk;
          Conn.find_opt Reveal_table.insert
            (mtup8 opaid source_id pk consumed_gas
               fee nonce status (error_list_to_json error_list)) >>=
          caqti_or_fail ~__LOC__ >>= fun _ ->
          return_unit
        end
(**# ifndef __META9__ OPEN_COMMENTS #**)
      | Register_global_constant { value } ->
        let status, error_list, balance_updates, consumed_gas, size_of_constant, global_address =
          match operation_result with
          | Applied (Apply_results.Register_global_constant_result
                       { balance_updates ; consumed_gas ; size_of_constant ; global_address }) ->
            (int_of_status operation_result, None, balance_updates
            , Some (normalize_gas consumed_gas), Some size_of_constant, Some global_address)
          | Backtracked (Apply_results.Register_global_constant_result
                           { balance_updates ; consumed_gas ; size_of_constant ; global_address }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates
            , Some (normalize_gas consumed_gas), Some size_of_constant, Some global_address)
          | Failed (Register_global_constant_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, []
            , None, None, None)
          | Skipped Register_global_constant_manager_kind ->
            (int_of_status operation_result, None, []
            , None, None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun () ->
        Conn.find_opt Register_global_constant_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; value
          ; consumed_gas
          ; size_of_constant
          ; global_address
          ; error_list = error_list_to_json error_list (* 1 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifndef __META9__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
(**# ifdef __META17__ OPEN_COMMENTS #**)
      | Set_deposits_limit value ->
        let status, error_list, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Set_deposits_limit_result { consumed_gas }) ->
            (int_of_status operation_result, None, Some (normalize_gas consumed_gas))
          | Backtracked (Apply_results.Set_deposits_limit_result { consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, Some (normalize_gas consumed_gas))
          | Failed (Set_deposits_limit_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None)
          | Skipped Set_deposits_limit_manager_kind ->
            (int_of_status operation_result, None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        Conn.find_opt Set_deposits_limit_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; value
          ; consumed_gas
          ; error_list = error_list_to_json error_list (* 2 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifdef __META17__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
      | Sc_rollup_refute { rollup (* : Sc_rollup.t *)
                         ; opponent (* : Sc_rollup.Staker.t = pkh  *)
                         ; refutation (* : Sc_rollup.Game.refutation; -- use Sc_rollup.Game.refutation_encoding *) } ->
        let status, error_list, consumed_gas, game_status, balance_updates =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_refute_result { consumed_gas; game_status; balance_updates }) ->
            (int_of_status operation_result, None, Some (normalize_gas consumed_gas), Some game_status, balance_updates)
          | Backtracked (Apply_results.Sc_rollup_refute_result { consumed_gas; game_status; balance_updates }, error_list_option) ->
            (int_of_status operation_result, error_list_option, Some (normalize_gas consumed_gas), Some game_status, balance_updates)
          | Failed (Sc_rollup_refute_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None, None, [])
          | Skipped Sc_rollup_refute_manager_kind ->
            (int_of_status operation_result, None, None, None, [])
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun () ->
        record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup ->
        record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:opponent >>= fun opponent ->
        Conn.find_opt Sc_rollup_refute_table.insert
          {
            opaid
          ; source_id
          ; fee
          ; status
          ; rollup
          ; opponent
          ; refutation
          ; game_status
          ; consumed_gas
          ; error_list = error_list_to_json error_list (* 2- *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Sc_rollup_timeout { rollup  (* : Sc_rollup.t *)
                          ; stakers (* : Sc_rollup.Game.Index.t --> use Sc_rollup.Game.Index.encoding *)
                          } ->
        let status, error_list, consumed_gas, game_status, balance_updates =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_timeout_result { consumed_gas; game_status; balance_updates }) ->
            (int_of_status operation_result, None, Some (normalize_gas consumed_gas), Some game_status, balance_updates)
          | Backtracked (Apply_results.Sc_rollup_timeout_result { consumed_gas; game_status; balance_updates }, error_list_option) ->
            (int_of_status operation_result, error_list_option, Some (normalize_gas consumed_gas), Some game_status, balance_updates)
          | Failed (Sc_rollup_timeout_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None, None, [])
          | Skipped Sc_rollup_timeout_manager_kind ->
            (int_of_status operation_result, None, None, None, [])
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun () ->
        record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup ->
        Conn.find_opt Sc_rollup_timeout_table.insert
          {
            opaid
          ; source_id
          ; fee
          ; status
          ; rollup
          ; stakers
          ; game_status
          ; consumed_gas
          ; error_list = error_list_to_json error_list (* 2-- *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META12__ OPEN_COMMENTS 1 #**)
(**# ifdef __META14__ OPEN_COMMENTS 2 #**)
      | Sc_rollup_refute _ (* NOT ACTIVATED before proto 16 *)
      | Sc_rollup_timeout _ -> assert false
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META12__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
(**#X ifdef __META15__ OPEN_COMMENTS X#**)
      | Tx_rollup_origination ->
        let status, error_list, balance_updates, consumed_gas, originated_tx_rollup =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_origination_result
                       { balance_updates
                       ; consumed_gas
                       ; originated_tx_rollup (* Tx_rollup.t *)
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas), Some originated_tx_rollup)
          | Backtracked (Apply_results.Tx_rollup_origination_result
                           { balance_updates
                           ; consumed_gas
                           ; originated_tx_rollup (* Tx_rollup.t *) }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas), Some originated_tx_rollup)
          | Failed (Alpha_context.Kind.Tx_rollup_origination_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None)
          | Skipped Alpha_context.Kind.Tx_rollup_origination_manager_kind ->
            (int_of_status operation_result, None, [], None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin match originated_tx_rollup with
          | Some originated_tx_rollup ->
            record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup:originated_tx_rollup
            >>= fun kid -> Lwt.return_some kid
          | None -> Lwt.return_none
        end >>= fun originated_tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_origination_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; originated_tx_rollup
          ; error_list = error_list_to_json error_list (* 3 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_return_bond { tx_rollup } ->
        let status, error_list, balance_updates, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_return_bond_result
                       { balance_updates
                       ; consumed_gas
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas))
          | Backtracked (Apply_results.Tx_rollup_return_bond_result
                           { balance_updates
                           ; consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas))
          | Failed (Alpha_context.Kind.Tx_rollup_return_bond_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None)
          | Skipped Alpha_context.Kind.Tx_rollup_return_bond_manager_kind ->
            (int_of_status operation_result, None, [], None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_return_bond_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; error_list = error_list_to_json error_list (* 4 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_rejection { tx_rollup
                            ; level
                            ; message  (* Tx_rollup_message_repr.t *)
                            ; message_position  (* : int *)
                            ; message_path  (* : Tx_rollup_inbox_repr.Merkle.path *)
                            ; message_result_hash  (* : Tx_rollup_message_result_hash_repr.t *)
                            ; message_result_path  (* : Tx_rollup_commitment_repr.Merkle.path *)
                            ; previous_message_result  (* : Tx_rollup_message_result_repr.t *)
                            ; previous_message_result_path  (* : Tx_rollup_commitment_repr.Merkle.path *)
                            ; proof  (* : Tx_rollup_l2_proof.t / proto ≥ 15: Tx_rollup_l2_proof.serialized *)
                            } ->
        let status, error_list, balance_updates, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_rejection_result
                       { balance_updates
                       ; consumed_gas
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas))
          | Backtracked (Apply_results.Tx_rollup_rejection_result
                           { balance_updates
                           ; consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas))
          | Failed (Alpha_context.Kind.Tx_rollup_rejection_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None)
          | Skipped Alpha_context.Kind.Tx_rollup_rejection_manager_kind ->
            (int_of_status operation_result, None, [], None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_rejection_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; error_list = error_list_to_json error_list (* 5 *)
          ; level
          ; message  (* Tx_rollup_message_repr.t *)
          ; message_position  (* : int *)
          ; message_path  (* : Tx_rollup_inbox_repr.Merkle.path *)
          ; message_result_hash  (* : Tx_rollup_message_result_hash_repr.t *)
          ; message_result_path  (* : Tx_rollup_commitment_repr.Merkle.path *)
          ; previous_message_result  (* : Tx_rollup_message_result_repr.t *)
          ; previous_message_result_path  (* : Tx_rollup_commitment_repr.Merkle.path *)
          ; proof  (* : Tx_rollup_l2_proof.t / proto ≥ 15: Tx_rollup_l2_proof.serialized *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_dispatch_tickets { tx_rollup
                                   ; level
                                   ; context_hash
                                   ; message_index
                                   ; message_result_path
                                   ; tickets_info
                                   } ->
        let status, error_list, balance_updates, consumed_gas, paid_storage_size_diff =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_dispatch_tickets_result
                       { balance_updates
                       ; consumed_gas
                       ; paid_storage_size_diff
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff)
          | Backtracked (Apply_results.Tx_rollup_dispatch_tickets_result
                           { balance_updates
                           ; paid_storage_size_diff
                           ; consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff)
          | Failed (Alpha_context.Kind.Tx_rollup_dispatch_tickets_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None)
          | Skipped Alpha_context.Kind.Tx_rollup_dispatch_tickets_manager_kind ->
            (int_of_status operation_result, None, [], None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        Conn.find_opt Tx_rollup_dispatch_tickets_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; error_list = error_list_to_json error_list (* 6 *)
          ; level
          ; context_hash
          ; message_index
          ; message_result_path
          ; tickets_info
          ; paid_storage_size_diff
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**#X ifdef __META15__ CLOSE_COMMENTS X#**)
      | Transfer_ticket {
          contents (* Script_repr.lazy_expr *)
        ; ty (* Script_repr.lazy_expr *)
        ; ticketer
        ; amount (* Ticket_amount.t *)
        ; destination (* Contract_repr.t *)
        ; entrypoint (* Entrypoint_repr.t *)
        } ->
(**# ifndef __META13__ OPEN_COMMENTS #**)
        let amount : Z.t = Script_int.((amount :> n num) |> to_zint) in
(**# ifndef __META13__ CLOSE_COMMENTS #**)
        let status, error_list, balance_updates, consumed_gas, paid_storage_size_diff(**# ifdef __META14__ , ticket_receipt #**) =
          match operation_result with
          | Applied (Apply_results.Transfer_ticket_result
                       { balance_updates
                       ; consumed_gas
                       ; paid_storage_size_diff
                         (**# ifdef __META14__ ; ticket_receipt #**)
                       }) ->
            int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff(**# ifdef __META14__ , Some ticket_receipt #**)
          | Backtracked (Apply_results.Transfer_ticket_result
                           { balance_updates
                           ; paid_storage_size_diff
                           ; consumed_gas
                             (**# ifdef __META14__ ; ticket_receipt #**) }, error_list_option) ->
            int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff(**# ifdef __META14__ , Some ticket_receipt #**)
          | Failed (Alpha_context.Kind.Transfer_ticket_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, [], None
            , None(**# ifdef __META14__ , None #**)
          | Skipped Alpha_context.Kind.Transfer_ticket_manager_kind ->
            int_of_status operation_result, None, [], None
            , None(**# ifdef __META14__ , None #**)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
        begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        get_kid ticketer >>= fun ticketer ->
        get_kid destination >>= fun destination ->
        Conn.find_opt Transfer_ticket_table.insert
          {
            opaid
          (**# ifdef __META14__ ; ticket_receipt #**)
          ; source_id
          ; fee
          ; status
          ; consumed_gas
          ; error_list = error_list_to_json error_list (* 7 *)
          ; contents
          ; ty
          ; ticketer
          ; destination
          ; amount
          ; entrypoint
          ; paid_storage_size_diff
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**#X ifdef __META15__ OPEN_COMMENTS X#**)
      | Tx_rollup_remove_commitment { tx_rollup } ->
        let status, error_list, balance_updates, consumed_gas, level =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_remove_commitment_result
                       { balance_updates
                       ; consumed_gas
                       ; level
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas), Some level)
          | Backtracked (Apply_results.Tx_rollup_remove_commitment_result
                           { balance_updates
                           ; consumed_gas
                           ; level }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas), Some level)
          | Failed (Alpha_context.Kind.Tx_rollup_remove_commitment_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None)
          | Skipped Alpha_context.Kind.Tx_rollup_remove_commitment_manager_kind ->
            (int_of_status operation_result, None, [], None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_remove_commitment_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; error_list = error_list_to_json error_list (* 8 *)
          ; level
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_finalize_commitment { tx_rollup } ->
        let status, error_list, balance_updates, consumed_gas, level =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_finalize_commitment_result
                       { balance_updates
                       ; consumed_gas
                       ; level
                       }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas), Some level)
          | Backtracked (Apply_results.Tx_rollup_finalize_commitment_result
                           { balance_updates
                           ; consumed_gas
                           ; level }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas), Some level)
          | Failed (Alpha_context.Kind.Tx_rollup_origination_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None)
          | Skipped Alpha_context.Kind.Tx_rollup_origination_manager_kind ->
            (int_of_status operation_result, None, [], None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_finalize_commitment_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; error_list = error_list_to_json error_list (* 9 *)
          ; level
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_submit_batch { tx_rollup; content; burn_limit } ->
        let content = Misc.pg_bytea content in
        let status, error_list, balance_updates, consumed_gas, paid_storage_size_diff =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_submit_batch_result
                       { balance_updates
                       ; paid_storage_size_diff
                       ; consumed_gas }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas), Some paid_storage_size_diff)
          | Backtracked (Apply_results.Tx_rollup_submit_batch_result
                           { balance_updates
                           ; paid_storage_size_diff
                           ; consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas), Some paid_storage_size_diff)
          | Failed (Alpha_context.Kind.Tx_rollup_origination_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None)
          | Skipped Alpha_context.Kind.Tx_rollup_origination_manager_kind ->
            (int_of_status operation_result, None, [], None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_submit_batch_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; content
          ; burn_limit
          ; error_list = error_list_to_json error_list (* 10 *)
          ; paid_storage_size_diff
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Tx_rollup_commit { tx_rollup; commitment } ->
        let status, error_list, balance_updates, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Tx_rollup_commit_result
                       { balance_updates
                       ; consumed_gas }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas))
          | Backtracked (Apply_results.Tx_rollup_commit_result
                           { balance_updates
                           ; consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas))
          | Failed (Alpha_context.Kind.Tx_rollup_commit_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None)
          | Skipped Alpha_context.Kind.Tx_rollup_commit_manager_kind ->
            (int_of_status operation_result, None, [], None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_tx_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~tx_rollup >>= fun tx_rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Tx_rollup_commit_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; tx_rollup
          ; commitment
          ; error_list = error_list_to_json error_list (* 11 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**#X ifdef __META15__ CLOSE_COMMENTS X#**)
(**# ifdef __META13__ OPEN_COMMENTS #**)
      | Sc_rollup_publish _ ->
        assert false (* NOT a TODO because deactivated in this context.
                        However it is treated further in the code. *)
(**# ifdef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META13__ OPEN_COMMENTS #**)
      | Sc_rollup_publish { rollup ; commitment } ->
        let status, error_list, balance_updates, consumed_gas, staked_hash, published_at_level =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_publish_result
                       { balance_updates
                       ; staked_hash (* Sc_rollup.Commitment.Hash.t *)
                       ; published_at_level (* : Raw_level.t *)
                       ; consumed_gas }) ->
            int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas)
            , Some staked_hash, Some published_at_level
          | Backtracked (Apply_results.Sc_rollup_publish_result
                           { balance_updates
                           ; staked_hash (* Sc_rollup.Commitment.Hash.t *)
                           ; published_at_level (* : Raw_level.t *)
                           ; consumed_gas }, error_list_option) ->
            int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas)
            , Some staked_hash, Some published_at_level
          | Failed (Alpha_context.Kind.Sc_rollup_publish_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, [], None, None, None
          | Skipped Alpha_context.Kind.Sc_rollup_publish_manager_kind ->
            int_of_status operation_result, None, [], None, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        Conn.find_opt Sc_rollup_publish_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; rollup
          ; commitment
          ; staked_hash
          ; published_at_level
          ; error_list = error_list_to_json error_list (* 11 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Zk_rollup_origination {
          public_parameters (* : Plonk.public_parameters *)
        ; circuits_info (* : bool Zk_rollup.Account.SMap.t *)
                     (* becomes from proto ≥16: [ `Fee | `Private | `Public ] Zk_rollup.Account.SMap.t *)
        ; init_state (* : Zk_rollup.State.t *)
        ; nb_ops (* : int *)
        } ->
        assert (nonce = None);
        let status, error_list, balance_updates, consumed_gas, originated_zk_rollup, storage_size =
          match operation_result with
          | Applied (Apply_results.Zk_rollup_origination_result {
              balance_updates (* : Receipt.balance_updates *);
              originated_zk_rollup (* : Zk_rollup.t *);
              consumed_gas (* : Gas.Arith.fp *);
              storage_size (* : Z.t *);
            }) ->
            int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas)
            , Some originated_zk_rollup, Some storage_size
          | Backtracked (Apply_results.Zk_rollup_origination_result {
              balance_updates (* : Receipt.balance_updates *);
              originated_zk_rollup (* : Zk_rollup.t *);
              consumed_gas (* : Gas.Arith.fp *);
              storage_size (* : Z.t *);
            }, error_list_option) ->
            int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas)
            , Some originated_zk_rollup, Some storage_size
          | Failed (Alpha_context.Kind.Zk_rollup_origination_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, [], None
            , None, None
          | Skipped Alpha_context.Kind.Zk_rollup_origination_manager_kind ->
            int_of_status operation_result, None, [], None
            , None, None
          | _ -> assert false
        in
        begin match originated_zk_rollup with
          | Some originated_zk_rollup ->
            record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination:(Destination.Zk_rollup originated_zk_rollup)
            >>= fun x -> Lwt.return_some x
          | None -> Lwt.return_none
        end >>= fun originated_zk_rollup ->
        begin (* logged *)
          (* FIXME: avoid having that many repetitions of "let get_kid"  *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        let circuits_info : _ Zk_rollup_account_repr.SMap.t = Obj.magic circuits_info in
        let circuits_info_encoding = Zk_rollup_account_repr.circuits_info_encoding in
        Conn.find_opt (Zk_rollup_origination_table.insert circuits_info_encoding)
          {
            opaid
          ; source_id
          ; originated_zk_rollup
          ; fee
          ; status
          ; consumed_gas
          ; public_parameters
          ; circuits_info
          ; init_state
          ; nb_ops
          ; storage_size
          ; error_list = error_list_to_json error_list
          }
        >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
      | Zk_rollup_publish { zk_rollup (* : Zk_rollup.t *);
                            ops (* (Zk_rollup.Operation.t * Zk_rollup.Ticket.t option) list *) }
        ->
        let status, error_list, balance_updates, consumed_gas, paid_storage_size_diff =
          match operation_result with
          | Applied (Apply_results.Zk_rollup_publish_result
                       { balance_updates
                       ; paid_storage_size_diff
                       ; consumed_gas }) ->
            int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff
          | Backtracked (Apply_results.Zk_rollup_publish_result
                           { balance_updates
                           ; paid_storage_size_diff
                           ; consumed_gas }, error_list_option) ->
            int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas)
            , Some paid_storage_size_diff
          | Failed (Alpha_context.Kind.Zk_rollup_publish_manager_kind, error_list) ->
            int_of_status operation_result, Some error_list, [], None, None
          | Skipped Alpha_context.Kind.Zk_rollup_publish_manager_kind ->
            int_of_status operation_result, None, [], None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination:(Destination.Zk_rollup zk_rollup) >>= fun zk_rollup ->
        assert (nonce = None);
        Conn.find_opt Zk_rollup_publish_table.insert
          {
            opaid
          ; source_id
          ; fee
          ; status
          ; consumed_gas
          ; zk_rollup
          ; ops
          ; paid_storage_size_diff
          ; error_list = error_list_to_json error_list (* Zk publish *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifndef __META13__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
      | Sc_rollup_add_messages _ -> assert false (* NOT TODO, implemented for __META14__ further in the code *)
      | Sc_rollup_cement _ -> assert false (* NOT TODO, implemented for __META14__ further in the code *)
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
      | Sc_rollup_add_messages { messages (* ; rollup *) } ->
        assert (nonce = None);
        let status, error_list, consumed_gas =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_add_messages_result
                        { consumed_gas }) ->
            (int_of_status operation_result, None, Some (normalize_gas consumed_gas))
          | Backtracked (Apply_results.Sc_rollup_add_messages_result
                           { consumed_gas }, error_list_option) ->
            (int_of_status operation_result, error_list_option, Some (normalize_gas consumed_gas))
          | Failed (Alpha_context.Kind.Sc_rollup_add_messages_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None)
          | Skipped Alpha_context.Kind.Sc_rollup_add_messages_manager_kind ->
            (int_of_status operation_result, None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        (* record_sc_rollup ~__LINE__ ~block_level ~addr_counter conn conn2 ~sc_rollup:rollup >>= fun rollup -> *)
        Conn.find_opt Sc_rollup_add_messages_table.insert
          {
            opaid
          ; source_id
          ; nonce
          ; fee
          ; status
          ; consumed_gas
          ; messages
          ; error_list = error_list_to_json error_list (* 13 *)
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
      | Sc_rollup_cement {
          rollup (* : Sc_rollup.t *);
          (**# ifdef __META16__ OPEN_COMMENTS #**)
          commitment (* : Sc_rollup.Commitment.Hash.t *);
          (**# ifdef __META16__ CLOSE_COMMENTS #**)
        }
        ->
        assert (nonce = None);
        (**# ifdef __META16__ OPEN_COMMENTS #**)
        let commitment = Some commitment in
        (**# ifdef __META16__ CLOSE_COMMENTS #**)
        (**# ifndef __META16__ OPEN_COMMENTS #**)
        let commitment = None in
        (**# ifndef __META16__ CLOSE_COMMENTS #**)
        let status, error_list, consumed_gas, inbox_level, commitment =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_cement_result { consumed_gas ; inbox_level (**# ifdef __META15__ ; commitment_hash = commitment #**) }) ->
            (int_of_status operation_result, None, Some (normalize_gas consumed_gas), Some inbox_level, (**# ifdef __META15__ Some #**) commitment)
          | Backtracked (Apply_results.Sc_rollup_cement_result { consumed_gas ; inbox_level (**# ifdef __META15__ ; commitment_hash = commitment #**) }, error_list_option) ->
            (int_of_status operation_result, error_list_option, Some (normalize_gas consumed_gas), Some inbox_level, (**# ifdef __META15__ Some #**) commitment)
          | Failed (Alpha_context.Kind.Sc_rollup_cement_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None, None, commitment)
          | Skipped (Alpha_context.Kind.Sc_rollup_cement_manager_kind) ->
            (int_of_status operation_result, None, None, None, commitment)
          | _ -> assert false
        in
        record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination:(Destination.Sc_rollup rollup) >>= fun rollup ->
        Conn.find_opt Sc_rollup_cement_table.insert
          {
            opaid
          ; source_id
          ; fee
          ; status
          ; rollup
          ; commitment
          ; consumed_gas
          ; inbox_level = Option.map Raw_level.to_int32 inbox_level
          ; error_list = error_list_to_json error_list
          }
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META13__ OPEN_COMMENTS #**)
      | Sc_rollup_originate _ -> assert false (* NOT TODO, implemented for __META13__ further in the code *)
(**# ifdef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META13__ OPEN_COMMENTS #**)
      | Sc_rollup_originate { kind; boot_sector ; parameters_ty  (**# ifndef __META16__  ; origination_proof #**)(**# ifdef __META16__ ; whitelist = _ #**) } ->
        (**# ifdef __META16__ OPEN_COMMENTS #**)
        (**# ifdef __META14__ let origination_proof = (origination_proof :> string) in #**)
        let origination_proof = Some origination_proof in
        (**# ifdef __META16__ CLOSE_COMMENTS #**)
        (**# ifndef __META16__ OPEN_COMMENTS #**)
        let origination_proof = None in
        (**# ifndef __META16__ CLOSE_COMMENTS #**)
        let status, error_list, balance_updates, consumed_gas, address, size, genesis_commitment_hash =
          match operation_result with
          | Applied (Apply_results.Sc_rollup_originate_result
                        { address
                        ; consumed_gas
                        ; size
                        ; balance_updates
                        ; genesis_commitment_hash
                        }) ->
            (int_of_status operation_result, None, balance_updates, Some (normalize_gas consumed_gas), Some address, Some size, Some genesis_commitment_hash)
          | Backtracked (Apply_results.Sc_rollup_originate_result
                           { balance_updates
                           ; consumed_gas
                           ; address
                           ; size
                           ; genesis_commitment_hash
                           }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, Some (normalize_gas consumed_gas), Some address, Some size, Some genesis_commitment_hash)
          | Failed (Alpha_context.Kind.Sc_rollup_originate_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, [], None, None, None, None)
          | Skipped Alpha_context.Kind.Sc_rollup_originate_manager_kind ->
            (int_of_status operation_result, None, [], None, None, None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin match address with
          | None -> Lwt.return_none
          | Some sc_rollup ->
            record_sc_rollup ~__LINE__ ~addr_counter ~block_level conn conn2 ~sc_rollup >>= fun x -> Lwt.return_some x
        end >>= fun address ->
        begin (* logged *)
          let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
(**# ifdef __META16__ OPEN_COMMENTS #**)
          Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
        end >>= fun _ ->
        let boot_sector_hash = Digest.string boot_sector |> Digest.to_hex in
        begin
          let module Conn2 = (val conn2 : Caqti_lwt.CONNECTION) in
          (* Verbose.printf "boot_sector: hash=%S value=%S" boot_sector_hash boot_sector; *)
          if Verbose.is_human_readable_ascii boot_sector then
            Conn2.find_opt Sc_rollup_boot_sector_table.insert_txt
              {
                boot_sector_hash
              ; boot_sector
              }
          else
            Conn2.find_opt Sc_rollup_boot_sector_table.insert_bin
              {
                boot_sector_hash
              ; boot_sector
              }
        end >>= begin function
          | Ok h when h = Some boot_sector_hash ->
            Conn.find_opt Sc_rollup_originate_table.insert
              {
                opaid
              ; source_id
              ; nonce
              ; fee
              ; status
              ; consumed_gas
              ; kind
              ; boot_sector_hash
              ; address
              ; size
              ; parameters_ty
              ; origination_proof
              ; genesis_commitment_hash
              ; error_list = error_list_to_json error_list (* 12 *)
              }
          | Ok (Some _) -> assert false
          | Ok None -> assert false
          | Error _ as e -> Lwt.return e
        end
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
        return_unit
(**# ifndef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
  (* end of process *)

(**# ifndef __META12__ OPEN_COMMENTS #**)
  let process_internal :
    type a b.
    (module Caqti_lwt.CONNECTION) ->
    (module Caqti_lwt.CONNECTION) ->
    opaid:_ ->
    (**# ifndef __PROTO1__ bigmap_counter:_ -> #**)
    addr_counter:_ ->
    (**# ifdef __FA2__ tokens:bool -> cctxt:#rpc_context -> #**)
    block_level:block_level ->
    nonce:int ->
    internal:int ->
    source_id:kid ->
    ophid:int64 ->
    Block_hash.t -> Operation_hash.t -> int ->
    (**# ifdef __META14__ Destination.t -> #**)(**# else Contract.t -> #**)
    a internal_operation_contents ->
    b internal_operation_result -> unit tzresult Lwt.t =
    fun conn conn2
      ~opaid
      (**# ifndef __PROTO1__ ~bigmap_counter #**)
      ~addr_counter
      (**# ifdef __FA2__ ~tokens ~cctxt #**)
      ~block_level ~nonce ~internal ~source_id
      ~ophid
      bh op_hash op_id source operation operation_result ->
      let fee = None in
      (**# ifdef __META14__ let source_pp = Destination.pp in (* FIXME/TODO: logs *) #**)
      (**# else let source_pp = Contract.pp in Verbose.Log.process_mgr_operation block_level bh op_hash op_id source fee; #**)
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      match operation with
      | Event {
          ty (* : Script.expr *);
          tag (* : Entrypoint.t *);
          payload (* : Script.expr *);
        } ->
        let status, error_list, consumed_gas =
          match operation_result with
          | Applied (Apply_internal_results.IEvent_result { consumed_gas }) ->
            let consumed_gas = normalize_gas consumed_gas in
            (int_of_status operation_result, None, Some consumed_gas)
          | Backtracked (Apply_internal_results.IEvent_result { consumed_gas }, error_list_option) ->
            let consumed_gas = normalize_gas consumed_gas in
            (int_of_status operation_result, error_list_option, Some consumed_gas)
          | Failed (Event_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, None)
          | Skipped (Event_manager_kind) ->
            (int_of_status operation_result, None, None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin (* logged *)
          (* Verbose.Log. TODO; *)
          Conn.find_opt Event_table.insert
            (mtup10 opaid source_id
               nonce fee status ty tag payload consumed_gas
               (error_list_to_json error_list))
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
      | Transaction { amount ; destination ; parameters ; entrypoint } ->
        let status, error_list, balance_updates, originated_contracts, storage
            , (consumed_gas : _ option), storage_size, paid_storage_size_diff
            , lazy_storage_diff
            , ticket_hash
            (**# ifdef __META13__ , ticket_receipt #**)
          =
          match operation_result with
          | Applied (Apply_internal_results.ITransaction_result
                       (Apply_internal_results.Transaction_to_contract_result
                         { balance_updates
                         ; originated_contracts
                         ; storage
                         ; lazy_storage_diff
                         ; consumed_gas
                         ; storage_size
                         ; paid_storage_size_diff
                         ; allocated_destination_contract = (true|false)
   (**# ifdef __META13__ ; ticket_receipt #**)
                         }
                       )
                    )
            ->
            (int_of_status operation_result, None, balance_updates, originated_contracts, storage
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
            , lazy_storage_diff
            , None
              (**# ifdef __META13__ , Some ticket_receipt #**))
          | Backtracked (Apply_internal_results.ITransaction_result
                           (Apply_internal_results.Transaction_to_contract_result
                              { balance_updates
                              ; originated_contracts
                              ; storage
                              ; lazy_storage_diff
                              ; consumed_gas
                              ; storage_size
                              ; paid_storage_size_diff
                              ; allocated_destination_contract = (true|false)
        (**# ifdef __META13__ ; ticket_receipt #**)
                              }), error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, originated_contracts
            , storage, Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
            , lazy_storage_diff
            , None
              (**# ifdef __META13__ , Some ticket_receipt #**))
(**# ifdef __META16__ OPEN_COMMENTS #**)
          | Applied (Apply_internal_results.ITransaction_result
                       (Apply_internal_results.Transaction_to_tx_rollup_result
                          { balance_updates
                          ; consumed_gas
                          ; paid_storage_size_diff
                          ; ticket_hash
                          })) ->
            (int_of_status operation_result, None, balance_updates, [], None
            , Some (normalize_gas consumed_gas), None, Some paid_storage_size_diff
            , None
            , Some ticket_hash
              (**# ifdef __META13__ , None #**))
          | Backtracked ((Apply_internal_results.ITransaction_result
                            (Apply_internal_results.Transaction_to_tx_rollup_result
                               { balance_updates
                               ; consumed_gas
                               ; paid_storage_size_diff
                               ; ticket_hash
                               }))
                        , error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, []
            , None, Some (normalize_gas consumed_gas), None, Some paid_storage_size_diff
            , None
            , Some ticket_hash
              (**# ifdef __META13__ , None #**))
(**# ifdef __META16__ CLOSE_COMMENTS #**)
          (**# ifndef __META14__ OPEN_COMMENTS #**)
          | Applied (Apply_internal_results.ITransaction_result
                       (Apply_internal_results.Transaction_to_sc_rollup_result
                          { consumed_gas
                          ; ticket_receipt
                          })) ->
            (int_of_status operation_result, None, [], [], None
            , Some (normalize_gas consumed_gas), None, None
            , None
            , None
            , Some ticket_receipt)
          | Backtracked ((Apply_internal_results.ITransaction_result
                            (Apply_internal_results.Transaction_to_sc_rollup_result
                               { consumed_gas
                               ; ticket_receipt
                               }))
                        , error_list_option) ->
            (int_of_status operation_result, error_list_option, [], []
            , None, Some (normalize_gas consumed_gas), None, None
            , None
            , None
            , Some ticket_receipt)
          (**# ifndef __META14__ CLOSE_COMMENTS #**)
          | Failed (Transaction_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, []
            , [], None, None, None, None
            , None
            , None
              (**# ifdef __META13__ , None #**))
          | Skipped Transaction_manager_kind ->
            (int_of_status operation_result, None, []
            , [], None, None, None, None
            , None
            , None
              (**# ifdef __META13__ , None #**))
          | Applied (IDelegation_result _) (* nonsense *)
          | Applied (IEvent_result _) (* nonsense *)
          | Applied (IOrigination_result _) (* nonsense *)
          | Backtracked (IDelegation_result _, _) (* nonsense *)
          | Backtracked (IEvent_result _, _) (* nonsense *)
          | Backtracked (IOrigination_result _, _) (* nonsense *)
          (**# ifdef __PROTO14__ | Failed (Sc_rollup_dal_slot_subscribe_manager_kind, _) #**)
          (**# ifdef __PROTO14__ | Skipped Sc_rollup_dal_slot_subscribe_manager_kind #**)
          (**# ifdef __PROTO15__ | Failed (Sc_rollup_dal_slot_subscribe_manager_kind, _) #**)
          (**# ifdef __PROTO15__ | Skipped Sc_rollup_dal_slot_subscribe_manager_kind #**)
          | Failed (Dal_publish_slot_header_manager_kind, _)
          | Failed (Delegation_manager_kind, _)
          | Failed (Event_manager_kind, _)
          | Failed (Increase_paid_storage_manager_kind, _)
          | Failed (Origination_manager_kind, _)
          | Failed (Register_global_constant_manager_kind, _)
          | Failed (Reveal_manager_kind, _)
          | Failed (Sc_rollup_add_messages_manager_kind, _)
          | Failed (Sc_rollup_cement_manager_kind, _)
          | Failed (Sc_rollup_execute_outbox_message_manager_kind, _)
          | Failed (Sc_rollup_originate_manager_kind, _)
          | Failed (Sc_rollup_publish_manager_kind, _)
          | Failed (Sc_rollup_recover_bond_manager_kind, _)
          | Failed (Sc_rollup_refute_manager_kind, _)
          | Failed (Sc_rollup_timeout_manager_kind, _)
(**# ifdef __META17__ OPEN_COMMENTS #**)
          | Failed (Set_deposits_limit_manager_kind, _)
(**# ifdef __META17__ CLOSE_COMMENTS #**)
          | Failed (Transfer_ticket_manager_kind, _)
(**# ifdef __META15__ OPEN_COMMENTS #**)
          | Failed (Tx_rollup_commit_manager_kind, _)
          | Failed (Tx_rollup_dispatch_tickets_manager_kind, _)
          | Failed (Tx_rollup_finalize_commitment_manager_kind, _)
          | Failed (Tx_rollup_origination_manager_kind, _)
          | Failed (Tx_rollup_rejection_manager_kind, _)
          | Failed (Tx_rollup_remove_commitment_manager_kind, _)
          | Failed (Tx_rollup_return_bond_manager_kind, _)
          | Failed (Tx_rollup_submit_batch_manager_kind, _)
          | Skipped Tx_rollup_commit_manager_kind
          | Skipped Tx_rollup_dispatch_tickets_manager_kind
          | Skipped Tx_rollup_finalize_commitment_manager_kind
          | Skipped Tx_rollup_origination_manager_kind
          | Skipped Tx_rollup_rejection_manager_kind
          | Skipped Tx_rollup_remove_commitment_manager_kind
          | Skipped Tx_rollup_return_bond_manager_kind
          | Skipped Tx_rollup_submit_batch_manager_kind
(**# ifdef __META15__ CLOSE_COMMENTS #**)
          | Skipped Dal_publish_slot_header_manager_kind
          | Skipped Delegation_manager_kind
          | Skipped Event_manager_kind
          | Skipped Increase_paid_storage_manager_kind
          | Skipped Origination_manager_kind
          | Skipped Register_global_constant_manager_kind
          | Skipped Reveal_manager_kind
          | Skipped Sc_rollup_add_messages_manager_kind
          | Skipped Sc_rollup_cement_manager_kind
          | Skipped Sc_rollup_execute_outbox_message_manager_kind
          | Skipped Sc_rollup_originate_manager_kind
          | Skipped Sc_rollup_publish_manager_kind
          | Skipped Sc_rollup_recover_bond_manager_kind
          | Skipped Sc_rollup_refute_manager_kind
          | Skipped Sc_rollup_timeout_manager_kind
(**# ifdef __META17__ OPEN_COMMENTS #**)
          | Skipped Set_deposits_limit_manager_kind
(**# ifdef __META17__ CLOSE_COMMENTS #**)
          | Skipped Transfer_ticket_manager_kind
          (**# ifndef __META13__ OPEN_COMMENTS #**)
          | Applied (ITransaction_result (Transaction_to_zk_rollup_result _)) (* behind feature flag *)
          | Backtracked (ITransaction_result (Transaction_to_zk_rollup_result _), _) (* behind feature flag *)
          | Failed (Update_consensus_key_manager_kind, _)
          | Failed (Zk_rollup_origination_manager_kind, _)
          | Failed (Zk_rollup_publish_manager_kind, _)
          | Skipped Update_consensus_key_manager_kind
          | Skipped Zk_rollup_origination_manager_kind
          | Skipped Zk_rollup_publish_manager_kind
          (**# ifndef __META13__ CLOSE_COMMENTS #**)
          (**# ifndef __META14__ OPEN_COMMENTS #**)
          | Failed (Zk_rollup_update_manager_kind, _)
          | Skipped Zk_rollup_update_manager_kind
          (**# ifndef __META14__ CLOSE_COMMENTS #**)
          (**# ifdef __META14__ OPEN_COMMENTS #**)
          | Applied (ITransaction_result (Transaction_to_sc_rollup_result _)) (* already processed above *)
          | Backtracked (ITransaction_result (Transaction_to_sc_rollup_result _), _) (* already processed above *)
          (**# ifdef __META14__ CLOSE_COMMENTS #**)
            ->
            (**# ifndef __META14__ OPEN_COMMENTS #**)
            Verbose.error "Processing of operation failed: %a" Data_encoding.Json.pp (Data_encoding.Json.construct Apply_internal_results.internal_operation_encoding (Apply_internal_results.(Internal_operation {
                (**# ifdef __META16__ sender = #**) source;
                nonce;
                operation})));
            (**# ifndef __META14__ CLOSE_COMMENTS #**)
            assert false (* stupid big bug if this happens *)
        in
        let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in
        Lwt_list.map_s (fun originated_contract ->
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:originated_contract
          )
          originated_contracts
        >>= fun originated_contracts ->
        record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination
        >>= fun destination_id ->
        (**# ifdef __PROTO1__ OPEN_COMMENTS #**)
        begin
        if status = status_applied then
            begin
              process_bigmap_diffs
                conn conn2
                (**# ifndef __PROTO1__ ~bigmap_counter #**)
                ~block_level
                ~bh
                ~opaid:(Some opaid)
                ~iorid:None
                ~sender:(Some source)
                ~sender_pp:source_pp
                ~contract:None (* process_bigmap_diffs does not use contract for other purpose than debug messages *)
                ~senderid:(Some source_id)
                ~contract_id:(Some destination_id)
                ~diffs:lazy_storage_diff
                ~ophid:(Some ophid)
            end
            (**# ifndef __FA2__ OPEN_COMMENTS #**)
            >>= fun () ->
            begin (* self logged *)
              match tokens, destination with
              | true, Destination.Contract destination ->
                let entrypoint = Some (Entrypoint.to_string entrypoint) in
                Tokens.store_token_op ~cctxt ~opaid ~addr_counter ~block_level ~bh ~source_id ~contract_id:destination_id
                  destination source source_pp op_hash op_id entrypoint parameters conn conn2
              | _ -> return_unit
            end >>= caqti_or_fail ~__LOC__
            (**# ifndef __FA2__ CLOSE_COMMENTS #**)
          else
            Lwt.return_unit
        end >>= fun () ->
        (**# ifdef __PROTO1__ CLOSE_COMMENTS #**)
        let entrypoint = Entrypoint.to_string entrypoint in
        let parameters = Some parameters in
        let storage =
          match storage with
          | Some storage -> Some (Script.lazy_expr storage)
          | None -> None in
        begin (* self logged *)
          process_tx
            conn conn2
            ~originated_contracts
            ~opaid
            ~addr_counter
            ~destination_id
            ~nonce:(Some nonce) ~internal
            ~block_level ~bh ~source ~source_id ~op_hash ~op_id ~fee
            ~source_pp
            ~consumed_gas:(consumed_gas : _ option)
            ~storage_size ~paid_storage_size_diff
            ~amount ~destination
            ~entrypoint
            ~balance_updates ~parameters
            ~storage
            ~status
            ~ticket_hash
            (**# ifdef __META13__ ~ticket_receipt #**)
            ~error_list:(error_list_to_json error_list)
        end
      | Origination { delegate; credit; script } ->
        let status, error_list, balance_updates, originated_contracts
            , consumed_gas, storage_size, paid_storage_size_diff
            , lazy_storage_diff =
          match operation_result with
          | Applied (Apply_internal_results.IOrigination_result (* K *)
                       { balance_updates ; originated_contracts ;
                         lazy_storage_diff ;
                         consumed_gas ; storage_size ; paid_storage_size_diff ;
                       }) ->
            (int_of_status operation_result, None, balance_updates, originated_contracts
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
            , lazy_storage_diff)
          | Backtracked (Apply_internal_results.IOrigination_result
                           { balance_updates ; originated_contracts ;
                             lazy_storage_diff ;
                             consumed_gas ; storage_size ; paid_storage_size_diff ;
                           }, error_list_option) ->
            (int_of_status operation_result, error_list_option, balance_updates, originated_contracts
            , Some (normalize_gas consumed_gas), Some storage_size, Some paid_storage_size_diff
            , lazy_storage_diff)
          | Failed (Origination_manager_kind, error_list) ->
            (int_of_status operation_result, Some error_list, []
            , [], None, None, None
            , None)
          | Skipped Origination_manager_kind ->
            (int_of_status operation_result, None, []
            , [], None, None, None
            , None)
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin match delegate with
          | None -> Lwt.return_none
          | Some d ->
            let k = implicit_contract d in
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k
            >>= fun kid -> Lwt.return_some kid
        end >>= fun delegate_id ->
        let script = Some script in
        begin (* self logged *)
          let preorigination = None in
          let preorigination_id = None in
          let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in
          process_origination
            conn conn2
            ~opaid
            ~addr_counter
            (**# ifndef __PROTO1__ ~bigmap_counter #**)
            ~block_level ~delegate_id ~preorigination_id
            ~internal ~nonce:(Some nonce)
            ~bh ~source ~source_id ~op_hash ~op_id ~fee
            (**# ifdef __FA2__ ~tokens ~cctxt #**)
            ~source_pp
            ~delegate
            (**# ifndef __PROTO1__ ~ophid #**)
            credit preorigination script balance_updates originated_contracts
            ~lazy_storage_diff
            ~consumed_gas
            ~storage_size ~paid_storage_size_diff
            ~status
            ~error_list:(error_list_to_json error_list)
        end
      | Delegation maybe_pkh ->
        let status, consumed_gas, error_list =
          match operation_result with
          | Applied (
              Apply_internal_results.IDelegation_result { consumed_gas (**# ifdef __META16__ ; balance_updates = _ (* FIXME *) #**) }
            ) ->
            (int_of_status operation_result
            , Some (normalize_gas consumed_gas)
            , None)
          | Backtracked (
              Apply_internal_results.IDelegation_result { consumed_gas (**# ifdef __META16__ ; balance_updates = _ (* FIXME *) #**) }
            , error_list_option) ->
            (int_of_status operation_result
            , Some (normalize_gas consumed_gas)
            , error_list_option)
          | Failed (Delegation_manager_kind, error_list) ->
            int_of_status operation_result, None, Some error_list
          | Skipped Delegation_manager_kind ->
            int_of_status operation_result, None, None
          | _ -> assert false (* stupid big bug if this happens *)
        in
        begin
          match maybe_pkh with
          | None -> Lwt.return_none
          | Some pkh ->
            let receiver = implicit_contract pkh in
            record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:receiver
            >>= fun addr -> Lwt.return_some addr
        end >>= fun maybe_pkhid ->
        begin (* logged *)
          Verbose.Log.add_delegation block_level bh source source_pp maybe_pkh (consumed_gas: Fpgas.t option) fee;
          Conn.find_opt Delegation_table.insert
            (mtup8 opaid source_id maybe_pkhid consumed_gas
               fee (Some nonce) status (error_list_to_json error_list))
        end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
        return_unit
  (* end of process_internal *)
(**# ifndef __META12__ CLOSE_COMMENTS #**)

end (* of Manager_operation *)

(**# ifndef __META8__ OPEN_COMMENTS #**)
let () = process_implicit_operations_results := (
  fun ~conn ~conn2 ~bh ~level ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) list ->
    let get_kid k = record_k ~__LINE__ ~block_level:level ~addr_counter conn conn2 ~k in
    let id = ref (-1) in
    Lwt_list.iter_s (fun ior -> incr id; begin match ior with
        (* Beware: Apply_results.Successful_manager_result is a GADT constructor.
           The verbose repetition of ```| Apply_results.Successful_manager_result]``` here is because of that. *)
(**# ifndef __META11__ OPEN_COMMENTS #**)
(**#X ifdef __META15__ OPEN_COMMENTS X#**)
      | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_origination_result
             { balance_updates
             ; consumed_gas
             ; originated_tx_rollup (* Tx_rollup.t *)
             } as r) ->
        record_tx_rollup ~__LINE__ ~block_level:level ~addr_counter conn conn2 ~tx_rollup:originated_tx_rollup
        >>= fun originated_tx_rollup ->
        Lwt.return
          (Implicit_operations_results_table.{
              operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = Some originated_tx_rollup
            ; sc_address = None
            ; sc_size = None
            ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
      | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_submit_batch_result
             { balance_updates
             ; paid_storage_size_diff
             ; consumed_gas
             } as r) ->
        Lwt.return
          (Implicit_operations_results_table.{
              operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; sc_address = None
            ; sc_size = None
            ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
      | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_commit_result
             { balance_updates
             ; consumed_gas
             } as r) ->
        Lwt.return
          (Implicit_operations_results_table.{
              operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; sc_address = None
            ; sc_size = None
            ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }, None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**#X ifdef __META15__ CLOSE_COMMENTS X#**)
      | Apply_results.Successful_manager_result
          (Apply_results.Sc_rollup_originate_result
             { balance_updates
             ; consumed_gas
             ; address
             ; size
             (**# ifdef __META13__ ; genesis_commitment_hash #**)
             } as r) ->
        record_sc_rollup ~__LINE__ ~block_level:level ~addr_counter conn conn2 ~sc_rollup:address
        >>= fun address ->
        Lwt.return
          (Implicit_operations_results_table.{
              operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; sc_address = Some address
            ; sc_size = Some size
            ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = Some genesis_commitment_hash #**)
            }, None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
      | Apply_results.Successful_manager_result
          (Apply_results.Sc_rollup_add_messages_result
             { consumed_gas (**# ifndef __META14__ ; inbox_after = _ #**)  } as r) ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; sc_address = None
            ; sc_size = None
            ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }, None,
           Lwt.return)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
      | Apply_results.Successful_manager_result
          (Apply_results.Transaction_result
             ((**#X ifdef __META11__ Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_contract_result X#**)
               { balance_updates
               ; originated_contracts
               ; storage
               ; lazy_storage_diff
               ; consumed_gas
               ; storage_size
               ; paid_storage_size_diff
               ; allocated_destination_contract
               (**# ifdef __META13__ ; ticket_receipt #**)
               }) as r)
        ->
        (**# ifdef __META12__ let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in #**)
        Lwt_list.map_s get_kid originated_contracts
        >>= fun originated_contracts ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            (**# ifdef __META13__ ; ticket_receipt = Some ticket_receipt ; genesis_commitment_hash = None #**)
            ; originated_contracts = Some originated_contracts
            ; storage
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = Some storage_size
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = Some allocated_destination_contract
            ; iorid = !id
            ; block_level = level
            ; strings = (if !Tezos_indexer_lib.Config.no_smart_contract_extraction then None
                         else Misc.flatten_option (Std.Option.map extract_strings_and_bytes_from_expr storage))
            (**# ifdef __META11__ ; originated_tx_rollup = None
               ; sc_size = None ; sc_address = None ; sc_inbox_after = None
               ; tx_rollup_level = None
               ; ticket_hash = None #**)
            }
          , lazy_storage_diff
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**#XX ifndef __META13__ OPEN_COMMENTS XX#**)
       | Apply_results.Successful_manager_result
          (Apply_results.Transaction_result (Apply_internal_results.Transaction_to_zk_rollup_result
             { balance_updates
             ; ticket_hash
             ; consumed_gas
             ; paid_storage_size_diff
             }) as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; tx_rollup_level = None
            ; ticket_hash = Some ticket_hash
            ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; ticket_receipt = None ; genesis_commitment_hash = None
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**#XX ifndef __META13__ CLOSE_COMMENTS XX#**)
(**#XX ifndef __META11__ OPEN_COMMENTS XX#**)
(**#XX ifdef __META16__ OPEN_COMMENTS XX#**)
       | Apply_results.Successful_manager_result
          (Apply_results.Transaction_result (Apply_(**# ifdef __META12__ internal_#**)results.Transaction_to_tx_rollup_result
             { balance_updates
             ; ticket_hash
             ; consumed_gas
             ; paid_storage_size_diff
             }) as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
              operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; tx_rollup_level = None
            ; ticket_hash = Some ticket_hash
            ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
               Verbose.Log.balance_update level bh balance_updates;
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**#X ifdef __META15__ OPEN_COMMENTS X#**)
       | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_return_bond_result
             { balance_updates
             ; consumed_gas
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None
            ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
       | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_finalize_commitment_result
             { balance_updates
             ; consumed_gas
             ; level = tx_rollup_level (* Tx_rollup_level.t (abstract int32) *)
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = Some (Tx_rollup_level.to_int32 tx_rollup_level)
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
       | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_remove_commitment_result
             { balance_updates
             ; consumed_gas
             ; level = tx_rollup_level (* Tx_rollup_level.t *)
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = Some (Tx_rollup_level.to_int32 tx_rollup_level)
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
       | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_rejection_result
             { balance_updates
             ; consumed_gas
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
       | Apply_results.Successful_manager_result
          (Apply_results.Tx_rollup_dispatch_tickets_result
             { balance_updates
             ; consumed_gas
             ; paid_storage_size_diff
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**#X ifdef __META15__ CLOSE_COMMENTS X#**)
       | Apply_results.Successful_manager_result
          (Apply_results.Transfer_ticket_result
             { balance_updates
             ; consumed_gas
             ; paid_storage_size_diff
               (**# ifdef __META14__ ; ticket_receipt #**)
             } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**#X ifdef __META13__ ; ticket_receipt = (**# ifdef __META14__ Some ticket_receipt #**)(**# else None #**) ; genesis_commitment_hash = None X#**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
(**# ifndef __META13__ OPEN_COMMENTS #**)
       | Apply_results.Successful_manager_result Apply_results.Update_consensus_key_result _ -> assert false (* this wouldn't make sense *)
(**# ifndef __META13__ CLOSE_COMMENTS #**)
       | Apply_results.Successful_manager_result
          (Apply_results.((* Sc_rollup_originate_result _ | Sc_rollup_add_messages_result _ | *) Sc_rollup_cement_result _ | Sc_rollup_publish_result _))
        -> assert false (* there's no SC with implicit operations results *)
(**#XX ifndef __META11__ CLOSE_COMMENTS XX#**)
(**#XX ifndef __META13__ OPEN_COMMENTS XX#**)
       | Apply_results.Successful_manager_result                                    (* there's no ZK with implicit operations results *)
          (Apply_results.Zk_rollup_origination_result _)                            (* there's no ZK with implicit operations results *)
       | Apply_results.Successful_manager_result                                    (* there's no ZK with implicit operations results *)
          (Apply_results.Zk_rollup_publish_result _)                                (* there's no ZK with implicit operations results *)
        -> assert false                                                             (* there's no ZK with implicit operations results *)
(**#XX ifndef __META13__ CLOSE_COMMENTS XX#**)
(**#XX ifndef __META12__ OPEN_COMMENTS XX#**)
       | Apply_results.Successful_manager_result
          (Apply_results.Increase_paid_storage_result { balance_updates; consumed_gas } as r)
         (* Does this make sense at all? *)
         ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
            ; tx_rollup_level = None
            ; ticket_hash = None
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
       | Apply_results.Successful_manager_result Sc_rollup_refute_result _ -> assert false (* there's no SC with implicit operations results *)
       | Apply_results.Successful_manager_result Sc_rollup_timeout_result _ -> assert false (* there's no SC with implicit operations results *)
       | Apply_results.Successful_manager_result (Transaction_result (Transaction_to_sc_rollup_result _)) -> assert false (* there's no SC with implicit operations results *)
       | Apply_results.Successful_manager_result ((Dal_publish_slot_header_result { consumed_gas (**# ifdef __META15__ ; slot_header = _FIXME_TODO_FEATURE_FLAG #**) }) as r) ->
         Lwt.return
           (Implicit_operations_results_table.{
	       operation_kind = int_of_manager_operation_result r
             ; originated_contracts = None
             ; storage = None
             ; consumed_gas = normalize_gas consumed_gas
             ; storage_size = None
             ; paid_storage_size_diff = None
             ; allocated_destination_contract = None
             ; iorid = !id
             ; block_level = level
             ; strings = None
             ; originated_tx_rollup = None ; sc_size = None ; sc_address = None ; sc_inbox_after = None
             ; tx_rollup_level = None
             ; ticket_hash = None
             (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
           , None
           , (fun () -> Lwt.return_unit))
       | Successful_manager_result (Sc_rollup_execute_outbox_message_result _)      (* there's no SC with implicit operations results *)
         -> assert false                                                            (* there's no SC with implicit operations results *)
       | Successful_manager_result (Sc_rollup_recover_bond_result _)                (* there's no SC with implicit operations results *)
         -> assert false                                                            (* there's no SC with implicit operations results *)
(**# ifdef __META14__ OPEN_COMMENTS #**)
       | Successful_manager_result (Sc_rollup_dal_slot_subscribe_result _)          (* there's no SC with implicit operations results *)
         -> assert false                                                            (* there's no SC with implicit operations results *)
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
       | Successful_manager_result (Zk_rollup_update_result _)                      (* there's no ZK with implicit operations results *)
         -> assert false                                                            (* there's no ZK with implicit operations results *)
(**# ifndef __META14__ CLOSE_COMMENTS #**)
(**#XX ifndef __META12__ CLOSE_COMMENTS XX#**)
      | Apply_results.Successful_manager_result
          (Apply_results.Origination_result
             { balance_updates
             ; originated_contracts
             ; lazy_storage_diff
             ; consumed_gas
             ; storage_size
             ; paid_storage_size_diff
             } as r)
        ->
        (**# ifdef __META12__ let originated_contracts = List.map (fun k -> Contract.Originated k) originated_contracts in #**)
        Lwt_list.map_s get_kid originated_contracts
        >>= fun originated_contracts ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = Some originated_contracts
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = Some storage_size
            ; paid_storage_size_diff = Some paid_storage_size_diff
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            (**# ifdef __META11__ ; originated_tx_rollup = None
               ; sc_size = None ; sc_address = None ; sc_inbox_after = None
               ; tx_rollup_level = None
               ; ticket_hash = None #**)
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , lazy_storage_diff
          , (fun () ->
(**# ifdef __META16__ OPEN_COMMENTS #**)
               Verbose.Log.balance_update level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates))
      | Apply_results.Successful_manager_result
          (Apply_results.Reveal_result { consumed_gas } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            (**# ifdef __META11__ ; originated_tx_rollup = None
               ; sc_size = None ; sc_address = None ; sc_inbox_after = None
               ; tx_rollup_level = None
               ; ticket_hash = None #**)
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , Lwt.return)
      | Apply_results.Successful_manager_result
          (Apply_results.Delegation_result { consumed_gas (**# ifdef __META16__ ; balance_updates #**) } as r)
        ->
        Lwt.return
          (Implicit_operations_results_table.{
	      operation_kind = int_of_manager_operation_result r
            ; originated_contracts = None
            ; storage = None
            ; consumed_gas = normalize_gas consumed_gas
            ; storage_size = None
            ; paid_storage_size_diff = None
            ; allocated_destination_contract = None
            ; iorid = !id
            ; block_level = level
            ; strings = None
            (**# ifdef __META11__ ; originated_tx_rollup = None
               ; sc_size = None ; sc_address = None ; sc_inbox_after = None
               ; tx_rollup_level = None
               ; ticket_hash = None #**)
            (**# ifdef __META13__ ; ticket_receipt = None ; genesis_commitment_hash = None #**)
            }
          , None
          , (**# ifdef __META16__ (fun () ->
               Balance_table.update ~get_kid ~iorid:!id conn ~block_level:level balance_updates)
               #**)(**# else Lwt.return #**)
          )
(**# ifndef __META9__ OPEN_COMMENTS #**)
      | Apply_results.Successful_manager_result
          (Apply_results.Register_global_constant_result { consumed_gas = _ ; size_of_constant = _ ; balance_updates = _ ; global_address = _ })
        ->
        assert false (* it doesn't make sense to have a new global constant without knowing its value *)
(**# ifndef __META9__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
(**# ifdef __META17__ OPEN_COMMENTS #**)
      | Apply_results.Successful_manager_result
          (Apply_results.Set_deposits_limit_result { consumed_gas = _ })
        ->
        assert false (* it doesn't make sense to have a deposits limit without knowing its value *)
(**# ifdef __META17__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ CLOSE_COMMENTS #**)
      end >>= fun (implicit_operations_results, lazy_storage_diff, do_balance_updates) ->
      let module Conn = (val conn : Caqti_lwt.CONNECTION) in
      Conn.find_opt
        Implicit_operations_results_table.insert implicit_operations_results
      >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
      do_balance_updates () >>= fun () ->
      Manager_operation.process_bigmap_diffs
        ?script:None
        conn conn2
        (**# ifndef __PROTO1__ ~bigmap_counter #**)
        ~block_level:implicit_operations_results.block_level
        ~bh
        ~opaid:None
        ~iorid:(Some implicit_operations_results.iorid)
        ~sender:None
        ~sender_pp:Obj.magic (* it's kind of OK since [sender = None] *)
        ~contract:None
        ~senderid:None
        ~contract_id:None
        ~diffs:lazy_storage_diff
        ~ophid:None
      )
      list
  )
(**# ifndef __META8__ CLOSE_COMMENTS #**)

let process_proposals ~opal_counter ~opaid ~addr_counter ~conn conn2 ~op_hash:_ ~op_id:_ ~bh:_ ~block_level ~source ~period ~proposals =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (* FIXME: log me *)
  (**# ifndef __META6__ let period = Voting_period.to_int32 period in #**)
  record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:source >>= fun source ->
  match proposals with
  | [] -> return_unit
  | proposal::tl ->
    Proposals_table.(Conn.find_opt insert { opaid; source; period; proposal; i = opal_counter() })
    >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) ->
    Lwt_list.iter_s (fun proposal ->
        Proposals_table.(Conn.find_opt insert2 { opaid; source; period; proposal; i = opal_counter() })
        >>= caqti_or_fail ~__LOC__ >>= fun (None|Some()) -> Lwt.return_unit
      ) tl
    >>= fun () ->
    return_unit

let process_ballot ~opal_counter ~opaid ~addr_counter ~conn conn2 ~op_hash:_ ~op_id:_ ~bh:_ ~block_level ~source ~period ~proposal ~ballot =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  (**# ifndef __META6__ let period = Voting_period.to_int32 period in #**)
  (* FIXME: log me *)
  record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:source >>= fun source ->
  Ballot_table.(Conn.find_opt insert { opaid; source; period; proposal; ballot; i = opal_counter() })
  >>= caqti_or_fail ~__LOC__ >>= fun _ ->
  return_unit


(**# ifdef __META16__ OPEN_COMMENTS #**)
let get_offender_id ~block_level ~addr_counter conn conn2 balance_updates =
  match
    List.find_opt (function (_, Delegate.Debited _ (**# ifdef __META7__ , _ #**)) -> true | _ -> false) balance_updates
  with
  | None ->
    Verbose.error "Error: inconsistent data (balance updates). Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
    Stdlib.exit 1
  | Some (balance, _(**# ifdef __META7__ , _ #**)) -> match balance with
    | Delegate.Contract k ->
      record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k
      >>= fun id -> Lwt.return_some id
(**# ifdef __META10__ OPEN_COMMENTS #**)
    | Rewards (pkh, _)
    | Fees (pkh, _)
    | Deposits (pkh, _) ->
      record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh
      >>= fun id -> Lwt.return_some id
(**# ifdef __META10__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
    | Block_fees
    | Nonce_revelation_rewards
(**# ifdef __META16__ OPEN_COMMENTS #**)
    | Double_signing_evidence_rewards
    | Endorsing_rewards
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
    | Attesting_rewards
(**# ifndef __META16__ CLOSE_COMMENTS #**)
    | Baking_rewards
    | Baking_bonuses
    | Storage_fees
    | Double_signing_punishments
    | Liquidity_baking_subsidies
    | Burned
    | Bootstrap
    | Invoice
    | Initial_commitments
    | Commitments _
    | Minted -> Lwt.return_none
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifdef __META12__
    | Sc_rollup_refutation_punishments -> Lwt.return_none #**)
(**# ifdef __META13__
    | Sc_rollup_refutation_rewards -> Lwt.return_none #**)
(**# ifndef __META11__ OPEN_COMMENTS #**)
    | Deposits (**# ifndef __META16__ pkh #**)(**# else (Single_staker { delegate = pkh ; staker = _ (* FIXME *) } | Shared_between_stakers { delegate = pkh } | Baker pkh) #**) ->
      record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh >>= fun id -> Lwt.return_some id
    | Frozen_bonds (k, _ (* bond *)) ->
      record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k >>= fun id -> Lwt.return_some id
(**# ifdef __META16__ OPEN_COMMENTS #**)
    | Tx_rollup_rejection_rewards
    | Tx_rollup_rejection_punishments -> Lwt.return_none
    | Lost_endorsing_rewards (pkh, _, _) ->
      record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh >>= fun id -> Lwt.return_some id
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
    | Unstaked_deposits ((Single (_ (* k *), pkh) | Shared pkh), _ (* cycle *))
    | Staking_delegate_denominator { delegate = pkh }
    | Lost_attesting_rewards (pkh, _, _) ->
      record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh >>= fun id -> Lwt.return_some id
    | Staking_delegator_numerator { delegator = k } ->
       record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k >>= fun id -> Lwt.return_some id
(**# ifndef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META11__ CLOSE_COMMENTS #**)
(**# ifndef __PROTO12__ OPEN_COMMENTS #**)
    | Deposits pkh
    | Legacy_rewards (pkh, _ (* cycle *))
    | Legacy_deposits (pkh, _)
    | Legacy_fees (pkh, _)
    | Lost_endorsing_rewards (pkh, _, _) ->
      record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh >>= fun id -> Lwt.return_some id
(**# ifndef __PROTO12__ CLOSE_COMMENTS #**)
(**# ifdef __META16__ CLOSE_COMMENTS #**)

(**# ifndef __META16__ OPEN_COMMENTS #**)
let get_offender_id ~block_level:_ ~addr_counter:_ _conn _conn2 _balance_updates =
  Lwt.return_none
(**# ifndef __META16__ CLOSE_COMMENTS #**)



let baker_id_of_block cctxt conn conn2 lvl =
  (* This is a little bit messy because of segment indexing: a seed nonce endorsement may
     refer to a block far enough in the past to be a block of the previous protocol *)
  (**#X ifdef __TRANSITION__ (**# ifdef __PROTO1__ let open Tezos_indexer_lib.BS in #**)X#**)
  (**# ifndef __PROTO1__ let open Tezos_indexer_lib.BS in #**)
  let open BS in
  !- "info cctxt ~block:(`Level lvl) ()" @@
  info cctxt ~block:(`Level lvl) ()
  >>= function
  | Ok { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
      metadata = Some ({ protocol_data = {
             baker (**# ifdef __META13__ = { consensus_pkh = baker; delegate = _ } #**)
           ; _ }
                       ; _ } : BS.block_metadata) ; operations = _ } ->
    record_pkh ~__LINE__
      ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
      ~block_level:lvl conn conn2 ~pkh:baker >>= (* 1 *)
    begin function
      | baker_id ->
        return baker_id
    end
  | Ok {header=_; metadata=None; _ } ->
    Verbose.error "Error: failed to get block %ld metadata from node because it's unavailable" lvl;
    Stdlib.exit 1
  | Error _e ->
    Verbose.warn "Error: failed to get block %ld metadata from node: %a" lvl pp_print_trace _e;
    !- "info cctxt ~block:(`Level lvl) ()" @@
    (**#! ifdef PPBS (**# get PPBS #**).!#**)(**# else BS. #**)info cctxt ~block:(`Level lvl) ()
    >>= function
    | Ok { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
        metadata = Some ({ protocol_data = {
               baker (**#! ifdef PPBS (**# ifdef __META14__ = { consensus_pkh = baker ; delegate = _ } #**) !#**)
             ; _ } ; _ } : (**#! ifdef PPBS (**# get PPBS #**).!#**)(**# else BS. #**)block_metadata) ; operations = _ } ->
      (**# ifdef __PROTO16__ let baker = Tezos_crypto.Signature.Of_V0.public_key_hash baker in (* 1 *) #**)
      record_pkh ~__LINE__
        ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
        ~block_level:lvl conn conn2 ~pkh:baker >>= (* 2 *)
      begin function
        | baker_id ->
          return baker_id
      end
    | Ok {header=_; metadata=None; _ } ->
      Verbose.error "Error: failed to get block %ld metadata from node because it's unavailable" lvl;
      Stdlib.exit 1
    | Error _e ->
      Verbose.warn "Error: failed to get block %ld metadata from node: %a" lvl pp_print_trace _e;
      !- "info cctxt ~block:(`Level lvl) ()" @@
      (**#! ifdef PBS (**# get PBS #**).!#**)(**# else BS. #**)info cctxt ~block:(`Level lvl) ()
      >>= function
      | Ok { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
             metadata = Some ({ protocol_data = {
                 baker
               (**# ifdef __TRANSITION_15_16__ = { consensus_pkh = baker ; delegate = _ } #**)
               (**# elseifdef __TRANSITION_15_A__ = { consensus_pkh = baker ; delegate = _ } #**)
               (**# elseifdef __META14__ = { consensus_pkh = baker ; delegate = _ } #**)
               ; _ } ; _ } : (**#! ifdef PBS (**# get PBS #**).!#**)(**# else BS. #**)block_metadata) ; operations = _ } ->
        (**#! ifndef __TRANSITION__
           (**# ifdef __PROTO16__ let baker = Tezos_crypto.Signature.Of_V0.public_key_hash baker in (* 2 *) #**)
        !#**)
        record_pkh ~__LINE__
          ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
          ~block_level:lvl conn conn2 ~pkh:baker >>= (* 3 *)
        begin function
          | baker_id ->
            return baker_id
        end
      | Ok {header=_; metadata=None; _ } ->
        Verbose.error "Error: failed to get block %ld metadata from node because it's unavailable" lvl;
        Stdlib.exit 1
      | Error _e ->
        (**# ifndef __PROTOA__ OPEN_COMMENTS In protocol alpha, sometimes we need to jump back 2 protocols... #**)
        Verbose.warn "Error: failed to get block %ld metadata from node: %a" lvl pp_print_trace _e;
        !- "info cctxt ~block:(`Level lvl) ()" @@
        (**# tryget APBS #**).info cctxt ~block:(`Level lvl) ()
        >>= function
        | Ok {header=_; metadata=None; _ } ->
          Verbose.error "Error: failed to get block %ld metadata from node because it's unavailable" lvl;
          Stdlib.exit 1
        | Ok { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
               metadata = Some ({ protocol_data = {
                   baker = { consensus_pkh = baker ; delegate = _ }
                 ; _ } ; _ } : (**# tryget APBS #**).block_metadata) ; operations = _ } ->
          (**# ifndef __META16__ let baker = Tezos_crypto.Signature.Of_V0.public_key_hash baker in (* 3 *) #**)
          record_pkh ~__LINE__
            ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
            ~block_level:lvl conn conn2 ~pkh:baker >>= (* 4 *)
          begin function
            | baker_id ->
              return baker_id
          end
        | Error _e ->
          Verbose.warn "Error: failed to get block %ld metadata from node: %a" lvl pp_print_trace _e;
          !- "info cctxt ~block:(`Level lvl) ()" @@
          (**# tryget APPBS #**).info cctxt ~block:(`Level lvl) ()
          >>= function
          | Ok {header=_; metadata=None; _ } ->
            Verbose.error "Error: failed to get block %ld metadata from node because it's unavailable" lvl;
            Stdlib.exit 1
          | Ok { chain_id = _ ; hash = _ ; header = { shell = _ ; protocol_data = _ };
                 metadata = Some ({ protocol_data = {
                     baker = { consensus_pkh = baker ; delegate = _ }
                   ; _ } ; _ } : (**# tryget APPBS #**).block_metadata) ; operations = _ } ->
            (**# ifndef __META16__ let baker = Tezos_crypto.Signature.Of_V0.public_key_hash baker in (* 4 *) #**)
            record_pkh ~__LINE__
              ~addr_counter:(fun () -> Int64.add Misc.min_int53 (Int64.of_int32 lvl))
              ~block_level:lvl conn conn2 ~pkh:baker >>= (* 4 *)
            begin function
              | baker_id ->
                return baker_id
            end
          | Error _e ->
            (**# ifndef __PROTOA__ CLOSE_COMMENTS #**)
            Verbose.error "Error: failed to get block %ld metadata from node... %a" lvl pp_print_trace _e;
            Stdlib.exit 1


let store_op_contents :
  type a b.
  opaid:int64 ->
  baker_id:int64 ->
  (**# ifndef __PROTO1__ bigmap_counter:_ -> #**)
  opal_counter:_ ->
  addr_counter:_ ->
  (module Caqti_lwt.CONNECTION) ->
  (module Caqti_lwt.CONNECTION) ->
  #rpc_context ->
  (**# ifdef __FA2__ tokens:bool -> #**)
  block_level:int32 ->
  ophid:int64 ->
  Operation_hash.t -> int -> Block_hash.t ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun
    ~opaid ~baker_id
    (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter ~addr_counter
    conn conn2 cctxt
    (**# ifdef __FA2__ ~tokens #**)
    ~block_level ~ophid op_hash op_id bh contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let get_kid k = record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k in
  match contents, meta with
  | Seed_nonce_revelation { level ; nonce },
    Apply_results.Seed_nonce_revelation_result balance_updates ->
    let level = Raw_level.to_int32 level in
    begin
      (* this is required when indexing by segments since it's possible the referred block
         hasn't been indexed yet. FIXME: TODO: if not indexing by segment, get the value from
         the DB instead of making an RPC call. Or at least use disk cache.... *)
      baker_id_of_block cctxt conn conn2 level
    end >>= warn_if_error ~__LOC__ >>=? fun sender_id ->
    begin (* logged *)
      (**# ifdef __META16__ OPEN_COMMENTS #**)
      Verbose.Log.balance_update
        ~op:(op_hash, op_id, 0) block_level bh balance_updates;
      (**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun _ ->
    begin (* logged *)
      Verbose.Log.seed_nonce_revelation block_level bh op_hash op_id ~level ~nonce;
      Conn.find_opt Seed_nonce_revelation_table.insert
        { level; sender_id ; baker_id ; nonce ; opaid }
    end
    >>= caqti_or_fail ~__LOC__ >>= fun ((Some ())|None) -> return_unit
  (**# ifdef __META16__ OPEN_COMMENTS #**)
  | Double_endorsement_evidence { op1 ; op2 (**#X ifdef __META7__ (**# ifndef __META10__ ; slot = _ #**) X#**) },
    Apply_results.Double_endorsement_evidence_result balance_updates
    (**# ifdef __META16__ CLOSE_COMMENTS #**)
    (**# ifndef __META16__ OPEN_COMMENTS #**)
  | Double_attestation_evidence { op1 ; op2 },
    Apply_results.Double_attestation_evidence_result { forbidden_delegate = _ ; balance_updates }
    (**# ifndef __META16__ CLOSE_COMMENTS #**)
    ->
    begin (* logged *)
      (**# ifdef __META16__ OPEN_COMMENTS #**) Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates; (**# ifdef __META16__ CLOSE_COMMENTS #**)
      (**# ifndef __META16__ OPEN_COMMENTS #**)
      let balance_updates  = (balance_updates : Alpha_context.Receipt.balance_updates) in
      (**# ifndef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun _ ->
    get_offender_id ~block_level ~addr_counter conn conn2 balance_updates
    >>= fun offender_id ->
    begin (* logged *)
      Verbose.Log.double_endorsement_evidence block_level bh op_hash op_id;
      Double_endorsement_evidence_table.(
        Conn.find_opt insert
          { op1 = Alpha_context.Operation.pack op1
          ; op2 = Alpha_context.Operation.pack op2
          ; offender_id
          ; baker_id
          ; opaid }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
  | Double_baking_evidence { bh1 ; bh2 },
    Apply_results.Double_baking_evidence_result
    (**# ifndef __META16__ balance_updates #**)(**# else { balance_updates ; forbidden_delegate = _ } #**) ->
    begin (* logged *)
      (**# ifdef __META16__ OPEN_COMMENTS #**) Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates; (**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun _ ->
    get_offender_id ~block_level ~addr_counter conn conn2 balance_updates
    >>= fun offender_id ->
    begin (* logged *)
      Verbose.Log.double_baking_evidence block_level bh op_hash op_id
        ~baking_level:bh1.Block_header.shell.level;
      Double_baking_evidence_table.(
        Conn.find_opt insert
          { bh1 ; bh2 ; opaid ; baker_id ; offender_id ; }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
  | Activate_account { id; activation_code; },
    Apply_results.Activate_account_result balance_updates ->
    begin
      let k = implicit_contract (Ed25519 id) in
      record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k >>= fun kid ->
      begin (* logged *)
        Verbose.Log.insert_activated block_level bh (Ed25519 id) op_hash;
        let activation_code =
          Format.asprintf "%a"
            Data_encoding.Json.pp
            (Data_encoding.Json.construct Blinded_public_key_hash.activation_code_encoding activation_code)
        in
        Conn.find_opt Activation_table.insert (opaid, kid, activation_code)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      (* Update balances *)
      begin (* logged *)
        (**# ifdef __META16__ OPEN_COMMENTS #**) Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates; (**# ifdef __META16__ CLOSE_COMMENTS #**)
        Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
      end >>= fun _ ->
      return_unit
    end
(**# ifndef __META13__ OPEN_COMMENTS #**)
  | Drain_delegate { delegate ; destination ; consensus_key },
    Apply_results.Drain_delegate_result
      { balance_updates ; allocated_destination_contract }
    ->
    begin (* logged *)
      (**# ifdef __META16__ OPEN_COMMENTS #**) Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates; (**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun _ ->
    record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:delegate >>= fun delegate ->
    record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:destination >>= fun destination ->
    record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:consensus_key >>= fun consensus_key ->
    begin (* logged *)
      (* Verbose.Log.; TODO *)
      Drain_delegate_table.(
        Conn.find_opt insert
          { opaid
          ; delegate
          ; destination
          ; consensus_key
          ; allocated_destination_contract
          }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
(**# ifndef __META13__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
  | Double_preendorsement_evidence { op1 ; op2 (**#X ifdef __META7__ (**# ifndef __META10__ ; slot = _ #**) X#**) },
    Apply_results.Double_preendorsement_evidence_result balance_updates
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
  | Double_preattestation_evidence { op1 ; op2 },
    Apply_results.Double_preattestation_evidence_result { balance_updates ; forbidden_delegate = _ }
(**# ifndef __META16__ CLOSE_COMMENTS #**)
    ->
    begin (* logged *)
      (**# ifdef __META16__ OPEN_COMMENTS #**) Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates; (**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun _ ->
    get_offender_id ~block_level ~addr_counter conn conn2 balance_updates
    >>= fun offender_id ->
    begin (* logged *)
      Verbose.Log.double_endorsement_evidence ~pre:true block_level bh op_hash op_id;
      Double_preendorsement_evidence_table.(
        Conn.find_opt insert
          { op1 = Alpha_context.Operation.pack op1 ;
            op2 = Alpha_context.Operation.pack op2 ;
            offender_id ;
            baker_id ;
            opaid }
      ) >>= caqti_or_fail ~__LOC__ >>= fun _ -> return_unit
    end
(**# ifndef __META10__ CLOSE_COMMENTS #**)

(**#P ifdef __META7__
(**#A ifndef __META10__
  | Endorsement_with_slot { endorsement = { shell = {branch = _} ; protocol_data = {contents = Single (Endorsement {level}); signature = _} } ; slot = _ },
    Endorsement_with_slot_result Apply_results.Endorsement_result { balance_updates; delegate; slots }
A#**) P#**)
(**# ifndef __META10__ OPEN_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
  | Preendorsement { level ; slot ; round = _ ; block_payload_hash = _ },
    Apply_results.Preendorsement_result { balance_updates ; delegate
   (**# ifndef __META16__ ; preendorsement_power = _ #**)(**# else ; consensus_power = _ #**) (* FIXME *)
   (**# ifdef __META13__ ; consensus_key = _ #**) (* FIXME *) }
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
  | Preattestation { level ; slot ; round = _ ; block_payload_hash = _ },
    Apply_results.Preattestation_result { balance_updates ; delegate
   (**# ifndef __META16__ ; preattestation_power = _ #**)(**# else ; consensus_power = _ #**) (* FIXME *)
   (**# ifdef __META13__ ; consensus_key = _ #**) (* FIXME *) }
(**# ifndef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META10__ CLOSE_COMMENTS #**)
(**# ifdef __META16__ OPEN_COMMENTS #**)
  | Endorsement { level (**#X ifdef __META10__ ; slot ; round = _ ; block_payload_hash = _ X#**) }, (* FIXME *)
    Apply_results.Endorsement_result { balance_updates ;
                                       delegate
                                       (**# ifndef __META10__ ; slots #**)(**# elseifdef __META16__ ; consensus_power = _ #**)(**# else ; endorsement_power = _ #**)
                                       (**# ifdef __META13__ ; consensus_key = _ #**) (* FIXME *)
                                     }
(**# ifdef __META16__ CLOSE_COMMENTS #**)
(**# ifndef __META16__ OPEN_COMMENTS #**)
  | Attestation { level (**#X ifdef __META10__ ; slot ; round = _ ; block_payload_hash = _ X#**) }, (* FIXME *)
    Apply_results.Attestation_result { balance_updates ;
                                       delegate
                                       (**# ifndef __META10__ ; slots #**)(**# elseifdef __META16__ ; consensus_power = _ #**)(**# else ; attestation_power = _ #**)
                                       (**# ifdef __META13__ ; consensus_key = _ #**) (* FIXME *)
                                     }
(**# ifndef __META16__ CLOSE_COMMENTS #**)
->
(**#P ifdef __META7__
(**#A ifndef __META10__
    let slots = match contents with Endorsement_with_slot { slot ; _ } -> slot :: slots | _ -> slots in
A#**) P#**)
    let round, block_payload_hash =
      (**#X ifdef __META10__
         match contents with
         | (**# ifndef __META16__ Preendorsement #**)(**# else Preattestation #**) { round ; block_payload_hash ; _ }
         | (**# ifndef __META16__ Endorsement #**)(**# else Attestation #**) { round ; block_payload_hash ; _ } -> Some (Round.to_int32 round), Some (Format.asprintf "%a" Block_payload_hash.pp block_payload_hash)
         | _ -> None, None
         X#**)
      (**# else
         None, None
         #**)
    and endorsement_or_preendorsement_power =
      (**# ifndef __META16__ OPEN_COMMENTS #**)
         match meta with
         | Apply_results.Attestation_result { consensus_power = attestation_power ; _ } -> Some attestation_power
         | Apply_results.Preattestation_result { consensus_power = preattestation_power ; _ } -> Some preattestation_power
         | _ -> None
      (**# ifndef __META16__ CLOSE_COMMENTS #**)
      (**# ifndef __META10__ OPEN_COMMENTS #**)
        (**# ifdef __META16__ OPEN_COMMENTS #**)
         match meta with
         | Apply_results.Endorsement_result { endorsement_power ; _ } -> Some endorsement_power
         | Apply_results.Preendorsement_result { preendorsement_power ; _ } -> Some preendorsement_power
         | _ -> None
        (**# ifdef __META16__ CLOSE_COMMENTS #**)
      (**# ifndef __META10__ CLOSE_COMMENTS #**)
      (**# ifdef __META10__ OPEN_COMMENTS #**)
         None
      (**# ifdef __META10__ CLOSE_COMMENTS #**)
    and pre =
      (**#X ifdef __META16__ match contents with Preattestation _ -> true | _ -> false X#**)
      (**#X elseifdef __META10__ match contents with Preendorsement _ -> true | _ -> false X#**)
      (**# else false #**)
    in

    (**# ifdef __META10__
       (* let slots = [(Slot_repr.to_int: Slot_repr.t -> int) (slot: Slot_repr.t)] *)
       let slots = [ Obj.magic slot ]
       in #**)
    (* Update endorsements *)
    let level = Raw_level.to_int32 level in
    record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:delegate >>= fun delegate_id ->
    begin (* logged *)
      Verbose.Log.endorsement_result ~pre block_level bh
        op_hash op_id ~level ~delegate ~slots ~endorsement_or_preendorsement_power;
      Conn.find_opt (Endorsement_or_preendorsement_tables.insert ~pre)
        { level; delegate_id; slots; opaid; endorsement_or_preendorsement_power; round; block_payload_hash  }
    end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    (* Update balances *)
    begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid ~opaid conn ~block_level balance_updates
    end >>= fun () ->
    return_unit
  | Manager_operation { source; fee; operation; gas_limit; storage_limit ; counter },
    Apply_results.Manager_operation_result {
      balance_updates ; (* Correspond to the operation fees *)
      operation_result;
      internal_operation_results ; (* matched and processed later *)
    } ->
    begin
      (**# ifdef __MILLIGAS__
           let gas_limit = Gas.Arith.integral_to_z gas_limit in
           #**)
      begin (* logged *)
        Verbose.Log.manager_numbers block_level bh ~op_hash ~op_id
          ~counter_pp:(**# ifdef __META14__ Manager_counter.pp #**)(**# else Z.pp_print #**) ~counter ~gas_limit ~storage_limit;
        Conn.find_opt Manager_numbers.insert (opaid, counter, gas_limit, storage_limit)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      (**# ifdef __META1__ record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:source #**)
      (**# else record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:source #**)
      >>= fun source_id ->
      (**# ifdef __META14__ let source = Destination.Contract (implicit_contract source) in #**)
      (**# elseifdef __META3__ let source = (implicit_contract source) in #**)
      (**# elseifdef __META6__ let source = (implicit_contract source) in #**)
      begin (* self logged *)
        Manager_operation.process conn conn2
          ~opaid
          ~addr_counter
          (**# ifndef __PROTO1__ ~bigmap_counter #**)
          (**# ifdef __FA2__ ~tokens ~cctxt #**)
          ~block_level
          (**# ifndef __PROTO1__ ~ophid #**)
          ~internal:0 ~source_id
          bh op_hash op_id
          source (Some fee) operation operation_result (* 1 *)
      end >>= warn_if_error ~__LOC__ >>=? fun () ->
      begin
        let internal = ref 0 in
        let process_internal_op_results internal_op_res =
          list_iter_es
            (function
              | Apply_(**# ifdef __META12__ internal_#**)results.(**# ifdef __META13__ Internal_operation_result #**)(**# elseifdef __META11__ Internal_manager_operation_result #**)(**# else Internal_operation_result #**)
                  ({ (**# ifdef __META16__ sender = #**) source; operation; nonce; }, operation_result) ->
                (* source is Destination.t from proto 16, but Contract.t before *)
                (**# ifdef __META12__ #**)
                (**# elseifdef __META11__ let operation = Apply_results.manager_operation_of_internal_operation operation in #**)
                incr internal;
                let internal = !internal in
                (**# ifdef __META14__ record_destination ~__LINE__ ~block_level ~addr_counter conn conn2 ~destination:source #**)
                (**# else record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k:source (* 2 *) #**)
                >>= fun source_id ->
                let opaid = opal_counter() in
                begin (* logged *)
                  Verbose.Log.operation_alpha_internal block_level bh op_hash op_id internal;
                  Conn.find_opt Operation_alpha_table.insert @@
                  mtup6 ophid op_id (int_of(**# ifdef __META12__ _internal#**)_manager_operation operation) block_level internal opaid
                end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
                begin (* self logged *)
                  (**# ifdef __META12__ Manager_operation.process_internal#**)(**# else Manager_operation.process #**)
                    conn
                    conn2
                    ~opaid
                    ~addr_counter
                    (**# ifndef __PROTO1__ ~bigmap_counter #**)
                    ~block_level
                    ~internal ~source_id
                    (**# ifndef __PROTO1__ ~ophid #**)
                    (**# ifdef __FA2__ ~tokens ~cctxt #**)
                    ~nonce
                    bh op_hash op_id
                    source
                    (**# ifndef __META12__ None (*fee*) #**)
                    operation operation_result (* 2 *)
                end >>= fun _ ->
                return_unit
            )
            internal_op_res
        in
        begin (* self logged *)
          process_internal_op_results internal_operation_results
        end
      end >>= warn_if_error ~__LOC__ >>=? fun () ->
      (* Update balances (global operation fee) *)
      begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
        Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
        Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
      end >>= fun () ->
      return_unit
    end

  (* failed/skipped/backtracked manager operation, ignore for now... *)
  | Proposals { source; period; proposals; },
    Proposals_result ->
    process_proposals ~opal_counter ~addr_counter ~conn conn2 ~op_hash ~opaid ~op_id ~bh ~block_level ~source ~period ~proposals
  | Ballot { source; period; proposal; ballot },
    Ballot_result ->
    process_ballot ~opal_counter ~addr_counter ~conn conn2 ~op_hash ~opaid ~op_id ~bh ~block_level ~source ~period ~proposal ~ballot
  | Manager_operation _, (
      Proposals_result
    | Ballot_result
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    | Seed_nonce_revelation_result _
    | Double_baking_evidence_result _
    | Activate_account_result _)
  | (**# ifndef __META16__ Endorsement #**)(**# else Attestation #**) _, (
      Proposals_result
    |  Ballot_result
    |  Seed_nonce_revelation_result _
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    |  Double_baking_evidence_result _
    |  Activate_account_result _
    |  Manager_operation_result _)
  | Activate_account _, (
      Ballot_result
    | Double_baking_evidence_result _
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    | Manager_operation_result _
    | Proposals_result
    | Seed_nonce_revelation_result _
    )
  | Double_baking_evidence _, (
      Proposals_result
    | Ballot_result
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | (**# ifndef __META16__ Double_endorsement_evidence #**)(**# else Double_attestation_evidence #**) _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    | Manager_operation_result _
    | Seed_nonce_revelation_result _
    | Activate_account_result _
    )
  | Seed_nonce_revelation _, (
      Proposals_result
    | Ballot_result
    | Double_baking_evidence_result _
    | Manager_operation_result _
    | Activate_account_result _
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    )
  | Proposals _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Seed_nonce_revelation_result _
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    )
  | Ballot _, (
      Proposals_result
    | Double_baking_evidence_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Seed_nonce_revelation_result _
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    )
(**#! ifdef __META10__
  | (**# ifndef __META16__ Preendorsement #**)(**# else Preattestation #**) _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Seed_nonce_revelation_result _
    | Proposals_result
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    )
  | (**# ifndef __META16__ Double_preendorsement_evidence #**)(**# else Double_preattestation_evidence #**) _, (
      Ballot_result
    | Double_baking_evidence_result _
    | Manager_operation_result _
    | Activate_account_result _
    | Seed_nonce_revelation_result _
    | Proposals_result
    (**# ifndef __META16__ | Endorsement_result _ #**)(**# else | Attestation_result _ #**)
    (**# ifndef __META16__ | Double_endorsement_evidence_result _ #**)(**# else | Double_attestation_evidence_result _ #**)
    )
!#**)
    -> assert false (* stupid big bug if this happens *)
(**# ifdef __META13__
   | (Drain_delegate _, _)
   | (_, Drain_delegate_result _) -> assert false
#**)
(**# ifndef __META12__ OPEN_COMMENTS #**)
  | Vdf_revelation {solution (* : Seed.vdf_solution *)},
    Vdf_revelation_result balance_updates ->
    begin (* logged *)
(**# ifdef __META16__ OPEN_COMMENTS #**)
      Verbose.Log.balance_update ~op:(op_hash, op_id, 0) block_level bh balance_updates;
(**# ifdef __META16__ CLOSE_COMMENTS #**)
      Balance_table.update ~get_kid conn ~block_level ~opaid balance_updates
    end >>= fun () ->
    Conn.find_opt Vdf_revelation_table.insert (opaid, solution)
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | _, (Vdf_revelation_result _) -> assert false
(**# ifdef __META14__ OPEN_COMMENTS #**)
  | _, (Dal_slot_availability_result _) -> assert false
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifdef __META14__ OPEN_COMMENTS #**)
  | Dal_slot_availability (_, _), _ -> assert false
(**# ifdef __META14__ CLOSE_COMMENTS #**)
(**# ifndef __META14__ OPEN_COMMENTS #**)
  (* | Dal_slot_availability { endorser = _ ; slot_availability = _ ; level = _ ; }, _ -> assert false (\* TODO/FIXME *\) *)
  | Dal_attestation {
      (**# ifndef __META16__ attestor (* : Signature.Public_key_hash.t *); #**)
      attestation (* : Dal.Attestation.t *);
      level (* : Raw_level.t *);
      (**# ifdef __META16__ slot = _; #**)
    }, Dal_attestation_result { delegate (* : Signature.Public_key_hash.t *) } ->
    record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:delegate >>= fun delegate_id ->
    (**# ifndef __META16__ record_pkh ~__LINE__ ~block_level ~addr_counter conn conn2 ~pkh:attestor >>= fun attestor_id -> #**)
    Conn.find_opt
      Dal_attestation_table.insert
      (**# ifndef __META16__ ((opaid, attestor_id, attestation, Raw_level.to_int32 level), delegate_id) #**)
      (**# ifdef __META16__ (opaid, attestation, Raw_level.to_int32 level, delegate_id) #**)
    >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    return_unit
  | _, Dal_attestation_result _ -> assert false (* can never happen *)
  | Dal_attestation _, _ -> assert false (* can never happen *)
(**# ifndef __META14__ CLOSE_COMMENTS #**)
  | Vdf_revelation _, _ -> assert false (* FIXME: add explanation *)
(**# ifndef __META12__ CLOSE_COMMENTS #**)
(**#x ifdef __META7__
   | (Failing_noop _, _) -> return_unit (* should never happen though? *)
(**#A ifndef __META10__
   | (_, Endorsement_with_slot_result _) -> assert false  (* stupid big bug if this happens *)
   | (Endorsement_with_slot _, _) -> assert false  (* stupid big bug if this happens *)
A#**)(**# elseifdef __META16__ | (_, (Preattestation_result _ | Double_preattestation_evidence_result _)) ->
   assert false (* stupid big bug if this happens *)
  #**)(**# else | (_, (Preendorsement_result _ | Double_preendorsement_evidence_result _)) ->
   assert false (* stupid big bug if this happens *)
  #**)

x#**)


let store_op_id :
  type a b.
  baker_id:int64 ->
  (**# ifndef __PROTO1__ bigmap_counter:(int32 -> int64) -> #**)
  opal_counter:(unit -> int64) ->
  addr_counter:(unit -> int64) ->
  (module Caqti_lwt.CONNECTION) ->
  (module Caqti_lwt.CONNECTION) ->
  #rpc_context ->
  (**# ifdef __FA2__ tokens:bool -> #**)
  block_level:int32 ->
  ophid:int64 ->
  Operation_hash.t -> int -> Block_hash.t ->
  a contents -> b Apply_results.contents_result -> unit tzresult Lwt.t =
  fun ~baker_id (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter ~addr_counter
    conn conn2 cctxt
    (**# ifdef __FA2__ ~tokens #**)
    ~block_level ~ophid op_hash op_id bh contents meta ->
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let opaid = opal_counter() in
  begin (* logged *)
    Verbose.Log.operation_alpha_preinsert block_level bh op_hash op_id (string_of_contents contents);
    Conn.find_opt Operation_alpha_table.insert
      ((ophid, op_id, int_of_contents contents, block_level), 0, opaid)
  end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
    store_op_contents ~baker_id ~opaid ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter conn conn2 cctxt
      (**# ifdef __FA2__ ~tokens #**)
      ~block_level ~ophid op_hash op_id bh contents meta


let store_op ~baker_id ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter ~op_counter conn conn2 cctxt
    (**# ifdef __FA2__ ~tokens #**)
    ~block_level bh
    ({ BS.chain_id = _ ; hash = op_hash ;
       protocol_data = Operation_data { contents ; signature = _ } ;
       receipt ; shell = _ }) =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let rec inner :
    type a. int -> a contents_list -> _ -> ophid -> unit tzresult Lwt.t =
    fun op_id contents receipt ophid ->
        match contents, receipt with
        | Single op,
          Apply_results.Operation_metadata { contents = Apply_results.Single_result a } ->
          store_op_id ~baker_id ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter conn conn2 cctxt
            (**# ifdef __FA2__ ~tokens #**)
            ~block_level ~ophid op_hash op_id bh op a
        | Cons (op, rest),
          Apply_results.Operation_metadata { contents = Apply_results.Cons_result (a, meta)} ->
          store_op_id ~baker_id ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter conn conn2 cctxt
            (**# ifdef __FA2__ ~tokens #**)
            ~block_level ~ophid op_hash op_id bh op a
          >>= warn_if_error ~__LOC__ >>=? fun () ->
          inner (succ op_id) rest (Apply_results.Operation_metadata { contents = meta }) ophid
        | Single _, Apply_results.No_operation_metadata ->
          Verbose.error "Error: Cannot find operation metadata. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
          Stdlib.exit 1
        | Cons _, Apply_results.No_operation_metadata ->
          Verbose.error "Error: Cannot find operation metadata. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
          Stdlib.exit 1
        | Single _, Apply_results.Operation_metadata { contents = Apply_results.Cons_result _ } ->
          Verbose.error "Error: Cannot find operation metadata. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
          Stdlib.exit 1
        | Cons _, Apply_results.Operation_metadata { contents = Apply_results.Single_result _ } ->
          Verbose.error "Error: Cannot find operation metadata. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
          Stdlib.exit 1
  in
  begin (* logged *)
    Verbose.Log.store_raw_operation block_level bh op_hash;
    Conn.find_opt Operation_table.insert (op_hash, block_level, op_counter())
  end >>= caqti_or_fail ~__LOC__ >>= function
  | None ->
    Verbose.error "Error: failed to insert operation %a" Operation_hash.pp op_hash;
    Misc.unless_auto_recovery Stdlib.exit Verbose.ExitCodes.indexing_error
  | Some ophid ->
    match receipt with
    | Too_large ->
      Verbose.error
        "Error from the node: Receipt (aka metadata) is too large. \
         You have to use a node that has a larger capacity for receipts. \
         If you run your own node, you may want to look at the `--metadata-size-limit` CLI option, introduced in octez 12.2. \
         This indexer is not compatible with octez 12.1, which hardcodes a too low metadata size limit. \
         Note that the node may not automatically recompute blocks' metadata \
         after changing the value for --metadata-size-limit. \
         I'll exit now.";
      exit 1
    | Empty ->
      Verbose.error
        "Receipt (aka metadata) is not available from the node. \
         Either it's been pruned or the node had issues. \
         Is the node running with `--history-mode=archive`? \
         I'll exit now.";
      exit 1
    | Receipt receipt ->
      inner 0 contents receipt ophid

let store_ops ~baker_id ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter ~op_counter conn conn2 cctxt (**# ifdef __FA2__ ~tokens #**) ~block_level bh ops =
  match ops with
  | [endorsements; voting; anon; manager] ->
    begin (* self logged *)
      let f =
        List.iter_es
          (store_op ~baker_id ~addr_counter (**# ifndef __PROTO1__ ~bigmap_counter #**) ~opal_counter ~op_counter conn conn2
             (**# ifdef __FA2__ ~tokens #**)
             cctxt ~block_level bh)
      in
      f endorsements >>=? fun () ->
      f voting >>=? fun () ->
      f anon >>=? fun () ->
      f manager >>= function
      | Ok () -> return_unit
      | _ ->
        Verbose.error "Error: unexpected error while indexing operations. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
        Stdlib.exit 1
    end
  | _ ->
    Verbose.error "Error: unexpected operation data format. Please check the health of your node and try restarting. If it still doesn't work, please file bug report on https://gitlab.com/nomadic-labs/tezos-indexer/-/issues/ mentioning: %s @ %s" __LOC__ (Tezos_indexer_lib.Version.version());
    Stdlib.exit 1

let previous_chain_id = ref None

type block_info = BS.block_info

let block_info_from_string s : (block_info, string) result =
  match Data_encoding.Json.from_string s with
  | Error e -> Error e
  | Ok j ->
    try
      Ok (snd @@ Data_encoding.Json.destruct BS.block_info_encoding j)
    with e -> Error (Printexc.to_string e)

let string_of_block_info_from_string s =
  block_info_from_string s |> function
  | Ok b ->
    Data_encoding.Json.construct BS.block_info_encoding (Block_services.Version_2, b)
    |> Data_encoding.Json.to_string
  | Error e ->
    Stdlib.failwith ("string_of_block_info_from_string: " ^ e)


module type File_blocks =
sig
  val get_block : ?dir:string -> int32 -> string option
end

type speed_meter = {
  print_result : unit -> unit
}

let make_operation_speed_meter ?h ?l () =
  let start = Unix.gettimeofday () in
  let i_op = Verbose.CLog.OpSpeed.get_counter () in
  let i_opa = Verbose.CLog.OpAlphaSpeed.get_counter () in
  {
    print_result = (fun () ->
        let time = Unix.gettimeofday () -. start
        and count_op = Verbose.CLog.OpSpeed.get_counter () - i_op
        and count_opa = Verbose.CLog.OpAlphaSpeed.get_counter () - i_opa in
        Verbose.printf ~vl:1
          "# block %a%a processed in %f seconds; \
           %d operations processed (%f/second); \
           %d operations alpha processed (%f/second)"
          (fun out -> function None -> () | Some l -> Format.fprintf out "%ld" l) l
          (fun out -> function None -> () | Some h -> Format.fprintf out "%a" Block_hash.pp h) h
          time
          count_op (float count_op /. time)
          count_opa (float count_opa /. time)
    );
  }

let store_block_full
    ?(file_blocks=(module Tezos_indexer_lib.File_blocks:File_blocks))
    ?use_disk_cache ?(tokens=false) ?chain ?block (cctxt:#rpc_context) conn conn2 =
  (**# ifndef __FA2__ let _ = tokens in #**)
  (* let _ : #RPC_context.simple = cctxt in *)

  (* begin (* debug code, re. RPC issue at {mainnet block 2834413} *)
   *   let module M = Tezos_shell_services.Block_services.Make(Tezos_protocol_014_PtKathma.Protocol)(Tezos_protocol_014_PtKathma.Protocol)
   *   in M.info cctxt ~metadata:`Always ~block:(`Level 2834413l) ()
   *   >>= function
   *   | Ok _ -> Lwt.return_unit
   *   | Error e ->
   *     Verbose.Debug.eprintf ~vl:6 "RPC request failed: %a" pp_print_trace e;
   *     exit 42
   * end >>= fun () -> *)

  let operation_speed_meter =
    if !Tezos_indexer_lib.Config.print_opspeed || !Verbose.debug || !Verbose.verbose then
      match block with
      | Some (`Hash (h, _)) -> make_operation_speed_meter ~h ()
      | Some (`Level l) when !Tezos_indexer_lib.Config.print_opspeed -> make_operation_speed_meter ~l ()
      | _ when !Tezos_indexer_lib.Config.print_opspeed -> make_operation_speed_meter ()
      | _ -> { print_result = ignore }
    else
      { print_result = ignore }
  in
  let module File_blocks = (val file_blocks : File_blocks) in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let module Conn2 = (val conn : Caqti_lwt.CONNECTION) in
  begin
    Conn.start () >>= caqti_or_fail ~__LOC__ >>= fun () ->
    (* Conn2.exec Db_base.Indexer_log.record_action ("transaction start at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
    Verbose.Debug.transaction_start __LOC__;
    Lwt.return_unit
  end >>= fun () ->
  begin
    match use_disk_cache, block with
    | Some dir, Some(`Level lvl) ->
      Verbose.Debug.eprintf ~vl:6 "# GETTING BLOCK #%ld... %s" lvl __LOC__;
      begin
        Verbose.Debug.eprintf ~vl:1 "reading block %ld from disk cache" lvl;
        match File_blocks.get_block ~dir lvl with
        | Some b ->
            block_info_from_string b |> begin function
            | Ok { metadata = Some _ ; header = { shell = { level ; _ } ; _ } ; _ } as b when level = lvl ->
              Verbose.Debug.eprintf ~vl:1 "got block %ld from disk cache" lvl;
              Lwt.return b
            | Ok _ ->
              Verbose.Debug.eprintf ~vl:0 "disk cache for block %ld returned wrong or broken block" lvl;
              !- "BS.info cctxt ?chain ?block ()" @@
              BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
            | Error e ->
              Verbose.Debug.eprintf ~vl:0 "failed to use disk cache for getting block %ld, error %s" lvl e;
              !- "BS.info cctxt ?chain ?block ()" @@
              BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
            end
        | None ->
          Verbose.Debug.eprintf ~vl:0 "block %ld not found in disk cache" lvl;
          !- "BS.info cctxt ?chain ?block ()" @@
          BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
        | exception e ->
          Verbose.Debug.eprintf ~vl:0 "failed hard to use disk cache for getting block %ld: %s" lvl (Printexc.to_string e);
          !- "BS.info cctxt ?chain ?block ()" @@
          BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
      end
    | Some _, _ ->
      Verbose.Debug.eprintf ~vl:1 "weird use case detected";
      !- "BS.info cctxt ?chain ?block ()" @@
      BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
    | None, _ ->
      Verbose.Debug.eprintf ~vl:6 "getting block from node ... %s" __LOC__;
      !- "BS.info cctxt ?chain ?block ()" @@
      BS.info cctxt ~metadata:`Always ?chain ?block () (* CCTX *)
  end >>= function
  | Error (((Tezos_rpc.Context.Not_found {uri=_; meth=_})::_) as e)
  | Error (((RPC_client_errors.Request_failed {uri=_; error=_; meth=_})::_) as e) ->
    Verbose.Debug.eprintf ~vl:6 "GETTING BLOCK FAILED ... %s" __LOC__;
    Verbose.Debug.eprintf ~vl:6 "RPC request failed: %a" pp_print_trace e;
    Conn.rollback () >>= caqti_or_fail ~__LOC__ >>= fun () ->
    (* Conn2.exec Db_base.Indexer_log.record_action ("transaction rollback at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
    begin if !Tezos_indexer_lib.Config.notify then
        Conn.exec Db_base.Indexer_log.notify_failure () >>= caqti_or_fail ~__LOC__
      else
        Lwt.return_unit
    end >>= fun () ->
    begin match block with
      | Some (`Level l) ->
        return (Int32.pred l)
      | _ ->
        Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
        Verbose.error "[Unexpected] %a" pp_print_trace e;
        Misc.unless_auto_recovery Stdlib.exit 1
    end
  | Error e ->
    Verbose.Debug.eprintf ~vl:6 "# GETTING BLOCK FAILED ... %s" __LOC__;
    Conn.rollback () >>= caqti_or_fail ~__LOC__ >>= fun () ->
    (* Conn2.exec Db_base.Indexer_log.record_action ("transaction rollback at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "[While trying to fetch a block] %a" pp_print_trace e;
    Misc.unless_auto_recovery Stdlib.exit 1
  | Ok { chain_id ; hash ; header = { shell ; protocol_data = _ } as header ;
         metadata ; operations } ->
    Verbose.Debug.eprintf ~vl:6 "# GETTING BLOCK WORKED ... %s" __LOC__;
    (* Store chain *)
    (if Some chain_id = !previous_chain_id || !Tezos_indexer_lib.Config.multicore_mode then
       Lwt.return_unit
     else
       let () = previous_chain_id := Some chain_id in
       Conn.find_opt Chain_id_table.insert chain_id >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit)
    >>= fun () ->
    (* Store block header (shell + alpha) *)
    match metadata with
    | None ->
      Verbose.error
        "Metadata is not available. Either it's been pruned or the node had issues. \
         Is the node running with `--history-mode=archive`? I'll exit now.";
      exit 1
    | Some metadata ->
      (**# ifdef __PROTO1__ OPEN_COMMENTS #**)
      let bigmap_counter = Misc.make_counter64() in
      let bigmap_counter bl = Int64.add (bigmap_counter ()) (Int64.mul 10_000_000L (Int64.of_int32 bl)) in
      (**# ifdef __PROTO1__ CLOSE_COMMENTS #**)
      store_block conn conn2 hash header metadata (**# ifdef __META8__ ~bigmap_counter #**)
      >>= function
      | Error e as r ->
        Verbose.Debug.eprintf ~vl:1 "Warning: Error at %s: %a" __LOC__ pp_print_trace e;
        Lwt.return r
      | Ok (block_level, addr_counter, baker_id) ->
      (* Store ops (shell + alpha) *)
      begin
        store_ops
          ~baker_id
          (**# ifndef __PROTO1__ ~bigmap_counter #**)
          ~opal_counter:(
            let bl = Int64.of_int32 block_level in
            let prefix =
              (* this limits the number of operations alpha per block at about 99M *)
              (* this also makes removing *all* reorg-involved blocks mandatory *)
              Int64.mul bl (10_000_000L) in
            Misc.make_counter64 ~start:prefix ()
          )
          ~op_counter:(
            let bl = Int64.of_int32 block_level in
            let prefix =
              (* this limits the number of operations per block at about 99M *)
              (* this also makes removing *all* reorg-involved blocks mandatory *)
              Int64.mul bl (10_000_000L) in
            Misc.make_counter64 ~start:prefix ()
          )
          ~addr_counter
          conn conn2 cctxt (**# ifdef __FA2__ ~tokens #**) ~block_level:shell.level hash operations >>=
        function
          | Ok () ->
            Lwt.return_unit
          | Error e ->
            Conn.rollback () >>= caqti_or_fail ~__LOC__ >>= fun () ->
            (* Conn2.exec Db_base.Indexer_log.record_action ("transaction rollback at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
            Verbose.Debug.eprintf ~vl:0 "Indexing an operation failed with error %a" pp_print_trace e;
            exit 1
      end >>= fun () ->
      begin
        Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        (* Conn2.exec Db_base.Indexer_log.record_action ("transaction commit at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
        Verbose.Debug.transaction_committed __LOC__;
        Tezos_indexer_lib.Misc.last_indexed_block := Some (Format.asprintf "%a" Block_hash.pp_short hash);
        begin
          if Tezos_indexer_lib.Config.benchmarking then
            Conn.exec Db_base.Indexer_log.block_indexed hash >>= caqti_or_fail ~__LOC__
          else
            Lwt.return_unit
        end >>= fun () ->
        if !Tezos_indexer_lib.Config.notify then
          Conn.exec Db_base.Indexer_log.notify_block_indexed () >>= caqti_or_fail ~__LOC__
        else
          Lwt.return_unit
      end >>= fun () ->
      Verbose.Debug.block_stored Time.Protocol.pp_hum shell.timestamp hash shell.level;
      operation_speed_meter.print_result ();
      return block_level


let discover_initial_ks (cctxt:#rpc_context) blockid conn conn2 =
  (* When the chain starts being indexed, we need to become aware of
     all existing contracts.  *)
  let addr_counter = let r = ref 0L in
    fun () -> let n = Int64.pred !r in r := n ; n
  in
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  !- "BS.info cctxt ~chain:(fst blockid) ~block:(snd blockid) ()" @@
  BS.info cctxt (* CCTX *)
    ~metadata:`Always ~chain:(fst blockid) ~block:(snd blockid) ()
  >>= warn_if_error ~__LOC__ >>=?
  fun { chain_id ; hash = bh; header = { shell ; protocol_data = _ };
        metadata = _;  operations = _ } ->
  let block_level = shell.level in
  (* Get all contracts at that given context: *)
  (!- "Alpha_services.Contract.list cctxt blockid -- 10 times higher timeout") ~timeout:(!Config.node_timeout *. 10.) @@
  Alpha_services.Contract.list cctxt blockid (* CCTX *)
  >>= warn_if_error ~__LOC__ >>=? fun ks ->
  (* For each contract, record it: *)
  list_iter_es (fun k ->
      !- "Alpha_services.Contract.info" @@
      Alpha_services.Contract.info (* CCTX 3871 *)
        (**# ifdef __META11__ ~normalize_types:true #**)
        cctxt blockid k >>= warn_if_error ~__LOC__ >>=? fun
        { (**# ifdef __META1__ manager ; spendable = _ ; #**)
          balance ; delegate ; counter = _ ; script ;
        }
      ->
(**# ifdef __META1__
    record_pkh ~__LINE__ ~addr_counter ~block_level conn conn2 ~pkh:manager >>= fun _manager_id ->
   (match snd delegate with
   | None -> Lwt.return_none
   | Some delegate ->
    record_pkh ~__LINE__ ~addr_counter ~block_level conn conn2 ~pkh:delegate >>= fun d ->
    Lwt.return_some d
   ) >>= fun _delegate_id ->
   #**)
(**# else
    (* record delegate's address, if any *)
    begin
      match delegate with
      | None -> Lwt.return_none
      | Some delegate ->
        record_pkh ~__LINE__ ~addr_counter ~block_level conn conn2 ~pkh:delegate >>= fun delegate_id ->
        Lwt.return_some delegate_id
    end >>= fun _delegate_id ->
#**)
(**# ifdef __META1__ (* let delegatable = Some (fst delegate) in *) #**)
(**# else (* let delegatable = Some (Contract.is_implicit k = None) in *) #**)
      (* add chain *)
      begin (* logged *)
        if Some chain_id = !previous_chain_id || !Tezos_indexer_lib.Config.multicore_mode then
          Lwt.return_unit
        else
          let () = previous_chain_id := Some chain_id in
          Verbose.Log.chain_id block_level bh chain_id;
          Conn.find_opt Chain_id_table.insert chain_id
          >>= caqti_or_fail ~__LOC__ >>= fun _ -> Lwt.return_unit
      end >>= fun () ->
      let block_level = 2l in
      record_k ~__LINE__ ~block_level ~addr_counter conn conn2 ~k >>= fun kid ->
      begin (* logged *)
        Verbose.Log.contract_update_balance ~bh ~level:block_level ~k ~balance ();
        Conn.find_opt Contract_balance_table.insert_balance_full
          (mtup6
             kid
             balance
             block_level
             script
             (if !Tezos_indexer_lib.Config.no_smart_contract_extraction then None
              else Misc.flatten_option (Std.Option.map extract_strings_and_bytes script))
             Tezos_indexer_lib.Config.extracted_address_length_limit.contents)
      end >>= caqti_or_fail ~__LOC__ >>= fun _ ->
      return_unit
    )
    ks
  >>= warn_if_error ~__LOC__ >>=? fun () ->
  return_unit

let rec run_one f =
  f () >>= function
  | `Continue -> run_one f
  | `Stop -> Lwt.return_unit

let _ = run_one


let rec bootstrap_generic
    ~threads:_
    ?(stepback=0l)
    ?from ?up_to ~first_alpha_level
    (cctxt:#rpc_context)
    conn conn2
    ~(do_store_block:
        (module File_blocks) ->
      (**## ifdef __META1__ #Client.Alpha_client_context.full ->##**)(**# else _ ->#**)
      (module Caqti_lwt.CONNECTION) ->
      (module Caqti_lwt.CONNECTION) ->
      int32 ->
      int32 tzresult Lwt.t)
  =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let module Conn2 = (val conn : Caqti_lwt.CONNECTION) in
  begin match from with
    | Some lvl when lvl > 0l ->
      Lwt.return_none
    | _ ->
      Conn.find_opt Block_table.select_max_level_with_bh () >>=
      caqti_or_fail ~__LOC__ >>= begin function
        | None ->
          resume_from_block_hash := None;
          Lwt.return_none
        | Some (db_max_level, db_max_level_block_hash) ->
          let bh = Some db_max_level_block_hash in
          if !resume_from_block_hash = bh then
            begin (* we tried before but failed! Therefore we should take a step back. *)
              Conn.find_opt Block_table.delete_last_block () >>= caqti_or_fail ~__LOC__ >>=
              function
              | Some highest_level ->
                begin if !Tezos_indexer_lib.Config.notify then
                    Conn.exec Db_base.Indexer_log.notify_block_deleted () >>= caqti_or_fail ~__LOC__
                    >>= fun () ->
                    Lwt.fail (Misc.Reorg highest_level)
                  else
                    Lwt.fail (Misc.Reorg highest_level)
                end
              | None ->
                Verbose.error "Tried deleting the last block. It failed. I'm exiting now.";
                exit 1
            end
          else
            (resume_from_block_hash := bh;
             Lwt.return_some db_max_level)
      end
  end >>= fun db_max_level  ->
  (**#X ifndef __TRANSITION__ let db_max_level = Std.Option.map (Int32.add stepback) db_max_level in X#**)(**# else let _ = stepback in #**)
  let do_from =
    (* FIXME: this implementation is too complicated! *)
    let do_from_tmp =
      max first_alpha_level @@
      match from, db_max_level with
      | Some lvl, _ when lvl > 0l ->
        lvl
      | Some from_neg, Some level ->
        max (Int32.neg from_neg) (Int32.succ level)
      | None, Some lvl ->
        Int32.succ lvl
      | None, None
      | Some _, None ->
        first_alpha_level
    in
    (* Verbose.CLog.printf ~force:true "first_alpha_level=%ld do_from_tmp=%ld" first_alpha_level do_from_tmp; *)
    max first_alpha_level do_from_tmp
  in
  begin match db_max_level with
    | _ when do_from > 2l ->
      Verbose.Debug.eprintf "not discovering initial contracts";
      return_unit
    | Some n when n > 2l && do_from > 2l ->
      Verbose.Debug.eprintf "not discovering initial contracts";
      return_unit
    | Some _ | None ->
      let db_max_level = match db_max_level with Some n -> n | None -> 0l in
      Verbose.Debug.eprintf "discovering initial contracts first_alpha_level=%ld n=%ld do_from=%ld" first_alpha_level db_max_level do_from;
      Conn.start () >>= caqti_or_fail ~__LOC__ >>= fun () ->
      (* Conn2.exec Db_base.Indexer_log.record_action ("transaction start at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
      Verbose.Debug.transaction_start __LOC__;
      Verbose.Debug.eprintf "indexing block 2 before discovering initial contracts";
      do_store_block (module Tezos_indexer_lib.File_blocks) cctxt conn conn2 2l >>= function
      | Error _ as e ->
        Conn.rollback () >>= caqti_or_fail ~__LOC__ >>= fun () ->
        (* Conn2.exec Db_base.Indexer_log.record_action ("transaction rollback at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
        begin if !Tezos_indexer_lib.Config.notify then
            Conn.exec Db_base.Indexer_log.notify_failure () >>= caqti_or_fail ~__LOC__
          else
            Lwt.return_unit
        end >>= fun () ->
        Lwt.return e
      | Ok _ ->
        discover_initial_ks cctxt (`Main, `Level first_alpha_level) conn conn2
        >>= function
        | Ok _ ->
          Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
          (* Conn2.exec Db_base.Indexer_log.record_action ("transaction commit at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
          Verbose.Debug.transaction_committed __LOC__;
          return_unit
        | Error _ as e ->
          Conn.commit () >>= caqti_or_fail ~__LOC__ >>= fun () ->
          (* Conn2.exec Db_base.Indexer_log.record_action ("transaction commit at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
          Verbose.Debug.transaction_committed __LOC__;
          Lwt.return e
  end >>= begin function
    | Ok () ->
      (* because if it's 2l, it was already indexed *)
      Lwt.return (max 3l do_from)
    | Error e ->
      Verbose.Debug.eprintf ~vl:0 "Discovering contracts failed with error %a" pp_print_trace e;
      Verbose.Debug.eprintf ~vl:0 "Don't panic if it's just an unexpected protocol error: I'll just try the next protocol (if any).";
      (* discovering failed, so we'll assume it's because it's the wrong protocol,
         but mostly we should not modify the value of [do_from] *)
      Lwt.return do_from
  end >>= fun do_from ->
  Verbose.Log.start_downloading_chain ~prefix:__FILE__ do_from;
  let rec process_n_blocks file_blocks conn conn2 counter lvl =
    match counter, up_to with
    | c, _ when c <= 0l -> return (`Counter_exhausted, lvl)
    | _, Some target when lvl > target ->
      Verbose.Log.bootstrapping_target_reached target;
      return (`Target_reached, lvl)
    | _ ->
      Verbose.Log.processing_block lvl;
      protect (* https://tezos.gitlab.io/api/odoc/_html/tezos-base/Tezos_base__TzPervasives/index.html *)
        (fun () ->
           Lwt.catch (fun () ->
               do_store_block file_blocks cctxt conn conn2 lvl
               >>= warn_if_error ~__LOC__ >>=? fun reached ->
               if reached = lvl then
                 return `Reached
               else
                 return (`Stuck reached)
             )
             (function Misc.Reorg lvl -> return (`Reorg lvl) | e -> Lwt.fail e)
        )
        ~on_error:(function
            | [Tezos_rpc.Context.Not_found _] ->
              return `Reached_head
            | e ->
              Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
              Verbose.error "Error while trying to get block %ld: %a" lvl pp_print_trace e;
              Misc.unless_auto_recovery exit 1
          ) >>= warn_if_error ~__LOC__ >>=? function
      | `Reached ->
        process_n_blocks file_blocks conn conn2 (Int32.pred counter) (Int32.succ lvl)
      | `Reached_head ->
        return (`Reached_head, lvl)
      | `Stuck lvl ->
        return (`Stuck, lvl)
      | `Reorg lvl ->
        return (`Reorg, lvl)
  in
  let rec inner lvl =
    let blocks_to_download =
      match up_to with
      | None -> 1l
      | Some up_to ->
        Int32.sub up_to lvl
    in
    if blocks_to_download <= 0l then begin
      (**#X ifdef __TRANSITION__ Verbose.Debug.printf ~vl:1 "(Transition protocol)"; X#**)
      Verbose.Log.bootstrapping_done proto;
      return lvl
    end else
      process_n_blocks (module Tezos_indexer_lib.File_blocks) conn conn2 blocks_to_download lvl >>=
      function
      | Error _ as e -> Lwt.return e
      | Ok (status, lvl) ->
        match status with
        | `Stuck             -> return (Int32.succ lvl)
        | `Reached_head      -> return (Int32.pred lvl)
        | `Target_reached    -> return lvl
        | `Counter_exhausted -> inner lvl
        | `Reorg             -> Lwt.fail (Misc.Reorg lvl)
  in
  Lwt.catch
    (fun () -> inner do_from)
    (function
      | Misc.Reorg lvl -> catch_reorg ?up_to cctxt conn conn2 ~do_store_block lvl
      | e -> Lwt.fail e)

and catch_reorg ?up_to cctxt conn conn2 ~do_store_block lvl =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  let module Conn2 = (val conn : Caqti_lwt.CONNECTION) in
  Conn.rollback () >>= fun _ ->
  (* Conn2.exec Db_base.Indexer_log.record_action ("transaction rollback at " ^ __LOC__) >>= caqti_or_fail ~__LOC__ >>= fun () -> *)
  begin if !Tezos_indexer_lib.Config.notify then
      Conn.exec Db_base.Indexer_log.notify_failure () >>= caqti_or_fail ~__LOC__
    else
      Lwt.return_unit
  end >>= fun () ->
  Verbose.printf ~force:true "Reorg happened, SQL transaction rolled back if any.";
  (* block at level [lvl] could not be indexed, which means block [lvl - 1] is problematic *)
  Verbose.Debug.eprintf ~vl:0 "Deleting last block";
  Conn.find_opt Block_table.delete_last_block () >>= caqti_or_fail ~__LOC__ >>=
  begin function
    | Some highest_level ->
      begin if !Tezos_indexer_lib.Config.notify then
          Conn.exec Db_base.Indexer_log.notify_block_deleted () >>= caqti_or_fail ~__LOC__
          >>= fun () ->
          Lwt.return highest_level
        else
          Lwt.return highest_level
      end
    | None ->
      Verbose.error "Tried deleting the last block. It failed. I'm exiting now.";
      exit 1
  end >>= fun first_alpha_level ->
  Lwt.catch (fun () ->
      bootstrap_generic
        ~threads:0
        ?from:None (* do not give a value for [from] here, otherwise it might be wrong *)
        ?up_to
        ~first_alpha_level
        cctxt
        conn
        conn2
        ~do_store_block)
    (function
      | Misc.Reorg rlvl -> catch_reorg ?up_to cctxt conn conn2 ~do_store_block (min rlvl lvl)
      | e -> Lwt.fail e)


let bootstrap_chain
    ?stepback
    ?use_disk_cache ?from ?up_to ?tokens ~first_alpha_level ?(threads=1) (cctxt:#rpc_context) conn conn2 =
  match up_to with
  | Some n when	n < 2l ->
    return n
  | _ ->
    let do_store_block file_blocks cctxt conn conn2 lvl =
      store_block_full ~file_blocks ?use_disk_cache ?tokens ~block:(`Level lvl) cctxt conn conn2
    in
    Lwt.catch (fun () ->
        bootstrap_generic
          ~threads
          ?stepback
          ?from
          ?up_to
          ~first_alpha_level
          cctxt
          conn
          conn2
          ~do_store_block)
      (function
        | Misc.Reorg rlvl -> catch_reorg ?up_to cctxt conn conn2 ~do_store_block rlvl
        | e -> Lwt.fail e)


(* Export a simplfied version: *)
let store_block_full ?use_disk_cache ?tokens ?chain ?block (cctxt:#rpc_context) conn =
  store_block_full ?use_disk_cache ?tokens ?chain ?block (cctxt:#rpc_context) conn conn
