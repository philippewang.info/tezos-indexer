(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

val store_block_full :
  ?use_disk_cache:string ->
  ?tokens:bool ->
  ?chain:Tezos_shell_services.Block_services.chain ->
  ?block:Tezos_shell_services.Block_services.block ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->
  X#**)(**#X else #(**#2 ifdef __TRANSITION__ Tezos_client_(**# get PROTO #**).2#**)Protocol_client_context.full ->X#**)
  (module Caqti_lwt.CONNECTION) ->
  int32 tzresult Lwt.t


val bootstrap_chain :
  ?stepback:int32 ->
  ?use_disk_cache:string ->
  ?from:int32 ->
  ?up_to:int32 ->
  ?tokens:bool ->
  first_alpha_level:int32 ->
  ?threads:int ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->
  X#**)(**#X else #(**#2 ifdef __TRANSITION__ Tezos_client_(**# get PROTO #**).2#**)Protocol_client_context.full ->X#**)
  (module Caqti_lwt.CONNECTION) ->
  (module Caqti_lwt.CONNECTION) ->
  int32 tzresult Lwt.t

val string_of_block_info_from_string : string -> string  (* identity unless it fails *)


(**# ifdef __TRANSITION__ OPEN_COMMENTS #**)
(**# ifdef __XMETA11__ OPEN_COMMENTS #**)
val record_contract_balance :
  (module Caqti_lwt.CONNECTION) ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full
  X#**)(**#X else #Protocol_client_context.full X#**) ->
  block_level:Db_base.block_level ->
  bh:Block_hash.t ->
  k:Tezos_raw_protocol_(**# get PROTO#**).Alpha_context.Contract.t ->
  kid:Db_base.kid ->
  db_max_level:Db_base.block_level ->
(**#X ifdef __META11__ (unit, Environment.Error_monad.shell_tztrace) result Lwt.t X#**)
(**#X else unit tzresult Lwt.t X#**)
(**# ifdef __XMETA11__ CLOSE_COMMENTS #**)
(**# ifdef __TRANSITION__ CLOSE_COMMENTS #**)
