(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)


(** Convert gas to fp gas *)
type fp = Tezos_raw_protocol_007_PsDELPH1.Alpha_context.Gas.Arith.fp
type t = fp

let t_of_z : Z.t -> t = fun x ->
  Tezos_raw_protocol_007_PsDELPH1.Alpha_context.Gas.Arith.(integral x |> fp)

let pp = Tezos_raw_protocol_007_PsDELPH1.Alpha_context.Gas.Arith.pp

let encoding = Tezos_raw_protocol_007_PsDELPH1.Alpha_context.Gas.Arith.z_fp_encoding

let unsafe_t_of_z : Z.t -> t = Obj.magic
let unsafe_z_of_t : t -> Z.t = Obj.magic

let p8_to_p7 :
  Tezos_raw_protocol_008_PtEdo2Zk.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p9_to_p7 :
  Tezos_raw_protocol_009_PsFLoren.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p10_to_p7 :
  Tezos_raw_protocol_010_PtGRANAD.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p11_to_p7 :
  Tezos_raw_protocol_011_PtHangz2.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p12_to_p7 :
  Tezos_raw_protocol_012_Psithaca.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p13_to_p7 :
  Tezos_raw_protocol_013_PtJakart.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p14_to_p7 :
  Tezos_raw_protocol_014_PtKathma.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p15_to_p7 :
  Tezos_raw_protocol_015_PtLimaPt.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p16_to_p7 :
  Tezos_raw_protocol_016_PtMumbai.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p17_to_p7 :
  Tezos_raw_protocol_017_PtNairob.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

let p18_to_p7 :
  Tezos_raw_protocol_018_Proxford.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic

(* __NEW_PROTO__ 19 *)
(* let p19_to_p7 : *)
(*   Tezos_raw_protocol_019_.Alpha_context.Gas.Arith.fp -> t *)
(*   = *)
(*   Obj.magic *)

let alpha_to_p7 :
  Tezos_raw_protocol_alpha.Alpha_context.Gas.Arith.fp -> t
  =
  Obj.magic
