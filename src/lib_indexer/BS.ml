(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
module BS0_mainnet = Tezos_shell_services.Block_services.Make(Tezos_protocol_000_Ps9mPmXa_lifted.Lifted_protocol)(Tezos_protocol_001_PtCJ7pwo_lifted.Lifted_protocol)

module BS0 = BS0_mainnet

module BSA  = Tezos_shell_services.Block_services.Make(Tezos_protocol_alpha.Protocol)(Tezos_protocol_alpha.Protocol)
module BS1  = Tezos_client_001_PtCJ7pwo.Alpha_client_context.Alpha_block_services
module BS2  = Tezos_client_002_PsYLVpVv.Alpha_client_context.Alpha_block_services
module BS3  = Tezos_client_003_PsddFKi3.Alpha_client_context.Alpha_block_services
module BS4  = Tezos_client_004_Pt24m4xi.Alpha_client_context.Alpha_block_services
module BS5  = Tezos_shell_services.Block_services.Make(Tezos_protocol_005_PsBabyM1_lifted.Lifted_protocol)(Tezos_protocol_005_PsBabyM1_lifted.Lifted_protocol)
module BS6  = Tezos_shell_services.Block_services.Make(Tezos_protocol_006_PsCARTHA_lifted.Lifted_protocol)(Tezos_protocol_006_PsCARTHA_lifted.Lifted_protocol)
module BS7  = Tezos_shell_services.Block_services.Make(Tezos_protocol_007_PsDELPH1_lifted.Lifted_protocol)(Tezos_protocol_007_PsDELPH1_lifted.Lifted_protocol)
module BS8  = Tezos_shell_services.Block_services.Make(Tezos_protocol_008_PtEdo2Zk_lifted.Lifted_protocol)(Tezos_protocol_008_PtEdo2Zk_lifted.Lifted_protocol)
module BS9  = Tezos_shell_services.Block_services.Make(Tezos_protocol_009_PsFLoren_lifted.Lifted_protocol)(Tezos_protocol_009_PsFLoren_lifted.Lifted_protocol)
module BS10 = Tezos_shell_services.Block_services.Make(Tezos_protocol_010_PtGRANAD_lifted.Lifted_protocol)(Tezos_protocol_010_PtGRANAD_lifted.Lifted_protocol)
module BS11 = Tezos_shell_services.Block_services.Make(Tezos_protocol_011_PtHangz2_lifted.Lifted_protocol)(Tezos_protocol_011_PtHangz2_lifted.Lifted_protocol)
module BS12 = Tezos_shell_services.Block_services.Make(Tezos_protocol_012_Psithaca_lifted.Lifted_protocol)(Tezos_protocol_012_Psithaca_lifted.Lifted_protocol)
module BS13 = Tezos_shell_services.Block_services.Make(Tezos_protocol_013_PtJakart_lifted.Lifted_protocol)(Tezos_protocol_013_PtJakart_lifted.Lifted_protocol)
module BS14 = Tezos_shell_services.Block_services.Make(Tezos_protocol_014_PtKathma_lifted.Lifted_protocol)(Tezos_protocol_014_PtKathma_lifted.Lifted_protocol)
module BS15 = Tezos_shell_services.Block_services.Make(Tezos_protocol_015_PtLimaPt_lifted.Lifted_protocol)(Tezos_protocol_015_PtLimaPt_lifted.Lifted_protocol)
module BS16 = Tezos_shell_services.Block_services.Make(Tezos_protocol_016_PtMumbai_lifted.Lifted_protocol)(Tezos_protocol_016_PtMumbai_lifted.Lifted_protocol)
module BS17 = Tezos_shell_services.Block_services.Make(Tezos_protocol_017_PtNairob_lifted.Lifted_protocol)(Tezos_protocol_017_PtNairob_lifted.Lifted_protocol)
module BS18 = Tezos_shell_services.Block_services.Make(Tezos_protocol_018_Proxford_lifted.Lifted_protocol)(Tezos_protocol_018_Proxford_lifted.Lifted_protocol)
(* __NEW_PROTO__ 19 *)
(* module BS19 = Tezos_shell_services.Block_services.Make(Tezos_protocol_019_.Protocol)(Tezos_protocol_019_.Protocol) *)


module BST12 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_001_PtCJ7pwo_lifted.Lifted_protocol)(Tezos_protocol_002_PsYLVpVv_lifted.Lifted_protocol)

module BST23 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_002_PsYLVpVv_lifted.Lifted_protocol)(Tezos_protocol_003_PsddFKi3_lifted.Lifted_protocol)

module BST34 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_003_PsddFKi3_lifted.Lifted_protocol)(Tezos_protocol_004_Pt24m4xi_lifted.Lifted_protocol)

module BST45 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_004_Pt24m4xi_lifted.Lifted_protocol)(Tezos_protocol_005_PsBabyM1_lifted.Lifted_protocol)

module BST56 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_005_PsBabyM1_lifted.Lifted_protocol)(Tezos_protocol_006_PsCARTHA_lifted.Lifted_protocol)

module BST67 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_006_PsCARTHA_lifted.Lifted_protocol)(Tezos_protocol_007_PsDELPH1_lifted.Lifted_protocol)

module BST78 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_007_PsDELPH1_lifted.Lifted_protocol)(Tezos_protocol_008_PtEdo2Zk_lifted.Lifted_protocol)

module BST89 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_008_PtEdo2Zk_lifted.Lifted_protocol)(Tezos_protocol_009_PsFLoren_lifted.Lifted_protocol)

module BST910 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_009_PsFLoren_lifted.Lifted_protocol)(Tezos_protocol_010_PtGRANAD_lifted.Lifted_protocol)

module BST1011 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_010_PtGRANAD_lifted.Lifted_protocol)(Tezos_protocol_011_PtHangz2_lifted.Lifted_protocol)

module BST1112 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_011_PtHangz2_lifted.Lifted_protocol)(Tezos_protocol_012_Psithaca_lifted.Lifted_protocol)

module BST1213 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_012_Psithaca_lifted.Lifted_protocol)(Tezos_protocol_013_PtJakart_lifted.Lifted_protocol)

module BST1314 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_013_PtJakart_lifted.Lifted_protocol)(Tezos_protocol_014_PtKathma_lifted.Lifted_protocol)

module BST1415 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_014_PtKathma_lifted.Lifted_protocol)(Tezos_protocol_015_PtLimaPt_lifted.Lifted_protocol)

module BST1516 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_015_PtLimaPt_lifted.Lifted_protocol)(Tezos_protocol_016_PtMumbai_lifted.Lifted_protocol)

module BST1617 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_016_PtMumbai_lifted.Lifted_protocol)(Tezos_protocol_017_PtNairob_lifted.Lifted_protocol)

module BST1718 =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_017_PtNairob_lifted.Lifted_protocol)(Tezos_protocol_018_Proxford_lifted.Lifted_protocol)

(* __NEW_PROTO__ 19 *)
module BST17A =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_017_PtNairob_lifted.Lifted_protocol)(Tezos_protocol_alpha.Protocol)

module BST18A =
  Tezos_shell_services.Block_services.Make(Tezos_protocol_018_Proxford_lifted.Lifted_protocol)(Tezos_protocol_alpha.Protocol)

(* __NEW_PROTO__ 19 *)
(* module BST19A = *)
(*   Tezos_shell_services.Block_services.Make(Tezos_protocol_019_.Protocol)(Tezos_protocol_alpha.Protocol) *)
