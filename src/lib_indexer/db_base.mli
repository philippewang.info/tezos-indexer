(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2022 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 Encoders from Tezos types to Caqti (SQL) types.}  *)

open Tezos_base__TzPervasives

val connect : Uri.t -> (module Caqti_lwt.CONNECTION) Lwt.t
val chain_id : Chain_id.t Caqti_type.t

val to_caqti_error : ('a, Tezos_base.TzPervasives.tztrace) result -> ('a, string) result

val caqti_or_fail : __LOC__:string -> ('a, [< Caqti_error.t ]) Stdlib.result -> 'a Lwt.t
val caqti_or_fail_msg : msg:string -> __LOC__:string -> ('a, [< Caqti_error.t ]) Stdlib.result -> 'a Lwt.t
val caqti_BUT_NO_FAIL : msg:string -> cmd:string option -> __LOC__:string -> ('a, [< Caqti_error.t ]) Stdlib.result -> 'a option Lwt.t

val query_zero :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`Zero ]) Caqti_request.t
val query_zero_or_one :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`Zero | `One ]) Caqti_request.t
val query_zero_or_more :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, Caqti_mult.zero_or_more) Caqti_request.t
val query_one :
  'a Caqti_type.t ->
  'b Caqti_type.t ->
  string ->
  ('a, 'b, [`One ]) Caqti_request.t

val find_opt :
  ((module Caqti_lwt.CONNECTION),
   [> Caqti_error.call_or_retrieve ] as 'e)
    Caqti_lwt.Pool.t ->
  ('a, 'b, [< `One | `Zero ]) Caqti_request.t ->
  'a -> ('b option, 'e) Stdlib.result Lwt.t


val bh : Block_hash.t Caqti_type.t
val time : Tezos_base.Time.Protocol.t Caqti_type.t
val fitness : Fitness.t Caqti_type.t

val pkh : Tezos_crypto.Signature.V0.public_key_hash Caqti_type.t
val pk : Tezos_crypto.Signature.V0.public_key Caqti_type.t

val operation_list_list_hash : Operation_list_list_hash.t Caqti_type.t
val context_hash : Context_hash.t Caqti_type.t

val shell_header : Block_header.shell_header Caqti_type.t
val json : Data_encoding.json Caqti_type.t

val integer : int Caqti_type.t
val bigint : int Caqti_type.t

val z : Z.t Caqti_type.t
val milligas : Fpgas.t Caqti_type.t
val oph : Operation_hash.t Caqti_type.t

type block_level = int32
type kid = int64
type ophid = int64

val block_level : block_level Caqti_type.t
val kid : kid Caqti_type.t
val ophid : ophid Caqti_type.t

module Indexer_log : sig
  val record : (string * string, unit, [`Zero]) Caqti_request.t
  val record_action : (string, unit, [`Zero]) Caqti_request.t
  val block_indexed : (Block_hash.t, unit, [`Zero]) Caqti_request.t
  val notify_block_indexed : (unit, unit, [`Zero]) Caqti_request.t
  val notify_block_deleted : (unit, unit, [`Zero]) Caqti_request.t
  val notify_failure : (unit, unit, [`Zero]) Caqti_request.t
end

(** {1 Typed requests for Indexer's SQL DB.} *)

module Chain_id_table : sig
  val insert : (Chain_id.t, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Block_table : sig
  val insert0 :
    (Block_hash.t * Block_header.shell_header,
     block_level option, Caqti_mult.one) Caqti_request.t

  val insert :
    (Block_hash.t * Block_header.shell_header,
     block_level option, Caqti_mult.one) Caqti_request.t

  val select_max_level_with_bh :
    (unit, (block_level * Block_hash.t), Caqti_mult.zero_or_one) Caqti_request.t

  val select_max_level :
    (unit, block_level, Caqti_mult.zero_or_one) Caqti_request.t

  val booted :
    (unit, bool, Caqti_mult.one) Caqti_request.t

  val get_block_level :
    (Block_hash.t, block_level, Caqti_mult.zero_or_one) Caqti_request.t

  val delete_last_block :
    (unit, block_level, Caqti_mult.zero_or_one) Caqti_request.t

end

module Operation_table : sig
  val insert :
    (Operation_hash.t *  block_level * ophid,
     ophid, Caqti_mult.zero_or_one) Caqti_request.t
end

module Deactivated_delegate_table : sig
  val insert :
    (kid * block_level, unit, Caqti_mult.zero_or_one) Caqti_request.t
end

module Addresses : sig
  type t
  val address : t Caqti_type.t
  val make : string -> t
  val insert : (string * kid, kid, Caqti_mult.zero_or_one) Caqti_request.t
end

module Snapshot_table : sig
  val store_snapshot_levels :
    ((module Caqti_lwt.CONNECTION), 'a) Caqti_lwt.Pool.t -> (int * int32) list -> unit Lwt.t
end

(** {1 Caqti helpers} *)

val without_transaction :
  (module Caqti_lwt.CONNECTION) ->
  ('a, unit, [`Zero]) Caqti_request.t -> 'a list -> unit Lwt.t

val without_transaction_ignore :
  (module Caqti_lwt.CONNECTION) ->
  ('a, unit, Caqti_mult.zero_or_one) Caqti_request.t -> 'a list -> unit Lwt.t

val without_transaction_pool_ignore :
  ((module Caqti_lwt.CONNECTION), 'a) Caqti_lwt.Pool.t ->
  ('a, unit, Caqti_mult.zero_or_one) Caqti_request.t -> 'a list -> unit Lwt.t

module Version : sig
  val select : (unit, string * bool * bool, Caqti_mult.zero_or_one) Caqti_request.t
end
