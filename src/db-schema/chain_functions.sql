-- Open Source License
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-- Naming conventions:
-- - for functions:
--   * I.table -> insert into table
--   * U_table -> update table
--   * IU_table -> insert or update table (aka upsert)
--   * u_concept -> update more than one table
--   * B_action -> action on bigmaps
--   * BEWARE: upper/lower cases for prefixes are only for aesthetic purposes!
--     Function names are case-insensitive!
-----------------------------------------------------------------------------

SELECT 'chain_functions.sql' as file;

CREATE OR REPLACE FUNCTION record_log (msg text)
RETURNS void
AS $$
insert into indexer_log values (CURRENT_TIMESTAMP, '', '', msg) on conflict do nothing;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.chain(c char)
RETURNS void
AS $$
BEGIN
  INSERT INTO C.chain(hash) VALUES (c)
  ON CONFLICT DO NOTHING;
  IF (SELECT COUNT(*) FROM C.chain) <> 1
  THEN
    RAISE 'You are trying to index a chain on a other chain‘s database.';
  END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.block(bh char, l int, p smallint, pr char, t timestamp with time zone, vp smallint, m char, f char, c char)
RETURNS int
AS $$
DECLARE bl int := (SELECT level FROM C.block WHERE hash = bh);
BEGIN
  IF bl IS NOT NULL
  THEN
--EXTRA UPDATE indexer_measurements SET last_indexed = CURRENT_TIMESTAMP;
   RETURN bl;
  ELSE
    IF block_level(pr) IS NULL --SEQONLY
    THEN --SEQONLY
      RAISE 'block % cannot be inserted, predecessor % is absent', bh, pr; --SEQONLY
    END IF; --SEQONLY
    INSERT INTO C.block VALUES (bh, l, p, block_level(pr), t, vp, m, f, c); --SEQONLY
    INSERT INTO C.block VALUES (bh, l, p,             l-1, t, vp, m, f, c); --MULTICORE
--EXTRA INSERT INTO indexer_measurements (block_hash, level, timestamp, first_indexed) VALUES (bh, l, t, CURRENT_TIMESTAMP); --MULTICORE
    RETURN l; --MULTICORE
    bl := (SELECT level FROM C.block WHERE hash = bh); --SEQONLY
    IF bl IS NOT NULL --SEQONLY
    THEN --SEQONLY
--EXTRA INSERT INTO indexer_measurements (block_hash, level, timestamp, first_indexed) VALUES (bh, l, t, CURRENT_TIMESTAMP) ON CONFLICT (level, timestamp) DO UPDATE SET last_indexed = CURRENT_TIMESTAMP; --SEQONLY
      RETURN bl; --SEQONLY
    ELSE --SEQONLY
      RETURN -1; --SEQONLY
    END IF; --SEQONLY
  END IF;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.block0(bh char, l int, p smallint, pr char, t timestamp with time zone, vp smallint, m char, f char, c char)
RETURNS int
AS $$
DECLARE result int;
BEGIN
  IF bh = pr
  THEN
    insert into C.block values (bh, l, p, l, t, vp, m, f, c) on conflict do nothing;
  ELSE
    insert into C.block values (bh, l, p, block_level(pr), t, vp, m, f, c) on conflict do nothing;
  END IF;
  result := (SELECT block_level(bh));
  IF result IS NULL
  THEN RAISE 'block % failed to be inserted', bh;
  END IF;
--EXTRA INSERT INTO indexer_measurements (block_hash, level, timestamp, first_indexed) VALUES (bh, l, t, CURRENT_TIMESTAMP) ON CONFLICT (level, timestamp) DO UPDATE SET last_indexed = CURRENT_TIMESTAMP;
  -- result := l;
  RETURN result;
END;
$$ LANGUAGE PLPGSQL;


-- CREATE OR REPLACE FUNCTION confirm_block(block_level int, depth smallint)
-- RETURNS void
-- AS $$
-- update C.block set indexing_depth = depth where hash_id = block_level; --SEQONLY
-- $$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION max_level()
RETURNS int
AS $$
select level from C.block
order by level desc limit 1;
$$ LANGUAGE SQL stable;

CREATE OR REPLACE FUNCTION max_level_bh()
RETURNS table (level int, block_hash char)
AS $$
select (level, hash) from C.block
order by level desc limit 1;
$$ LANGUAGE SQL stable;

CREATE OR REPLACE FUNCTION I.operation_aux(h char, b int, hi bigint)
RETURNS bigint
AS $$
insert into C.operation (hash, block_level, hash_id)
values (h, b, hi)
on conflict do nothing
returning hash_id;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.operation(h char, b int, hi bigint)
RETURNS bigint
AS $$
DECLARE r bigint := null;
BEGIN
  r := I.operation_aux(h, b, hi);
  if r is not null
  then
    return r;
  else
    r := (select hash_id from C.operation where hash = h and block_level = b);
    return r;
  end if;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.operation(h char, b int, hi bigint) --MULTICORE
RETURNS bigint --MULTICORE
AS $$ --MULTICORE
insert into C.operation (hash, block_level, hash_id) --MULTICORE
values (h, b, hi) --MULTICORE
returning hash_id; --MULTICORE
$$ LANGUAGE SQL; --MULTICORE

DROP FUNCTION IF EXISTS I.block_alpha (block_level int, baker bigint, level_position int, cycle int, cycle_position int, voting_period jsonb, voting_period_position int, voting_period_kind smallint, consumed_milligas numeric);
CREATE OR REPLACE FUNCTION I.block_alpha (block_level int, baker bigint, level_position int, cycle int, cycle_position int, voting_period jsonb, voting_period_position int, voting_period_kind smallint, consumed_milligas numeric, delegate_id_ bigint)
RETURNS int
AS $$
BEGIN
  IF block_level = 2 AND (select count(*) from c.block_alpha where level = 2) = 1 THEN--MULTICORE
    RETURN block_level; --MULTICORE
  END IF;--MULTICORE
  INSERT INTO C.block_alpha
  VALUES (block_level, baker, level_position, cycle, cycle_position, voting_period, voting_period_position, voting_period_kind, consumed_milligas, delegate_id_)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
  RETURN (select level from c.block_alpha where level = block_level);
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.opalpha (ophid bigint, opid smallint, opkind smallint, block_level int, i smallint, a bigint)
returns void
as $$
insert into C.operation_alpha(hash_id, id, operation_kind, block_level, internal, autoid)
values (ophid, opid, opkind, block_level, i, a)
on conflict do nothing --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.deactivated (pkhid bigint, block_level int)
returns void
as $$
insert into C.deactivated (pkh_id, block_level) values (pkhid, block_level)
on conflict do nothing  --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.activate (opaid bigint, pkhid bigint, ac char)
returns void
as $$
insert into C.activation(operation_id, pkh_id, activation_code)
values (opaid, pkhid, ac)
on conflict do nothing; --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION I.proposal (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal))
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.proposal2 (opaid bigint, i bigint, s bigint, period int, proposal char)
RETURNS VOID
AS $$
-- the only difference with I.proposal is that this one does not create an entry in C.operation_sender_and_receiver because we know there already is one
insert into C.proposals values (proposal, i) on conflict do nothing;
insert into C.proposal values (opaid, s, period, proposal_id(proposal)) on conflict do nothing;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION I.ballot (opaid bigint, i bigint, s bigint, period int, proposal char, ballot one_ballot)
RETURNS VOID
AS $$
-- in non-multicore mode, there's no vote for proposals that are unknown
-- in multicore mode, we can be recording ballots for proposals that haven't been recorded yet
insert into C.proposals values (proposal, i) on conflict do nothing; --MULTICORE
insert into C.ballot
values (opaid, s, period, proposal_id(proposal), ballot)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, s, null)
on conflict do nothing; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.vdf_revelation (opaid bigint, vdf_sol json)
RETURNS VOID
AS $$
insert into C.vdf_revelation (operation_id, vdf_solution) values (opaid, vdf_sol) on conflict do nothing;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.double_endorsement(opaid bigint, baker_id bigint, offender_id bigint, xop1 text, xop2 text)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_endorsement_evidence (operation_id, op1, op2) VALUES (opaid, xop1::jsonb, xop2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_endorsement_evidence (operation_id, op1_, op2_) VALUES (opaid, xop1::json, xop2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING  --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.double_preendorsement(opaid bigint, baker_id bigint, offender_id bigint, xop1 text, xop2 text)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_preendorsement_evidence (operation_id, op1, op2) VALUES (opaid, xop1::jsonb, xop2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_preendorsement_evidence (operation_id, op1_, op2_) VALUES (opaid, xop1::json, xop2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING  --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.double_baking(opaid bigint, xbh1 text, xbh2 text, baker_id bigint, offender_id bigint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  BEGIN
    INSERT INTO C.double_baking_evidence (operation_id, bh1, bh2) VALUES (opaid, xbh1::jsonb, xbh2::jsonb)
    ON CONFLICT DO NOTHING --SEQONLY
    ;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
        INSERT INTO C.double_baking_evidence (operation_id, bh1_, bh2_) VALUES (opaid, xbh1::json, xbh2::json)
        ON CONFLICT DO NOTHING --SEQONLY
        ;
      ELSE
        RAISE EXCEPTION 'Error: %', err;
      END IF;
  END;
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, baker_id, offender_id)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION I.manager_numbers (opaid bigint, counter numeric, gas_limit numeric, storage_limit numeric)
returns void
as $$
insert into C.manager_numbers
values (opaid, counter, gas_limit, storage_limit)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.endorsement(opaid bigint, level int, del bigint, sl smallint[], power integer, round integer, block_payload_hash char)
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl, round, block_payload_hash, power)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;

CREATE OR REPLACE FUNCTION I.preendorsement(opaid bigint, level int, del bigint, sl smallint[], power integer, round integer, block_payload_hash char)
returns void
as $$
insert into C.preendorsement values (opaid, level, del, sl, round, block_payload_hash, power)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;

CREATE OR REPLACE FUNCTION I.endoslot(opaid bigint, del bigint, sl smallint[], level int, slot smallint)
returns void
as $$
insert into C.endorsement values (opaid, level, del, sl, slot)
on conflict (operation_id) do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, del, null)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.seed_nonce (opaid bigint, sender_id bigint, baker_id bigint, l int, n char)
returns void
as $$
insert into C.seed_nonce_revelation (operation_id, level, nonce)
values (opaid, l, n)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (
  opaid
, sender_id
, baker_id
)
on conflict do nothing --SEQONLY
;
$$ language sql;


CREATE OR REPLACE FUNCTION I.snapshot (c int, l int)
returns void
as $$
insert into C.snapshot
values (c, l)
on conflict do nothing;
$$ language sql;


CREATE OR REPLACE FUNCTION extract_uris (xstrings char[], max_length smallint)
RETURNS int[]
AS $$
  select array_agg(I.uri(element)) from unnest(xstrings) as element where char_length(element) <= max_length AND element ~ '^([a-zA-Z][a-zA-Z]*://..*|tezos-storage:.*)'
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION extract_contracts (xstrings char[], opaid bigint)
RETURNS bigint[]
AS $$
(select array_agg(I.address(element::char(36),opaid)) from unnest(xstrings) as element where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36)
$$ LANGUAGE SQL STABLE;


-- This is only for updating the script on mainnet after transitioning to Babylon
DROP FUNCTION IF EXISTS U.script (kid bigint, xscript text, block_level int, xstrings text[], max_length smallint);
CREATE OR REPLACE FUNCTION U.script (kid bigint, xscript text, block_level_ int, xstrings text[], max_length smallint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  IF (select level from c.block where level = block_level_) is null
  THEN
    RAISE NOTICE 'U.script: block of hash_id = % does not exist', (block_level_::text);
    RETURN;
  END IF;
  UPDATE C.contract_script set script = xscript::jsonb, block_level = block_level_, strings = xstrings
  , uri = (select extract_uris(xstrings, max_length))
  , contracts = (select extract_contracts(xstrings, 0::bigint))
  where address_id = kid and (script is null AND script_ is null);
  EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
    IF err = 'unsupported Unicode escape sequence'
    THEN
      UPDATE C.contract_script set script_ = xscript::json, block_level = block_level_, strings = xstrings where address_id = kid and script is null AND script_ is null;
    ELSE
      RAISE EXCEPTION 'Error: %', err;
    END IF;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION G.scriptless_contracts (lowest bigint)
-- RELEVANT ONLY FOR MAINNET
returns table (address char, address_id bigint)
as $$
select address(address_id), address_id
from C.contract_script s
where address_id > lowest and s.script is null and s.script_ is null
and missing_script is null
order by address_id asc
limit 100;
$$ language sql stable;


-- when the block level is too close to head, U.c_bs2 should be used instead!
CREATE OR REPLACE FUNCTION U.c_bs (xaddress bigint, bl int, xbalance bigint, xscript text, xstrings text[], max_length smallint)
RETURNS void
AS $$
DECLARE err text;
BEGIN
  IF (select level from c.block where level = bl) is null
  THEN
    RAISE NOTICE 'U.c_bs: block of level = % does not exist', bl::text;
    RETURN;
  END IF;
  UPDATE C.contract_balance SET balance = xbalance WHERE (address_id, block_level) = (xaddress, bl);
  IF xscript IS NOT NULL
  THEN
    IF (SELECT block_level FROM C.contract_script WHERE address_id = xaddress AND (script IS NOT NULL OR script_ IS NOT NULL) LIMIT 1) IS NULL
    THEN
      BEGIN
        INSERT INTO C.contract_script
        VALUES (xaddress, xscript::jsonb, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
        ON CONFLICT DO NOTHING;
        EXCEPTION WHEN OTHERS THEN
          GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
          IF err = 'unsupported Unicode escape sequence'
          THEN
            INSERT INTO C.contract_script (address, script_, block_level, strings, uri, contracts)
            VALUES (xaddress, xscript::json, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
            ON CONFLICT DO NOTHING;
          ELSE
            RAISE EXCEPTION 'Error: %', err;
          END IF;
      END;
    END IF;
  END IF;
  EXCEPTION WHEN OTHERS THEN return; --FAILSAFE: if update doesn't work, then it means the block was likely deleted
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION U.c_bs2 (xaddress bigint, bh char, xbalance bigint, xscript jsonb, xstrings text[], max_length smallint)
returns void
as $$
declare bl int;
begin
bl := (select block_level(bh));
if bl is null
then
  RAISE NOTICE 'U.c_bs2: block of hash = % does not exist', bh;
  return;
end if;
update C.contract_balance set balance = xbalance where (address_id, block_level) = (xaddress, bl) ;

if xscript is not null
then
  if (select block_level from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, bl, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
    on conflict do update set script = xscript, strings = xstrings, uri = (select extract_uris(xstrings, max_length)), contracts = (select extract_contracts(xstrings, 0::bigint));
  end if;
end if;
EXCEPTION WHEN OTHERS THEN RETURN; --FAILSAFE FOR WHEN THE BLOCK HAS BEEN DELETED IN THE MEANTIME BECAUSE OF A REORG
end;
$$ language plpgsql;
-- this alternative version of U.c_bs serves to insert balances that are close to head's level, such that in case of a reorganization happening, it'll safely update balances: if the block was deleted because of a reorganization, the update will do nothing because the function takes "block hashes" instead of "block hash ids". This version is not systematically used because it's much slower to deal with those hashes: additional data (char vs int) and id lookup from hash.


CREATE OR REPLACE FUNCTION I.c_bs (xaddress bigint, xbalance bigint, xblock_level int, xscript jsonb, xstrings text[], max_length smallint)
returns void
as $$
begin
  IF (select level from c.block where level = xblock_level) is null
  THEN
    RAISE NOTICE 'I.c_bs: block at level = % does not exist', xblock_level::text;
    RETURN;
  END IF;
insert into C.contract_balance (address_id, block_level, balance)
values (xaddress, xblock_level, xbalance)
on conflict do nothing --SEQONLY
;
if xscript is not null
then
  if (select block_level from C.contract_script where address_id = xaddress and script = xscript limit 1) is null
  then
    insert into C.contract_script values(xaddress, xscript, xblock_level, xstrings, (select extract_uris(xstrings, max_length)), (select extract_contracts(xstrings, 0::bigint)))
    on conflict (address_id) do update set script = xscript, strings = xstrings, uri = (select extract_uris(xstrings, max_length)), contracts = (select extract_contracts(xstrings, 0::bigint));
  end if;
end if;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION H.c_bs (xaddress bigint, xblock_level int)
returns void
as $$
begin
insert into C.contract_balance (address_id, block_level)
values (xaddress, xblock_level)
on conflict do nothing --SEQONLY
;
end;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION G.balanceless_contracts (lim int)
returns table(address char, address_id bigint, block_hash char, block_level int) -- block_level is for logs
as $$
select address(address_id) as address, address_id, block_hash(block_level) as block_hash, block_level
from C.contract_balance
where balance is null
order by block_level desc
limit lim;
$$ language sql stable;

CREATE OR REPLACE FUNCTION G.balanceless_contracts_rev (p int, lim int)
returns table(address char, address_id bigint, block_hash char, block_level int) -- block_level is for logs
as $$
select address(address_id) as address, address_id, block_hash(block_level) as block_hash, block_level
from C.contract_balance
where balance is null
and block_level >= (select level
                      from c.block
                      where proto = p::smallint
                      order by level asc
                      limit 1)
and block_level <= (select level
                      from c.block
                      where proto = p::smallint
                      order by level desc
                      limit 1)
order by block_level asc
limit lim;
$$ language sql stable;



DROP FUNCTION IF EXISTS I.tx (opaid bigint, xsource_id bigint, xdestination_id bigint, xfee bigint, xamount bigint, xorig bigint[], xparameters text, xstorage text, xconsumed_milligas numeric, xstorage_size numeric, xpaid_storage_size_diff numeric,  xentrypoint char, xnonce int, xstatus smallint, errors jsonb, xstrings text[], max_length smallint, ticket_hash_ jsonb);
-- `g text` and not `g jsonb` because sometimes g receives PG-unsupported json
CREATE OR REPLACE FUNCTION I.tx (opaid bigint, xsource_id bigint, xdestination_id bigint, xfee bigint, xamount bigint, xorig bigint[], xparameters text, xstorage text, xconsumed_milligas numeric, xstorage_size numeric, xpaid_storage_size_diff numeric,  xentrypoint char, xnonce int, xstatus smallint, errors jsonb, xstrings text[], max_length smallint, ticket_hash_ jsonb, ticket_receipt_ jsonb)
RETURNS void
AS $$
DECLARE err text; jsonb_parameters jsonb; jsonb_storage jsonb; json_parameters json; json_storage json;
BEGIN
  BEGIN
    jsonb_parameters := xparameters::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_parameters := null;
         json_parameters := xparameters::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  BEGIN
    jsonb_storage := xstorage::jsonb;
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS err = MESSAGE_TEXT;
      IF err = 'unsupported Unicode escape sequence'
      THEN
         jsonb_storage := null;
         json_storage := xstorage::json;
      ELSE
        RAISE EXCEPTION 'I.tx: Error: %', err;
      END IF;
  END;
  INSERT INTO C.tx (operation_id, source_id, destination_id, fee, amount, originated_contracts, parameters, storage, parameters_, storage_, consumed_milligas, storage_size, status, paid_storage_size_diff, entrypoint, nonce, error_trace, strings, ticket_hash, ticket_receipt
  , uri, contracts --SEQONLY
  )
  VALUES (opaid, xsource_id, xdestination_id, xfee, xamount, xorig, jsonb_parameters, jsonb_storage, json_parameters, json_storage, xconsumed_milligas, xstorage_size, xstatus, xpaid_storage_size_diff, xentrypoint, xnonce, errors, xstrings, ticket_hash_, ticket_receipt_
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  )
  ON CONFLICT DO NOTHING --SEQONLY
  ;
  UPDATE C.tx SET strings = xstrings WHERE operation_id = opaid; --SEQONLY
  INSERT INTO C.operation_sender_and_receiver VALUES (opaid, xsource_id, xdestination_id)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
END;
$$ LANGUAGE PLPGSQL;


-- create or replace function foo() returns text
-- AS $$
-- DECLARE x text;
-- BEGIN
-- select '{"error" : "\u0000"}'::jsonb;
-- EXCEPTION WHEN OTHERS THEN
--   GET STACKED DIAGNOSTICS x = PG_EXCEPTION_CONTEXT;
-- return x;
-- END $$ language plpgsql;


DROP FUNCTION IF EXISTS I.origination (opaid bigint, source bigint, k bigint, consumed_milligas numeric, storage_size numeric, paid_storage_size_diff numeric, fee bigint, nonce int, preorigination_id bigint, xscript json, delegate_id bigint, credit bigint, manager_id bigint, block_level_ int, status smallint, errors jsonb, xstrings text[], max_length smallint);
CREATE OR REPLACE FUNCTION I.origination (opaid bigint, source bigint, k bigint, consumed_milligas numeric, storage_size numeric, paid_storage_size_diff numeric, fee bigint, nonce int, preorigination_id bigint, xscript json, delegate_id bigint, credit bigint, manager_id bigint, block_level_ int, status_ smallint, errors jsonb, xstrings text[], max_length smallint)
returns void
as $$
begin
insert into C.origination
values
(opaid, source, k, consumed_milligas, storage_size, paid_storage_size_diff, fee, nonce, preorigination_id, delegate_id, credit, status_, errors)
on conflict do nothing --SEQONLY
;
-- If you don't need scripts, you might want to remove the following insertion:
if xscript is not null and k is not null and status_ = 0
then
  insert into C.contract_script (address_id, script, block_level, strings
    , uri, contracts --SEQONLY
  ) values (k, xscript, block_level_, xstrings
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
  )
  on conflict (address_id) do update set script = xscript, block_level = block_level_ --SEQONLY
  ;
end if;
-- The immediate following insertion might be useless, depending on your needs:
if manager_id is not null
then
insert into C.manager values (opaid, manager_id)
on conflict do nothing--SEQONLY
;
end if;
insert into C.operation_sender_and_receiver
values (opaid, source, k)
on conflict do nothing --SEQONLY
;
end
$$ language plpgsql;


DROP FUNCTION IF EXISTS I.delegation (opaid bigint, source bigint, pkh bigint, gas numeric, f bigint, n int, status smallint, errors jsonb);
CREATE OR REPLACE FUNCTION I.delegation (opaid bigint, source bigint, pkh bigint, gas numeric, f bigint, n int, status_ smallint, errors jsonb)
returns void
as $$
insert into C.delegation values (opaid, source, pkh, gas, f, n, status_, errors)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, source, pkh)
on conflict do nothing --SEQONLY
;
$$ language sql;


DROP FUNCTION IF EXISTS I.reveal (opaid bigint, source bigint, pk char, gas numeric, f bigint, n int, status smallint, errors jsonb);
DROP FUNCTION IF EXISTS I.reveal (opaid bigint, source bigint, pk char, gas numeric, f bigint, n int, status_ smallint, errors jsonb);
CREATE OR REPLACE FUNCTION I.reveal (opaid bigint, source bigint, pk text, gas numeric, f bigint, n int, status_ smallint, errors jsonb)
returns void
as $$
insert into C.reveal values (opaid, source, pk, gas, f, n, status_, errors)
on conflict do nothing --SEQONLY
;
insert into C.operation_sender_and_receiver values (opaid, source, null)
on conflict do nothing --SEQONLY
;
$$ language sql;


DROP FUNCTION IF EXISTS I.rgc (opaid bigint, source bigint, n int, f bigint, status smallint, v jsonb, gas numeric, sc int, ga char, errors jsonb);
CREATE OR REPLACE FUNCTION I.rgc (opaid bigint, source bigint, n int, f bigint, status_ smallint, v jsonb, gas numeric, sc int, ga char, errors jsonb)
RETURNS VOID
AS $$
-- FIXME: n is no longer used
DECLARE ga_id bigint := (SELECT global_address_id(ga));
BEGIN
IF ga_id IS NULL AND (status_ = 0 OR status_ = 1)
THEN
  INSERT INTO C.global_constants VALUES (ga, v, opaid, sc) ON CONFLICT DO NOTHING;
  ga_id := (SELECT global_address_id FROM C.global_constants WHERE global_address = ga);
  IF ga_id IS NULL
  THEN
    RAISE 'could not record global constant opaid=%, ga=%', opaid, ga;
  END IF;
END IF;
INSERT INTO C.register_global_constant VALUES (opaid, source, ga_id, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.set_deposits_limit (opaid bigint, source bigint, n int, f bigint, status smallint, v numeric, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.set_deposits_limit (opaid bigint, source bigint, n int, f bigint, status_ smallint, v numeric, gas numeric, errors jsonb)
RETURNS VOID
AS $$
-- FIXME: n is no longer used
BEGIN
INSERT INTO C.set_deposits_limit VALUES (opaid, source, v, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_origination (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_origination (operation_id, source_id, tx_rollup_origination, consumed_milligas, fee, status, error_trace) VALUES (opaid, source, ro, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c text, bl numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c bytea, bl numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro text, gas numeric, errors jsonb, c bytea, bl numeric, paid_storage_size_diff_ numeric);
CREATE OR REPLACE FUNCTION I.tx_rollup_submit_batch (opaid bigint, source bigint, n int, f bigint, st smallint, ro bigint, gas numeric, errors jsonb, c bytea, bl numeric, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_submit_batch
(operation_id,source_id,tx_rollup,consumed_milligas,fee,status,error_trace,content,burn_limit,paid_storage_size_diff)
VALUES (opaid, source, ro, gas, f, st, errors, c, bl, paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, c jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, c jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_commit (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_commit
(operation_id,source_id,tx_rollup,consumed_milligas,fee,commitment,status,error_trace)
VALUES
(opaid, source, ro, gas, f, c, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int);
CREATE OR REPLACE FUNCTION I.tx_rollup_finalize_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_finalize_commitment
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace)
VALUES (opaid, source, ro, gas, f, l, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_rejection (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int, message_ jsonb, message_position_ int, message_path_ jsonb, message_result_hash_ jsonb, message_result_path_ jsonb, previous_message_result_ jsonb, previous_message_result_path_ jsonb, proof_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_rejection
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace,message,message_position,message_path,message_result_hash,message_result_path,previous_message_result,previous_message_result_path,proof)
VALUES (opaid, source, ro, gas, f, l, status_, errors,message_,message_position_,message_path_,message_result_hash_,message_result_path_,previous_message_result_,previous_message_result_path_,proof_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ jsonb, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ int, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric);
CREATE OR REPLACE FUNCTION I.tx_rollup_dispatch_tickets (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int, context_hash_ jsonb, message_index_ int, message_result_path_ jsonb, tickets_info_ jsonb, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_dispatch_tickets
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace,context_hash,message_index,message_result_path,tickets_info,paid_storage_size_diff)
VALUES (opaid, source, ro, gas, f, l, status_, errors,context_hash_,message_index_,message_result_path_,tickets_info_,paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, l int);
DROP FUNCTION IF EXISTS I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, l int);
CREATE OR REPLACE FUNCTION I.tx_rollup_remove_commitment (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, l int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_remove_commitment
(operation_id,source_id,tx_rollup,consumed_milligas,fee,level,status,error_trace)
VALUES (opaid, source, ro, gas, f, l, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb);
DROP FUNCTION IF EXISTS I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb);
CREATE OR REPLACE FUNCTION I.tx_rollup_return_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.tx_rollup_return_bond
(operation_id,source_id,tx_rollup,consumed_milligas,fee,status,error_trace)
VALUES (opaid, source, ro, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric);
DROP FUNCTION IF EXISTS I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ text, ty_ jsonb, ticketer_ jsonb, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric, ticket_receipt_ jsonb);
CREATE OR REPLACE FUNCTION I.transfer_ticket (operation_id_ bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_milligas_ numeric, error_trace_b jsonb, contents_ jsonb, ty_ jsonb, ticketer_ bigint, amount_ numeric, destination_id_ bigint, entrypoint_ jsonb, paid_storage_size_diff_ numeric, ticket_receipt_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.transfer_ticket
(operation_id, source_id, fee, status, consumed_milligas, error_trace, contents, ty, ticketer, amount, destination_id, entrypoint, paid_storage_size_diff, ticket_receipt)
VALUES
(operation_id_, source_id_, fee_, status_, consumed_milligas_, error_trace_b, contents_, ty_, ticketer_, amount_, destination_id_, entrypoint_, paid_storage_size_diff_, ticket_receipt_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (operation_id_, source_id_, destination_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra text, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ jsonb, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs text, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, bs bytea, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text);
CREATE OR REPLACE FUNCTION I.sc_rollup_originate (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, scrk text, boot_sector_hash_ char, scra bigint, sz numeric, parameters_ty_ jsonb, origination_proof_ text, genesis_commitment_hash_ text)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_originate (operation_id, source_id, address_id, size, kind, boot_sector_hash, consumed_milligas, fee, parameters_ty, origination_proof, genesis_commitment_hash, status, error_trace)
VALUES (opaid, source, scra, sz, scrk, boot_sector_hash_, gas, f, parameters_ty_, origination_proof_, genesis_commitment_hash_, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, scra)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.sc_rollup_boot_sector_txt (hash_ char, boot_sector text)
RETURNS char
AS $$
INSERT INTO c.sc_rollup_boot_sector (hash, txt) VALUES (hash_, boot_sector) ON CONFLICT DO NOTHING;
SELECT hash_;
$$ LANGUAGE SQL;
CREATE OR REPLACE FUNCTION I.sc_rollup_boot_sector_bin (hash_ char, boot_sector bytea)
RETURNS char
AS $$
INSERT INTO c.sc_rollup_boot_sector (hash, bin) VALUES (hash_, boot_sector) ON CONFLICT DO NOTHING;
SELECT hash_;
$$ LANGUAGE SQL;


DROP FUNCTION IF EXISTS I.sc_rollup_cement (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, inbox_level_ int, rollup_ bigint, commitment_hash_ jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_cement (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, inbox_level_ int, rollup_ bigint, commitment_hash_ char) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_cement (operation_id, source_id, fee, consumed_milligas, status, error_trace, inbox_level, rollup_id, commitment_hash)
VALUES (opaid, source, f, gas, status_, errors, inbox_level_, rollup_, commitment_hash_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, rollup_id_ bigint, stakers jsonb, game_status jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric,  errors jsonb, stakers jsonb, game_status jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_timeout (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric,  errors jsonb, stakers_ jsonb, game_status_ jsonb) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_timeout
       (operation_id, source_id, fee, consumed_milligas, status , error_trace, rollup_id , stakers , game_status)
VALUES (opaid       , source   , f  , gas              , status_, errors     , rollup_id_, stakers_, game_status_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_refute (opaid bigint, source bigint, f bigint, gas numeric, status_ smallint, errors jsonb, rollup_id_ bigint, opponent_id_ bigint, refutation_ jsonb, game_status jsonb);
DROP FUNCTION IF EXISTS I.sc_rollup_refute (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric, errors jsonb, opponent_id_ bigint, refutation_ jsonb, game_status jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_refute (opaid bigint, source bigint, f bigint, status_ smallint, rollup_id_ bigint, gas numeric, errors jsonb, opponent_id_ bigint, refutation_ jsonb, game_status_ jsonb) -- since v10.4.0
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_refute (operation_id, source_id, fee, consumed_milligas, status, error_trace, rollup_id, opponent_id, refutation, game_status)
VALUES (opaid, source, f, gas, status_, errors, rollup_id_, opponent_id_, refutation_, game_status_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;



-- since v10.3.0
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status smallint, ro text, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro text, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
DROP FUNCTION IF EXISTS I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb, staked_hash_ text, published_at_level_ int);
CREATE OR REPLACE FUNCTION I.sc_rollup_publish (opaid bigint, source bigint, n int, f bigint, status_ smallint, ro bigint, gas numeric, errors jsonb, c jsonb, staked_hash_ char, published_at_level_ int)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_publish
(operation_id, source_id, rollup_id, consumed_milligas, fee, commitment, status, error_trace, staked_hash, published_at_level)
VALUES
(opaid, source, ro, gas, f, c, status_, errors, staked_hash_, published_at_level_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, ro)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

-- since v10.3.0
CREATE OR REPLACE FUNCTION I.zk_rollup_publish (opaid bigint, source bigint, fee_ bigint, status_ smallint, zk_rollup bigint, gas numeric, errors jsonb, ops_ jsonb, paid_storage_size_diff_ numeric)
RETURNS VOID
AS $$
BEGIN
-- FIXME: jsonb vs json ("error_trace" vs "error_trace_")
INSERT INTO C.zk_rollup_publish
(operation_id,source_id,fee,status,zk_rollup_id,consumed_milligas,error_trace,ops,paid_storage_size_diff)
VALUES
(opaid, source, f, status_, zk_rollup, gas, errors, ops_, paid_storage_size_diff_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, zk_rollup)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_add_messages (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, ia text, r char, ms text[]);
CREATE OR REPLACE FUNCTION I.sc_rollup_add_messages (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, ms text[])
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_add_messages (operation_id, source_id, messages, consumed_milligas, fee, status, error_trace)
VALUES (opaid, source, ms, gas, f, status_, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.event (opaid bigint, source bigint, n int, f bigint, status_ smallint, ty_ jsonb, tag_ jsonb, payload_ jsonb, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: f is no longer used
INSERT INTO C.event VALUES (opaid, source, n, status_, ty_, tag_, payload_, gas, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION I.increase_paid_storage (opaid bigint, source bigint, n int, f bigint, status_ smallint, amount_in_bytes_ numeric, destination_id_ bigint, gas numeric, errors jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.increase_paid_storage (operation_id, source_id, fee, status, amount_in_bytes, destination_id, consumed_milligas, error_trace) VALUES (opaid, source, f, status_, amount_in_bytes_, destination_id_, gas, errors)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, destination_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.dal_publish_slot_header (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, dal_slot_ jsonb, errors jsonb);
CREATE OR REPLACE FUNCTION I.dal_publish_slot_header (opaid bigint, source_id_ bigint, fee_ bigint, status_ smallint, consumed_gas_ numeric, published_level_ int, index_ jsonb, error_trace_ jsonb, commitment_ jsonb, proof_ jsonb)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.dal_publish_slot_header
  (operation_id, source_id, fee, published_level, index, commitment, proof, status, consumed_milligas, error_trace)
VALUES
  (opaid, source_id_, fee_, published_level_, index_, commitment_, proof_, status_, consumed_milligas_, error_trace_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.dal_attestation (opaid bigint, source_id_ bigint, attestor_id_ bigint, attestation_ jsonb, level_ int, delegate_id_ bigint)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.dal_attestation
  (operation_id, attestor_id, attestation, level, delegate_id)
VALUES
  (opaid, attestor_id_, attestation_, level_, delegate_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, attestor_id_, delegate_id_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ text, cemented_commitment_ jsonb, output_proof_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ jsonb, output_proof_ text);
DROP FUNCTION IF EXISTS I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ jsonb, output_proof_ text, ticket_receipt_ jsonb);
CREATE OR REPLACE FUNCTION I.sc_rollup_execute_outbox_message (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, paid_storage_size_diff_ numeric, rollup_ bigint, cemented_commitment_ char, output_proof_ text, ticket_receipt_ jsonb)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.sc_rollup_execute_outbox_message (operation_id, source_id, fee, status, consumed_milligas, error_trace, paid_storage_size_diff, rollup_id, cemented_commitment, output_proof, ticket_receipt)
VALUES (opaid, source, f, status_, gas, errors, paid_storage_size_diff_, rollup_, cemented_commitment_, output_proof_, ticket_receipt_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION I.update_consensus_key (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, pk char)
RETURNS VOID
AS $$
BEGIN
-- FIXME: n is no longer used
INSERT INTO C.update_consensus_key (operation_id, source_id, fee, status, consumed_milligas, error_trace, public_key) VALUES (opaid, source, f, status_, gas, errors, pk)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, null)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS I.sc_rollup_recover_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, rollup_ text);
CREATE OR REPLACE FUNCTION I.sc_rollup_recover_bond (opaid bigint, source bigint, n int, f bigint, status_ smallint, gas numeric, errors jsonb, rollup_ bigint)
RETURNS VOID
AS $$
BEGIN
INSERT INTO C.sc_rollup_recover_bond (operation_id, source_id, fee, status, consumed_milligas, error_trace, rollup_id)
VALUES (opaid, source, f, status_, gas, errors, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
INSERT INTO C.operation_sender_and_receiver VALUES (opaid, source, rollup_)
ON CONFLICT DO NOTHING --SEQONLY
;
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS I.sc_rollup_dal_slot_subscribe; -- this operation no longer exists and was never used

DROP FUNCTION IF EXISTS I.balance (block_level int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char);
DROP FUNCTION IF EXISTS I.balance (block_level_ int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char);
CREATE OR REPLACE FUNCTION I.balance (block_level_ int, opaid bigint, iorid int, bal smallint, k bigint, cy int, di bigint, id int, bpkh char, origin_ smallint)
RETURNS VOID
AS $$
BEGIN
  INSERT INTO C.balance_updates (block_level, operation_id, implicit_operations_results_id, balance_kind, contract_address_id, cycle, diff, id, blinded_public_key_hash, origin)
  VALUES (block_level_, opaid, iorid, bal, k, cy, di, id, bpkh, origin_)
  ON CONFLICT ON CONSTRAINT balance_updates_pkey DO NOTHING --SEQONLY
  ;
  IF k IS NOT NULL THEN INSERT INTO C.contract_balance (address_id, block_level) --SEQONLY
  VALUES (k, block_level_) --SEQONLY
  ON CONFLICT DO NOTHING; --SEQONLY
  END IF; --SEQONLY
END;
$$ LANGUAGE PLPGSQL;


-- insertion for C.implicit_operations_results
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb);
DROP FUNCTION IF EXISTS I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr text, scadd text, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb, ticket_receipt_ jsonb, genesis_commitment_hash_ jsonb);
CREATE OR REPLACE FUNCTION I.ior (op_kind smallint, orig_ks bigint[], ss text[], sto jsonb, gas numeric, sto_size numeric, paid_sto numeric, allocated bool, iorid int, bl int, otr bigint, scadd bigint, scsize numeric, scia jsonb, tx_rollup_level_ int, ticket_hash_ jsonb, ticket_receipt_ jsonb, genesis_commitment_hash_ jsonb)
RETURNS VOID
AS $$
  INSERT INTO C.implicit_operations_results (block_level, operation_kind, consumed_milligas, storage, originated_contracts, storage_size, paid_storage_size_diff, allocated_destination_contract, strings, id, originated_tx_rollup_id, sc_rollup_address_id, sc_rollup_size, sc_rollup_inbox_after, tx_rollup_level, ticket_hash, ticket_receipt, genesis_commitment_hash)
  VALUES (bl, op_kind, gas, sto, orig_ks, sto_size, paid_sto, allocated, ss, iorid, otr, scadd, scsize, scia, tx_rollup_level_, ticket_hash_, ticket_receipt_, genesis_commitment_hash_)
  ON CONFLICT DO NOTHING --SEQONLY
  ;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION I.drain_delegate(opaid bigint, consensus_key_ bigint, delegate_ bigint, destination_ bigint, allocated_destination_contract_ bool)
RETURNS VOID
AS $$
INSERT INTO C.drain_delegate(operation_id,consensus_key_id,delegate_id,destination_id,allocated_destination_contract)
VALUES (opaid, consensus_key_, delegate_, destination_, allocated_destination_contract_)
ON CONFLICT DO NOTHING; --SEQONLY
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION balance_at_level(x varchar, lev int)
RETURNS TABLE(bal bigint)
AS $$
select coalesce(
  (SELECT cb.balance
   FROM C.contract_balance cb
   WHERE address_id = address_id(x)
   AND cb.block_level <= lev
   order by cb.block_level desc limit 1
  ),
  0) as bal
$$ LANGUAGE SQL stable;
-- SELECT balance_at_level('tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93', 1000000);


CREATE OR REPLACE FUNCTION delete_one_operation (xoperation_hash varchar)
returns  void
as $$
select record_log(concat('delete from C.operation where hash = ', xoperation_hash)) where xoperation_hash is not null;
delete from C.operation where hash = xoperation_hash;
$$ language sql;


CREATE OR REPLACE FUNCTION delete_one_block (x varchar)
returns varchar
as $$
select record_log(concat('delete from C.block where hash = ', x)) where x is not null;
delete from C.block where x is not null and hash = x;
select x;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_blocks_from_level (l int)
returns int
as $$
select record_log(concat('delete blocks from level ', l::text, ' where highest level is ', (select concat(level::text, concat(' ', hash)) from C.block order by level::int desc limit 1)));
delete from C.block where level >= l;
select record_log(concat('new highest block: ', (select level from C.block order by level desc limit 1)::text));
select level from C.block order by level desc limit 1;
$$ language SQL;


CREATE OR REPLACE FUNCTION delete_last_block ()
returns int
as $$
select record_log(concat('delete last block, where highest level is ', (select concat(level::text, concat(' ', hash)) from C.block order by level::int desc limit 1)));
delete from C.block where level = (select level from c.block order by level desc limit 1);
select record_log(concat('new highest block: ', (select level from C.block order by level desc limit 1)::text));
select level from C.block order by level desc limit 1;
$$ language SQL;
