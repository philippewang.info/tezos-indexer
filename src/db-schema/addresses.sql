-- Open Source License
-- Copyright (c) 2020-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

SELECT 'addresses.sql' as file;

-- table of all existing addresses, so their storage can be factorized here

CREATE TABLE IF NOT EXISTS C.address ( --NL
 address text,
 address_id bigint not null unique,
 alias bigint,
 primary key(address)
);
-- No need to create index on address because it's the primary key.
-- The following index is not useful during multicore mode. To be confirmed.
CREATE INDEX IF NOT EXISTS addresses_autoid on C.address using btree (address_id); --1 --SEQONLY

CREATE OR REPLACE VIEW C.addresses AS SELECT * FROM C.address;

-- insert into C.address values ('tz1UzKDcDyvK9fq7nfXvXr85v5CqXnhU8r37', -1) on conflict do nothing;
-- insert into C.address values ('tz1bVjLh33Q7dJ7YEgr4pEbqGCxNMmg2QZvu', -2) on conflict do nothing;


-- If you need to know if an address is for an implicit contract (tz...) or an originated contract (KT...), the fastest way is likely to use < and > comparisons.
-- select * from C.address where address < 'L' ; -- returns all originated contracts
-- select * from C.address where address > 't' ; -- returns all implicit contracts
-- select * from C.address where address > 'r' and address < 't' ; -- upcoming rollups (address format not set in stone yet)
-- That should perform a lot faster than something like
-- select * from C.address where address like 'tz%' ; -- returns all implicit contracts
-- because the contents of the `address` column are sorted in an (btree) index.

DROP FUNCTION IF EXISTS address_id(char); -- since v9.9.8
CREATE OR REPLACE FUNCTION address_id(a text) -- since v9.9.8
returns bigint
as $$
select address_id from C.address where address = a;
$$ language sql stable;

DROP FUNCTION IF EXISTS I.address_aux(char,bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION I.address_aux(a text, id bigint) -- since v9.9.8
returns bigint
as $$
insert into C.address (address, address_id) values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

DROP FUNCTION IF EXISTS I.address(char,bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION I.address(a text, id bigint) -- since v9.9.8
returns bigint
as $$
DECLARE r bigint := null; tmp bigint := null;
BEGIN
r := (select address_id from C.address where address = a);
if r is not null
then
  return r;
else
  r := (select I.address_aux(a, id));
  if r is null
  then
    r := (select address_id from C.address where address = a);
    if r is null then
      r := (select I.address_aux(a, -id));
    end if;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.address where address = a);
    if r is null then
      tmp := (select address_id from c.address where address_id < 20000000 order by address_id desc limit 1);
      if tmp is null then
        tmp := coalesce((select address_id from c.address order by address_id desc limit 1), 0);
      end if;
    end if;
    loop
      if r is null then
        tmp := tmp + 1;
        r := (select I.address_aux(a, tmp));
        if r is not null then
          return r;
        else
          r := (select address_id from C.address where address = a);
        end if;
      else
        return r;
      end if;
    end loop;
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record address % % % %', a, r, (select address from c.address where address = a), (select address_id::text from c.address where address = a);
  else
    if a < 't' then insert into C.contract_script(address_id)values(r) on conflict do nothing; end if; --SEQONLY
    return r;
  end if;
end if;
END
$$ language plpgsql;


DROP FUNCTION IF EXISTS address(bigint); -- since v9.9.8
CREATE OR REPLACE FUNCTION address(id bigint)
returns text
as $$
select address from C.address where address_id = id;
$$ language sql stable;

DROP FUNCTION IF EXISTS address(bigint[]); -- since v9.9.8
CREATE OR REPLACE FUNCTION address(id bigint[])
returns text[]
as $$
select array_agg(address) from C.address where address_id in (select unnest(id));
$$ language sql stable;

CREATE TABLE IF NOT EXISTS C.uri ( --NL
 address text,
 address_id int not null unique,
 primary key(address)
);
CREATE INDEX IF NOT EXISTS uri_address_id on C.uri using btree (address_id); --1

CREATE OR REPLACE FUNCTION uri(id bigint)
returns text
as $$
select address from C.uri where address_id = id;
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri(id int[])
returns text[]
as $$
select array_agg(address) from C.uri where address_id in (select unnest(id));
$$ language sql stable;

CREATE OR REPLACE FUNCTION uri_id(i text)
returns int
as $$
select address_id from C.uri where address = i;
$$ language sql stable;



CREATE OR REPLACE FUNCTION I.uri_aux(a text, id integer)
returns integer
as $$
insert into C.uri values(a, id) on conflict do nothing returning address_id;
$$ language SQL;

CREATE OR REPLACE FUNCTION I.uri(a text)
returns integer
as $$
DECLARE r integer := null;
BEGIN
r := (select address_id from C.uri where address = a);
if r is not null
then
  return r;
else
  r := (select I.uri_aux(a, coalesce((select address_id+1 from c.uri order by address_id desc limit 1), 0)));
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      r := (select I.uri_aux(a, coalesce((select address_id-1 from c.uri order by address_id asc limit 1), 0)));
    end if;
  else
    return r;
  end if;
  if r is null
  then
    r := (select address_id from C.uri where address = a);
    if r is null then
      raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
    end if;
  else
    return r;
  end if;
  if r is null
  then
    raise 'Failed to record URI address % % % %', a, r, (select address from C.uri where address = a), (select address_id::text from C.uri where address = a);
  else
    return r;
  end if;
end if;
END
$$ language plpgsql;
