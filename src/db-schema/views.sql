-- Open Source License
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- VIEWS
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Views making querying the chain easier when testing/developing, however it costs in performance.
-- These views also make queries over versions prior to v9 easier since they use the names of the tables of v8.
--------------------------------------------------------------------------------

SELECT 'views.sql' as file;


CREATE OR REPLACE VIEW addresses as select * from C.address;

CREATE OR REPLACE VIEW block as
select *, block_hash(predecessor_id) as predecessor
from C.block b;

CREATE OR REPLACE VIEW "operation" as
select *, block_hash(block_level)
from C.operation;

CREATE OR REPLACE VIEW operation_alpha as
select *
, block_hash(block_level) as block_hash
, operation_hash(hash_id) as hash
from C.operation_alpha;

CREATE OR REPLACE FUNCTION block_hash_alpha(id bigint)
returns char as $$
select block_hash(block_level)
from C.operation
where hash_id = (select hash_id from C.operation_alpha where autoid = id)
$$ language sql stable;

CREATE OR REPLACE VIEW operation_sender_and_receiver as
select *
, block_hash_alpha(operation_id) as block_hash
, operation_hash(operation_hash_id_alpha(operation_id)) as hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(sender_id) as sender
, address(receiver_id) as receiver
from C.operation_sender_and_receiver;

CREATE OR REPLACE VIEW proposal as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(source_id) as source
, proposal(proposal_id) as proposal
from C.proposal;

CREATE OR REPLACE VIEW ballot as
select *
, proposal(proposal_id) as proposal
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(source_id) as source
from C.ballot;

CREATE OR REPLACE VIEW double_endorsement_evidence as
select *
, operation_hash_alpha(operation_id)
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id)
from C.double_endorsement_evidence;

CREATE OR REPLACE VIEW double_baking_evidence as
select *
, operation_hash_alpha(operation_id)
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id)
from C.double_baking_evidence;

CREATE OR REPLACE VIEW manager_numbers as
select *
, operation_hash_alpha(operation_id) as hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
from C.manager_numbers;

CREATE OR REPLACE VIEW endorsement as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(delegate_id) as delegate
from C.endorsement;

CREATE OR REPLACE VIEW seed_nonce_revelation as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
from C.seed_nonce_revelation;

CREATE OR REPLACE VIEW block_alpha as
select *
, block_hash(hash_id) as hash
, address(baker_id) as baker
from C.block_alpha;

CREATE OR REPLACE VIEW deactivated as
select *
, block_hash(block_level) as block_hash
, address(pkh_id) as pkh
from C.deactivated;

CREATE OR REPLACE VIEW contract as
select *
, address(address_id) as address
, block_hash(block_level) as block_hash
, address(mgr_id) as mgr
, address(delegate_id) as delegate
, address(preorig_id) as preorig
from C.contract;

CREATE OR REPLACE VIEW contract_balance as
select *
, address(address_id) as address
, block_hash(block_level) as block_hash
from C.contract_balance;


CREATE OR REPLACE FUNCTION operation_id_alpha (opaid bigint)
RETURNS smallint
AS $$
select id from c.operation_alpha where autoid = opaid;
$$ language SQL stable;

CREATE OR REPLACE FUNCTION operation_internal_alpha (opaid bigint)
RETURNS smallint
AS $$
select internal from c.operation_alpha where autoid = opaid;
$$ language SQL stable;


CREATE OR REPLACE VIEW tx as
select *
, block_hash_alpha(operation_id) as block_hash
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, address(source_id) as source
, address(destination_id) as destination
, operation_internal_alpha (operation_id) as internal
from C.tx;

CREATE OR REPLACE VIEW origination as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, address(k_id) as k
, block_hash_alpha(operation_id) as block_hash
from C.origination;

CREATE OR REPLACE VIEW delegation as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, address(pkh_id) as pkh
, block_hash_alpha(operation_id) as block_hash
from C.delegation;

CREATE OR REPLACE VIEW reveal as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, block_hash_alpha(operation_id) as block_hash
from C.reveal;



CREATE OR REPLACE VIEW balance as
select
block_hash_alpha(b1.operation_id) as block_hash
, operation_hash_alpha(b1.operation_id) as operation_hash
, operation_id_alpha(b1.operation_id) as op_id
, operation_internal_alpha(b1.operation_id) as internal
, b1.internal_operations_results_id
, b1.balance_kind
, b1.cycle
, b1.diff
, b1.id
, address(b1.contract_address_id) as contract_address
from C.balance_updates b1;


CREATE OR REPLACE VIEW bigmap as select *, block_hash(block_level) as block_hash, address(sender_id) as sender, address(receiver_id) as receiver from C.bigmap;


CREATE OR REPLACE VIEW light_mempool_operations as
select
  hash,
  first_seen_level,
  last_seen_level,
  (last_seen_timestamp-first_seen_timestamp) as presence,
  status,
  id,
  operation_kind,
  source,
  destination,
  autoid
from M.operation_alpha;

CREATE OR REPLACE VIEW token_contract as select *, address(address_id) as address, block_hash(block_level) as block_hash from T.contract;

CREATE OR REPLACE VIEW token_balance as select *, address(address_id) as address, address(token_address_id) as token_address from T.balance;

CREATE OR REPLACE VIEW token_operation as select *, operation_hash_alpha(operation_id) as operation_hash, address(token_address_id) as token_address, address(caller_id) as caller, block_hash_alpha(operation_id) as block_hash from T.operation;

CREATE OR REPLACE VIEW token_transfer as select *, operation_hash_alpha(operation_id) as operation_hash, address(source_id) as source, address(destination_id) as destination, block_hash_alpha(operation_id) as block_hash from T.transfer;

CREATE OR REPLACE VIEW token_approve as select *, operation_hash_alpha(operation_id) as operation_hash, address(address_id) as address, block_hash_alpha(operation_id) as block_hash from T.approve;

CREATE OR REPLACE VIEW token_get_balance as select *, operation_hash_alpha(operation_id) as operation_hash, address(address_id) as address, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_balance;

CREATE OR REPLACE VIEW token_get_allowance as select *, operation_hash_alpha(operation_id) as operation_hash, address(source_id) as source, address(destination_id) as destination, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_allowance;

CREATE OR REPLACE VIEW token_get_total_supply as select *, operation_hash_alpha(operation_id) as operation_hash, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_total_supply;
