-- Open Source License
-- Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>
-- Copyright (c) 2019-2022 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Naming conventions:
-- - for tables:
--   * use singular for names, use plural for column names that are arrays
--   * table 'proposals' is plural because what it contains is plural (it contains 'proposals' on each row)
--   * exceptions: C.address (probably should've been named C.address)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Foreign keys:
-- They are declared in special comments starting with `--FKEY`, and must start the line.
--  --FKEY name_of_foreign_key ; name_of_table ; set, of, columns ; foreign_table_name(column_name) ; action
-- where action can be CASCADE or SET NULL
-- Refer to existing one for examples.
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

SELECT 'chain.sql' as file;

-----------------------------------------------------------------------------
-- Some logs about what happens while running the indexer

CREATE TABLE IF NOT EXISTS indexer_log (
   timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   version text not null default '',
   argv text not null default '',
   action text not null default '',
   primary key (timestamp, version, argv, action)
);
-- CREATE INDEX IF NOT EXISTS indexer_log_timestamp on indexer_log using btree(timestamp); --1

-----------------------------------------------------------------------------
-- storing the chain id

CREATE TABLE IF NOT EXISTS C.chain ( --L
  hash char(15) primary key
);


-----------------------------------------------------------------------------
-- this table inlines blocks and block headers
-- see lib_base/block_header.mli

CREATE TABLE IF NOT EXISTS C.block ( --NL
  hash char(51) not null,
  -- Block hash.
  -- 51 = 32 bytes hashes encoded in b58check + length of prefix "B"
  -- see lib_crypto/base58.ml
  level int not null,
  -- Height of the block, from the genesis block.
  proto smallint not null,
  -- Number of protocol changes since genesis modulo 256.
  predecessor int not null,
  -- preceding block -- useful mostly for cascade deletion!
  timestamp timestamp not null,
  -- Timestamp at which the block is claimed to have been created.
  validation_passes smallint not null,
  -- Number of validation passes (also number of lists of operations).
  merkle_root char(53) not null,
  -- see [operations_hash]
  -- Hash of the list of lists (actually root hashes of merkle trees)
  -- of operations included in the block. There is one list of
  -- operations per validation pass.
  -- 53 = 32 bytes hashes encoded in b58 check + "LLo" prefix
  fitness text,
  -- A sequence of sequences of unsigned bytes, ordered by length and
  -- then lexicographically. It represents the claimed fitness of the
  -- chain ending in this block.
  context_hash char(52) not null
  -- Hash of the state of the context after application of this block.
);
--PKEY block_pkey; C.block; hash --SEQONLY
--FKEY block_predecessor_fkey ; C.block ; predecessor ; C.block(level) ; CASCADE --SEQONLY
CREATE UNIQUE INDEX IF NOT EXISTS block_level on C.block using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_proto_level  on C.block using btree (proto, level); --SEQONLY --1

-----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS C.operation_kind ( -- since 10.3.0
  kind text not null primary key
, id smallint not null
);
-- this table is populated when DB is created or each time the DB is updated
-- do not populate this table manually or you may make your DB incompatible with the indexer
CREATE UNIQUE INDEX IF NOT EXISTS operation_kind_id on C.operation_kind using btree (id); --SEQONLY --1

CREATE OR REPLACE FUNCTION operation_kind (k smallint)
RETURNS	char
AS
$$
SELECT kind FROM C.operation_kind WHERE id = k;
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_kind (k int)
RETURNS char
AS
$$
SELECT operation_kind(k::smallint)
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION declare_operation (op text, n int) -- since 10.3.0
RETURNS VOID
AS $$
DECLARE x text := (SELECT operation_kind(n));
BEGIN
  IF x = op
  THEN
    RETURN;
  ELSIF x IS NULL
  THEN
    INSERT INTO C.operation_kind(kind, id) VALUES (op, n::smallint);
  -- ELSE
  --   RAISE 'Your database has incompatible data in table C.operation_kind';
  -- --> we do nothing if the id is already taken
  END IF;
END;
$$ LANGUAGE PLPGSQL;


-----------------------------------------------------------------------------
-- implicit operations results (since protocol PtGRANAD) -- since v9.5.0
CREATE TABLE IF NOT EXISTS C.implicit_operations_results ( --NL
  block_level int not null
, operation_kind smallint not null
, consumed_milligas numeric not null
, storage jsonb
, originated_contracts bigint[]
, storage_size numeric
, paid_storage_size_diff numeric
, allocated_destination_contract bool
, strings text[]
, id int not null
, originated_tx_rollup_id bigint  -- since v9.9.3
, tx_rollup_level int -- since v10.0.0rc1
, ticket_hash jsonb -- since v10.0.0rc1
, sc_rollup_address_id bigint -- since v9.9.3 -- unused until at least proto J
, sc_rollup_size numeric -- since v9.8.2 -- unused until at least proto J
, sc_rollup_inbox_after jsonb -- since v9.9 -- unused until at least proto J
, ticket_receipt jsonb -- since v10.3.0
, genesis_commitment_hash char(54)  -- since v10.4.0
-- balance updates are detailed in C.balance_updates
);
CREATE UNIQUE INDEX IF NOT EXISTS implicit_operations_results_block_id on C.implicit_operations_results using btree (block_level, id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_block on C.implicit_operations_results using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_opkind on C.implicit_operations_results using btree (operation_kind); --SEQONLY --1
--FKEY implicit_operations_results_tx_roll; C.implicit_operations_results; originated_tx_rollup_id; C.address(address_id); CASCADE --SEQONLY
--FKEY implicit_operations_results_op_kind; C.implicit_operations_results; operation_kind; C.operation_kind(id); CASCADE --SEQONLY -- since v10.3.0

DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN tx_rollup_level int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN ticket_hash jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN ticket_receipt jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ADD COLUMN genesis_commitment_hash char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.implicit_operations_results ALTER COLUMN genesis_commitment_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;



-----------------------------------------------------------------------------
-- operations seen from block-level, therefore non-protocol-specific information

CREATE TABLE IF NOT EXISTS C.operation ( --NL
  -- Note: an operation may point to a rejected block only if the
  -- operation itself was deleted from the chain.
  -- If the operation was included in a rejected block but then
  -- reinjected into another block, then this table contains the
  -- latest block_hash associated to that operation.
  -- Hypothesis: the latest write is always right.
  hash char(51) not null, -- operation hash
  block_level int not null, -- char(51) not null, -- block hash
  hash_id bigint not null
);
--FKEY operation_block_level_fkey; C.operation; block_level; C.block(level) ; CASCADE --SEQONLY
--PKEY operation_pkey; C.operation; hash_id --SEQONLY

CREATE INDEX IF NOT EXISTS operation_block on C.operation using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash on C.operation using btree (hash); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash_id on C.operation using btree (hash_id); --SEQONLY --1

CREATE OR REPLACE FUNCTION operation_hash(id bigint) returns char as $$ select hash from C.operation where hash_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_hash_id(h char) returns bigint as $$ select hash_id from C.operation where hash = h $$ language sql stable;

-----------------------------------------------------------------------------
-- Index of protocol-specific contents of an operation
-- An "operation" at the "shell" level is a "set of operations" at the "protocol" level.
-- In the following table, "hash_id" refers to an operation at the shell level.
-- At the protocol level, a shell-level operation is a list of operations (the word "operation" has different meanings).
-- Inside protocol-level operations, there can be some additional "internal operations".
-- "Internal operations" (a.k.a. "internal manager operation") are operations that are at manager-operation-level.
-- An "internal operation", so far, for protocols 1 to 11, are only manager operations (inside manager operations).
-- There are differences between "manager operations" and "internal manager operations".
-- For instance, internal ones don't have data in manager_numbers, but are the only ones that have a "nonce" (which are integers).

CREATE TABLE IF NOT EXISTS C.operation_alpha ( --NL
    block_level int not null
  -- block hash id
  , hash_id bigint not null
  -- operation hash id
  , id smallint not null
  -- index of op in contents_list
  , operation_kind smallint not null -- cf. table C.operation_kind
  , internal smallint not null
  -- block hash
  , autoid bigint not null -- counter id
);
--PKEY operation_alpha_pkey; C.operation_alpha; hash_id, id, internal, block_level --SEQONLY
--FKEY operation_alpha_hash_fkey; C.operation_alpha; hash_id; C.operation(hash_id) ; CASCADE --SEQONLY
--FKEY operation_alpha_block_hash_fkey; C.operation_alpha; block_level; C.block(level) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS operation_alpha_kind on C.operation_alpha using btree (operation_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_hash on C.operation_alpha using btree (hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_block_hash on C.operation_alpha using btree (block_level); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS operation_alpha_autoid on C.operation_alpha using btree (autoid); --1
--FKEY operation_alpha_op_kind; C.operation_alpha; operation_kind; C.operation_kind(id); CASCADE --SEQONLY -- since v10.3.0


CREATE OR REPLACE FUNCTION operation_alpha_autoid(ophid bigint, opid smallint, i smallint, block_level bigint)
RETURNS bigint
AS $$
SELECT autoid
FROM C.operation_alpha
WHERE (hash_id, id, internal, block_level) = (ophid, opid, i, block_level);
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS operation_hash_alpha; --change of signature in v9.7.7
CREATE OR REPLACE FUNCTION operation_hash_alpha(opaid bigint)
RETURNS char as $$
select o.hash from c.operation o, c.operation_alpha a where a.autoid = opaid and a.hash_id = o.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_hash_id_alpha(opaid bigint)
returns bigint as $$ select hash_id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id_alpha(opaid bigint)
returns smallint as $$ select id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_alpha_ids(h char)
returns table (id bigint) as $$ select autoid from C.operation_alpha where hash_id = operation_hash_id(h) $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id (h char, i smallint, inter smallint)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i and internal = inter $$ language SQL stable;
CREATE OR REPLACE FUNCTION operation_id (h char, i int, inter int)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i::smallint and internal = inter::smallint $$ language SQL stable;


CREATE OR REPLACE FUNCTION block_hash(id int) returns char as $$ select hash from C.block where level = id $$ language sql stable;
CREATE OR REPLACE FUNCTION op_shift() returns bigint as $$ select 10000000::bigint $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_level_from_opid(opid bigint) returns int as $$ select (opid / op_shift())::int where opid > 0 $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_hash(opid bigint) returns char as $$ select hash from C.block where level = block_level_from_opid(opid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level(h char) returns int as $$ select level from C.block where hash = h $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_block_level(block_level int) returns int as $$ select block_level $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_opaid(opaid bigint) returns int as $$ select block_level_from_opid(opaid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_operation_hash_id(ophid bigint) returns int as $$ select block_level_from_opid(ophid) $$ language sql stable;

-----------------------------------------------------------------------------
-- Convenience table to rapidly get a list of operations linked to an address

CREATE TABLE IF NOT EXISTS C.operation_sender_and_receiver ( --L
  operation_id bigint not null,
  sender_id bigint not null,
  receiver_id bigint
);
--PKEY operation_sender_and_receiver_pkey; C.operation_sender_and_receiver; operation_id --SEQONLY
--FKEY operation_sender_and_receiver_operation_id_fkey; C.operation_sender_and_receiver; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY operation_sender_and_receiver_sender_id_fkey; C.operation_sender_and_receiver; sender_id; C.address(address_id) ; CASCADE
--FKEY operation_sender_and_receiver_receiver_id_fkey; C.operation_sender_and_receiver; receiver_id; C.address(address_id) ; CASCADE

-- since v9.9.3, the following 2 indexes replace indexes on solely sender_id and receiver_id
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_sender_opaid on C.operation_sender_and_receiver using btree (sender_id, operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_receiver_opaid on C.operation_sender_and_receiver using btree (receiver_id, operation_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Protocol amendment proposals

CREATE TABLE IF NOT EXISTS C.proposals ( --NL
  proposal char(51) not null,
  proposal_id bigint not null
  -- about proposal_id: the important factor is to have a unique
  -- value. That value could be smaller and probably using a smallint
  -- would be enough to represent all proposals happening on tezos for
  -- many years to come. However here we use a bigint, to "simply" use
  -- the first operation's ophid that needs to access `proposal_id`.
  -- The first operation that needs that access will write the
  -- proposal's hash value into the `proposal` column, and give its
  -- `ophid` as `proposal_id`. Further operations will attempt to do
  -- the same, and will fail the writing (since `proposal` is a pkey)
  -- but will be able to access `proposal_id`.
  -- All that is to avoid using Postgresql's SERIAL because those
  -- generate values that cannot be accessed within an SQL transaction.
);
--PKEY proposals_pkey; C.proposals; proposal
CREATE UNIQUE INDEX IF NOT EXISTS proposals_proposal_id on C.proposals using btree (proposal_id); --1

-- pre-filling c.proposals for multicore mode
-- note that the proposal_id is (generally) NOT the protocol number
insert into c.proposals (proposal, proposal_id) values --MULTICORE
  ('PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo', 1) --MULTICORE
, ('PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq', 2) --MULTICORE
, ('PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb', 3) --MULTICORE
, ('PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH', 4) --MULTICORE
, ('PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU', 5) --MULTICORE
, ('PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD', 6) --MULTICORE
, ('Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd', 7) --MULTICORE
, ('PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f', 8) --MULTICORE
, ('Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z', 9) --MULTICORE
, ('PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV', 10) --MULTICORE
, ('PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY', 24310510000223) --MULTICORE
, ('PtJakartaiDz69SfDDLXJSiuZqTSeSKRDbKVZC8MNzJnvRjvnGw', 23132140000212) --MULTICORE
, ('PtSEBSEBSEBSEBSEBSEBSEBSEBSEBSEBSEBSEBSEBSEBS5VTrmo', 23239140000216) --MULTICORE
, ('PtGXSEBUHXFARMxDEXxNFTxRPCxSEBxSEBxSEBxSEBxSE3GRJJC', 23239140000217) --MULTICORE
, ('Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A', 22030520000061) --MULTICORE
, ('PtGaNoLBZ5cEKq9B2xxT3wsJb7GLGt7zP1ZHabKn1E9JVcZ6mfA', 20791890000075) --MULTICORE
, ('PscPJfNBhckv3mS4f6bg3VhTw9fBbR8DUcdZeSKpfgiufsgCEoE', 20710590000067) --MULTICORE
, ('PsiThaCaT47Zboaw71QWScM8sXeMM7bbQFncK9FLqYc6EKdpjVP', 20774600000076) --MULTICORE
, ('PteJgr89XGANzE5xNa6H9gLDYRrnS7mDaNmQYxHCn72atssDrMy', 19930420000067) --MULTICORE
, ('PtHangzHogokSuiMHemCuowEavgYTP8J5qQ9fQS793MHYFpCY3r', 18730050000071) --MULTICORE
, ('PsCUKj6wydGtDNGPRdxasZDs4esZcVD9tf44MMiVy46FkD9q1h9', 17510050000076) --MULTICORE
, ('PsmarYW3qFVZUNYwJaYvPEYG96N3awx5SpYs3PxiAWmCzRBAhY8', 15036810000024) --MULTICORE
, ('PtCH1CKENxtchickentchickentchickenXbockXbockX6kjJkR', 15036810000025) --MULTICORE
, ('PtGRENLBgC4kALRCjYwBAgDNSvK9ZDCst7cRQ1aNP9Ke1cZDwnY', 15018870000023) --MULTICORE
, ('PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i', 14390340000023) --MULTICORE
, ('PtPLENTYDEFiDoTCoMxPLENTYofDEFionTEZoSRiPETHxCKT7dp', 14984500000020) --MULTICORE
, ('PtGxSEBUHxTHiSxiSxAxTESTxoFxTHExEMERGENCYxMSG3SEkQs', 14964120000022) --MULTICORE
, ('PtGxSEBUHxBAKERxBRoADCASTxSYSTEMxTEZoSxderpxx6bsL3U', 14964220000025) --MULTICORE
, ('PtGxSEBUHxTHiSxGRANADAXPRoposaLXisXNoTXGoooDx6Y1FUP', 14964290000021) --MULTICORE
, ('PtGxSEBUHXthereXisXaXbakerXoverde1agatedx13376siwry', 14964330000019) --MULTICORE
, ('PtGxSEBUHXxPLEASExSENDxHELPxHoDLGANGHoDLGANGx7BNY3A', 14964370000022) --MULTICORE
, ('PtGTHEXCHANCELLoRX1SXoNXBR1NKXoFXBA1LoUTTZBTC7AwbQm', 14964470000024) --MULTICORE
, ('PtGxRAKxNxSTAKxALLxYoURxTEZoSxrxBELoNGxToxUSx7Bzcfn', 14964510000025) --MULTICORE
, ('Ptezosti11youbezostezosti11youbezosbezosbezosAH6zwu', 14964750000025) --MULTICORE
, ('PtCRUNCHYxNETWoRKxTEZoSxASxAxSERV1CExPLATFoRMD4GMZL', 14964880000020) --MULTICORE
, ('PtBARTxBULL1SHxARTxUNSToPPABLExARTxBARTBARTBA6jefXM', 14964950000023) --MULTICORE
, ('PtSPACEFARMDoTXYZFLAMEDEF1FLAMEFLAMEF1REFLAMEC43qUk', 14964960000021) --MULTICORE
, ('PtDaotdaoxTDAoxTEZoN1ANSxTACoDAoxTACoTACoTACo4z8QHD', 14965000000024) --MULTICORE
, ('PtBoHNERDYLDoHVXPRoxBoNERDiLDoxBDVXPx666xGAP3D9UdEy', 14965060000020) --MULTICORE
, ('PtEASTERNxTURKEYx1SxWESTERNxARMEN1AxARTSAKHAM3tv5NV', 14965100000022) --MULTICORE
, ('PtHdaoXobjktXsyncXhicetnuncDoTxyzHERExANDxNoW8ffnzM', 14965160000023) --MULTICORE
, ('PsFLorBArSaXjuy9oP76Qv1v2FRYnUs7TFtteK5GkRBC24JvbdE', 13750180000027) --MULTICORE
, ('PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg', 25339530000221) --MULTICORE
, ('PtSEBUHxxWANTSwor1dPEACEstopAZERBAYCANkiLLers5iuuKs', 27486830000227) --MULTICORE
, ('PtLimaPtLMwfNinJi9rCfDPWea8dFgTZ1MeJ9f1m2SRic6ayiwW', 27837330000220) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGG7BLa7S', 28059440000228) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG17gK5Vx', 28059440000229) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG2B9SN3p', 28059440000230) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG34Awa2v', 28059440000231) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG47pk4ks', 28059440000232) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG59q16Q3', 28059440000233) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG68AbC87', 28059440000234) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG77xs636', 28059440000235) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG8AkUyPm', 28059440000236) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGG97fVQHT', 28059440000237) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGZ4SCdat', 28059440000238) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGzBJnJ1Q', 28059440000239) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGs5UY8my', 28059440000240) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGtAkjqSL', 28059440000241) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGq9t69MS', 28059440000242) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftGGp8rJhZE', 28059440000243) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftREE8H482t', 28059440000246) --MULTICORE
, ('PtSEBUHforSALEatTHEopenseaBUYsebuhBESThnftXXX52vyMV', 28059440000247) --MULTICORE
, ('PtMumbaiiFFEGbew1rRjzSPyzRbA51Tm3RVZL5suHPxSZYDhCEc', 30654780000222) --MULTICORE
, ('PtMumbaiveNjgvoAng9E3AtNqtBtCQWqpXJdPEdAanAYCZbCgey', 30654790000224) --MULTICORE
on conflict do nothing; --MULTICORE


CREATE OR REPLACE FUNCTION proposal(id bigint) returns char as $$ select proposal from C.proposals where proposal_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION proposal_id(p char) returns bigint as $$ select proposal_id from C.proposals where proposal = p $$ language sql stable;

CREATE TABLE IF NOT EXISTS C.proposal ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
);
--PKEY proposal_pkey; C.proposal; operation_id, proposal_id
--FKEY proposal_operation_id_fkey; C.proposal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY proposal_source_fkey; C.proposal; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY proposal_proposal_id_fkey; C.proposal; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS proposal_operation on C.proposal using btree (operation_id); --1
CREATE INDEX IF NOT EXISTS proposal_source on C.proposal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS proposal_proposal on C.proposal using btree (proposal_id); --1
CREATE INDEX IF NOT EXISTS proposal_period on C.proposal using btree (period); --SEQONLY --1

-----------------------------------------------------------------------------
-- Ballots for proposal amendment proposals
DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'one_ballot') THEN
    CREATE TYPE one_ballot AS ENUM ('nay', 'yay', 'pass');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS C.ballot ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
  , ballot one_ballot not null
);
--PKEY ballot_pkey; C.ballot; operation_id --SEQONLY
--FKEY ballot_operation_id_fkey; C.ballot; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY ballot_proposal_id_fkey; C.ballot; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS ballot_source on C.ballot using btree (source_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Double endorsement evidence

CREATE TABLE IF NOT EXISTS C.double_endorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_endorsement_evidence_pkey; C.double_endorsement_evidence; operation_id --SEQONLY
--FKEY double_endorsement_evidence_operation_id_fkey; C.double_endorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Double preendorsement evidence

CREATE TABLE IF NOT EXISTS C.double_preendorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_preendorsement_evidence_pkey; C.double_preendorsement_evidence; operation_id --SEQONLY
--FKEY double_preendorsement_evidence_operation_id_fkey; C.double_preendorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Double baking evidence

CREATE TABLE IF NOT EXISTS C.double_baking_evidence ( --L
    operation_id bigint not null
  , bh1 jsonb -- block header 1
  , bh2 jsonb -- block header 2
  , bh1_ json
  , bh2_ json
);
--PKEY double_baking_evidence_pkey; C.double_baking_evidence; operation_id --SEQONLY
--FKEY double_baking_evidence_operation_id_fkey; C.double_baking_evidence;operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Common data for manager operations

CREATE TABLE IF NOT EXISTS C.manager_numbers ( --NL
    operation_id bigint not null
  , counter numeric not null
  -- counter
  , gas_limit numeric not null
  -- gas limit
  , storage_limit numeric not null
  -- storage limit
);
--PKEY manager_numbers_pkey; C.manager_numbers; operation_id --SEQONLY
--FKEY manager_numbers_operation_id_fkey; C.manager_numbers; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
-- Not sure the following indexes are relevant:
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_counter on C.manager_numbers using btree (counter); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_gas_limit on C.manager_numbers using btree (gas_limit); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_storage_limit on C.manager_numbers using btree (storage_limit); --SEQONLY


-----------------------------------------------------------------------------
-- Account Activations

CREATE TABLE IF NOT EXISTS C.activation ( --L
   operation_id bigint not null
 , pkh_id bigint not null
 , activation_code text not null
);
--PKEY activation_pkey; C.activation; operation_id --SEQONLY
--FKEY activation_operation_id_fkey; C.activation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY activation_pkh_fkey; C.activation; pkh_id; C.address(address_id) ; CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Endorsements

CREATE TABLE IF NOT EXISTS C.endorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , endorsement_power int -- nullable because not present until proto 12
);
--PKEY endorsement_pkey; C.endorsement; operation_id --SEQONLY
--FKEY endorsement_operation_id_fkey; C.endorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY endorsement_delegate_fkey; C.endorsement; delegate_id; C.address(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS endorsement_delegate_id on C.endorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS endorsement_level on C.endorsement using btree (level); --SEQONLY --1


-----------------------------------------------------------------------------
-- Preendorsements

CREATE TABLE IF NOT EXISTS C.preendorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , preendorsement_power int -- nullable because not present until proto 12
);
--PKEY preendorsement_pkey; C.preendorsement; operation_id --SEQONLY
--FKEY preendorsement_operation_id_fkey; C.preendorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY preendorsement_delegate_fkey; C.preendorsement; delegate_id; C.address(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS preendorsement_delegate_id on C.preendorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS preendorsement_level on C.preendorsement using btree (level); --SEQONLY --1


-----------------------------------------------------------------------------
-- Seed nonce revelation

CREATE TABLE IF NOT EXISTS C.seed_nonce_revelation ( --L
    operation_id bigint not null
  -- index of the operation in the block's list of operations
 , level int not null
 , nonce char(66) not null
);
--PKEY seed_nonce_revelation_pkey; C.seed_nonce_revelation; operation_id --SEQONLY
--FKEY seed_nonce_revelation_operation_id_fkey; C.seed_nonce_revelation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Blocks at alpha level.
-- "level_position = cycle * blocks_per_cycle + cycle_position"

CREATE TABLE IF NOT EXISTS C.block_alpha ( --L
  level int not null
  -- block hash id
  , baker_id bigint not null -- baker_id or consensus_pkh_id
  -- pkh of baker
  , level_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The level of the block relative to the block that
     starts protocol alpha. This is specific to the
     protocol alpha. Other protocols might or might not
     include a similar notion.
  */
  , cycle int not null
  -- cycle
  , cycle_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The current level of the block relative to the first
     block of the current cycle.
  */
  , voting_period jsonb not null
  /* increasing integer.
     from proto_alpha/level_repr:
     voting_period = level_position / blocks_per_voting_period */
  , voting_period_position int not null
  -- voting_period_position = remainder(level_position / blocks_per_voting_period)
  , voting_period_kind smallint not null
  /* Proposal = 0
     Testing_vote = 1
     Testing = 2
     Promotion_vote = 3
     Adoption = 4
   */
  , consumed_milligas numeric not null
  /* total milligas consumed by block. Arbitrary-precision integer. */
  , delegate_id bigint -- if not null, then baker_id contains consensus_pkh_id (i.e. the pkh used for baking by the baker), introduced by proto L
);
--PKEY block_alpha_pkey; C.block_alpha; level --SEQONLY
--FKEY block_alpha_hash_fkey; C.block_alpha; level; C.block(level); CASCADE --SEQONLY
--FKEY block_alpha_baker_fkey; C.block_alpha; baker_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS block_alpha_baker on C.block_alpha using btree (baker_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_level_position on C.block_alpha using btree (level_position); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle on C.block_alpha using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle_position on C.block_alpha using btree (cycle_position); --SEQONLY --1

DO $$
BEGIN
ALTER TABLE C.block_alpha ADD COLUMN delegate_id bigint;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
CREATE INDEX IF NOT EXISTS block_alpha_delegate on C.block_alpha using btree (delegate_id); --SEQONLY --1
--FKEY block_alpha_delegate; C.block_alpha; delegate_id; C.address(address_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Deactivated accounts

CREATE TABLE IF NOT EXISTS C.deactivated ( --L
  pkh_id bigint not null,
  -- pkh of the deactivated account(tz1...)
  block_level int not null
  -- block hash at which deactivation occured
);
--PKEY deactivated_pkey; C.deactivated; pkh_id, block_level  --SEQONLY
--FKEY deactivated_pkh_fkey; C.deactivated; pkh_id; C.address(address_id); CASCADE --SEQONLY
--FKEY deactivated_block_hash_fkey; C.deactivated; block_level; C.block(level); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS deactivated_pkh on C.deactivated using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS deactivated_block_hash on C.deactivated using btree (block_level); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS deactivated_autoid on C.deactivated using btree (autoid); --OPT --SEQONLY --FIXME: is it useful?


-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_balance ( --L
  address_id bigint not null,
  block_level int not null,
  balance bigint -- make it nullable so that it can be filled asynchronously
  -- N.B. it would be bad to have "address_id" as a primary key,
  -- because if you update a contract's balance using a
  -- rejected block(uncle block) and then the new balance is not updated
  -- once the rejected block is discovered,
  -- you end up with wrong information
);
--PKEY contract_balance_pkey; C.contract_balance; address_id, block_level  --SEQONLY
--FKEY contract_balance_address_fkey; C.contract_balance; address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY contract_balance_block_hash_fkey; C.contract_balance; block_level; C.block(level); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS contract_balance_address on C.contract_balance using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_balance on C.contract_balance using btree (balance); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS contract_balance_address_block_level on C.contract_balance using btree (address_id, block_level desc); --SEQONLY --1 <---- do not create this index, it's already a primary key
CREATE INDEX IF NOT EXISTS contract_balance_block_hash on C.contract_balance using btree (block_level); --SEQONLY --1

-- the following index is for insertion performance
CREATE INDEX IF NOT EXISTS contract_balance_balance_block_level on C.contract_balance using btree (balance, block_level); --SEQONLY --1

-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_script ( -- since v9.1.0 --L
  address_id bigint primary key
, script jsonb -- make it nullable so that it can be filled asynchronously
, block_level int -- if null then value of `script` is unreliable and should be updated, if not null, then it's origin of the script
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, script_ json
, missing_script smallint -- not null means the indexer failed to get the script -- since v9.5.0
);
--FKEY contract_script_address_fkey; C.contract_script; address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY contract_script_block_level_fkey; C.contract_script; block_level; C.block(level); SET NULL --SEQONLY
CREATE INDEX IF NOT EXISTS contract_script_address on C.contract_script using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_block_hash on C.contract_script using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_missing_script on C.contract_script using btree (missing_script); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS contract_script_strings on C.contract_script using GIN (strings); --SEQONLY

-- the following index is for speeding up insertions
CREATE INDEX IF NOT EXISTS contract_script_address_id_script on C.contract_script using btree (address_id, (script is null)); --SEQONLY --1
-- the following index is for speeding up insertions
CREATE INDEX IF NOT EXISTS contract_script_address_id_script_missing_script on C.contract_script using btree (address_id, (script is null), (script_ is null), (missing_script is null)); --SEQONLY --1

-- BEGIN keep up to date with conversion_from_multicore.sql
CREATE INDEX IF NOT EXISTS contract_script_uri on C.contract_script using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_contracts on C.contract_script using GIN (contracts); --SEQONLY --1
-- END keep up to date with conversion_from_multicore.sql

INSERT INTO C.contract_script (address_id) select address_id from C.address where address < 't' on conflict do nothing; --SEQONLY

CREATE INDEX IF NOT EXISTS contract_script_script_code ON C.contract_script USING HASH ((script -> 'code')); --SEQONLY


-----------------------------------------------------------------------------
-- Transactions -- manop

CREATE TABLE IF NOT EXISTS C.tx ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source address
, destination_id bigint not null
  -- dest address
, fee bigint -- null if internal operation -- fix, since v10.5.0
  -- fees
, amount bigint not null
  -- amount
, originated_contracts bigint[]
  -- since v9.5.0
, parameters jsonb -- jsonb since v9.3 -- invalid Unicode sequences are replaced by an error message
  -- optional parameters to contract in json-encoded Micheline
, storage jsonb
  -- optional parameter for storage update
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, entrypoint text
  -- entrypoint
, nonce int -- non null for internal operations
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, parameters_ json
, storage_ json
, ticket_hash jsonb -- since v10
, ticket_receipt jsonb -- since v10.3.0
);
--PKEY tx_pkey; C.tx; operation_id --SEQONLY
--FKEY tx_operation_id_fkey; C.tx; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_source_fkey; C.tx; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_destination_fkey; C.tx; destination_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_source on C.tx using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_destination on C.tx using btree (destination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_destination_opaid on C.tx using btree (destination_id, operation_id desc); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_strings on C.tx using GIN (strings); --SEQONLY

ALTER TABLE C.tx ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.tx WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE C.tx SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;

-- BEGIN keep up to date with conversion_from_multicore.sql
CREATE INDEX IF NOT EXISTS tx_uri on C.tx using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_contracts on C.tx using GIN (contracts); --SEQONLY --1
-- END keep up to date with conversion_from_multicore.sql

DO $$
BEGIN
ALTER TABLE C.tx ADD COLUMN ticket_hash jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.tx ADD COLUMN ticket_receipt jsonb;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- Origination table -- manop

CREATE TABLE IF NOT EXISTS C.origination ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source of origination op
, k_id bigint
  -- address of originated contract
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
, nonce int -- non null for internal operations
, preorigination_id bigint -- optional for protos 1 to 8
, delegate_id bigint -- optional for protos 1,2,3,4,5,6,7,8
, credit bigint not null
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, error_trace_ json
-- for script, cf. C.contract_script
-- for manager, cf. C.manager -- protos 1 to 4 included only
);
--PKEY origination_pkey; C.origination; operation_id --SEQONLY
--FKEY origination_operation_id_fkey; C.origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY origination_operation_source_fkey; C.origination; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY origination_k_fkey; C.origination; k_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS origination_source         on C.origination using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_k              on C.origination using btree (k_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_preorigination on C.origination using btree (preorigination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_delegate       on C.origination using btree (delegate_id); --SEQONLY --1
ALTER TABLE C.origination ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.origination WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE c.origination SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;



-----------------------------------------------------------------------------
-- Manager table for old protocols (1 to 4)
-- This would be part of C.origination if it was not dropped by protocol 5 and replaced by a non-null script (which was optional before protocol 5).
-- You might want to choose not to fill this table at all. In that case, modify I.origination in chain_functions.sql to simply remove the insertion.

CREATE TABLE IF NOT EXISTS C.manager ( --L
  operation_id bigint not null
, manager_id bigint not null
);
--PKEY manager_pkey; C.manager; operation_id --SEQONLY
--FKEY manager_operation_id_fkey; C.manager; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY manager_manager_id_fkey; C.manager; manager_id; C.address(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Delegation -- manop

CREATE TABLE IF NOT EXISTS C.delegation ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the delegation op
  , pkh_id bigint
  -- optional delegate
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY delegation_pkey; C.delegation; operation_id --SEQONLY
--FKEY delegation_operation_id_fkey; C.delegation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY delegation_source_fkey; C.delegation; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegation_pkh_fkey; C.delegation; pkh_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS delegation_source         on C.delegation using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_pkh            on C.delegation using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_operation_hash on C.delegation using btree (operation_id); --SEQONLY --1
ALTER TABLE C.delegation ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.delegation WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE C.delegation SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;


-----------------------------------------------------------------------------
-- Reveals -- manop

CREATE TABLE IF NOT EXISTS C.reveal ( --L
    operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , pk text not null
  -- revealed pk
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint -- null when internal -- fix, since v10.5.0
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY reveal_pkey; C.reveal; operation_id --SEQONLY
--FKEY reveal_operation_hash_op_id_fkey; C.reveal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY reveal_source_fkey; C.reveal; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS reveal_source         on C.reveal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS reveal_pk             on C.reveal using btree (pk); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.reveal ALTER COLUMN pk TYPE text;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
ALTER TABLE C.reveal ALTER COLUMN fee DROP NOT NULL ; -- null if internal operation -- fix, since v10.5.0
DO $$
BEGIN
IF NOT((SELECT operation_id FROM C.reveal WHERE fee IS NULL LIMIT 1) IS NOT NULL)
THEN
  UPDATE c.reveal SET fee = NULL WHERE nonce IS NOT NULL;
END IF;
END $$;


-----------------------------------------------------------------------------
-- global constants -- since v9.7.0
CREATE TABLE IF NOT EXISTS C.global_constants ( --NL
  global_address char(54) not null
, "value" jsonb not null
, global_address_id bigint not null
, size int not null
-- , strings text[]
);
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address on C.global_constants using btree (global_address); --SEQONLY --1
-- CREATE UNIQUE INDEX IF NOT EXISTS global_constants_value on C.global_constants using btree ("value"); -- this index crashes on a value that is too large to fit
DROP INDEX IF EXISTS c.global_constants_value; --since v9.7.b and v9.8.3
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address_id on C.global_constants using btree (global_address_id); --SEQONLY --1
--FKEY global_constants_global_address_id_fkey; C.global_constants; global_address_id; C.operation_alpha(autoid); CASCADE --SEQONLY

CREATE OR REPLACE FUNCTION global_address_id(ga char)
RETURNS bigint
AS $$
SELECT global_address_id FROM C.global_constants WHERE global_address = ga;
$$ LANGUAGE SQL STABLE;

-----------------------------------------------------------------------------
-- Register_Global_Constant -- manop -- since v9.7.0

CREATE TABLE IF NOT EXISTS C.register_global_constant ( -- introduced by proto 11 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the register_global_constant op
  , global_address_id bigint -- this is equal to operation_id if all goes well, otherwise it's equal to another operation's operation_id: the operation that is indexed first will give its operation_id
  -- id of hash of the "value"
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY register_global_constant_pkey; C.register_global_constant; operation_id --SEQONLY
--FKEY register_global_constant_operation_id_fkey; C.register_global_constant; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY register_global_constant_source_fkey; C.register_global_constant; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS register_global_constant_source         on C.register_global_constant using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS register_global_constant_gaid            on C.register_global_constant using btree (global_address_id); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.register_global_constant DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


--The following key is not really necessary because `register_global_constant_operation_id_fkey` should be enough.
--fkey register_global_constant_global_address_id_fkey; C.register_global_constant; global_address_id; C.global_constants(global_address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Set_deposits_limit -- manop -- since v9.7.a

CREATE TABLE IF NOT EXISTS C.set_deposits_limit ( -- introduced by proto 12 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , value numeric
  -- value of the limit
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY set_deposits_limit_pkey; C.set_deposits_limit; operation_id --SEQONLY
--FKEY set_deposits_limit_operation_id_fkey; C.set_deposits_limit; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY set_deposits_limit_source_fkey; C.set_deposits_limit; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS set_deposits_limit_source on C.set_deposits_limit using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS set_deposits_limit_value  on C.set_deposits_limit using btree (value); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.set_deposits_limit DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- Tx_rollup_origination -- manop -- since v9.8.2
CREATE TABLE IF NOT EXISTS C.tx_rollup_origination ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup_origination bigint -- since v9.9.3
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_origination_pkey; C.tx_rollup_origination; operation_id --SEQONLY
--FKEY tx_rollup_origination_operation_id_fkey; C.tx_rollup_origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_origination_source_fkey; C.tx_rollup_origination; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_origination_tx_roll; C.tx_rollup_origination; tx_rollup_origination; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_origination_source on C.tx_rollup_origination using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_origination_value  on C.tx_rollup_origination using btree (tx_rollup_origination); --SEQONLY --1


-----------------------------------------------------------------------------
-- tx_rollup_submit_batch -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_submit_batch ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , content bytea not null
  , burn_limit numeric
  , paid_storage_size_diff numeric
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch ALTER COLUMN content TYPE bytea;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch ADD COLUMN paid_storage_size_diff numeric;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_submit_batch DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


--PKEY tx_rollup_submit_batch_pkey; C.tx_rollup_submit_batch; operation_id --SEQONLY
--FKEY tx_rollup_submit_batch_operation_id_fkey; C.tx_rollup_submit_batch; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_submit_batch_source_fkey; C.tx_rollup_submit_batch; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_submit_batch_tx_roll; C.tx_rollup_submit_batch; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_submit_batch_source on C.tx_rollup_submit_batch using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_submit_batch_value  on C.tx_rollup_submit_batch using btree (tx_rollup_submit_batch); --SEQONLY --1


-----------------------------------------------------------------------------
-- tx_rollup_commit -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_commit ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , commitment jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_commit_pkey; C.tx_rollup_commit; operation_id --SEQONLY
--FKEY tx_rollup_commit_operation_id_fkey; C.tx_rollup_commit; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_commit_source_fkey; C.tx_rollup_commit; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_commit_tx_roll; C.tx_rollup_commit; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_commit_source on C.tx_rollup_commit using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_commit_value  on C.tx_rollup_commit using btree (tx_rollup_commit); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_commit DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_finalize_commitment -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_finalize_commitment ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_finalize_commitment_pkey; C.tx_rollup_finalize_commitment; operation_id --SEQONLY
--FKEY tx_rollup_finalize_commitment_operation_id_fkey; C.tx_rollup_finalize_commitment; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_finalize_commitment_source_fkey; C.tx_rollup_finalize_commitment; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_finalize_commitment_tx_roll; C.tx_rollup_finalize_commitment; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_finalize_commitment_source on C.tx_rollup_finalize_commitment using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_finalize_commitment_value  on C.tx_rollup_finalize_commitment using btree (tx_rollup_finalize_commitment); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_finalize_commitment DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_rejection -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_rejection ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , message jsonb not null
  , message_position int not null
  , message_path jsonb not null
  , message_result_hash jsonb not null
  , message_result_path jsonb not null
  , previous_message_result jsonb not null
  , previous_message_result_path jsonb not null
  , proof jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_rejection_pkey; C.tx_rollup_rejection; operation_id --SEQONLY
--FKEY tx_rollup_rejection_operation_id_fkey; C.tx_rollup_rejection; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_rejection_source_fkey; C.tx_rollup_rejection; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_rejection_tx_roll; C.tx_rollup_rejection; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_rejection_source on C.tx_rollup_rejection using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_rejection_value  on C.tx_rollup_rejection using btree (tx_rollup_rejection); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_position int not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_result_hash jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN previous_message_result jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN previous_message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_rejection ADD COLUMN proof jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_dispatch_tickets -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_dispatch_tickets ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , context_hash jsonb not null
  , message_index int not null
  , message_result_path jsonb not null
  , tickets_info jsonb not null
  , paid_storage_size_diff numeric
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_dispatch_tickets_pkey; C.tx_rollup_dispatch_tickets; operation_id --SEQONLY
--FKEY tx_rollup_dispatch_tickets_operation_id_fkey; C.tx_rollup_dispatch_tickets; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_dispatch_tickets_source_fkey; C.tx_rollup_dispatch_tickets; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_dispatch_tickets_tx_roll; C.tx_rollup_dispatch_tickets; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_dispatch_tickets_source on C.tx_rollup_dispatch_tickets using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_dispatch_tickets_value  on C.tx_rollup_dispatch_tickets using btree (tx_rollup_dispatch_tickets); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN context_hash jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN message_index int not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ALTER COLUMN message_index TYPE int USING message_index::int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN message_result_path jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN tickets_info jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.tx_rollup_dispatch_tickets ADD COLUMN paid_storage_size_diff numeric;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_remove_commitment -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_remove_commitment ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , level int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_remove_commitment_pkey; C.tx_rollup_remove_commitment; operation_id --SEQONLY
--FKEY tx_rollup_remove_commitment_operation_id_fkey; C.tx_rollup_remove_commitment; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_remove_commitment_source_fkey; C.tx_rollup_remove_commitment; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_remove_commitment_tx_roll; C.tx_rollup_remove_commitment; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_remove_commitment_source on C.tx_rollup_remove_commitment using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_remove_commitment_value  on C.tx_rollup_remove_commitment using btree (tx_rollup_remove_commitment); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_remove_commitment DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- tx_rollup_return_bond -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.tx_rollup_return_bond ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_return_bond_pkey; C.tx_rollup_return_bond; operation_id --SEQONLY
--FKEY tx_rollup_return_bond_operation_id_fkey; C.tx_rollup_return_bond; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_return_bond_source_fkey; C.tx_rollup_return_bond; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY tx_rollup_return_bond_tx_roll; C.tx_rollup_return_bond; tx_rollup; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_return_bond_source on C.tx_rollup_return_bond using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_return_bond_value  on C.tx_rollup_return_bond using btree (tx_rollup_return_bond); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.tx_rollup_return_bond DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- transfer_ticket -- manop -- since v10
CREATE TABLE IF NOT EXISTS C.transfer_ticket ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , contents jsonb not null
  , ty jsonb not null
  , ticketer bigint not null
  , amount numeric not null
  , destination_id bigint not null
  , entrypoint jsonb not null
  , paid_storage_size_diff numeric
  , ticket_receipt jsonb -- since v10.4.0
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY transfer_ticket_pkey; C.transfer_ticket; operation_id --SEQONLY
--FKEY transfer_ticket_operation_id_fkey; C.transfer_ticket; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY transfer_ticket_source_fkey; C.transfer_ticket; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS transfer_ticket_source on C.transfer_ticket using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS transfer_ticket_value  on C.transfer_ticket using btree (transfer_ticket); --SEQONLY --1

DO $$
BEGIN
ALTER TABLE C.transfer_ticket DROP COLUMN tx_rollup; -- since v10.2.2 (was created by mistake)
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.transfer_ticket ADD COLUMN ticket_receipt jsonb; -- necessary since v10.4.0 for DB upgraded from a prior version, alteration added in v10.6.3
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- Drain delegate -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.drain_delegate ( --L
  operation_id bigint not null
, consensus_key_id bigint not null
, delegate_id bigint not null
, destination_id bigint not null
, allocated_destination_contract bool not null
);
--PKEY drain_delegate_pkey; C.drain_delegate; operation_id --SEQONLY
--FKEY drain_delegate_operation_id_fkey; C.drain_delegate; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY drain_delegate_consensus_key_id_fkey; C.drain_delegate; consensus_key_id; C.address(address_id); CASCADE --SEQONLY
--FKEY drain_delegate_delegate_id_fkey; C.drain_delegate; delegate_id; C.address(address_id); CASCADE --SEQONLY
--FKEY drain_delegate_destination_id_fkey; C.drain_delegate; destination_id; C.address(address_id); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Sc_rollup_originate -- manop -- since v9.8.2

CREATE TABLE IF NOT EXISTS C.sc_rollup_originate ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , address_id bigint
  -- address
  , size numeric
  -- size
  , kind text
  -- kind
  , boot_sector_hash char(32) not null -- since v10.4.0
  -- boot sector
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , parameters_ty jsonb not null
  , origination_proof text
  , genesis_commitment_hash char(54)
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_originate_pkey; C.sc_rollup_originate; operation_id --SEQONLY
--FKEY sc_rollup_originate_operation_id_fkey; C.sc_rollup_originate; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_originate_source_fkey; C.sc_rollup_originate; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_originate_address_fkey; C.sc_rollup_originate; address_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_originate_source on C.sc_rollup_originate using btree (source_id); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN boot_sector_hash char(32) not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN genesis_commitment_hash char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN parameters_ty jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ADD COLUMN origination_proof text not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate DROP COLUMN boot_sector;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN genesis_commitment_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN origination_proof TYPE text;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_originate ALTER COLUMN origination_proof DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

CREATE TABLE IF NOT EXISTS C.sc_rollup_boot_sector ( -- since v10.4.0 --L
  hash char(32) primary key -- md5sum
, txt text -- more human readable
, bin bytea -- fallback when it doesn't fit boot_sector_txt
);
CREATE INDEX IF NOT EXISTS sc_rollup_boo_sectors_hash on C.sc_rollup_boot_sector using btree(hash);

-----------------------------------------------------------------------------
-- sc_rollup_publish -- manop -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_publish ( -- introduced by proto 13 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , commitment jsonb not null
  , staked_hash char(54)
  , published_at_level int
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_publish_pkey; C.sc_rollup_publish; operation_id --SEQONLY
--FKEY sc_rollup_publish_operation_id_fkey; C.sc_rollup_publish; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_publish_source_fkey; C.sc_rollup_publish; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_publish_sc_rollup_fkey; C.sc_rollup_publish; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_publish_source on C.sc_rollup_publish using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS sc_rollup_publish_value  on C.sc_rollup_publish using btree (sc_rollup_publish); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.sc_rollup_publish DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_publish ALTER COLUMN staked_hash TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS C.sc_rollup_cement ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , consumed_milligas numeric
  -- consumed milligas
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , inbox_level int
  , rollup_id bigint
  , commitment_hash char(54)
  , error_trace_ json
);
--PKEY sc_rollup_cement_pkey; C.sc_rollup_cement; operation_id --SEQONLY
--FKEY sc_rollup_cement_operation_id_fkey; C.sc_rollup_cement; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_cement_source_fkey; C.sc_rollup_cement; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_cement_rollup_fkey; C.sc_rollup_cement; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_cement_source on C.sc_rollup_cement using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_cement_rollup on C.sc_rollup_cement using btree (rollup_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- sc_rollup_timeout -- manop -- since v10.4.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_timeout ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , stakers jsonb not null
  , game_status jsonb
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_timeout_pkey; C.sc_rollup_timeout; operation_id --SEQONLY
--FKEY sc_rollup_timeout_operation_id_fkey; C.sc_rollup_timeout; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_timeout_source_fkey; C.sc_rollup_timeout; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_timeout_sc_rollup_fkey; C.sc_rollup_timeout; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_timeout_source on C.sc_rollup_timeout using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_timeout_rollup on C.sc_rollup_timeout using btree (rollup_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- sc_rollup_refute -- manop -- since v10.4.0
CREATE TABLE IF NOT EXISTS C.sc_rollup_refute ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , rollup_id bigint not null
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , refutation jsonb not null
  , opponent_id bigint not null
  , game_status jsonb
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_refute_pkey; C.sc_rollup_refute; operation_id --SEQONLY
--FKEY sc_rollup_refute_operation_id_fkey; C.sc_rollup_refute; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_refute_source_fkey; C.sc_rollup_refute; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_refute_opponent_fkey; C.sc_rollup_refute; opponent_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_refute_sc_rollup_fkey; C.sc_rollup_refute; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_refute_source on C.sc_rollup_refute using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_refute_rollup on C.sc_rollup_refute using btree (rollup_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_refute_opponent on C.sc_rollup_refute using btree (opponent_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- zk_rollup_publish -- manop -- since v10.3.0
CREATE TABLE IF NOT EXISTS C.zk_rollup_publish ( -- introduced by proto 13 --L
  operation_id bigint not null
  , source_id bigint not null
  , fee bigint not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric
  , error_trace jsonb
  , error_trace_ json
  , zk_rollup_id bigint not null
  , ops jsonb not null
  , paid_storage_size_diff numeric
);
--PKEY zk_rollup_publish_pkey; C.zk_rollup_publish; operation_id --SEQONLY
--FKEY zk_rollup_publish_operation_id_fkey; C.zk_rollup_publish; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY zk_rollup_publish_source_fkey; C.zk_rollup_publish; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY zk_rollup_publish_zk_rollup_fkey; C.zk_rollup_publish; zk_rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS zk_rollup_publish_source on C.zk_rollup_publish using btree (source_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Sc_rollup_add_messages -- manop -- since v9.9.9

CREATE TABLE IF NOT EXISTS C.sc_rollup_add_messages ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , messages text[]
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_add_messages_pkey; C.sc_rollup_add_messages; operation_id --SEQONLY
--FKEY sc_rollup_add_messages_operation_id_fkey; C.sc_rollup_add_messages; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_add_messages_source_fkey; C.sc_rollup_add_messages; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_add_messages_source on C.sc_rollup_add_messages using btree (source_id); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN rollup_id;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_add_messages DROP COLUMN inbox_after;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;



-----------------------------------------------------------------------------
-- event -- manop -- since v10.2.0

CREATE TABLE IF NOT EXISTS C.event ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , nonce int not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , ty jsonb not null
  , tag jsonb not null
  , payload jsonb not null
  , consumed_milligas numeric -- nullable
  -- consumed milligas
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY event_pkey; C.event; operation_id --SEQONLY
--FKEY event_operation_id_fkey; C.event; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY event_source_fkey; C.event; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS event_source on C.event using btree (source_id); --SEQONLY --1
ALTER TABLE C.event ALTER COLUMN nonce SET NOT NULL;
DO $$
BEGIN
ALTER TABLE C.event DROP COLUMN fee;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- increase_paid_storage -- manop -- since v10.2.0

CREATE TABLE IF NOT EXISTS C.increase_paid_storage ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  , fee bigint not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , amount_in_bytes numeric not null
  , destination_id bigint not null
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY increase_paid_storage_pkey; C.increase_paid_storage; operation_id --SEQONLY
--FKEY increase_paid_storage_operation_id_fkey; C.increase_paid_storage; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY increase_paid_storage_source_fkey; C.increase_paid_storage; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY increase_paid_storage_destination_fkey; C.increase_paid_storage; destination_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS increase_paid_storage_source on C.increase_paid_storage using btree (source_id); --SEQONLY --1
DO $$
BEGIN
  ALTER TABLE C.increase_paid_storage DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- dal_publish_slot_header -- manop -- since v10.2.1

CREATE TABLE IF NOT EXISTS C.dal_publish_slot_header ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , published_level int -- nullable since proto O
  , index jsonb not null
  , commitment jsonb not null
  , proof jsonb not null
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY dal_publish_slot_header_pkey; C.dal_publish_slot_header; operation_id --SEQONLY
--FKEY dal_publish_slot_header_operation_id_fkey; C.dal_publish_slot_header; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY dal_publish_slot_header_source_fkey; C.dal_publish_slot_header; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS dal_publish_slot_header_source on C.dal_publish_slot_header using btree (source_id); --SEQONLY --1

DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.dal_publish_slot_header DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header DROP COLUMN dal_slot;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN published_level int;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ALTER COLUMN published_level DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN index jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN commitment jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.dal_publish_slot_header ADD COLUMN proof jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- dal_attestation -- since v10.4.0

CREATE TABLE IF NOT EXISTS C.dal_attestation ( --L
  operation_id bigint not null
  , attestor_id bigint -- nullable since proto 18
  , attestation jsonb not null
  , level int not null
  , delegate_id bigint not null
);
--PKEY dal_attestation_pkey; C.dal_attestation; operation_id --SEQONLY
--FKEY dal_attestation_operation_id_fkey; C.dal_attestation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY dal_attestation_attestor_fkey; C.dal_attestation; attestor_id; C.address(address_id); CASCADE --SEQONLY
--FKEY dal_attestation_delegate_fkey; C.dal_attestation; delegate_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS dal_attestation_attestor on C.dal_attestation using btree (attestor_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS dal_attestation_delegate on C.dal_attestation using btree (delegate_id); --SEQONLY --1

DO $$
BEGIN
ALTER TABLE C.dal_attestation ALTER COLUMN attestor_id DROP NOT NULL;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

-----------------------------------------------------------------------------
-- sc_rollup_execute_outbox_message -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.sc_rollup_execute_outbox_message ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , paid_storage_size_diff numeric -- nullable
  , rollup_id bigint not null
  , cemented_commitment char(54) not null
  , output_proof text not null
  , ticket_receipt jsonb not null
);
--PKEY sc_rollup_execute_outbox_message_pkey; C.sc_rollup_execute_outbox_message; operation_id --SEQONLY
--FKEY sc_rollup_execute_outbox_message_operation_id_fkey; C.sc_rollup_execute_outbox_message; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_execute_outbox_message_source_fkey; C.sc_rollup_execute_outbox_message; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_execute_outbox_message_rollup_fkey; C.sc_rollup_execute_outbox_message; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_execute_outbox_message_source on C.sc_rollup_execute_outbox_message using btree (source_id); --SEQONLY --1

DO $$ -- since v10.4.0
BEGIN
ALTER TABLE C.sc_rollup_execute_outbox_message ADD COLUMN ticket_receipt jsonb not null;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.sc_rollup_execute_outbox_message ALTER COLUMN cemented_commitment TYPE char(54);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


-----------------------------------------------------------------------------
-- update_consensus_key -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.update_consensus_key ( -- introduced by proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , public_key text
);
--PKEY update_consensus_key_pkey; C.update_consensus_key; operation_id --SEQONLY
--FKEY update_consensus_key_operation_id_fkey; C.update_consensus_key; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY update_consensus_key_source_fkey; C.update_consensus_key; source_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS update_consensus_key_source on C.update_consensus_key using btree (source_id); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.update_consensus_key DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;

----------------------------------------------------------------------------
-- sc_rollup_recover_bond -- manop -- since v10.3.0

CREATE TABLE IF NOT EXISTS C.sc_rollup_recover_bond ( -- introduced by proto 14 but implemented in proto 15 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , fee bigint not null
  -- fees
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , consumed_milligas numeric -- nullable
  , error_trace jsonb -- nullable
  , error_trace_ json
  , rollup_id bigint not null
);
--PKEY sc_rollup_recover_bond_pkey; C.sc_rollup_recover_bond; operation_id --SEQONLY
--FKEY sc_rollup_recover_bond_operation_id_fkey; C.sc_rollup_recover_bond; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_recover_bond_source_fkey; C.sc_rollup_recover_bond; source_id; C.address(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_recover_bond_rollup_fkey; C.sc_rollup_recover_bond; rollup_id; C.address(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_recover_bond_source on C.sc_rollup_recover_bond using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS sc_rollup_recover_bond_rollup on C.sc_rollup_recover_bond using btree (rollup_id); --SEQONLY --1
DO $$
BEGIN
ALTER TABLE C.sc_rollup_recover_bond DROP COLUMN nonce;
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;


---------------------------------------------------------------------------
-- sc_rollup_dal_slot_subscribe
DROP TABLE IF EXISTS C.sc_rollup_dal_slot_subscribe CASCADE; -- this operation no longer exists, and has never been used on mainnet

-----------------------------------------------------------------------------
-- vdf_revelation -- since v10.2.1

CREATE TABLE IF NOT EXISTS C.vdf_revelation ( -- introduced by proto 14 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , vdf_solution jsonb
  , error_trace jsonb -- nullable
  , error_trace_ json
);
--PKEY vdf_revelation_pkey; C.vdf_revelation; operation_id --SEQONLY
--FKEY vdf_revelation_operation_id_fkey; C.vdf_revelation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY

-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION status (s smallint)
RETURNS char
AS $$
begin
case
when s = 0 then return 'applied';
when s = 1 then return 'backtracked';
when s = 2 then return 'failed';
when s = 3 then return 'skipped';
end case;
end;
$$ LANGUAGE PLPGSQL STABLE;


-----------------------------------------------------------------------------
-- Balance: record balance diffs

-- v9.5 merged C.balance_updates_block and C.balance_updates_op into a single table, and adds column implicit_operations_results_id
CREATE TABLE IF NOT EXISTS C.balance_updates ( --L
  block_level int not null,
  -- block hash
  operation_id bigint,
  -- references C.operation_alpha.autoid if any
  implicit_operations_results_id int,
  -- references C.implicit_operations_results.id if any
  balance_kind smallint not null,
  -- balance kind: -- TODO/FIXME: make this list automatically generated (so that clashes are guaranteed to never exist)
  --  0 : Contract
  --  1 : Rewards, or legacy rewards
  --  2 : Fees, or legacy fees
  --  3 : Deposits, or legacy deposits
  --  4 : Block_fees (since proto 12)
  --  5 : Nonce_revelation_rewards (since proto 12)
  --  6 : Double_signing_evidence_rewards (since proto 12)
  --  7 : Endorsing_rewards (since proto 12)
  --  8 : Baking_rewards (since proto 12)
  --  9 : Baking_bonuses (since proto 12)
  -- 10 : Storage_fees (since proto 12)
  -- 11 : Double_signing_punishments (since proto 12)
  -- 12 : Lost_endorsing_rewards (since proto 12)
  -- 13 : Liquidity_baking_subsidies (since proto 12)
  -- 14 : Burned (since proto 12)
  -- 15 : Commitments (since proto 12)
  -- 16 : Bootstrap (since proto 12)
  -- 17 : Invoice (since proto 12)
  -- 18 : Initial_commitments (since proto 12)
  -- 19 : Minted (since proto 12)
  contract_address_id bigint, -- nullable since protocol I -- since v9.9.5
  blinded_public_key_hash char(37), -- since proto 12
  -- b58check encoded address of contract(either implicit or originated)
  cycle int, -- only balance_kind 1,2,3 have cycle
  -- cycle
  diff bigint not null,
  -- balance update
  -- credited if positve
  -- debited if negative
  id int not null -- unique position within the block to allow rightful duplicates and reject wrong duplicates -- this number is ≥ 0
  , origin smallint -- 0: block application, 1: protocol migration, 2: inflationary subsidy, 3: simulation of an operation -- since v10.3.0
    -- 0 | Block_application
    -- 1 | Protocol_migration
    -- 2 | Subsidy
    -- 3 | Simulation
    -- 4 | Delayed_operation : since proto Oxford2
  , delayed_operation char(51) -- (non null) <=> (if origin = 4)
  , primary key (block_level, id)
);
DO $$
BEGIN
ALTER TABLE C.balance_updates ADD COLUMN origin int; -- since v10.3.0
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
DO $$
BEGIN
ALTER TABLE C.balance_updates ADD COLUMN delayed_operation char(51); -- since v10.6.4
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
--FKEY balance_updates_block_hash_fkey; C.balance_updates; block_level; C.block(level); CASCADE --SEQONLY
--FKEY balance_updates_contract_address_fkey; C.balance_updates; contract_address_id; C.address(address_id); CASCADE --SEQONLY
--FKEY balance_updates_iorid_fkey; C.balance_updates; block_level, implicit_operations_results_id; C.implicit_operations_results(block_level, id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS balance_block  on C.balance_updates using btree (block_level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cat    on C.balance_updates using btree (balance_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_k      on C.balance_updates using btree (contract_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cycle  on C.balance_updates using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_operation_id on C.balance_updates using btree (operation_id); --SEQONLY --1

--CREATE INDEX IF NOT EXISTS balance_blinded_public_key_hash on C.balance_updates using btree (blinded_public_key_hash); --SEQONLY --1


CREATE OR REPLACE FUNCTION balance_kind (bk smallint) -- TODO: generate this function
RETURNS char
AS
$$
SELECT coalesce ((select 'contract' where bk = 0),
       coalesce ((select 'rewards' where bk = 1),
       coalesce ((select 'fees' where bk = 2),
       coalesce ((select 'deposits' where bk = 3),
       coalesce ((select 'block_fees' where bk = 4),
       coalesce ((select 'nonce_revelation_rewards' where bk = 5),
       coalesce ((select 'double_signing_evidence_rewards' where bk = 6),
       coalesce ((select 'endorsing_rewards' where bk = 7),
       coalesce ((select 'baking_rewards' where bk = 8),
       coalesce ((select 'baking_bonuses' where bk = 9),
       coalesce ((select 'storage_fees' where bk = 10),
       coalesce ((select 'double_signing_punishments' where bk = 11),
       coalesce ((select 'lost_endorsing_rewards' where bk = 12),
       coalesce ((select 'liquidity_baking_subsidies' where bk = 13),
       coalesce ((select 'burned' where bk = 14),
       coalesce ((select 'commitments' where bk = 15),
       coalesce ((select 'bootstrap' where bk = 16),
       coalesce ((select 'invoice' where bk = 17),
       coalesce ((select 'initial_commitments' where bk = 18),
       coalesce ((select 'minted' where bk = 19),
       coalesce ((select 'frozen_bonds' where bk = 20),
       coalesce ((select 'tx_rollup_rejection_punishments' where bk = 21),
       coalesce ((select 'tx_rollup_rejection_rewards' where bk = 22),
       coalesce ((select 'sc_rollup_refutation_punishments' where bk = 23),
       coalesce ((select 'sc_rollup_refutation_rewards' where bk = 24))))))))))))))))))))))))))
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION balance_kind (bk int)
RETURNS char
AS $$ SELECT balance_kind (bk::smallint) $$ LANGUAGE SQL STABLE;



-----------------------------------------------------------------------------
-- Snapshot blocks
-- The snapshot block for a given cycle is obtained as follows
-- at the last block of cycle n, the snapshot block for cycle n+6 is selected
-- Use [Storage.Roll.Snapshot_for_cycle.get C.txt cycle] in proto_alpha to
-- obtain this value.
-- RPC: /chains/main/blocks/${block}/context/raw/json/cycle/${cycle}
-- where:
-- ${block} denotes a block(either by hash or level)
-- ${cycle} denotes a cycle which must be in [cycle_of(level)-5,cycle_of(level)+7]

CREATE TABLE IF NOT EXISTS C.snapshot ( --L
  cycle int,
  level int,
  primary key (cycle, level)
);

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS delegate (
--   cycle int not null,
--   level int not null,
--   pkh char(36) not null,
--   balance bigint not null,
--   frozen_balance bigint not null,
--   staking_balance bigint not null,
--   delegated_balance bigint not null,
--   deactivated bool not null,
--   grace smallint not null,
--   primary key (cycle, pkh),
--   , foreign key (cycle, level) references snapshot(cycle, level)
--   , foreign key (pkh) references implicit(pkh)
-- );

-----------------------------------------------------------------------------
-- Delegated contract table -- NOT FILLED

CREATE TABLE IF NOT EXISTS C.delegated_contract ( --L
  delegate_id bigint,
  -- tz1 of the delegate
  delegator_id bigint,
  -- address of the delegator (for now, KT1 but this could change)
  cycle int,
  level int
  , primary key (delegate_id, delegator_id, cycle, level)
);
--PKEY delegated_contract_pkey; C.delegated_contract; delegate_id, delegator_id, cycle, level --SEQONLY
--FKEY delegated_contract_delegate_fkey; C.delegated_contract; delegate_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_delegator_fkey; C.delegated_contract; delegator_id; C.address(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_cycle_level_fkey; C.delegated_contract; cycle, level; C.snapshot(cycle, level); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS delegated_contract_cycle     on C.delegated_contract using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_level     on C.delegated_contract using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegate  on C.delegated_contract using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegator on C.delegated_contract using btree (delegator_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS stake (
--   delegate char(36) not null,
--   level int not null,
--   k char(36) not null,
--   kind smallint not null,
--   diff bigint not null,
--   primary key (delegate, level, k, kind, diff),
--   , foreign key (delegate) references implicit(pkh)
--   , foreign key (k) references C.address(address_id)
-- );
