# Please note that this project is no longer funded and development has ceased.


[[_TOC_]]
# The Nomadic Labs `tezos-indexer`

## What is an "indexer"

Say you want to see the last 10 transactions for an account.
If you use [tezos RPCs](https://tezos.gitlab.io/alpha/rpc.html#rpcs-full-description)
to get the information, you'll have to query the blockchain's
blocks one by one, starting from the latest, until you find those 10 transactions.
If you wanted all transactions for that account, you'd have to browse the entire
blockchain.

The job of an "indexer" is to make such information easy and fast to get, and how it does
that is by *indexing* those information, hence its name.

Indexers are the backbones of block explorers, dApps, and wallets.

## How it works

This indexer connects to a running `tezos-node` in `archive mode`, queries blocks,
and puts them into a database.
We're using [PostgreSQL](https://www.postgresql.org/) to handle the database, a
popular engine, so that you can access the data using virtually any programming
language you want.

The source files for the database schema is available in
[src/db-schema](https://gitlab.com/nomadic-labs/tezos-indexer/blob/master/src/db-schema/)
but to get the full schema, you should run the following command:
```
make db-schema-all-default
```

or, if you have the binary, the following command will output the DB schema:
```
tezos-indexer --db-schema
```

or, from tezos-indexer v10.4.4's docker image:
```
docker run -it --rm registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v10.4.4 --db-schema
```
(replace v10.4.4 by the version you want to use)


## Supported Tezos networks

`tezos-indexer` supports the following networks:
* mainnet
* any network (testnet, private or sandbox) that uses mainnet's current baking protocol, the protocol preceding the current one and possibly the one succeeding the current one
    * v10.4.4 supports mainnet, ghostnet, limanet, mumbainet, and provides (possibly limited) support for Mondaynet, Dailynet, private nets


Support for networks that actively run old protocols (i.e., actively bake using old protocols) is no longer provided (≥v10.3.0). But if you need those for some reason, please get in touch, it can be arranged.

## Getting the data (i.e., running the indexer)

### Tezos `octez-node` running in mode archive

You'll need a `tezos-node` running with `--history-mode=archive`, otherwise you
won't have anything to index!

In order for the indexer to connect to the node, you need to tell the indexer how to, either by using CLI options, or via an appropriate `~/.tezos-client/config` (or otherwise specified) file.

*Once your indexer is fully synchronized with your node, you may
abandon the archive mode (`--history-mode=archive`) and use any other
mode (full or rolling), although it's not recommended.*

### Set up a PostgreSQL database

First, install `postgresql` and create a database. It is recommended that you use Postgres 14 or 15.
Versions older than 14 may work but they're no longer tested.

If you need to create a DB user, `createuser username` may be what you want,
and perhaps even `createuser username -dw`.

Give DB ownership to the user running the indexer (or at least, the necessary read/write permissions).

So it should be something like after you have installed PostGres *and*
it's running.
```
createdb name_of_your_database -O username
```

If you decide to set up a password, it's up to you to provide it
via environment variable `PGPASSWORD` or by any other means that work.
*Some configurations of Postgres will require you to have a non-empty password. It's all up to you (or your Postgresql administrator).*

### Build the indexer

It's highly recommended that you simply use the
Docker images [built by the Gitlab CI](https://gitlab.com/nomadic-labs/tezos-indexer/container_registry/842779),
or built by yourself using the provided [Dockerfile](https://gitlab.com/nomadic-labs/tezos-indexer/-/blob/master/Dockerfile).

If you're okay with using docker images, skip to the next section.

However if you still want to proceed, here's how:

#### First, build octez

Warning: it is quite straightforward when it works, but can easily become difficult.

The official documentation is there: https://tezos.gitlab.io/introduction/howtoget.html#install-tezos-dependencies

#### Second, get a copy of tezos-indexer

```
git clone https://gitlab.com/nomadic-labs/tezos-indexer.git
cd tezos-indexer
make tezos # this clones a specific version of octez
```

#### Third, install tezos-indexer dependencies and build

```
make install-opam-deps
make
```

### Get the database schema

A simple way to get the schema is:
```
make db-schema-all-default | psql
```

If there's any error, `psql` will exit.

There are other ways to generate the DB schema, but they're undocumented.

All database schema source code is in `src/db-schema/`.


### Run the indexer for the blockchain

Note that once you have started indexing a network, that database contain the chain's ID.
You won't be able to use that database to index another network (unless you empty it first).


Run the indexer using a docker image:
```
docker run -it --rm registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v10.4.4 --verbose --db=postgresql://my-pg-server.example.com/name_of_your_database
```

Run the indexer using your own binary:
```
./tezos-indexer --verbose --db=postgresql://my-pg-server.example.com/name_of_your_database
```

Note that although by default the indexer will try to connect to and use `postgresql://localhost/chain`,
you should probably not rely on any default values.

You should drop the `--verbose` option in order to gain speed or if you don't need the logs, or use `--verbosity=1` if you want to get some minimal logs.


For faster bootstrapping, which can be very handy for mainnet, please refer to `utils/run-tezos-indexer-multicore.bash`.


To check where the indexer is at, you could do something like this:
```
$ psql name_of_your_database -c 'select count(*) from c.block;'
 count
--------
 474987
(1 row)
```

The following query is much faster (but doesn't have useful semantics if you're running in multicore mode and/or have sparse indexing):
```
$ psql name_of_your_database -c 'select level from c.block order by level desc limit 1;'
```

### Run the indexer for the mempool

`tezos-indexer` allows you to record (with minimal indexing) the contents of the mempool.
When activated, this feature deactivates the indexing of the blockchain.
To index both the blockchain and the mempool, you need to run two instances
of `tezos-indexer`. Preferably, they should share the same database, so that
you may write more powerful requests to retrieve data. However, you may
run just one of them: you do not need to index the blockchain to index
the mempool, nor do you need to index the mempool to index the blockchain.

The mempool indexer is quite simple and minimal. The database schema for the mempool is mainly in
`src/db-schema/mempool.sql`.

#### The mempool indexer is intended to work with the 2 latest mainnet protocols

```
./tezos-indexer --mempool-only [other options]
```

Note that it is expected that your node is synchronized with the blockchain, otherwise there's no mempool, and you'll get 404 errors from the node.

### Building a block cache for the indexer

If you run `./tezos-indexer --help`, you'll see that the indexer may use a disk cache for getting the blocks
from a cache stored on disk instead of making RPCs to the node.
Doing so may allow you to use a slow (and/or distant) node. And even with a fast node, this might still help speed up the bootstrap.
Also, those *block cache* files can be stored in an HDD and it will not be extremely slow!
Each block cache file stores 1000 blocks. When reading from the cache fails, `tezos-indexer` falls back to querying the node.

To build the cache, you should use the `utils/get.bash` script (you can read it — it's quite simple — and make sure it points to the right URL for the node).

### Run the indexer in docker and multicore mode

First create a file `env` in any path, here we use `~/.tezos-client`. Add the following content and modify to match your setup:

```
POSTGRES_HOST=172.17.0.1 # Here, I connect to a postgresql instance on the host that runs docker
DB_NAME=tezos_indexer_db # Database name, if it exists, indexer will delete it. Then it creates an empty database
POSTGRES_USER=POSTGRESQLUSER
DB_USER=POSTGRESQLUSER
DB_PASS=POSTGRESQLPASSWORD
TEZOS_NODE=http://172.17.0.1:8732
# TEZOS_NODE2, TEZOS_NODE3 and TEZOS_NODE4 can also be set, to distribute load on different nodes
MULTISEGMENT_TEZOS_INDEXER=true
PROCESSES=8 # The optimal value depends on your machines and network delay between node and indexer,
            # you can test different values to find the one that is fastst for you
BLOCK_CACHE_DIR=/home/tezos/blocks # This address and the next are inside the container, container's user will be tezos
LOGS_DIR=/home/tezos/temp/logs
TEZOS_TOP=3000000 # Normally, a value close to the latest block level
```

Then you can run the indexer using:

```
docker run -it --rm --env-file=/home/YOURUSER/.tezos-client/env registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v10.4.4
```

The indexer will:
* Delete the database if it exists. If there are active connections to the database, the whole process fails
* Creates a new empty database
* Applies the minimal (multicore) schema
* Queries the Tezos node to get a list of all blocks (This step can take tens of minutes depending on performance of your node)
* Splits the blocks and launches separate processes to index. If you have specified multiple nodes, different processes can connect to different nodes
* After all indexers finish, it applies the default (full) schema
* It exits or runs the indexer in default mode with the given options

### Run the indexer in docker and multicore mode with block cache

The fastest way to run indexer is in multicore mode and using a block cache.
First, build a block cache as described [above](building-a-block-cache-for-the-indexer).

Then run the indexer using:

```
docker run -it --rm --env-file=/home/YOURUSER/.tezos-client/env -v /path-to/disk-cache:/home/tezos/blocks registry.gitlab.com/nomadic-labs/tezos-indexer/tezos-indexer:v10.4.4
```

A few notes:

* if the file `list_of_contracts-1500000.txt` (if your TEZOS_TOP is set to 1500000 in env file) exists in the disk cache (possibly from a previous execution of indexer), the indexer reuses it. This can save the better part of an hour and reduce load on your tezos node
* if you are running SELinux, you need to add `:Z` to the end of `-v /path-to/disk-cache:/home/tezos/blocks`. The indexer in docker needs to create a file `list_of_contracts-1500000.txt` in that folder, so it needs read-write access to the mapped volume
* The indexing process will be slow at start when it's indexing newer blocks and will speed up considerably for older blocks
* If you run `utils/get.bash` in an existing block cache, it only downloads newer blocks. This will be much faster

### Issues

Please feel free to [open an issue](https://gitlab.com/nomadic-labs/tezos-indexer/-/issues).

### Precisions

* The "bootstrap" consists in processing the blocks that are known to
already exist: when you start the indexer, it retrieves the current
level (L) from the octez node, and it'll process all the blocks it
doesn't have between block 0 and L (note that blocks 0 and 1 are expected not to contain any operations).

* When you launch the indexer, the first thing it will do is to build
the list of snapshot blocks (a table that associates cycles with the
selected snapshot blocks).
Then it will bootstrap.
Then it will watch.

* "Watching" means the indexer gets the blocks octez-node feeds it.

* Blockchain reorganisations: when they happen, all blocks from lowest level of rejected blocks are deleted and indexing resumes from there.


Performance
-----------

On a Intel NUC 10, with a Core i7-10710U, 16GB RAM DDR4-2666MHz and a very fast NVME
SSD, indexing the first 1.32 M blocks of mainnet may take as little as
1h30min if you use `utils/run-tezos-indexer-multicore.bash`.
That includes all the data coming directly from blocks, and FA1.2 tokens. That does not include data coming from contexts (such as context-level contract balances).

That scripts makes the indexer write into the DB with a O(1)
complexity for any given block instead of O(log n) where n is the
quantity of already indexed blocks, and allows you to take advantage
of multicore architectures by running multiple indexers, each treating a segment of the blockchain.

If your node is slow and/or far, it's highly recommended that you build a cache.

#### Distance to DB engine

The distance from the indexer to the DB engine can have quite a huge impact on the speed of the bootstrap process.

With v9+, the distance to the DB engine can however be compensated by running multiple indexers (each indexing a segment of the blockchain).

#### Blocks per SQL transaction

Each block is treated in a single SQL transaction.
Therefore, if the indexer exits (for whichever reason) while it's indexing a block, the block will not be recorded at all in the database.


#### Skipping non-required data

Since v8, SQL write queries are all in SQL functions. Those functions may be replaced by dummy functions if you want some of the writes to be skipped. Or by some functions of your craft to perform differently.
You may pick what you need from `src/db-schema/feign-recording.sql`. Be sure to pick a set that functionally makes sense!
For instance, if table A references table B, and you skip the writes into table A without skipping writes into table B, it will probably crash.


#### Verbosity

Logs obtained by using `--debug --verbose --verbosity=10` are maximal.
They have a cost. Bootstrapping will be slower with those options than without any option.
If you want minimal non-nil logs you should use one of:
- `--verbose --verbosity=1`
- `--debug --verbosity=1`
- `--debug --verbose --verbosity=1`

#### Versions of postgresql

- we use postgresql 14 (and sometimes 15)
- anterior versions might work but are not supported

Hardware requirements
---------------------

### Storage

The space taken on disk by the DB is roughly the same as the one taken by the node in archive mode.

So, basically, if the node takes about 700GB, then the indexer will use about 700GB as well.

Please do not try to run too many things from a HDD (as opposed to a SSD). HDDs are too slow for handling large amounts of data. If you run a small private net, HDDs might work, but for mainnet or even ghostnet, it's very unlikely to work.

### RAM

Most tests are now run on machines with 128GB of RAM. That is because the fastest connection to nodes is when the node is on the same physical machine. And running many nodes (17 is the current standard) needs lots of RAM.

If you run a single node, you need much less RAM, and 16GB should be enough. Please refer to octez's minimal requirements to be sure.

### CPU

Any modern CPU should be fine. The CPUs used for development of this
project are Intel 10710U (6 physical cores, 12 logical cores) and AMD
5950X (16 physical cores, 32 logical cores).

It is recommended that you don't use a CPU significantly slower than the Intel 10710U, because it would be quite slow to index mainnet.


It's recommended to run the bootstrap phase in multicore mode, in
which case the bottleneck is usually the node if you have only one. If
you have many nodes on a single machine, beware of RAM requirements.
If your nodes are not on the same physical machine as the indexer, the
network connection will likely become the bottleneck, even if it's on
the same local network.


##  Work in progress / Future work

cf. https://gitlab.com/nomadic-labs/tezos-indexer/-/issues


Misc commands that might be helpful
-----------------------------------

Peek at the latest blocks
```bash
$ psql name_of_your_database -c 'select * from c.block order by level desc limit 10;'
```

Reset the chain database
```bash
# as root
$ su - postgres -c 'dropdb name_of_your_database && createdb name_of_your_database -O username'
```

## Contributions

Bug reports and merge requests are welcome.

## Issues or questions

You're very welcome to [raise issues if you have any](https://gitlab.com/nomadic-labs/tezos-indexer/issues) and/or to [submit merge requests](https://gitlab.com/nomadic-labs/tezos-indexer/merge_requests).

## Contact

You're welcome to create issues or merge requests.
You may also contact [Philippe Wang](@philippewang.info) by other means (email, slack,
messenger, sms, telegram, linkedin, etc.) as long as it works.
You may also contact any member of Nomadic Labs.
And if you want your name added here, please submit a merge request! ;)

### Database snapshots

If you'd like to download a snapshot of the database for mainnet, you may create an issue or reach out to [Philippe Wang](@philippewang.info).
Snapshots have been created and provided on demand to several parties.

Snapshots can save you hours or days of computations.


# `tezos-indexer --help`

The output of `tezos-indexer --help` follows:

<!-- --help -- do not manually edit this or after this line -->
```
NAME
       tezos-indexer - Store and index a Tezos blockchain into a Postgres
       database.

SYNOPSIS
       tezos-indexer [OPTION]…

       --help[=FMT] (default=auto)
           Show this help in format FMT. The value FMT must be one of auto,
           pager, groff or plain. With auto, the format is pager or plain
           whenever the TERM env var is dumb or undefined.

OPTIONS
       --alert=VAL
           Specify a command to execute when something bad happens but the
           indexer carries on (instead of exiting). The error message is
           given to the command on its stdin. You might want to use something
           like `mutt -s 'Indexer warning' email@example.com`. Note that if
           your command fails, the indexer will halt. If you do not give any
           command to run, the indexer will carry on.

       --auto-recovery-credit=VAL (absent=10)
           This option is deprecated and ignored. It'll be removed in a
           future release.

       --bin-rpc
           [Deprecated and ignored] The binary mode to query Octez node is
           now used by default. Note that this option has no effect when
           reading blocks from block cache.

       --block-cache-directory=VAL (absent=./cache/)
           Directory from which to read cached blocks.

       --bootstrap-upto=VAL (absent=-1)
           Bootstrap until it's over or until given level is reached
           (whichever comes first), then exit. Negative values mean no limit.

       --contract-balances-only
           Only fill balances in table contract_balances.

       --db=URL (absent=postgresql://localhost/chain)
           Database to connect to.

       --db-schema
           Print default full DB schema and exit.

       --db-schema-multicore
           Print "multicore" full DB schema and exit. Do not forget to
           convert to default schema after bootstrap is over. Warning: when
           DB in multicore mode, `chain_id` is ignored.

       --debug
           Debug mode.

       --do-not-get-missing-scripts
           Do not try and get contract missing scripts

       --first-block=VAL (absent=2)
           Specify level of first alpha block

       --force-from=VAL (absent=0)
           Force indexing from specified level. The purpose of this option is
           to start indexing from a level different from the latest indexed
           block. The first purpose is to be able to easily reindex blocks
           that have already been indexed, but which for some reason might
           need an update. You may index from an arbitrary high level only if
           you are also using `--no-watch` and the DB is in multicore mode.
           Also, you cannot start indexing from a level that doesn't exist. A
           negative value is considered as an offset (towards the past) from
           the highest block recorded in the DB.

       --heads-only
           [Deprecated] Index heads only. This option now has no effect.

       --make-conn2
           [deprecated option] This option now has no effect. Secondary
           connection will always be created.

       --max-address-length=VAL (absent=1024)
           Maximum length of extracted addresses (URI, etc) from smart
           contracts and big maps.

       --mempool-only
           Index mempool ONLY: every other indexing activity will be
           disabled. You may run those in another process.

       --no-big-maps
           Deactivate indexing of big maps.

       --no-check-db-schema-version
           Do not check DB schema version (not recommended)

       --no-contract-balances
           Don't fill balances in table contract_balances.

       --no-smart-contract-extraction
           Don't extract data from smart contracts or big maps (using this
           option makes `--max-address-length` irrelevant).

       --no-snapshots
           Deactivate snapshot blocks.

       --no-watch
           No watch: exit once bootstrap is done.

       --node-timeout=VAL (absent=3600.)
           Specify Tezos octez-node RPC timeout in seconds. Default is
           purposely likely too high: 3600 seconds (1 hour). If you have a
           fast node, it's recommended that this is set to less than 10
           minutes, perhaps less than 1 minute. If you set a value that is
           too small, the indexer will exit, therefore be unable to go
           forward. This is not configurable to infinite. This can be very
           useful if your instance gets stuck sometimes and you'd like it to
           crash so you can automatically restart it. (Since v9.8.0)

       --notify
           Activate PG notifications on block_indexed (experimental). There
           are 3 notifications available: block_indexed, block_deleted,
           indexer_failed.

       --op-speed
           Always print operation treatment speed. Implied if --debug or
           --verbose during watch.

       --skip-prebaby-contracts
           Skip indexing pre-babylon contracts (deprecated option, replaced
           by --do-not-get-missing-scripts)

       --snapshots
           Get and update snapshot blocks ONLY (forever).

       --textual-rpc
           Force use of text mode to query Octez node. The binary mode is now
           by default. Note that this option has no effect when reading
           blocks from block cache.

       --tezos-client-dir=PATH (absent=/home/phil/.tezos-client)
           Where Tezos client config resides

       --tezos-url=URL
           URL of a running Tezos octez-node. Will use http://localhost:8732
           if no URL is specified and --tezos-client-dir isn't used either.
           Note that --tezos-url has precedence over --tezos-client-dir.

       --timeout=VAL (absent=60s)
           Specify DB query timeout. If no unit is given, the integer is
           considered a number of milliseconds. 0 means no timeout. Examples:
           10s, 1234ms, 42min. Cf. Postgres' documentation for more
           information. This can be very useful if your instance gets stuck
           sometimes and you'd like it to crash so you can automatically
           restart it. (Since v9.7.9)

       --tokens-support
           Enable FA1.2 and FA2 tokens indexing.

       --use-disk-cache
           Use disk cache for reading blocks.

       --verbose
           Verbose mode.

       --verbosity=VAL (absent=5)
           Verbosity level (requires --verbose and/or --debug, otherwise will
           have no effect). 0: some logs. 1: block level. 2: operation level.
           3: sub-operation level. 4: more logs. 5: even more logs!

       --version
           Print version and exit.

       --watch-hook=VAL
           A command to execute after a new block is indexed during the watch
           process. If a reorganization happens, the hook will be executed
           after the reorganization is over.

EXIT STATUS
       tezos-indexer exits with:

       0   on success.

       123 on indiscriminate errors reported on standard error.

       124 on command line parsing errors.

       125 on unexpected internal errors (bugs).

```
