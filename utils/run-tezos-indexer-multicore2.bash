#!/bin/bash
# Open Source License
# Copyright (c) 2021-2022 Nomadic Labs <contact@nomadic-labs.com>
# Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


if [[ ${MULTISEGMENT_TEZOS_INDEXER+true} != "true" ]]
then
    exec $(dirname $(dirname "$0"))/tezos-indexer "$@"
fi

set -xe

epoch_start=$(date +%s)

###################################################################################
########## database
HOST=${POSTGRES_HOST:-localhost}
DB=${DB_NAME:$USER}
PORT=${POSTGRES_PORT:-5432}
PG_USER=${POSTGRES_USER:-tezos}

DB_USER=${DB_USER:-phil}
export PGPASSWORD=${DB_PASS:-phil}
PSQL_OPTIONS="-a"

########## node
TEZOS_NODE=${TEZOS_NODE:-"http://localhost:8732"}
TEZOS_NODE1=${TEZOS_NODE1:-$TEZOS_NODE}
TEZOS_NODE2=${TEZOS_NODE2:-$TEZOS_NODE1}
TEZOS_NODE3=${TEZOS_NODE3:-$TEZOS_NODE2}
TEZOS_NODE4=${TEZOS_NODE4:-$TEZOS_NODE3}
TEZOS_NODE5=${TEZOS_NODE5:-$TEZOS_NODE4}
TEZOS_NODE6=${TEZOS_NODE6:-$TEZOS_NODE5}
TEZOS_NODE7=${TEZOS_NODE7:-$TEZOS_NODE6}
TEZOS_NODE8=${TEZOS_NODE8:-$TEZOS_NODE7}
TEZOS_NODE9=${TEZOS_NODE9:-$TEZOS_NODE8}
TEZOS_NODEA=${TEZOS_NODEA:-$TEZOS_NODE9}

echo "${TEZOS_NODES[@]}"

TEZOS_NODES_COUNT=${#TEZOS_NODES[@]}
if (( TEZOS_NODES_COUNT == 0 ))
then
    TEZOS_NODES=($TEZOS_NODE1 $TEZOS_NODE2 $TEZOS_NODE3 $TEZOS_NODE4 $TEZOS_NODE5 $TEZOS_NODE6 $TEZOS_NODE7 $TEZOS_NODE8 $TEZOS_NODE9 $TEZOS_NODEA)
elif (( TEZOS_NODES_COUNT == 1 ))
then
    IFS_=$IFS
    IFS="$IFS ,;"
    X=$TEZOS_NODES
    TEZOS_NODES=()
    for i in $X ; do TEZOS_NODES+=($i) ; done
    IFS=$IFS_
fi

echo "${TEZOS_NODES[@]}"

###################################################################################

# Override settings 1/2:
override="$(dirname "$0")/run-tezos-indexer-multicore.override.bash"
if [[ -f "$override" ]]
then
    . "$override"
fi

########## indexer
TI=${INDEXER_SRC_PATH:-$(dirname $(dirname "$0"))}
LOGS_DIR=${LOGS_DIR:-"/var/log/tezos-indexer"}

BLOCK_CACHE_DIR=${BLOCK_CACHE_DIR:-"/blocks"} # no space in the path or it might fail

INDEXER_DATAKINDS=${INDEXER_DATAKINDS:-"--no-contract-balances --no-snapshot --do-not-get-missing-scripts --no-watch --tokens-support"}
INDEXER_RUN=${INDEXER_RUN:-"--use-disk-cache --debug --verbosity 1 --verbose --timeout 300s --auto-recovery-credit 1000"}
INDEXER_OTHER_OPTIONS=${INDEXER_OTHER_OPTIONS:-""}

########## parallelism

top=${TEZOS_TOP:-$top} # last block to index
if [[ "$top" == "auto" ]] || [[ "$top" == "" ]] ; then
    top_level=$(curl -s ${TEZOS_NODES[0]}/chains/main/blocks/head | jq .header.level)
    top=$((top_level / 1000 * 1000))
    if (( top > top_level - 100 )) ; then
        (( top -= 100 )) ; # it's dangerous to have a top so close to the current head of the chain
    fi
fi
if [[ $((top*1)) == 0 ]] ; then
    >&2 echo "Error: TEZOS_TOP has a wrong value, or TEZOS_NODE is unreachable, or jq is not installed. I'll exit now."
    exit 1
fi

# It seems the best speed is achieved with a number of parallel jobs equal to 1.5 times the number of logical cores:

if [[ "${PROCESSES}" == "" ]] ; then
    if [[ -f /proc/cpuinfo ]] ; then
        PROCESSES=$(grep processor /proc/cpuinfo | wc -l)
    else
        PROCESSES=$(sysctl -n hw.ncpu)
    fi
    ((PROCESSES= PROCESSES * 15 / 10))
    if (( PROCESSES < 1 )) ; then
        PROCESSES=4 ;
    fi
fi
if (( PROCESSES == 0 )) ; then
    PROCESSES=1
fi

RANDFILE=${RANDFILE:-/dev/zero}

###################################################################################

# Override settings 2/2:
override="$(dirname "$0")/run-tezos-indexer-multicore.override.bash"
if [[ -f "$override" ]]
then
    . "$override"
fi

###################################################################################
# If you need to modify something below this line, please consider opening a merge request!
###################################################################################
mkdir -p $LOGS_DIR
mkdir -p $BLOCK_CACHE_DIR

PSQL="time psql -h $HOST -p $PORT -U $DB_USER $DB $PSQL_OPTIONS"
LOGS="$LOGS_DIR"/indexer-segment-$gsize-$HOST-$DB

INDEXER_OPTIONS_BASE="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --block-cache-directory=$BLOCK_CACHE_DIR"

if [[ "$TEZOS_INDEXER_BOTTOM" == "" ]] ;
then
    dropdb   -h $HOST -p $PORT -U $PG_USER $DB || true
    createdb -h $HOST -p $PORT -U $PG_USER -O $DB_USER $DB
fi

# make sure we're in "known territory" (this is meant to avoid ending up in some random directory)
cd $TI

epoch_start_multicore_schema=$(date +%s)
make db-schema-all-multicore | $PSQL
epoch_end_multicore_schema=$(date +%s)


# get the list of all existing contract addresses
epoch_start_list_contracts=$(date +%s)
if ! [[ -f "$BLOCK_CACHE_DIR/list_of_contracts-$top.txt" ]]
then
    1>&2 echo "List of contract addresses ($BLOCK_CACHE_DIR/list_of_contracts-$top.txt) is missing. This will likely fail down the road."
else
    epoch_start_record_contracts_list=$(date +%s)
    # record all existing contract addresses
    echo "\\copy c.address (address, address_id) from '$BLOCK_CACHE_DIR/list_of_contracts-$top.txt' DELIMITER ' ';" | $PSQL
fi

(( iterations = top / gsize ))

#set +x
start="$(date)"

epoch_start_indexing=$(date +%s)

if (( PROCESSES <= 1 )) ;
then
    PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer $INDEXER_OPTIONS1 > $LOGS-solo-multicore.log 2> $LOGS-solo-multicore.error.log
else
    echo ${TEZOS_NODES[@]}
    printf "%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer $INDEXER_OPTIONS_BASE" | bottom=${TEZOS_INDEXER_BOTTOM} BALANCING=$BALANCING PSQL="$PSQL" TEZOS_NODES="${TEZOS_NODES[@]}" top=$top ocaml unix.cma -I +threads threads.cma utils/scheduler.ml "$PROCESSES" "$LOGS_DIR" 2>&1
fi | tee "$LOGS+benchmarks.txt"

if [[ "${DB_PRECONVERSION_SNAPSHOT}" == "true" ]] ; then
    time psql -h $HOST -p $PORT -U $DB_USER $PSQL_OPTIONS -c "create database ${DB}_snapshot template $DB;" || [[ "${TEZOS_INDEXER_BOTTOM}" != "" ]]
fi

epoch_start_db_convert=$(date +%s)
end="$(date)"
echo "Indexing started on $start - ended on $end"
if [[ "${NO_DB_CONVERSION}" != "true" ]]
then
    make db-schema-all-default | $PSQL
fi
echo "Indexing started on $start - ended on $end"

epoch_end=$(date +%s)

function pretty_time () {
    seconds=$1
    hours=$(( seconds / 3600 ))
    minutes=$(( (seconds % 3600) / 60 ))
    remaining_seconds=$(( seconds % 60 ))
    if (( hours > 0 )) ; then echo -n ${hours}h ; fi
    if (( hours > 0 )) || (( minutes > 0 )) ; then echo -n ${minutes}m ; fi
    echo ${remaining_seconds}s
}

total_operations="$($PSQL <<< 'select count(*) from c.operation_alpha;' | grep -vE '[(c-]')"

set +e
tee -a "$LOGS+benchmarks.txt" <<EOF
#####################################
Database:
HOST=$HOST
DB=$DB
PORT=$PORT
PG_USER=$PG_USER
DB_USER=$DB_USER
PSQL_OPTIONS=$PSQL_OPTION
PSQL=$PSQL

Tezos nodes:
TEZOS_NODES=($(for i in "${TEZOS_NODES[@]}" ; do echo -n "\"$i\" " ; done))

Tezos-indexer:
TI=$TI
LOGS_DIR=$LOGS_DIR
BLOCK_CACHE_DIR=$BLOCK_CACHE_DIR
INDEXER_DATAKINDS=$INDEXER_DATAKINDS
INDEXER_RUN=$INDEXER_RUN
INDEXER_OTHER_OPTIONS=$INDEXER_OTHER_OPTIONS
INDEXER_OPTIONS_BASE=$INDEXER_OPTIONS_BASE
gsize=$gsize
top=$top
PROCESSES=$PROCESSES
LOGS_DIR=$LOGS_DIR
BLOCK_CACHE_DIR=$BLOCK_CACHE_DIR
LOGS=$LOGS


Durations:
STARTED ON $(date -d @$epoch_start) -- TOTAL DURATION: $(pretty_time $((epoch_end-epoch_start)))
- apply DB schema from $(date -d @$epoch_start_multicore_schema) -- duration: $(pretty_time $((epoch_end_multicore_schema-epoch_start_multicore_schema)))
EOF
[[ "$ADDRESS_CACHE" == true ]] && tee -a "$LOGS+benchmarks.txt" <<EOF
- list contract addresses from $(date -d @$epoch_start_list_contracts) -- duration: $(pretty_time $((epoch_start_record_contracts_list-epoch_start_list_contracts)))
- record contract addresses into DB from $(date -d @$epoch_start_list_contracts) -- duration: $(pretty_time $((epoch_start_indexing-epoch_start_record_contracts_list)))
EOF
tee -a "$LOGS+benchmarks.txt" <<EOF
- start indexing from $(date -d @$epoch_start_indexing) -- duration: $(pretty_time $((epoch_start_db_convert-epoch_start_indexing)))
- conversion of DB from $(date -d @$epoch_start_db_convert) -- duration: $(pretty_time $((epoch_end-epoch_start_db_convert)))
- average speed: $((top/(epoch_end-epoch_start))) blocks / second, or $(((60*top)/(epoch_end-epoch_start))) blocks per minute, $((total_operations / (epoch_end-epoch_start))) operations per second

$(w|grep -o load.*)
############################
EOF

if [[ $# -gt 0 ]] ; then
    $TI/tezos-indexer "$@"
else
    true
fi
