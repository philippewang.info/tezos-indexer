utils/

This directory contains files that may or may not help you.

Please do not consider them part of tezos-indexer.

They're here just because for now it's more convenient to put them here than anywhere else.
