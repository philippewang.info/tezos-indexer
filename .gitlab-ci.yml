image:
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables:
  REGISTRY_NAME: "${CI_REGISTRY}/${CI_PROJECT_PATH}"
  CONTAINER_NAME: "${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"
  KANIKO_CONFIG: "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}"

stages:
  - test-pgsql-schema
  - build
  - check-dev
  - check-non-dev
  - check-tag
  - push
  - test

test-postgresql-schema:
  image: postgres:alpine
  stage: test-pgsql-schema
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - if: $CI_COMMIT_TAG
      when: always
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - when: manual
  script:
#    - apk add make bash ocaml opam gcc
    - >
      mkdir -p /var/lib/postgresql/data /run/postgresql
      && chown -R postgres:postgres /run/postgresql /var/lib/postgresql
      && chmod 0700 /var/lib/postgresql/data
    - >
      su - postgres -c 'initdb -D /var/lib/postgresql/data && pg_ctl start -D /var/lib/postgresql/data && createdb tmp'
      && cat utils/db-schema-all-multicore.sql | su - postgres -c 'psql -a tmp'
      && cat utils/db-schema-all-default.sql | su - postgres -c 'psql -a tmp'
      && su - postgres -c 'dropdb tmp && createdb tmp'
      && cat utils/db-schema-all-default.sql | su - postgres -c 'psql -a tmp'


build-docker-image:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /(WIP|wip)/
      when: manual
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - when: always
  script:
    - echo "${KANIKO_CONFIG}" >/kaniko/.docker/config.json
    - >
      /kaniko/executor
      --cache
      --cache-repo "$REGISTRY_NAME"/cache
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR"/Dockerfile
      --build-arg GIT_TAG="${CI_COMMIT_SHA}"
      --build-arg GIT_COMMIT="${CI_COMMIT_REF_NAME}"
      --destination "$CONTAINER_NAME":"latest"

do-check-dev:
  stage: check-dev
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /(fix|Fix|update|Update|upgrade|Upgrade|release|Release)/
      when: never
    - when: on_success
  script:
    - grep -q 'true -- dev' src/db-schema/versions.sql

do-check-non-dev:
  stage: check-non-dev
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: on_success
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  script:
    - grep -qE '(false -- dev|true -- dev beta)' src/db-schema/versions.sql

do-check-tag:
  stage: check-tag
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - when: never
  script:
    - grep -q "$(echo "$CI_COMMIT_TAG" | sed -e 's/^v//' -e 's/rc.*//' -e 's/beta.*//' -e 's/alpha.*//')' -- version" src/db-schema/versions.sql
    - grep -q "$(echo "$CI_COMMIT_TAG" | sed -e 's/^v//' -e 's/rc.*//' -e 's/beta.*//' -e 's/alpha.*//')' -- version" utils/db-schema-all-*sql

do-push:
  stage: push
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /(WIP|wip)/
      when: manual
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
    - if: $CI_COMMIT_TAG
      when: on_success
  script:
    - echo "${KANIKO_CONFIG}" >/kaniko/.docker/config.json
    - >
      /kaniko/executor
      --cache
      --cache-repo "$REGISTRY_NAME"/cache
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR"/Dockerfile
      --destination "$CONTAINER_NAME":"$CI_COMMIT_REF_NAME"
      --destination "$CONTAINER_NAME":"latest"
      --build-arg GIT_TAG="${CI_COMMIT_SHA}"
      --build-arg GIT_COMMIT="${CI_COMMIT_REF_NAME}"

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/License-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    GIT_STRATEGY: fetch
    DOCKERFILE_PATH: "Dockerfile"
    # DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
    # DOCKER_IMAGE: "$CONTAINER_NAME:$CI_COMMIT_REF_NAME"
    DOCKER_IMAGE: "$CONTAINER_NAME:latest"
    SECURE_LOG_LEVEL: "debug"
  # before_script:
  #   - export DOCKER_IMAGE="$CI_REGISTRY_IMAGE/$CI_COMMIT_BRANCH:$CI_COMMIT_SHA"
    # - |
    #   if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
    #     export DOCKER_IMAGE="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
    #   fi
  artifacts:
    paths:
    - gl-container-scanning-report.json
    expire_in: 1 month

secret_detection:
  stage: test

sast:
  stage: test
  artifacts:
    paths:
    - gl-sast-report.json
    - gl-secret-detection-report.json
    - gl-dependency-scanning-report.json
    - gl-container-scanning-report.json
    - gl-license-scanning-report.json
    expire_in: 1 month
